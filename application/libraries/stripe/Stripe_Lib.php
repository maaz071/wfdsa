<?php

if(!defined('BASEPATH')) exit('NO Direct Script access Allowed');

include("./vendor/autoload.php");

class Stripe_Lib {
    
	private $CI;
	 
	public function __construct() {
		$stripe = array(
			"secret"=> "sk_test_peHCZZnVzlVnAxZiKw5OJwZc",
			"public_key"=> "pk_test_EeweXBGE8QkUqS4ix7LwLjhQ"
		); 
		
		$this->CI = &get_instance();
	}
	
   public function Charge_Payment($data) {
	    $response_msg ="";
	    
	    \Stripe\Stripe::setApiKey('sk_test_peHCZZnVzlVnAxZiKw5OJwZc'); 
	    
	    $amount = (int)($data['amount'] * 100);
        $currency = 'usd';
        $source = 'tok_visa';
        $description = 'Event Fees - WFDSA';
        $post = array();
        
        
        try {  
            
            $this->CI->load->model('Payment_model');
            
            if($data['signin_type'] == 1) {
                
                $post['event_id'] = $data['event_id'];
                $post['member_id'] = $data['user_id'];
                $post['payment_date'] = date('Y-m-d H:i:s');
                $post['payment_status'] = 1;
                $post['payment_amount'] = $data['amount'];
                
            } else if($data['signin_type'] == 2) {
                
                $post['event_id'] = $data['event_id'];
                $post['non_member_id'] = $data['user_id'];
                $post['payment_date'] = date('Y-m-d H:i:s');
                $post['payment_status'] = 1;
                $post['payment_amount'] = $data['amount'];
            }
          
            $insert = $this->CI->Payment_model->create_record('payment', $post);
            $attend = $this->CI->Payment_model->create_record_attendees('attendees', $post);
            
            
            
            
            
            $charge = \Stripe\Charge::create(array(
                "amount" => $amount, // Amount in cents
                "currency" => $currency,
                "source" => $source,
                "description" => $description
            ));
            
            if($charge!=null && $insert == TRUE ){
                
                $response['result']['status'] = 'success';
                $response['result']['response'] = "Payment Successfully Paid";
                return array($response, 200);
            }
            
        } catch(\Stripe\Error\Card $e) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = $e->getMessage();
            return array($response, 406); 
        } 
	 
	}
	
	   public function Charge_invoice_Payment($data) {
	    $response_msg ="";
	    
	    \Stripe\Stripe::setApiKey('sk_test_peHCZZnVzlVnAxZiKw5OJwZc'); 
	    
	    $amount = (int)($data['amount'] * 100);
        $currency = 'usd';
        $source = 'tok_visa';
        $description = 'Invoice Fees - WFDSA';
        $post = array();
        
        
        try {  
            
            $this->CI->load->model('Payment_model');
            
            if($data['signin_type'] == 1) {
                
                $post['invoice_id'] = $data['invoice_id'];
                $post['member_id'] = $data['user_id'];
                $post['payment_date'] = date('Y-m-d H:i:s');
                $post['payment_status'] = 1;
                $post['payment_amount'] = $data['amount'];
                
            }
          
            $insert = $this->CI->Payment_model->create_record('payment', $post);
            $update = $this->CI->Payment_model->update_record('invoice',$post);
            
            $charge = \Stripe\Charge::create(array(
                "amount" => $amount, // Amount in cents
                "currency" => $currency,
                "source" => $source,
                "description" => $description
            ));
            
            if($charge!=null && $insert == TRUE && $update == TRUE){
                
                $response['result']['status'] = 'success';
                $response['result']['response'] = "Payment Successfully Paid";
                return array($response, 200);
            }
            
        } catch(\Stripe\Error\Card $e) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = $e->getMessage();
            return array($response, 406); 
        }
	   }
        public function payment_zero($data) {
	     
        
        $post = array();
        try {  
            
            $this->CI->load->model('Payment_model');
            
            if($data['signin_type'] == 1) {
                
                $post['event_id'] = $data['event_id'];
                $post['member_id'] = $data['user_id'];
                $post['payment_date'] = date('Y-m-d H:i:s');
                $post['payment_status'] = 1;
                $post['payment_amount'] = $data['amount'];
                
            }
            
             else if($data['signin_type'] == 2) {
                
                $post['event_id'] = $data['event_id'];
                $post['non_member_id'] = $data['user_id'];
                $post['payment_date'] = date('Y-m-d H:i:s');
                $post['payment_status'] = 1;
                $post['payment_amount'] = $data['amount'];
            }
          
            $insert = $this->CI->Payment_model->create_record('payment', $post);
             $attend = $this->CI->Payment_model->create_record_attendees('attendees', $post);
            
            if($insert == TRUE){
                
                $response['result']['status'] = 'success';
                $response['result']['response'] = "Successfully Registered";
                return array($response, 200);
            }
            
        } catch(\Stripe\Error\Card $e) {
            $response['result']['status'] = 'error';
            $response['result']['response'] = $e->getMessage();
            return array($response, 406); 
        }
	 
	}
	
	
	
	
}

?>