<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Invoice_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
    public function get_invoice($request = array())  {   
         
        $this->CI->load->model('Invoice_model');
        $getPayment = $this->CI->Invoice_model->invoice_item($request);  
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invoice Items is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Invoice Items Successfully Recieved";
        $response['result']['data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
    
    public function get_payment($request = array())  {   
         
        $this->CI->load->model('Invoice_model');
        $getPayment = $this->CI->Invoice_model->payment_details($request);  
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Payment Details are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Payment Details Successfully Recieved";
        $response['result']['data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
     

}
