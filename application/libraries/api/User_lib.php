<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class User_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;
    
    const MEMBER = 1;
    const NON_MEMBER = 2;
    
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('Member_model');
    }

    function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
    
     
    function email_forget_password($request=array()) {
        
        $email = $request['email'];
        $upd_check = 1;
        $user_record = $this->CI->db->get_where('non_member', array('status' => 1,'email'=>$email))->result_array();
       // $random_password = $this->randomPassword(); 
        if(!empty($user_record)) {
            $this->CI->db->where('non_member_id', $user_record[0]['non_member_id']); 
            // $upd_check = $this->CI->db->update('non_member', array('password'=>md5($random_password)));  

            $data=array();

        $data['email']=array(['heading' => 'Password Reset',
            'firstname' => $user_record[0]['first_name'],
            'lastname' => $user_record[0]['last_name'],
            'text'=> 'To change your password please click on the button below<br>
                <br>
                <a style="background-color:#2196F3;
    border:1px solid #2196F3;
    border-radius:2px;
    color:#ffffff;
    display:inline-block;
    font-family:Roboto, Helvetica, sans-serif;
    font-size:14px;
    font-weight:400;
    line-height:36px;
    text-align:center;
    text-decoration:none;
    text-transform:uppercase;
    width:200px;
    height: 36px;
    padding: 0 8px;
    margin: 0;
    outline: 0;
    outline-offset: 0;
    -webkit-text-size-adjust:none;
    mso-hide:all;" class="verifybtn" href="'.base_url().'/login/updatepassword?email='.base64_encode($email).'" class="btn">Update Password</a>
    <br><br>
             To download our moile application, click on the android OR iOS icon below.',
            'link' => ''
            
            
        ]);
        $emailbody = $this->CI->load->view('template/email',$data,true);
        // or
        // $emailbody = $this->load->view('template/email','',true);


        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->CI->email->initialize($config);
        
        $this->CI->email->set_mailtype("html");
        $this->CI->email->from('no-reply@wfdsa.org','WFDSA');
        $this->CI->email->to($email);
        $this->CI->email->subject('Password Reset - WFDSA');
        $this->CI->email->message($emailbody);
        $this->CI->email->send();

            
            if($upd_check) { 
                
                $response['result']['status'] = 'success';
                $response['result']['response'] = "Update Password request has been sent at ".$email;
                return array($response, 200);
                
            } 
             
            
        } 
        else
        {
            $user_record = $this->CI->db->get_where('member', array('status' => 1,'email'=>$email))->result_array();
            if(!empty($user_record)) {
            // $random_password = time()."_".rand(0,9999); 
            $this->CI->db->where('member_id', $user_record[0]['member_id']); 
            // $upd_check = $this->CI->db->update('member', array('password'=>base64_encode($random_password)));  
            $data=array();

        $data['email']=array([
            'heading' => 'Password Reset',
            'firstname' => $user_record[0]['first_name'],
            'lastname' => $user_record[0]['last_name'],
            'text'=> 'To change your password please click on the button below<br>
                <br>
                <a style="background-color:#2196F3;
    border:1px solid #2196F3;
    border-radius:2px;
    color:#ffffff;
    display:inline-block;
    font-family:Roboto, Helvetica, sans-serif;
    font-size:14px;
    font-weight:400;
    line-height:36px;
    text-align:center;
    text-decoration:none;
    text-transform:uppercase;
    width:200px;
    height: 36px;
    padding: 0 8px;
    margin: 0;
    outline: 0;
    outline-offset: 0;
    -webkit-text-size-adjust:none;
    mso-hide:all;" class="verifybtn" href="'.base_url().'/login/updatepassword?email='.base64_encode($email).'" class="btn">Update Password</a>
    <br><br>
             To download our moile application, click on the android OR iOS icon below.',
            'link' => ''
            
        ]);
        $emailbody = $this->CI->load->view('template/email',$data,true);
        // or
        // $emailbody = $this->load->view('template/email','',true);


        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->CI->email->initialize($config);
        
        $this->CI->email->set_mailtype("html");
        $this->CI->email->from('no-reply@wfdsa.org','Password Reset - WFDSA');
        $this->CI->email->to($email);
        $this->CI->email->subject('Password Reset - WFDSA');
        $this->CI->email->message($emailbody);
        $this->CI->email->send();
            
            if($upd_check) { 
                
                $response['result']['status'] = 'success';
                $response['result']['response'] = "Update Password request has been sent at ".$email;
                return array($response, 200);
                
            } 
        }
         else {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Email";
            return array($response, 406);
        } 
    }
        
    }
    
    function confirmUserPassword($request=array()) {
        
        $id = $request['member_id'];
        
        if($request['signin_type'] == 1){
            $password = base64_encode($request['password']);
         $user_record = $this->CI->db->get_where('member', array('status' => 1,'password'=>$password,'member_id'=>$id))->result_array(); 
        }
        else
        {
            $password = md5($request['password']);
            $user_record = $this->CI->db->get_where('non_member', array('status' => 1,'password'=>$password,'non_member_id'=>$id))->result_array();    
        }
            
        if(!empty($user_record)) {
            
            $response['result']['status'] = 'success';
            $response['result']['response'] = "Session Restored Successfully.";
            return array($response, 200);
                
        } else {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Password , Please Try Again";
            return array($response, 406);    
        }
        
    } 
    
     function confirmUserPasswords($request=array()) {
        
        $id = $request['member_id'];
        //$pass=$request['password'];
        $password = md5($request['password']);
        
        //print_r($password);
        //exit;
        
         $user_record = $this->CI->db->get_where('non_member', array('status' => 1,'password'=>$password,'non_member_id'=>$id))->result_array(); 
            
        if(!empty($user_record)) {
            
            $response['result']['status'] = 'success';
            $response['result']['response'] = "Session Restored Successfully.";
            return array($response, 200);
                
        } else {
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invalid Password , Please Try Again";
            return array($response, 406);    
        }
        
    }
    
    
     

}
