<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Payment_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
     public function Payments($request = array())  {   
         
        $this->CI->load->model('Payment_model');
        $getPayment = $this->CI->Payment_model->get_all_table('payment'); 
        
          
        /*foreach($getPayment as $key => $value) {
           $payment_invoice_arr[] = $value;
            $invoice_item = $this->CI->Payment_model->get_invoice_item($value['invoice_id']); 
             
           foreach($invoice_item as $value2) {
               
                 array_push( $payment_invoice_arr,$value2);
            }
        
        }*/ 
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Payment List is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Payment List Successfully Recieved";
        $response['result']['payment_data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
    
    public function Payments_all($request = array())  {   
         
        $this->CI->load->model('Payment_model');
        $getPayment = $this->CI->Payment_model->get_all_table_payment(); 
        
          
        /*foreach($getPayment as $key => $value) {
           $payment_invoice_arr[] = $value;
            $invoice_item = $this->CI->Payment_model->get_invoice_item($value['invoice_id']); 
             
           foreach($invoice_item as $value2) {
               
                 array_push( $payment_invoice_arr,$value2);
            }
        
        }*/ 
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Payment List is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Payment List Successfully Recieved";
        $response['result']['payment_data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
    
    public function Payments_byRole($data)  {   
        /**/
        $this->CI->load->model('Payment_model');
        $getPayment = $this->CI->Payment_model->get_all_table_payment_byRole($data); 
        
        
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Payment List is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Payment List Successfully Recieved";
        $response['result']['payment_data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
    
    public function invoices_byRole($data)  {   
        /**/
        $this->CI->load->model('Payment_model');
        $getPayment = $this->CI->Payment_model->get_all_table_invoices_byRole($data); 
        
        
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Invoices List is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Invoices List Successfully Recieved";
        $response['result']['payment_data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }
    
    public function invoices($request = array())  {   
         
        $this->CI->load->model('Payment_model');
        
        $getPayment = $this->CI->Payment_model->all_invoices('invoice'); 
        
          
        /*foreach($getPayment as $key => $value) {
           $payment_invoice_arr[] = $value;
            $invoice_item = $this->CI->Payment_model->get_invoice_item($value['invoice_id']); 
             
           foreach($invoice_item as $value2) {
               
                 array_push( $payment_invoice_arr,$value2);
            }
        
        }*/ 
        
        if (empty($getPayment)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Payment List is not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Payment List Successfully Recieved";
        $response['result']['payment_data'] = $getPayment; 
         
      //  $response['result']['invoice'] = $getInvoiceItem;
        return array($response, 200);
    }

}
