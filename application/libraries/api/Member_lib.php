<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Member_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
     public function Member_Roles($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $getRoles = $this->CI->Member_model->get_all_table('member_role'); 
        
        if (empty($getRoles)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Roles Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Roles Successfully Recieved";
        $response['result']['data'] = $getRoles;
        return array($response, 200);
    } 
    
    public function get_social_msg(){
        $this->CI->load->model('About_model');
        $record = $this->CI->About_model->get_social_msg(); 
        
        if (empty($record)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "No data";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Social share message";
        $response['result']['data'] = $record;
        return array($response, 200);
    }
     public function getDSA_member($request = array())  {   
         
        $this->CI->load->model('Dsa_model');
        $dsa_member = $this->CI->Dsa_model->get_dsa_member_record('dsa'); 
        
        if (empty($dsa_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "DSA Members not Found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "DSA Member Successfully Recieved";
        $response['result']['data'] = $dsa_member;
        return array($response, 200);
    }  
    
    
     public function getLeadership($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $dsa_member = $this->CI->Member_model->getApi_Leadership(); 
        
        if (empty($dsa_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Committees not Found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Committees Successfully Recieved";
        $response['result']['data'] = $dsa_member;
        return array($response, 200);
    }
    
    public function get_leadership_frm_roles($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $getmember = $this->CI->Member_model->get_Leaderby_Roles($request['role_id']); 
      
        if (empty($getmember)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "WFDSA Leadership Members Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "WFDSA Leadership Members Successfully Recieved";
        $response['result']['data'] = $getmember;
        return array($response, 200);
    }  
   
    
    public function getCommittee($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $dsa_member = $this->CI->Member_model->get_committee(); 
        
        if (empty($dsa_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Committees not Found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Committees Successfully Recieved";
        $response['result']['data'] = $dsa_member;
        return array($response, 200);
    } 
    
    public function get_Committee_frm_roles($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $dsa_member = $this->CI->Member_model->get_committee_member($request['role_id']); 
        
        if (empty($dsa_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Committee Members not Found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Committee Member Successfully Recieved";
        $response['result']['data'] = $dsa_member;
        return array($response, 200);
    } 
    
    
     public function get_Countries($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $getCountries = $this->CI->Member_model->get_all_table('country'); 
      
        if (empty($getCountries)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Coutries Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Coutries Successfully Recieved";
        $response['result']['data'] = $getCountries;
        return array($response, 200);
    }  
    
    
    
    
    
     
    public function edit_member($request = array())  {   
         
        $this->CI->load->model('Member_model'); 
        $edit_member = $this->CI->Member_model->update_api_member($request); 
       
      
        if (empty($edit_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Member Information not updated";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Member Information Successfully Updated";
        $response['result']['data'] = $edit_member;
        return array($response, 200);
    }  

}
