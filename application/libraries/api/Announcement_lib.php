<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Announcement_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
     public function Announcement($request = array())  {   
         
        $this->CI->load->model('Announcement_model');
        $getAnnouncement = $this->CI->Announcement_model->get_all_table('announcement'); 
        
        if (empty($getAnnouncement) || $getAnnouncement == FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Announcement Data Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Announcement Successfully Recieved";
        $response['result']['data'] = $getAnnouncement;
        return array($response, 200);
    }
    
    public function Announcement_byrole($data)  {   
         
        $this->CI->load->model('Announcement_model');
        $getAnnouncement = $this->CI->Announcement_model->get_all_table_byrole('announcement',$data);
        $getrows = $this->CI->Announcement_model->get_all_table_rows('announcement',$data);
        
        if (empty($getAnnouncement) || $getAnnouncement == FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Announcement Data Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Announcement Successfully Recieved";
        $response['result']['total_counts'] = $getrows;
        $response['result']['data'] = $getAnnouncement;
        return array($response, 200);
    }
    
    public function Announcement_byrole_loading($data)  {   
         
        $this->CI->load->model('Announcement_model');
        $getAnnouncement = $this->CI->Announcement_model->get_all_table_byrole_loading('announcement',$data);
        $getrows = $this->CI->Announcement_model->get_all_table_rows('announcement',$data);
        
        if ($getrows < 1) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Announcement Data Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Announcement Successfully Recieved";
        $response['result']['total_counts'] = $getrows;
        $response['result']['data'] = $getAnnouncement;
        return array($response, 200);
    }
    
    public function single_announcement($data)  {   
         
        $this->CI->load->model('Announcement_model');
        $getAnnouncement = $this->CI->Announcement_model->get_single_announcement('announcement',$data);
        $getrows = $this->CI->Announcement_model->get_all_table_rows('announcement',$data);
        
        if (empty($getAnnouncement) || $getAnnouncement==FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Announcement Data Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Announcement Successfully Recieved";
        $response['result']['total_counts'] = $getrows;
        $response['result']['data'] = $getAnnouncement;
        return array($response, 200);
    }
    
     
    public function record($request = array())  {   
         
        $this->CI->load->model('Announcement_model');
        $getAnnouncement = $this->CI->Announcement_model->get_all_table_record('about'); 
        
        if (empty($getAnnouncement) || $getAnnouncement==FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "About Us Data Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "About Us Data Successfully Recieved";
        $response['result']['data'] = $getAnnouncement;
        return array($response, 200);
    }
}
