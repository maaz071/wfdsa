<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Resource_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
     public function get_resource()  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_api_resources(); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resources Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resources Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    } 
    
    public function get_three_resource($role)  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_three_resources($role); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resources Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resources Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    } 
    
      public function get_resource_ctg()  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_api_resources_ctg(); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resources Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resources Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    }
    
    public function get_resource_by_ctg($data)  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_api_resources_by_ctg($data); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resources Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resources Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    }
    
    public function get_resource_by_role_ctg($data)  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_ios_api_resources_by_ctg($data); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resources Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resources Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    }
    
    
    public function get_resource_file($request = array())  {   
         
        $this->CI->load->model('Resources_model');
        $getResrouce = $this->CI->Resources_model->get_api_resource_file($request); 
        
        if (empty($getResrouce)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Resource Files Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Resource Files Successfully Recieved";
        $response['result']['data'] = $getResrouce;
        return array($response, 200);
    }
    
    
     public function getDSA_member($request = array())  {   
         
        $this->CI->load->model('Dsa_model');
        $dsa_member = $this->CI->Dsa_model->get_dsa_member_record('dsa'); 
        
        if (empty($dsa_member)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "DSA Members not Found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "DSA Member Successfully Recieved";
        $response['result']['data'] = $dsa_member;
        return array($response, 200);
    } 
    
     public function get_Countries($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $getCountries = $this->CI->Member_model->get_all_table('country'); 
      
        if (empty($getCountries)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Coutries Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Coutries Successfully Recieved";
        $response['result']['data'] = $getCountries;
        return array($response, 200);
    }  
    
    
    public function get_member_frm_roles($request = array())  {   
         
        $this->CI->load->model('Member_model');
        $getmember = $this->CI->Member_model->get_memberby_Role($request['role_id']); 
      
        if (empty($getmember)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Member Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Member Successfully Recieved";
        $response['result']['data'] = $getmember;
        return array($response, 200);
    } 
     

}
