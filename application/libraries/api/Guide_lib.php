<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Guide_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;

    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */

    
     
    public function User_guide($request = array())  {   
         
        $this->CI->load->model('Guide_model');
        $guide_data = $this->CI->Guide_model->get_all_table('guide'); 
        
        if (empty($guide_data)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "User Guide Videos Are not Available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "User Guide Videos Successfully Recieved";
        $response['result']['data'] = $guide_data;
        return array($response, 200);
    } 
    
    
    
     public function get_contact_us($request = array())  {   
         
        $this->CI->load->model('Guide_model');
        $contact_Data = $this->CI->Guide_model->get_contactdata('contact_us'); 
        
        if (empty($contact_Data)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Contact Info not found";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Contact Info Recieved";
        $response['result']['data'] = $contact_Data;
        return array($response, 200);
    } 
    
    
    
     public function Contact_us($request = array())  {   
         
        $this->CI->load->model('Contact_us_model');
        $contact_Data = $this->CI->Contact_us_model->ContactSubmit($request); 
       
        if (empty($contact_Data)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Contact Email not send";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Email Send to WFDSA";
        return array($response, 200);
    } 
      
      

}
