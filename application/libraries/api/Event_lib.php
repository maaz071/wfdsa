<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of Subject_lib
 *
 * @author: Muhammad Umar Hayat
 * @Description: users library to perform user functions.
 */
class Event_lib
{
    /*
     * Property: 		CI
     * Description:             This will hold CI_Controller instance to perform all CI functionality
     * Type:     		Private
     */
    private $CI;
    const api_message_key = "AIzaSyDLwcHk93UVYSGtp-eDNi2RvnGDPoCDnNU";
   
    public function __construct()
    {
        $this->CI = &get_instance();
         $this->api_message_key = "AIzaSyDLwcHk93UVYSGtp-eDNi2RvnGDPoCDnNU";
        
    }

    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             Add Subject
     * Returns: 		.....
     */
     
    public function Event_notification($request = array()) {
        
        $this->CI->load->model('Event_model');  
            
        $token_records = $this->db->get('notification_token')->result_array();
        $getEvents = $this->CI->Event_model->get_upcomming_event($request['token']);
        
        $body="";
        foreach($getEvents as $value) {
            $body = "Reminder: Upcomming Event ".$value['title']." At ".$value['start_date'];    
        }
        
        
       
        //send_fcm($token,$body,$click_action,$admin_id=NULL,self::api_message_key);
         
        
        if (empty($getEvents)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "No Upcomming Event Found";
            return array($response, 406);
        } 
        
        

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Upcomming Event";
        $response['result']['data'] = $getEvents;
        return array($response, 200);
        
    }

    
     
    public function Events($request = array())  {   
         
        $this->CI->load->model('Event_model');
        $getEvents = $this->CI->Event_model->get_all_table_events('event'); 
        
        if (empty($getEvents) || $getEvents == FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event Successfully Recieved";
        $response['result']['data'] = $getEvents;
        return array($response, 200);
    } 
    
     public function single_event($request = array())  {   
         
        $this->CI->load->model('Event_model');
        $getEvents = $this->CI->Event_model->get_single_events('event'); 
        $getEvents[0]['dashimg'] = 'http://www.wfdsamobileportal.com/uploads/wfdsaabout.jpeg';
        // $getEvents = $this->CI->Event_model->get_all_table('event'); 
        // print_r($getEvents);exit;
        
        if ($getEvents == FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Events are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event Successfully Recieved";
        $response['result']['data'] = $getEvents;
        return array($response, 200);
    } 
    
    
     public function get_event_gallery($request = array())  {
        $event_id = $request['event_id'];
        $userid = $request['user_id'];
        $this->CI->load->model('Event_gallery_model');
        $getGalleries = $this->CI->Event_gallery_model->get_gallery_img('event_gal_images',$event_id,$userid); 
        
        if (empty($getGalleries) || $getGalleries==FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event galleries are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event Galleries Successfully Recieved";
        $response['result']['data'] = $getGalleries;
        return array($response, 200);
    }
    function get_event_resources($request = array()){
        $event_id = $request['event_id'];
        $this->CI->load->model('Event_model');
        $getResources = $this->CI->Event_model->get_resources($event_id); 
        
        if (empty($getResources) || $getResources==FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event resources are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event resources Successfully Recieved";
        $response['result']['data'] = $getResources;
        return array($response, 200);   
    } 

    function get_event_notifications($request = array()) {
        $user_type = $request['user_type'];
        $this->CI->load->model('Event_model');
        $getResources = $this->CI->Event_model->get_events_by_usertype($user_type); 
        if (empty($getResources) || $getResources==FALSE) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event notifications are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event notifications Successfully Recieved";
        $response['result']['data'] = $getResources;
        return array($response, 200);   
    }

    public function get_album($request)
    {
     $role = $request['role'];
     $type = $request['type'];
        $this->CI->load->model('Gallery_model');
        if($type == 1)
        {
        $getGalleries = $this->CI->Gallery_model->get_albums_api($role); 
        }
        else
        {
         $getGalleries = $this->CI->Gallery_model->get_photoalbums_api($role);    
        }
        
        if (empty($getGalleries) || $getGalleries=='false') {
           
            $response['result']['status'] = 'error';
            if($type == 1)
            {
                $response['result']['response'] = "No video albums found";
            }            
            else{
                $response['result']['response'] = "No photo album found";
            }
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Albums";
        $response['result']['data'] = $getGalleries;
        return array($response, 200);     
    }

    public function get_gallery($request)
    {
        $album_id = $request['album_id'];
        $this->CI->load->model('Gallery_model');
        $getGalleries = $this->CI->Gallery_model->getgal($album_id); 
        
        if (empty($getGalleries) || $getGalleries==='false') {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Galleries are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Galleries Successfully Recieved";
        $response['result']['data'] = $getGalleries;
        return array($response, 200);  
    }
    
    
    function add_poll_answer($data) {
        
        $this->CI->load->model('Poll_model'); 
        $add_poll_answer = $this->CI->Poll_model->add_api_poll_answer($data); 
       
      
        if (!$add_poll_answer) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Poll Comment Failed Submit.";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Poll Comment Successfully Submit.";
        return array($response, 200);
        
    }
    
    public function add_likes($request = array()){
        $this->CI->load->model('Event_model');
        
        $add_likes = $this->CI->Event_model->add_likes($request);
        //print_r($add_likes);
        //exit();
        if ($add_likes==0) {
            
             $getEvents=$add_likes;
        
        $response['result']['status'] = 'success';
        $response['result']['response'] = "Like Successfully Recieved";
        $response['result']['data'] = $getEvents;
        return array($response, 200);
        
          
        } 
         
            $response['result']['status'] = 'error';
            $response['result']['response'] = "likes unable to update";
            return array($response, 406);
        
       
    }
    
    public function get_event_details($request = array()) {
        
        $this->CI->load->model('Event_model');
        
        $total_registered =$this->CI->Event_model->total_registered($request);
        $photo_uploaded =$this->CI->Event_model->photo_uploaded($request);
        $is_answered =$this->CI->Event_model->is_answered($request);
        
        $is_checkedin =$this->CI->Event_model->is_checkedin($request);
        //print_r($is_checkedin);
        //exit();
         $is_registered = $this->CI->Event_model->is_registered($request);
        
        //$getEvents = $this->CI->Event_model->get_events($request);
        $getEvents = $this->CI->Event_model->get_events_detail($request);
        //print_r($getEvents);
        //exit();
        $total_likes = $this->CI->Event_model->get_total_likes($request); 
        $is_like =  $this->CI->Event_model->is_like($request); 
        
       
        
        //print_r($is_like);
        //exit();        
        
        if (empty($getEvents)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event details are not available";
            return array($response, 406);
        } 
        
        if (!isset($getEvents[0]['upload_image'])) {
            $getEvents[0]['upload_image'] = base_url().'uploads/member_photos/dummy.jpg';
        } 
       
        //$like = $is_like[0]['is_like'];
        //print_r($is_like[0]['is_like']);
        //exit();
        $strt_date = $getEvents[0]['start_date'];
        $end_date = $getEvents[0]['end_date'];
        list($data,$strt_time) = explode(' ',$strt_date);
        $end_date = $getEvents[0]['end_date'];
        list($date,$end_time) = explode(' ',$end_date);
        
        if(strpos($getEvents[0]['latLng'],",")) {
            list($lat,$long) =  explode(',',$getEvents[0]['latLng']);
            $getEvents[0]['latitude'] = $lat;
            $getEvents[0]['longitude'] = $long;
        }
        
        $getEvents[0]['total_registered'] = $total_registered[0]['total_registered'];
        $getEvents[0]['photo_uploaded'] = $photo_uploaded;
        $getEvents[0]['is_answered'] = $is_answered;
        $getEvents[0]['is_checked_in']=$is_checkedin;
        $getEvents[0]['is_registered']= $is_registered;
        $getEvents[0]['is_like'] = $is_like;
        $getEvents[0]['start_time'] = $strt_time;
        $getEvents[0]['end_time'] = $end_time; 
        $getEvents[0]['total_likes'] = $total_likes[0]['total_like']; 
        

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event Details Successfully Recieved";
        $response['result']['data'] = $getEvents;
        return array($response, 200);
        
    }
    
    public function get_members_details($request = array()) {
        
        $this->CI->load->model('Event_model');
        
     
        //print_r($request);
        //echo "test";
        //exit();
        //$getEvents = $this->CI->Event_model->get_events($request);
        $getmembers = $this->CI->Event_model->get_members_detail($request);
        //print_r($getEvents);
        //exit();
        $total_likes = $this->CI->Event_model->get_total_likes($request); 
        
        
        if (empty($getmembers)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Members detail are not available";
            return array($response, 406);
        } 
        
       
        
        $event_id = $getmembers[0]['first_name'];
        $last_name = $getmembers[0]['last_name'];
        //list($data,$strt_time) = explode(' ',$strt_date);
        //$end_date = $getEvents[0]['end_date'];
        //list($date,$end_time) = explode(' ',$end_date);
        
        /*if(strpos($getEvents[0]['latLng'],",")) {
            list($lat,$long) =  explode(',',$getEvents[0]['latLng']);
            $getEvents[0]['latitude'] = $lat;
            $getEvents[0]['longitude'] = $long;
        }*/
       
        
        $getmembers[0]['first_name'] = $event_id;
        $getmembers[0]['last_name'] = $last_name; 
        //$getEvents[0]['total_likes'] = $total_likes[0]['total_like']; 
        

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Members Details Successfully Recieved";
        $response['result']['data'] = $getmembers;
        return array($response, 200);
        
    }
    
    
    public function get_polls($request = array()) {
        
         $this->CI->load->model('Event_model');
         
         
        if (empty($request)) {
             
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event Id not Found.";
            return array($response, 406);
            exit;
        } 
         
         
        $get_event_poll = $this->CI->Event_model->get_event_poll($request['event_id']); 
        
        if (empty($get_event_poll)) {
           
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Event polls are not available";
            return array($response, 406);
        } 

        $response['result']['status'] = 'success';
        $response['result']['response'] = "Event Polls Successfully Recieved";
        $response['result']['data'] = $get_event_poll;
        return array($response, 200);
        
    } 
    
    function upload_gallery($data) {
        $this->CI->load->model('Event_gallery_model'); 
         
        $json = json_decode(file_get_contents('php://input'),true);
        $user_id = $json["user_id"];       
        $event_id = $json["event_id"];
        //$event_id = $json["event_id"]; //within square bracket should be same as Utils.imageName & Utils.imageList
        $imageList = $json["imageList"];
        
        // print_r($imageList);exit;
        //$time = time();
        
        
        $i = 0;
        $response = array();
     
     
        if (isset($imageList)) {
        	if (is_array($imageList)) {
        	    $j=0;
        		foreach($imageList as $image) {
    	      		$time =time();
    	      		$j++;
    	      		$decodedImage = base64_decode($image);
    	      		
    	      		$file_name = "./uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG";
    	      		$uploadfile = base_url()."/uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG";
    	      		
    	      		$return = file_put_contents("uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG", $decodedImage);
    	      		
    	      		if($return !== false) {
    	      		    
    	      		   // $this->CI->Event_gallery_model->insert_userPhoto_data();
    	      		    $this->CI->Event_gallery_model->create_gallery_records($user_id,$event_id,$file_name);
    	      		    
        			        $response['success'] = 1;
        			        $response['message'] = "Image Uploaded Successfully ";
        			        }
        			        else{
    	      		            $response['success'] = 0;
        			            $response['message'] = "Image Uploaded Failed";        
    	      		         
    	      		              
    	      		        
    	      		    } 
    	      		    
    			$i++;
    	        }
        	}
        } else{
        	$response['success'] = 0;
            $response['message'] = "List is empty.";
        }
     
        echo json_encode($response);
    }
    
    
    function attendees_checked_in($request) {
        $this->CI->load->model('Event_model');
        $attendees_id = ($request['attendees_id']) ? $request['attendees_id'] : 0;
        $upd =  $this->CI->Event_model->api_checked_in($attendees_id); 
        
        if($upd) {
            
            $response['result']['status'] = 'success';
            $response['result']['response'] = "User Checked In Successfully.";
            $response['result']['data'] = $upd;
            return array($response, 200);
            
        } else {
            
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Checked In Failed";
            return array($response, 406);
        }
        
    }
    
     function attendee_checked_in($request) {
        $this->CI->load->model('Event_model');
        $attendees_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        
        $upd =  $this->CI->Event_model->api_checked_in_check($attendees_id,$event_id,$signin_type); 
        
        if($upd) {
            
            $response['result']['status'] = 'success';
            $response['result']['response'] = "User Checked In Successfully.";
            $response['result']['data'] = $upd;
            return array($response, 200);
            
        } else {
            
            $response['result']['status'] = 'error';
            $response['result']['response'] = "Checked In Failed";
            return array($response, 406);
        }
        
    }
}
