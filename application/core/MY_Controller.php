<?php

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();  
       
		$this->check_login();  
        $this->data['segment_1n2'] = $this->uri->segment(1) . '/' . $this->uri->segment(2);
 
    }
	
	
	function check_login(){
	    
      if($this->session->userdata('is_logged_in') == false && empty($this->session->userdata('admin_data')) ) { 
          	redirect('login/index');
        } 
     }

    function get_categories() {
        //$this->output->enable_profiler(TRUE);
        $this->db->from('categories');
        $this->db->where('category_parent', 0);
        $query = $this->db->get();
        $data = $query->result_array();


        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
   

    function get_navigation() {
        // $this->output->enable_profiler(TRUE);
        
        $this->db->from('modules');
        $this->db->where('status', 1);
        $this->db->where('module_parent_id', 0);
        $this->db->order_by('sort_id');
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
        function getPagination($url, $total_number,$limit,$extraConfig=false){


        $this->load->library('pagination');

        $pagerUriSegment = 2;
        $pagerBaseUrl = $url;


        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";



        $config['base_url'] = $pagerBaseUrl;
        $config['total_rows'] = $total_number;
        $config['per_page'] = $limit;
        $config['uri_segment'] = $pagerUriSegment;
        $config['use_page_numbers'] = true;

        if($extraConfig != false){
            $config = array_merge($config,$extraConfig);
        }

        $this->pagination->initialize($config);

        return $this->pagination->create_links();

    }
   
    function get_record_total($table_name)
    {
        $this->db->Select('count(*) as total_records');
        $this->db->from($table_name);
        // $this->db->where('us.status', 1);
        $query = $this->db->get();
        $data = $query->row_array();

        if (!empty($data)) {
            return $data['total_records'];
        } else {
            return FALSE;
        }
    }

}

?>