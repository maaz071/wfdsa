<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Member_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->table_name = lcfirst(__CLASS__);
        $this->relation_table_name = "member_select_role";
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
         
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
       $tableName = $this->table_name;
        $total_records = $this->get_record_total($tableName);
        $offset = ($this->uri->segment(2) == '') ? 0 : $this->uri->segment(2) - 1;
        $role= $this->input->get('role_id');
        if(isset($_GET['per_page'])){
            $offset = $_GET['per_page'] - 1;
        }
        $limit = LISTING_LIMIT;
        if($this->input->get('role_id')){
            $role= $this->input->get('role_id');
            $pagerBaseUrl = base_url() . "member/?role_id=".$this->input->get('role_id');
            //redirect('/member/create/');
            
            //copying 
            $this->data['record_list'] = $this->Member_model->get_all_table($this->table_name,null,$limit,$limit*$offset);
        
        
        foreach($this->data['record_list'] as $value) {
            $role_id = ($this->input->get('role_id')) ? $this->input->get('role_id') : $value[$this->row_id];
            $this->data['roles'][$value[$this->row_id]] = $this->Member_model->get_roles($value[$this->row_id],$role_id);  
        } 
        //echo "<pre>";
        //print_r($this->data['record_list'][0]['role']);
        //echo "</pre>";
        //exit;
        
        $roles = $this->Member_model->get_parent_child('member_role', 0);
        
        
         
        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('member_role', $value['member_role_id']);
            
            $roles[$key]['child'] = $child;
        }
            $this->data['roles_select'] = $roles;
            $this->data['role_selection'] = $role;
            
            //ending copy
            
            $query = "SELECT * FROM member WHERE role LIKE '%".$role."%'";
            $r=$this->db->query($query);
            $this->data['pagination'] = $this->getPagination($pagerBaseUrl,$total_records,$limit,array("enable_query_strings"=>true,"page_query_string"=>true));

            //echo "<pre>"; 
            //print_r($r->result_array());
            //exit();
            
            $this->data['record_list']=$r->result_array();
            $this->data['record_list1']=$r->result_array();
            //echo "</pre>";
            //exit();
            
            $this->load->view('admin/header', $this->data);
            $this->load->view('admin/member_list_view', $this->data);
            $this->load->view('admin/footer');
            //exit();        
            
        }else{
            $pagerBaseUrl = base_url() . "member/";
        


        $this->data['pagination'] = $this->getPagination($pagerBaseUrl,$total_records,$limit,array("enable_query_strings"=>true,"page_query_string"=>true));



        $this->data['record_list'] = $this->Member_model->get_all_table($this->table_name,null,$limit,$limit*$offset);
        
        $this->data['record_list1'] = $this->Member_model->get_all_table_rev($this->table_name,null,$limit,$limit*$offset);
        
       
        foreach($this->data['record_list'] as $value) {
            $role_id = ($this->input->get('role_id')) ? $this->input->get('role_id') : $value[$this->row_id];
            $this->data['roles'][$value[$this->row_id]] = $this->Member_model->get_roles($value[$this->row_id],$role_id);  
        } 
        //echo "<pre>";
        //print_r($this->data['record_list'][0]['role']);
        //echo "</pre>";
        //exit;
        
        $roles = $this->Member_model->get_parent_child('member_role', 0);
        $this->data['role_selection'] = $role;
        
         
        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('member_role', $value['member_role_id']);
            
            $roles[$key]['child'] = $child;
        }
       
        $this->data['roles_select'] = $roles;
        /*echo "<pre>";
        print_r($this->data['record_list']); exit;*/
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/member_list_view', $this->data);
        $this->load->view('admin/footer');
        }
    }

    function getroles()
    {
    	 
        $roles = $this->Member_model->get_parent_child('member_role', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }
        $html = '';
        foreach ($roles as $value) { 
        	$html .= '<option id="role_option_'.$value['member_role_id'].'" disabled="disabled" value="'.$value['member_role_id'].'"style="font-weight:bold;"> '.$value['name'].' </option>';

				if(!empty($value['child'])) {
					foreach ($value['child'] as $value2) {
						$html .= '<option id="role_option_'.$value2['member_role_id'].'" value="'.$value2['name'].'" > '.$value2['name'].' </option>';
                        }
					} 
                }
                echo $html;
                exit;         
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        
        $this->data['admin_name'] = $admin_data[0]['name'];
        
        
        $roles = $this->Member_model->get_parent_child('member_role', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $this->data['roles'] = $roles;
        $this->data['selected'] = array();
        $test = array(" "," ");
        $this->data['test1'] = $test;

        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['country'] = $this->Member_model->get_all_table('country');
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/member_form_view',$this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->data['country'] = $this->Member_model->get_all_table('country');
         $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        
        $this->data['member_select_role'] = $this->Member_model->get_member_select($this->relation_table_name, $id);
        $checkbox_select = array();
        if(!empty($this->data['member_select_role'])){
            foreach ($this->data['member_select_role'] as $value) {
                $checkbox_select[] = $value['member_role_id'];
            }
        }
        $this->data['selected'] = $checkbox_select;
        
        $roles = $this->Member_model->get_parent_child('member_role', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $this->data['roles'] = $roles;

        $newString = $this->data['record_info'][0]['role'];
        $test = explode(",",$newString);
        $this->data['test1'] = $test;

        //echo "<pre>";
        //print_r($this->data['record_info'][0]['role']);
        //echo "</pre>";
        //exit;
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/member_form_view', $this->data);
        $this->load->view('admin/footer');
    }

     function create_action() {
        $post = $this->input->post();
		$date =date('Y-m-d H:i:s');
		$newString = "";
		
		if (isset($post['member_role'])) 
		$roster = $post['member_role'];
		$newString = implode(',', $post['member_role']);
		$pssword = base64_encode($post['password']);
		
		if ($post['hdn-x1-axis']) {			
			$param['x1'] = $post['hdn-x1-axis'];
			$param['y1'] = $post['hdn-y1-axis'];
			$param['x2'] = $post['hdn-x2-axis'];
			$param['y2'] = $post['hdn-y2-axis'];
			$param['w1'] = $post['hdn-thumb-width'];
			$param['h1'] = $post['hdn-thumb-height'];
			$param['image_name'] = $post['image_name'];
	
    $newNamePrefix = time() . '_';

        $config = array(
            'image_library' => 'gd2',
            'source_image' => './uploads/Member_Photos/'.$post['image_name'],
            'new_image' => './uploads/coppedimg/'.$newNamePrefix.$post['image_name'],
            'maintain_ratio' => FALSE,
            'width' => $post['hdn-thumb-width'],
            'height' => $post['hdn-thumb-height'],
            'x_axis' => $post['hdn-x1-axis'],
            'y_axis' => $post['hdn-y1-axis']
            );
        $this->load->library('image_lib');
        $this->image_lib->clear();
$this->image_lib->initialize($config);
$this->image_lib->crop();
    }

		$filename = $newNamePrefix . $post['image_name'];
		
	/*	var_dump($filename);
		exit;*/
		
		$data = array(
			'first_name' => $post['first_name'],
			'last_name' => $post['last_name'],
			'email' => $post['email'],
			'password' => $pssword,
			'title' => $post['title'],
			'wfdsa_title' => $post['wfdsa_title'],
			'company' => $post['company'],
			'website' => $post['website'],
			'venue' => $post['company'],
			'address' => $post['address'],
			'designation' => $post['designation'],
			'telephone' => $post['telephone'],
			'country' => $post['country'],
			'cell' => $post['cell'],
			'fax' => $post['fax'],
			'biography' => $post['biography'],
			'app_user' => "1",
			'upload_image' => base_url(). '/uploads/coppedimg/' . $filename,
			'status' => "1",
			'created_on' => $date,
			'updated_on' => $post['title'],
			'created_by' => $post['title'],
			'updated_by' => $post['title'],
			'device_token' => "",
			'role' => $newString,
		);
		$query = $this->db->insert('member', $data);
		$recent_id = $this->db->insert_id();
		$param = time() . rand(999, 99999999999);
		$api_secret = md5($param) . substr(str_shuffle($param) , 0, 10);
		$query1 = $this->db->insert('api_keys', array(
			'member_id' => $recent_id,
			'api_secret' => $api_secret
		));
		
		foreach ($roster as $value){
		$query2 = $this->db->insert('member_select_role', array(
			'member_id' => $recent_id,
			'member_role_id' => $value,
			'sort_order' => "0"
		));
		}
		if ($query1) {

			// $data=array();

   //      $data['email']=array([
   //          'heading' => 'New User Registration',
   //          'firstname' => $post['first_name'],
   //          'lastname' => $post['last_name'],
   //          'text'=> 'You are Successfully Registered to use WFDSA Mobile Application. Kindly Review your information.<br>
   //              <b>Credentials:</b><br>
   //              usernmae: '.$post['email'].'<br>
   //              password: '.$post['password'].'<br>
   //              Role: '.$newString.'<br>
   //           To download our moile application, click on the android OR iOS icon below.',
   //          'link' => ''
            
   //      ]);
   //      $emailbody = $this->load->view('template/email',$data,true);


   //      $config=array(
   //      'charset'=>'utf-8',
   //      'wordwrap'=> TRUE,
   //      'mailtype' => 'html'
   //      );

   //      $this->email->initialize($config);

			// $this->email->set_mailtype("html");
			// $this->email->from('masteremail323@gmail.com', 'WFDSA');
			// $this->email->to($post['email']);
			// $this->email->subject('Member Credentials & Details - WFDSA');
			// $this->email->message($emailbody);
			// $this->email->send();
			
			$this->session->set_flashdata('msg', 'Member Added Successfully');
			redirect('/member/');
		}
	}

    function bulkemail($id = NULL)
    {
        $members = $this->Member_model->get_all_table('member',$id);
        $emailSentList=array();
        $counter =0;
        foreach($members as $member)
        {
            $data=array();

        $data['email']=array([
            'heading' => 'You are registered as a member to use WFDSA mobile app',
            'firstname' => $member['first_name'],
            'lastname' => $member['last_name'],
            'text'=> 'WFDSA has created your member account for WFDSA mobile app. Now you can download and use the mobile application with the following credentials:<br><br>
                username: '.$member['email'].'<br>
                password: '.base64_decode($member['password']).'<br><br>
             To download our moile application, click on the android OR iOS icon below.',
            'link' => ''
            
        ]);
        $emailbody = $this->load->view('template/email',$data,true);

        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html',
        );

        $this->email->initialize($config);

            $this->email->set_mailtype("html");
            $this->email->from('no-reply@wfdsa.org', 'WFDSA');
            $this->email->to($member['email']);
            $this->email->subject('Member Credentials - WFDSA');
            $this->email->message($emailbody);
            $this->email->send();

            // array_push($emailSentList,$member['first_name'],$member['last_name'],$member['email'],++$counter);
               
        }
        // echo '<pre>';
        // print_r($emailSentList);
        // echo '</pre>';
        // exit;
        
            $this->session->set_flashdata('msg', 'Member Credentials have been sent');
            redirect('/member/');
    }	
    

    
    function add_role() {
        $post = $this->input->post();
        $check = $this->Member_model->create_role('member_role', $post);
        $option = "<option value='" . $check . "'>" . $post['name'] . "</option>";
        if ($check !== "") {
            echo json_encode(array('success' => 'yes', 'option' => $option));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

    function delete_role() {
        $id = $this->uri->segment(3);

        $this->db->where('member_role_id', $id);
        $data = $this->db->delete('member_role');

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => '<strong> Record Deleted Successfully</strong>'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => '<strong> Some Bad had Happened Contact Developer</strong>'));
        }
    }

    function update_action() {
        $post = $this->input->post();
        $id= $post['member_id'];
        
        $newString="";
        if(isset($post['member_role'])){
            $newString = implode(',', $post['member_role']);
            $roster = $post['member_role'];
        }
        else{
            
            $newString = $post ['roleSrc'];
            $roster = explode (',',$post ['roleSrc']);
        }
         $pssword= '';
        if(isset($post['password']) && $post['password'] != ''){
            $pssword = base64_encode($post['password']);
        }
         
         
         
         if($post['hdn-x1-axis']) {
            $param['x1'] = $post['hdn-x1-axis'];
            $param['y1'] = $post['hdn-y1-axis'];
            $param['x2'] = $post['hdn-x2-axis'];
            $param['y2'] = $post['hdn-y2-axis'];
            $param['w1'] = $post['hdn-thumb-width'];
            $param['h1'] = $post['hdn-thumb-height'];
            $param['image_name'] = $post['image_name'];
    
    $newNamePrefix = time() . '_';

        $config = array(
            'image_library' => 'gd2',
            'source_image' => './uploads/Member_Photos/'.$post['image_name'],
            'new_image' => './uploads/coppedimg/'.$newNamePrefix.$post['image_name'],
            'maintain_ratio' => TRUE,
            'width' => $param['w1'],
            'height' => $param['h1'],
            'x_axis' => $param['x1'],
            'y_axis' => $param['y1']
            );
        $this->load->library('image_lib');
        $this->image_lib->clear();
$this->image_lib->initialize($config);
$this->image_lib->crop();
$file = base_url().'/uploads/coppedimg/'.$newNamePrefix.$post['image_name'];
    }
        else
        {
          $file = $post['imageSrc'];
        }
      
        
         $data = array(
         
          'first_name'=>  $post['first_name'],
		  'last_name'=>  $post['last_name'] , 
        
          'email'=>  $post['email'],
		  'password'=>  $pssword , 
		  'title'=>  $post['title'],
		  'wfdsa_title'=>  $post['wfdsa_title'], 
		  'company'=>  $post['company'] ,
          'website'=>  $post['website'], 
          'venue' => $post['company'],
          'address'=>  $post['address'],
          'designation'=>  $post['designation'], 
          'telephone'=>  $post['telephone'], 
          'country'=>  $post['country'] , 
          'cell'=>  $post['cell'] , 
          'fax'=>  $post['fax'],
          'biography'=>  $post['biography'],
		  'app_user'=> "1",
          'upload_image' => $file,
          'status' => "1",
          'created_on'=> $post['title']  ,
		  'updated_on'=>  $post['title'] , 
          'created_by'=>  $post['title'], 
          'updated_by'=>  $post['title'],
          'device_token' => "",
          'role'=>  $newString, 
        
        );
        if($pssword == ''){
            unset($data['password']);
        }
        $this->db->where('member_id',$id);
        $query =  $this->db->update('member', $data);
        
  //       $this->db->where('member_id',$id);
  //       $query1 = $this->db->delete('member_select_role');
        
  //       foreach ($roster as $value){
		// $query2 = $this->db->insert('member_select_role', array(
		// 	'member_id' => $id,
		// 	'member_role_id' => $value,
		// 	'sort_order' => "0"
		// ));
		// }
         
        //$this->db->where('id', $id);
        //$this->db->delete('mytable');

         
         if($query)
        {
            //echo "success";
         redirect('/member/');
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Member_Photos/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
       // $config['max_size'] = 100;
        $config['max_width'] = 1000;
        $config['max_height'] = 1000;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    function do_upload_resize() {
         $post = $this->input->post();
          $this->session->set_userdata('image_key', $post['primary_key']);
         $file = ($_FILES) ?  $post['primary_key']."_".$_FILES['profile-pic']['name'] : NULL; 
           
          $this->load->library('image_lib');
          $config['upload_path'] = './uploads/Member_Photos/';
          $config['allowed_types'] = 'jpg|jpeg|png';
          $config['file_name'] = $file;
          $config['max_width']  = 1024;
          $config['max_height'] = 768;
          $this->load->library('upload', $config);
          if (!$this->upload->do_upload('profile-pic')) {
             echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
          } else {
                $image_data =   $this->upload->data();
               
               if($image_data['image_width'] > 1024 && $image_data['image_height'] > 768) {
                   echo json_encode(array('success' => 'no', 'msg' => '<strong>Image Must be less then 1024x768</strong>'));
                   exit;
               }
                 
                $configer =  array(
                  'image_library'   => 'gd2',
                  'source_image'    =>  $image_data['full_path'],
                  'maintain_ratio'  =>  TRUE,
                  'width'           =>  200,
                  'height'          =>  200,
                ); 
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
          }
          
         echo $image = "<img id='photo' file-name='".$file."' class='' src='".base_url('uploads/Member_Photos/'.$file)."' class='preview'/>";
          
         
    }
    
    function upload_cropped_pic($param) {
        $t_width = 300; // Maximum thumbnail width
	    $t_height = 300;    // Maximum thumbnail height	
	    
	     $id = ($this->session->userdata('image_key')) ? $this->session->userdata('image_key') : "NULL";
	     
      // if(isset($post['t']) and $post['t'] == "ajax") {
            
    		extract($param);		
    		$imagePath = './uploads/Member_Photos/'.$id."_".$param['image_name'];
    		$ratio = ($t_width/$w1); 
    		$nw = ceil($w1 * $ratio);
    		$nh = ceil($h1 * $ratio);
    		$manipulator = new ImageManipulator($_FILES['image_name']['tmp_name']);
    		$nimg = $manipulator->imagecreatetruecolor($nw,$nh);
    		$im_src = $manipulator->imagecreatefromjpeg($imagePath);
    		$manipulator->imagecopyresampled($nimg,$im_src,0,0,$x1,$y1,$nw,$nh,$w1,$h1);
    		$manipulator->imagejpeg($nimg,$imagePath,90);		
    	
    	//  } 
	 
        
        
        
    }

}
