<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ---------------------------------------------------------------------------------------
 * File: api_doc.php
 * Type: Controller
 * Created by: Hafiz Haseeb Ali
 * Description: This is controller to handle User functionss
 * ---------------------------------------------------------------------------------------
 */

require APPPATH . 'libraries/api/REST_Controller.php';

class Api_doc extends CI_Controller
{

    public $base_path = API_SWAGGER_PATH;

    /*
     * Function: 		index
     * Method Accepted:	post
     * URI: 			/api_doc/
     * Params: 			.....
     * URI Segments: 	None
     * Written by: 		Haseeb Ali
     * Description: 	This is for Swagger documentation
     * Returns: 		Swagger Json Array
     */
    function index()
    {
        $allvalue = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "1.2",
            "apis" => array(
                array(
                    "path" => "/user",
                    "description" => "User Functions"
                ),

                array(
                    "path" => "/yacht",
                    "description" => "All functions yacht"
                ),

                array(
                    "path" => "/booking",
                    "description" => "All functions Booking Ordering"
                ),

            ),
            "info" => array(
                "title" => "Yacht Restful API",
                "description" => "Below is the list of available REST API functions",

            )
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($allvalue);

    }
    /*
     * Function: 		user
     * URI: 			/api_doc/user
     * Params: 			.....
        * Description: 	User Activity
     * Returns: 		Swagger Json Array
     */
    public function user()
    {
        $user = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "1.2",
            "basePath" => $this->base_path,
            "resourcePath" => "/user",
            "produces" => array("application/json"),
            "apis" => array(
                // all about login detail in Swagger Documentation and Testing
                array(
                        "path" => "/user/register",
                        "operations" => array(
                        array(
                                "method" => "POST",
                                "summary" => "Register New User.",
                                "notes" => "",
                                "type" => "string",
                                "nickname" => "Register New User",
                                "parameters" => array(

                                    array(
                                            "name" => "name",
                                            "description" => "Name",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "email",
                                            "description" => "Email Address",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "password",
                                            "description" => "Password, Note: Minmum Password Lenght Should Be 6",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),

                                    array(
                                            "name" => "signup_type",
                                            "description" => "Sign Up Type, 1 for Facebook, 2 for Custom",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "fb_id",
                                            "description" => "Facebook ID if signup_by is 1",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "fb_token",
                                            "description" => "Facebook Token if signup_by is 1",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                        "name" => "gcm_key",
                                        "description" => "Firebase token",
                                        "required" => false,
                                        "type" => "string",
                                        "paramType" => "form"
                                    )
                                ),
                                "responseMessages" => array(
                                        array(
                                                "code" => 200,
                                                "message" => "Operation Successful"
                                        ),
                                        array(
                                                "code" => 400,
                                                "message" => "Something Went Wrong"
                                        ),
                                        array(
                                                "code" => 409,
                                                "message" => "Email or User Name Already Exists."
                                        )
                                    )
                                )
                            )
                        ),
                    array(
                        "path" => "/user/login",
                        "operations" => array(
                        array(
                                "method" => "POST",
                                "summary" => "Login User With Email and Password or Facebook ID.",
                                "notes" => "",
                                "type" => "string",
                                "nickname" => "Login User",
                                "parameters" => array(
                                    array(
                                            "name" => "email",
                                            "description" => "Email Address",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "password",
                                            "description" => "Password Note: Minimum 6 Characters",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "fb_id",
                                            "description" => "Facebook ID in case if user is Facebook user",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                            "name" => "signin_type",
                                            "description" => "Sign in Type, 1 for Facebook, 2 for Custom",
                                            "required" => false,
                                            "type" => "string",
                                            "paramType" => "form"
                                        ),
                                    array(
                                        "name" => "gcm_key",
                                        "description" => "Firebase token",
                                        "required" => false,
                                        "type" => "string",
                                        "paramType" => "form"
                                    )
                                ),
                                "responseMessages" => array(
                                        array(
                                                "code" => 200,
                                                "message" => "Operation Successful"
                                        ),
                                        array(
                                                "code" => 400,
                                                "message" => "Something Went Wrong"
                                        )
                                  )
                            )
                        )
                    ),






                array(
                    "path" => "/user/update",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Update User Profile.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "Login User",
                            "parameters" => array(
                                array(
                                    "name" => "api_secret",
                                    "description" => "API Secret",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "email",
                                    "description" => "Email Address",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "name",
                                    "description" => "name",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "password",
                                    "description" => "password change",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            ),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                )
                            )
                        )
                    )
                ),

            ),
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($user);
    }
    /*
         * Function: 		subjects
         * URI: 			/api_doc/subjects
         * Params: 			.....
         * Description:             Orders Related Functions
         * Returns: 		Swagger Json Array
         */


    public function yacht()
    {
        $order = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "2.1.4",
            "basePath" => $this->base_path,
            "resourcePath" => "/yacht",
            "produces" => array("application/json"),
            "apis" => array(
                array(
                    "path" => "/yacht/AllYacht",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Get All Yacht.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "",
                            "parameters" => array(
                                array(
                                    "name" => "api_secret",
                                    "description" => "API Secret",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                ),
                            )
                        )
                    )
                ),
                array(
                    "path" => "/yacht/GetAYacht",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Get All Yacht.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "",
                            "parameters" => array(
                                array(
                                    "name" => "yacht_id",
                                    "description" => "signle yacht id",
                                    "required" => false,
                                    "type" => "Integer",
                                    "paramType" => "form"
                                ),
                                ),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                ),
                            )
                        )
                    )
                )
            ),
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($order);
    }



    public function booking()
    {
        $order = array(
            "apiVersion" => "1.0.0",
            "swaggerVersion" => "2.1.4",
            "basePath" => $this->base_path,
            "resourcePath" => "/book",
            "produces" => array("application/json"),
            "apis" => array(

                array(
                    "path" => "/book/Booking",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Get All Booking History.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "",
                            "parameters" => array(
                                array(
                                    "name" => "api_secret",
                                    "description" => "API Secret",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "yacht_id",
                                    "description" => "yacht ID",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "booking_date",
                                    "description" => "booking date like 2017-08-17",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "booking_time",
                                    "description" => "time booking start like 23:00:00",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "duration",
                                    "description" => "Hour like 1 , 2 , 3",
                                    "required" => true,
                                    "type" => "Integer",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "sub_total",
                                    "description" => "Yacht sub total amount",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "facilities_amount",
                                    "description" => "Faclities Amount",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "discount",
                                    "description" => "discount",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "total_amount",
                                    "description" => "Total Amount",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "payment_type",
                                    "description" => "payment type cash or stripe",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "payment_status",
                                    "description" => "payment status paid or unpaid",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),
                                array(
                                    "name" => "facilities",
                                    "description" => "main facilities ids like this  12, 15,50",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                ),

                                array(
                                    "name" => "sub_facilities",
                                    "description" => "sub facilities ids like this  1, 5,6,20",
                                    "required" => false,
                                    "type" => "string",
                                    "paramType" => "form"
                                )

                                
                                
                            ),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                ),
                            )
                        )
                    )
                ),

               array(
                    "path" => "/book/BookingHistory",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Get All Booking History.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "",
                            "parameters" => array(
                                array(
                                    "name" => "api_secret",
                                    "description" => "API Secret",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            ),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                ),
                            )
                        )
                    )
                ),
                
                array(
                    "path" => "/book/CallBack",
                    "operations" => array(
                        array(
                            "method" => "POST",
                            "summary" => "Call Back.",
                            "notes" => "",
                            "type" => "string",
                            "nickname" => "",
                            "parameters" => array(
                                array(
                                    "name" => "number",
                                    "description" => "Mobile Number",
                                    "required" => true,
                                    "type" => "string",
                                    "paramType" => "form"
                                )
                            ),
                            "responseMessages" => array(
                                array(
                                    "code" => 200,
                                    "message" => "Operation Successful"
                                ),
                                array(
                                    "code" => 400,
                                    "message" => "Something Went Wrong"
                                ),
                            )
                        )
                    )
                )
            ),
        );
        header('Access-Control-Allow-Origin: *');
        echo json_encode($order);
    }
}
