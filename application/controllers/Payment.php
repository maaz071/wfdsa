<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Payment_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $this->data['event_id'] = $this->input->get('event_id');
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['record_list'] = $this->Payment_model->get_payment_data($this->table_name, $this->data['event_id']); 
        $this->data['event'] = $this->Payment_model->get_all_table('event');
        
        /*echo "<pre>";
        print_r($this->data['record_list']);
        echo "</pre>";
        exit;*/
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');

        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view');
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3); 
        $this->data['record_info'] = $this->Payment_model->get_all_table($this->table_name, $id);  
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }
    
    function view() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        
        $id = $this->uri->segment(3); 
        $this->data['record_info'] = $this->Payment_model->get_all_table($this->table_name, $id); 
        $this->data['likes'] = $this->Payment_model->get_likes_records($id); 
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_display_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post(); 
            
        $create_id = $this->Payment_model->create_record($this->table_name, $post, $flename);


        if ($create_id !== "") {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
         
    }
    
    function insert_payment($member_id = NULL,$non_member_id=NULL,$event_id) {
        
        $post = array();
        if(!empty($member_id)) {
            $post['member_id'] = '';
        }
        
        if(!empty($non_member_id)) {
            $post['non_member_id'] = '';
        }
        
       
        $post['event_id'] = $event_id;
        $post['payment_date'] = date('Y-m-d h:i:s');
        
        $create_id = $this->Payment_model->create_record($this->table_name, $post);

        if ($create_id !== "") {
            echo json_encode(array('success' => 'yes', 'id' => $create_id));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function update_action() {
        $post = $this->input->post();
      
        $file = ($_FILES) ? $_FILES['upload_image']['name'] : NULL;
        $update = $this->Payment_model->update_record($this->table_name, $this->row_id, $post, $file, $post[$this->row_id]);
        $file_upload = (!empty($_FILES)) ? $this->do_upload('upload_image') : FALSE;

        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Event_Images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
        //  $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

}
