<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_admin extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_admin_model');
    }

    /**
     Feature like delete , changing status , delete selected row can be control from here
     */
     
     function delete_selected() {

        $ids = $this->input->post('ids');
        $table = $this->input->post('table');
        // $table = strtolower($table);

        $this->db->where_in($table.'_id', $ids);
        $data = $this->db->delete($table);

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');
        $table_name = $this->input->post('table');
        $table_name = strtolower($table_name);        
        $this->db->where($table_name.'_id', $id);
        $data = $this->db->delete($table_name);
        
        if($table_name == 'resources') { 
                
         $this->db->where($table_name.'_id', $id);
         $this->db->delete('resource_file');
                
         $this->db->where($table_name.'_id', $id);
         $this->db->delete('resource_categories');
     
        }
        
        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    
     function change_status() {

        $id = $this->input->post('id');
        $current = $this->input->post('current');
        $table = $this->input->post('table');
      

        $this->db->where($table.'_id', $id);
         
        
        if($current == 'active') {
            $check1 = $this->db->update($table, array('status' => 0)); 
            if ($check1) {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record Inactive Successfully'));
                exit;
            } 
        } else if($current == 'inactive') { 
            $check2 = $this->db->update($table, array('status' => 1));  
             if ($check2) {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record Activated Successfully'));
                exit;
            }
        } 
          
    }
    
     

}
