<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Non_member extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Non_member_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->table_name = lcfirst(__CLASS__); 
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        // $this->output->enable_profiler(TRUE); 
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        
        
        $tableName = $this->table_name;
        $total_records = $this->get_record_total($tableName);
        $offset = ($this->uri->segment(2) == '') ? 0 : $this->uri->segment(2) - 1;
        if (isset($_GET['per_page'])) {
            $offset = $_GET['per_page'] - 1;
        }
        $limit = LISTING_LIMIT;
        if ($this->input->get('role_id')) {
            $pagerBaseUrl = base_url() . "non_member/?role_id=" . $this->input->get('role_id');
        } else {
            $pagerBaseUrl = base_url() . "non_member/";
        }


        $this->data['pagination'] = $this->getPagination($pagerBaseUrl, $total_records, $limit, array("enable_query_strings" => true, "page_query_string" => true));

        $this->data['record_list'] = $this->Non_member_model->get_join_record($this->table_name, $limit, $limit * $offset);
        //echo "<pre>"; 
        //print_r($this->data['record_list']);
        //exit;
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/'.$this->table_name.'_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        
        $this->data['admin_name'] = $admin_data[0]['name']; 

       
        $this->data['country'] = $this->Non_member_model->get_all_table('country');
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/'.$this->table_name.'_form_view',$this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->data['country'] = $this->Non_member_model->get_all_table('country');
         $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/'.$this->table_name.'_form_view', $this->data);
        $this->load->view('admin/footer');
    }

     function create_action() {
        $post = $this->input->post();

        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim|is_unique[non_member.email]');
        $this->form_validation->set_rules('contact_no', 'Contact No.', 'required|trim');
        $this->form_validation->set_rules('country', 'Country', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim'); 
        

        if ($this->form_validation->run() !== FALSE) {
            
             
            
            if(strlen($post['password']) < 8 || strlen($post['password']) < 8) {
                 echo json_encode(array('success' => 'no', 'msg' => '<strong> Password Must be 8 Character Long </strong>'));
                 exit;
            } 
    
            $create_id = $this->Non_member_model->create_record($this->table_name, $post); 
            
             
            if ($create_id !== "") { 
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }
    
    

    function delete_role() {
        $id = $this->uri->segment(3);

        $this->db->where('member_role_id', $id);
        $data = $this->db->delete('member_role');

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => '<strong> Record Deleted Successfully</strong>'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => '<strong> Some Bad had Happened Contact Developer</strong>'));
        }
    }

    function update_action() {
        $post = $this->input->post();
        
        
        if(strlen($post['password']) < 8 || strlen($post['password']) < 8) {
            echo json_encode(array('success' => 'no', 'msg' => '<strong> Password Must be 8 Character Long </strong>'));
            exit;
        }  
        
        
        $update = $this->Non_member_model->update_record($this->table_name, $this->row_id,$post,$post[$this->row_id]);
      

        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => '<strong>Record Updated SuccessFully.</strong>'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/member_photos/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
       // $config['max_size'] = 100;
        $config['max_width'] = 300;
        $config['max_height'] = 440;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    function do_upload_resize() {
         $post = $this->input->post();
          $this->session->set_userdata('image_key', $post['primary_key']);
         $file = ($_FILES) ?  $post['primary_key']."_".$_FILES['profile-pic']['name'] : NULL; 
           
          $this->load->library('image_lib');
          $config['upload_path'] = './uploads/member_photos/';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['file_name'] = $file;
        /*  $config['max_width']  = 1024;
          $config['max_height'] = 768;*/
          $this->load->library('upload', $config);
          if (!$this->upload->do_upload('profile-pic')) {
             echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
          } else {
                $image_data =   $this->upload->data();
               
               if($image_data['image_width'] < 500 && $image_data['image_height'] < 257) {
                   echo json_encode(array('success' => 'no', 'msg' => '<strong>Image Must be 500x257</strong>'));
                   exit;
               }
                 
                $configer =  array(
                  'image_library'   => 'gd2',
                  'source_image'    =>  $image_data['full_path'],
                  'maintain_ratio'  =>  TRUE,
                  'width'           =>  500,
                  'height'          =>  257,
                ); 
                $this->image_lib->clear();
                $this->image_lib->initialize($configer);
                $this->image_lib->resize();
          }
          
         echo $image = "<img id='photo' file-name='".$_FILES['profile-pic']['name']."' class='' src='".base_url('uploads/member_photos/'.$file)."' class='preview'/>";
          
         
    }
    
    function upload_cropped_pic($param) {
        $t_width = 300; // Maximum thumbnail width
	    $t_height = 300;    // Maximum thumbnail height	
	    
	     $id = ($this->session->userdata('image_key')) ? $this->session->userdata('image_key') : "NULL";
	     
      // if(isset($post['t']) and $post['t'] == "ajax") {
            
    		extract($param);		
    		$imagePath = './uploads/member_photos/'.$id."_".$param['image_name'];
    		$ratio = ($t_width/$w1); 
    		$nw = ceil($w1 * $ratio);
    		$nh = ceil($h1 * $ratio);
    		$nimg = imagecreatetruecolor($nw,$nh);
    		$im_src = imagecreatefromjpeg($imagePath);
    		imagecopyresampled($nimg,$im_src,0,0,$x1,$y1,$nw,$nh,$w1,$h1);
    		imagejpeg($nimg,$imagePath,90);		
    	
    	//  } 
	 
        
        
        
    }

}
