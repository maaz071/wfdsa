<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller
{

    function __construct()
    {
        parent::__construct(); 
        $this->load->model('Event_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index()
    {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['record_list'] = $this->Event_model->get_all_table($this->table_name);
        $table ="event";
        $this->data['record_list'] = $this->Event_model->get_all_table_events($table);


        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function event_archive(){
    	$event_id = $this->input->get('event_id');
    	$event_info = $this->Event_model->get_all_table($this->table_name,$event_id);
    	$event_notifications = $this->Event_model->get_notifications($event_id);
    	$event_resources = $this->Event_model->get_resources('event_resources',$event_id);
    	$event_gallery = $this->Event_model->get_event_gallery('181');

    	// print_r($event_notifications[0]);exit;

    	if (!file_exists('assets/archives/'.$event_id)) {
    		mkdir('assets/archives/'.$event_id, 0777, true);
		}
		$directory = base_url().'assets/archives/'.$event_id;
		if (!file_exists('assets/archives/'.$event_id.'/gallery')) {
    		mkdir('assets/archives/'.$event_id.'/gallery', 0777, true);
		}
		$gallery_directory = base_url().'assets/archives/'.$event_id.'/gallery';
		foreach($event_gallery as $key=>$row)
		{
			$basename = basename($row['uploaded_image']);
			if(!file_exists('assets/archives/'.$event_id.'/gallery/'.$basename)){
				copy($row['uploaded_image'], 'assets/archives/'.$event_id.'/gallery/'.$basename);
			}
		}
		$notification_headers = $this->get_keys($event_notifications[0]);
		
		$csv = '';
		// foreach ($notification_headers as $record){
			for($j=0;$j<count($notification_headers);$j++){
				if($j == count($notification_headers) -1)
				{
					$csv.= $notification_headers[$j];
				}
				else{
		    		$csv.= $notification_headers[$j].',';
		    	}
		}
		$csv.= '\n';
		foreach ($event_notifications as $row){
			for($j=0;$j<count($notification_headers);$j++){
				if($j == count($notification_headers) -1)
				{
					$csv.= $row[$notification_headers[$j]];
				}
				else{
		    	$csv.= $row[$notification_headers[$j]].','; //Append data to csv
		    	}
		    }
		    $csv.= '\n';
		}
		// print_r($csv);exit;
		$csv_filename = 'assets/archives/'.$event_id.'/notifications'."_".date("Y-m-d",time()).".csv";
		$csv_handler = fopen ($csv_filename,'w');
		fwrite ($csv_handler,$csv);
		fclose ($csv_handler);

		print_r($notification_headers);exit; 
    }

    function get_keys($data){
    	$header = [];
    	foreach($data as $key => $row)
    	{
    		array_push($header,$key);
    	}
    	return $header;
    }
    function create()
    {
        $admin_data = $this->session->userdata('admin_data');

        $roles = $this->Event_model->get_parent_child('member_role', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $this->data['roles'] = $roles;
        $this->data['selected'] = array();

        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['country'] = $this->Event_model->get_all_table('country');
        //$this->data['city'] = $this->Event_model->get_all_table('country');
         $test = array(" "," ");
        $this->data['test1'] = $test;
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function event_resources(){
    	$admin_data = $this->session->userdata('admin_data');
    	$this->data['admin_name'] = $admin_data[0]['name'];
    	$this->data['event_id'] = $this->input->get('event_id');
    	$this->data['resources'] = $this->Event_model->get_resources($this->input->get('event_id'));
    	$this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_resources', $this->data);
        $this->load->view('admin/footer');
    }

    function event_notifications(){
    	$admin_data = $this->session->userdata('admin_data');
    	$this->data['admin_name'] = $admin_data[0]['name'];
    	$this->data['event_id'] = $this->input->get('event_id');
    	$this->data['notifications'] = $this->Event_model->get_notifications($this->input->get('event_id'));
    	$this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_notifications', $this->data);
        $this->load->view('admin/footer');	
    }

    function addnotification(){
    	$event_title = $this->Event_model->get_event_title('event',$this->input->post('event_id'));
    	$data = array(
				'notification' => $this->input->post('title'),
				'event_id' => $this->input->post('event_id'),
				'event_title'=>$event_title[0]['title']
			);
			$query = $this->db->insert('event_notifications', $data);
			$id = $this->db->insert_id();

			if(!empty($id))
				{
					$this->session->set_flashdata('msg', 'Notification Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Someting went wrong');
				}

			redirect('/Event/event_notifications?event_id='.$this->input->post('event_id'));
    }

    function addresource(){
    	if(!empty($_FILES["upload_image"]["name"])){
    	$file = "";
		$file = $_FILES["upload_image"]["name"];
		$file = base_url() . "uploads/Resource_File/" . $file;
		$date = date('Y-m-d H:i:s');
		$type="file";

				$target_dir = "./uploads/Resource_File/";
				$target_file = $target_dir . basename($_FILES["upload_image"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

					if (move_uploaded_file($_FILES["upload_image"]["tmp_name"], $target_file)) {
						$this->session->set_flashdata('msg', 'Resources Added Successfully');
					}
					else {
						
						$this->session->set_flashdata('msg', 'Something went wrong');
						redirect('/Event/event_resources?event_id='.$this->input->post('event_id'));
						exit;
					}
			}
			else{
				$file = $this->input->post('resource_link');
				$type="link";
				if(!empty($file))
				{
					$this->session->set_flashdata('msg', 'Resources Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Please upload file or enter file link');
					redirect('/Event/event_resources?event_id='.$this->input->post('event_id'));
					exit;			
				}
			}
			$data = array(
				'title' => $this->input->post('title'),
				'resource' => $file,
				'resource_type' => $type,
				'event_id' => $this->input->post('event_id')
			);
			$query = $this->db->insert('event_resources', $data);
			$id = $this->db->insert_id();

			redirect('/Event/event_resources?event_id='.$this->input->post('event_id'));

    }
    	function delete_single_notification(){
    		$id = $this->input->get('noti_id');
	
			$this->db->where('id', $id);
			$data = $this->db->delete('event_notifications');
	
			if ($data) {
				$this->session->set_flashdata('msg', 'Notification Deleted Successfully');
			} else {
				$this->session->set_flashdata('msg', 'Something went wrong');
			}
			redirect('/Event/event_notifications?event_id='.$this->input->get('event_id'));
    	}

		function delete_notifications()
		{
	//        $this->output->enable_profiler(true);
			$ids = $this->input->post('ids');
	
			$this->db->where_in('id', $ids);
			$data = $this->db->delete('event_notifications');
	
			if ($data) {
				$this->session->set_flashdata('msg', 'Notification Deleted Successfully');
			} else {
				$this->session->set_flashdata('msg', 'Something went wrong');
			}
			redirect('/Event/event_notifications?event_id='.$this->input->get('event_id'));
		}

    function delete_file() {
        $file = $this->input->get('file'); 
        $id = $this->input->get('id'); 
        $type = $this->input->get('type'); 
        
        if($type=="file"){
        	$file = (explode("/",$file)); 
        	unlink('./uploads/Resource_File/'.$file[6]);
        }
        
        $this->db->where('id', $id);
        $data = $this->db->delete('event_resources');
        
        redirect('/Event/event_resources?event_id='.$this->input->get('event_id'));
    }

    function get_cities()
    {
        $country_id = $this->input->post('country_id');

        $country_data = $this->db->get_where('country', array('country_id' => $country_id))->result_array();
        $city_data = $this->db->get_where('cities', array('country' => $country_data[0]['country_code']))->result_array();


        if (!empty($city_data)) {
            echo json_encode(array('success' => 'yes', 'cities' => $city_data));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

    function edit()
    {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);

        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        $this->data['country'] = $this->Event_model->get_all_table('country');
        $this->data['announce'] =$this->Event_model->get_all($id);

        $roles = $this->Event_model->get_parent_child('member_role', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }
        
        $this->data['roles'] = $roles;
        $this->data['selected'] = array();
        $newString = $this->data['record_info'][0]['permission'];
        $test = explode(",",$newString);
        $this->data['test1'] = $test;
        
        /*echo "<pre>";
        print_r($this->data['record_info'][0]['permission']);
        echo "</pre>";
        exit;*/
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function view()
    {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];

        $id = $this->uri->segment(3);
        $this->data['record_info'] = $this->Event_model->get_all_table($this->table_name, $id);
        $this->data['likes'] = $this->Event_model->get_likes_records($id);
        //echo "<pre>"; print_r($this->data); exit;
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_display_view', $this->data);
        $this->load->view('admin/footer');
    }
	public function addtoString($str, $item) {
    $parts = explode(',', $str);
    $parts[] = $item;

    return implode(',', $parts);
    }
    function update_action(){
        $post = $this->input->post();
        
        $id = $post['event_id'];
        if(empty($post['permission'])){
            
            $newString = $post['previous-permission'];
        }
        else
        {
            $newString = implode(',', $post['permission']);
        }
        
        if($post['event_user_type'] == 2)
        {
        $data = array(
        'title'=>  $post['title'], 
        'speaker'=>  $post['speaker'],
		'start_date'=>  $post['start_date'] , 
        'end_date'=>  $post['end_date'], 
        'place'=>  $post['venue_address'],
		'fee'=>  $post['fee'] , 
        'permission'=>  "Public", 
        'agenda'=>  $post['agenda'],
		'upload_image'=>  $post['title'] , 
        'no_seats'=>  $post['no_seats'], 
        'venue'=>  $post['venue'],
		'venue_address'=>  $post['venue_address'] , 
        'country_id'=>  $post['country_id'], 
        'city_id'=>  0,
		'location'=>  $post['location'] , 
        'latLng'=>  $post['latLng'], 
        'status'=>  1,
		 'created_on'=> $post['title']  ,
		'updated_on'=>  $post['title'] , 
        'created_by'=>  $post['title'], 
        'updated_by'=>  $post['title'],
		'event_user_type'=>  $post['event_user_type'],
	
            );
         $this->db->where('event_id', $id);
        $query =  $this->db->update('event',$data);
        if($query)
        {  
            //echo "success";
            redirect('/event');
        }  
        
        }//end of event user type 2
        
        else
        {
            //$newString = implode(',', $post['permission']);
        //var_dump($newString);
        
         $data = array(
        'title'=>  $post['title'], 
        'speaker'=>  $post['speaker'],
		'start_date'=>  $post['start_date'] , 
        'end_date'=>  $post['end_date'], 
        'place'=>  $post['venue_address'],
		'fee'=>  $post['fee'] , 
        'permission'=>  $newString, 
        'agenda'=>  $post['agenda'],
		'upload_image'=>  $post['title'] , 
        'no_seats'=>  $post['no_seats'], 
        'venue'=>  $post['venue'],
		'venue_address'=>  $post['venue_address'] , 
        'country_id'=>  $post['country_id'], 
        'city_id'=>  0,
		'location'=>  $post['location'] , 
        'latLng'=>  $post['latLng'], 
        'status'=>  1,
		 'created_on'=> $post['title']  ,
		'updated_on'=>  $post['title'] , 
        'created_by'=>  $post['title'], 
        'updated_by'=>  $post['title'],
		'event_user_type'=>  $post['event_user_type'],
	
    );
        
        //$query = $this->db->insert('event', $data);
         $this->db->where('event_id', $id);
        $query =  $this->db->update('event',$data);
            if ($query){
                //echo  "success";
                redirect('/event');
                
            }
        
            
        }
    }

    function create_action() {
		$post = $this->input->post();
		
		if ($post['event_user_type'] == 2) {
			$data = array(
				'title' => $post['title'],
				'speaker' => $post['speaker'],
				'start_date' => $post['start_date'],
				'end_date' => $post['end_date'],
				'place' => $post['venue_address'],
				'fee' => $post['fee'],
				'permission' => "Public",
				'agenda' => $post['agenda'],
				'upload_image' => $post['title'],
				'no_seats' => $post['no_seats'],
				'venue' => $post['venue'],
				'venue_address' => $post['venue_address'],
				'country_id' => $post['country_id'],
				'city_id' => 0,
				'location' => $post['location'],
				'latLng' => $post['latLng'],
				'status' => 1,
				'created_on' => $post['title'],
				'updated_on' => $post['title'],
				'created_by' => $post['title'],
				'updated_by' => $post['title'],
				'event_user_type' => $post['event_user_type'],
			);
			$query = $this->db->insert('event', $data);
			if ($query) {
		
				// sending response
				// sending fcm notification to all members and non members
		
				$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Event has been added by WFDSA';
				// $click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$click_action = 'com.codiansoftapp.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				foreach($tokens as $key => $value) {
					$device_tokens = $value['device_token'];
					$send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				foreach($tokenz as $key => $value) {
					$device_tokens = $value['device_token'];
					$send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
			}
			
			$this->session->set_flashdata('msg', 'Event Added Successfully');
			redirect('/event');
		}
		else {
			$newString = implode(',', $post['permission']);
			$notification = $post['permission'];
		
			// var_dump($newString);
		
			$data = array(
				'title' => $post['title'],
				'speaker' => $post['speaker'],
				'start_date' => $post['start_date'],
				'end_date' => $post['end_date'],
				'place' => $post['venue_address'],
				'fee' => $post['fee'],
				'permission' => $newString,
				'agenda' => $post['agenda'],
				'upload_image' => $post['title'],
				'no_seats' => $post['no_seats'],
				'venue' => $post['venue'],
				'venue_address' => $post['venue_address'],
				'country_id' => $post['country_id'],
				'city_id' => 0,
				'location' => $post['location'],
				'latLng' => $post['latLng'],
				'status' => 1,
				'created_on' => $post['title'],
				'updated_on' => $post['title'],
				'created_by' => $post['title'],
				'updated_by' => $post['title'],
				'event_user_type' => $post['event_user_type'],
			);
			$query = $this->db->insert('event', $data);
			if ($query) {
		
				// sending response
				// sending fcm notification to all members
		
				$this->load->model('api_model');
				//$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Event has been added by WFDSA';
				// $click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$click_action = 'com.codiansoftapp.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				foreach ($notification as $value1)
				{
				    $tokenz = $this->api_model->selectByOtherCol_byrole('status', 1, 'member',$value1);
				    foreach($tokenz as $key => $value) 
				    {
					    $device_tokens = $value['device_token'];
					    $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				    }
				}

				
				$this->session->set_flashdata('msg', 'Event Added Successfully');
				redirect('/event');
			}
		
			// redirect('/controller/method');
			// base_url() . ucfirst($this->uri->segment(1))
			// $this->load->view(base_url() . ucfirst($this->uri->segment(1)) );
			// redirect(base_url().'event')
			// $this->load->view('Event',$data);
		
			/*
			$this->form_validation->set_rules('title', 'Title', 'required|trim');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('end_date', 'End Date', 'required');
			$this->form_validation->set_rules('fee', 'Fee', 'required|trim');
		
			// echo '<pre>';
			// print_r($post);
			// echo '</pre>';
			// exit();
		
			if($post['event_user_type'] == 1) {
		
			if (empty($post['permission']))
			$this->form_validation->set_rules('permission', 'Permission', 'required|trim');
			else
			$this->form_validation->set_rules('permission', 'Permission', 'callback_cf_check_default');
			}
		
			$this->form_validation->set_rules('agenda', 'Agenda', 'required|trim');
			$this->form_validation->set_rules('venue', 'Venue', 'required|trim');
			if ($this->form_validation->run() !== FALSE) {
			$flename = (!empty($_FILES)) ? $_FILES['upload_image']['name'] : NULL;
			($_FILES) ? $this->do_upload('upload_image') : FALSE;
			if (!empty($post['permission'])) {
			$post['permission'] = @serialize($post['permission']);
			}
		
			var_dump($post['permission[]']);
			$create_id = $this->Event_model->create_record($this->table_name, $post, $flename);
			var_dump($create_id);
		
			// exit();
		
			/*
			if ($create_id !== "") {
			echo "adad";
			} else {
			echo "no";
			echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
			}
			} else {
			echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
			}
		
			*/
		}

		function cf_check_default()
		{
			$choice = $this->input->post("permission");
			if (empty($choice)) {
				//$this->form_validation->set_message('permission', 'text dont match captcha');
				return false;
			}
	
			$travel_cat = implode(',', $choice);
	
			if ($travel_cat != '')
				return true;
			else
				return false;
		}
	
		function update_action()
		{
		   echo "test built in method";
			exit;
			$post = $this->input->post();
		   
		  /*  echo '<pre>';
			print_r($post);
			echo '</pre>';
			exit;
		  */  
			if (!empty($post['permission'])) {
				$post['permission'] = @serialize($post['permission']);
			}
			$file = ($_FILES) ? $_FILES['upload_image']['name'] : NULL;
			$update = $this->Event_model->update_record($this->table_name, $this->row_id, $post, $file, $post[$this->row_id]);
			$file_upload = (!empty($_FILES)) ? $this->do_upload('upload_image') : FALSE;
	
			if ($update) {
				echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
			} else {
				echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
			}
		}
	
		function delete()
		{
	//        $this->output->enable_profiler(true);
			$ids = $this->input->post('ids');
	
			$this->db->where_in($this->row_id, $ids);
			$data = $this->db->update($this->table_name, array('status' => 0));
	
			if ($data) {
				echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
			} else {
				echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
			}
		}
	
		function do_upload($image)
		{
			$config['upload_path'] = './uploads/Event_Images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$new_name = $_FILES["upload_image"]['name'];
			$config['file_name'] = time() . "_" . $new_name;
			//  $config['max_size']             = 100;
			//  $config['max_width']            = 1024;
			//  $config['max_height']           = 768;
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload($image)) {
				echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
			} else {
				$this->upload->data($image);
			}
		}
	
		function delete_individual()
		{
	
			$id = $this->input->post('id');
	
			$this->db->where($this->row_id, $id);
			$data = $this->db->update($this->table_name, array('status' => 0));
	
			if ($data) {
				echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
			} else {
				echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
			}
		}

	}
}
