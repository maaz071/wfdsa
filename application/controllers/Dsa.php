<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dsa extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dsa_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        
        
        
        $tableName = $this->table_name;
        $total_records = $this->get_record_total($tableName);
        $offset = ($this->uri->segment(2) == '') ? 0 : $this->uri->segment(2) - 1;
        if (isset($_GET['per_page'])) {
            $offset = $_GET['per_page'] - 1;
        }
        $limit = LISTING_LIMIT;
        if ($this->input->get('role_id')) {
            $pagerBaseUrl = base_url() . "dsa/?role_id=" . $this->input->get('role_id');
        } else {
            $pagerBaseUrl = base_url() . "dsa/";
        }


        $this->data['pagination'] = $this->getPagination($pagerBaseUrl, $total_records, $limit, array("enable_query_strings" => true, "page_query_string" => true));
        
        
        
        $this->data['record_list'] = $this->Dsa_model->get_dsa_member_record($this->table_name, $limit, $limit * $offset); 
        

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['countries'] = $this->Dsa_model->get_all_table('country');
        $this->data['region'] = $this->Dsa_model->get_all_table('region');
        $this->load->view('admin/header',  $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->data['countries'] = $this->Dsa_model->get_all_table('country');
         $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        $this->data['region'] = $this->Dsa_model->get_all_table('region'); 

        $this->data['admin_name'] = $admin_data[0]['name'];
        //echo "<pre>";
        //print_r($this->data['record_info']); exit;
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }
    
     

    function create_action() {
        $post = $this->input->post();
        /*echo "<pre>";
        print_r($post);
        exit;*/
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'required');
        $this->form_validation->set_rules('member_name', 'Member Name', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required'); 
        $this->form_validation->set_rules('fax', 'Fax', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('website', 'Website', 'required');
        $this->form_validation->set_rules('country_id', 'Country', 'required');
        //$this->form_validation->set_rules('company_logo', 'Company Logo', 'required');

        if ($this->form_validation->run() !== FALSE) {
            
            $C_logo = (!empty($_FILES['company_logo']['name'])) ? $_FILES['company_logo']['name'] : NULL; 
            $logo_path  =  './uploads/DSA_Members/companyLogo/';
            $logowidth  =  '1366';
            $logoheight =  '1280';
            
            if($C_logo)
            {
                $response = $this->do_upload('company_logo',$logo_path,$logowidth,$logoheight);
                if($response != "uploaded")
                {
                    $this->session->set_flashdata('error',$response);
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
            else{
                False;
            }
            
            $member_photo = (!empty($_FILES['member_photo']['name'])) ? $_FILES['member_photo']['name'] : NULL;
            $member_path  =  './uploads/DSA_Members/companyLogo/';
            $memberwidth  =  '1366';
            $memberheight =  '1280';
            
            ($member_photo) ? $this->do_upload('member_photo',$logo_path,$logowidth,$logoheight) : FALSE;
            $create_id = $this->Dsa_model->create_record($this->table_name, $post, $C_logo,$member_photo);


            if ($create_id !== "") {
                redirect('/dsa');
                //echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
                
            } else {
                redirect('/dsa');
                //echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            redirect('/dsa');
            //echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function update_action() {
        $post = $this->input->post();
        
        if(empty($_FILES['company_logo']['name'])){
           $image = $post['imageSrc'];
        }
        else
        {
            $file = $_FILES['company_logo']['name'];
            $image = base_url()."uploads/DSA_Members/companyLogo/".$file;
        
                // uploading image
		
				$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/DSA_Members/companyLogo/";
				$target_file = $target_dir . basename($_FILES["company_logo"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
		
				// Check if image file is a actual image or fake image
		
				/*if (isset($_POST["submit"])) {
					$check = getimagesize($_FILES["company_logo"]["tmp_name"]);
					if ($check !== false) {
						echo "File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
					}
					else {
						echo "File is not an image.";
						$uploadOk = 0;
					}
				}*/
		
				// Check file size
		/*
				if ($_FILES["fileToUpload"]["size"] > 500000) {
		
					// echo "Sorry, your file is too large.";
					// $uploadOk = 0;
		
				}*/
		
				// Allow certain file formats
		
				/*if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
		
					// echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					// $uploadOk = 0;
		
				}
		
				// Check if $uploadOk is set to 0 by an error
	                */	
				if ($uploadOk == 0) {
		
					// echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
		
				}
				else {
					if (move_uploaded_file($_FILES["company_logo"]["tmp_name"], $target_file)) {
		
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						/*$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						redirect('/Announcement');*/
					}
					else {
						//$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						//redirect('/Announcement');
		
						// echo "Sorry, there was an error uploading your file.";
		
					}
				}
				
        }
        
                
         /*$C_logo = (!empty($_FILES['company_logo']['name'])) ? $_FILES['company_logo']['name'] : $post['imageSrc']; 
         $logo_path  =  './uploads/DSA_Members/companyLogo/';
         $logowidth  =  '1366';
         $logoheight =  '1280';
            
         ($C_logo) ? $this->do_upload('company_logo',$logo_path,$logowidth,$logoheight) : FALSE;
            
         $member_photo = (!empty($_FILES['member_photo']['name'])) ? $_FILES['member_photo']['name'] : NULL;
         $member_path  =  './uploads/DSA_Members/';
         $memberwidth  =  '1366';
         $memberheight =  '1280';
         
         ($member_photo) ? $this->do_upload('member_photo',$member_path,$memberwidth,$memberheight) : FALSE;*/
         /*
        echo "<pre>";
        print_r($post['Dsa_id']);
        exit;*/
        
        $update = $this->Dsa_model->update_record($this->table_name, $this->row_id, $post, $image);
        

        if ($update) {
            redirect('/dsa');
            //echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            redirect('/dsa');
            //echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image,$logo_path,$logowidth,$logoheight) {
        $config['upload_path'] = $logo_path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $new_name = $_FILES[$image]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        $config['max_width']  = $logowidth;
        $config['max_height'] = $logoheight;
        unset($logowidth);
        unset($logoheight);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            // echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            // exit;
            return $this->upload->display_errors(); 
        } else {
            $this->upload->data($image);
            return 'uploaded';
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

}
