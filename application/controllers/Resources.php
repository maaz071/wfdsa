<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resources extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Resources_model');
        $this->load->model('Member_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->relation_table_name = "resource_categories";
        $this->data['row_id'] = $this->row_id;
    }

    
    function index() {
       
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $tableName = 'resources';
        $total_records = $this->get_record_total($tableName);
        //$offset = ($this->uri->segment(2) == '') ? 0 : $this->uri->segment(2) - 1;
        if (isset($_GET['per_page'])) {
            $offset = $_GET['per_page'] - 1;
        }else{
            $offset = 0;
        }
        $limit = LISTING_LIMIT;
        if ($this->input->get('role_id')) {
            $pagerBaseUrl = base_url() . "resources/index/1/?role_id=" . $this->input->get('role_id');
        } else {
            $pagerBaseUrl = base_url() . "resources/index/1";
        }


        $this->data['pagination'] = $this->getPagination($pagerBaseUrl, $total_records, $limit, array("enable_query_strings" => true, "page_query_string" => true));

        $this->data['record_list'] = $this->Resources_model->get_joint_records($this->table_name, $limit, $limit * $offset);
        /*
        echo "<pre>";
        print_r($this->data['record_list']);
        
        echo "</pre>";
        exit;*/
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];

        $this->load->model('Member_model');
        $categories = $this->Member_model->get_parent_child('categories', 0);
        
        if(!empty($categories)) {
            foreach ($categories as $key => $value) {
                $child = $this->Member_model->get_parent_child('categories', $value['categories_id']);
                $categories[$key]['child'] = $child;
            }
        }

        $this->data['categories'] = $categories;
        $this->load->model('Event_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }
        $events = $this->Event_model->get_all_table_events();
        $this->data['roles'] = $roles;
        $this->data['events'] = $events;
        $this->data['selected'] = array();
        /**/
        $test = array(" "," ");
        $this->data['test1'] = $test;
        
        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }
    
    function delete_file() {
        $file = $this->input->post('file'); 
        $id = $this->input->post('id'); 
        
        $file = (explode("/",$file)); 

        // unlink('/home/codianso/public_html/wfdsa/uploads/Resource_File/'.$file[6]);
        unlink('./uploads/Resource_File/'.$file[6]);
        
        $this->db->where('resources_file_id', $id);
        $data = $this->db->delete('resource_file');
        
        if($data) {
            echo json_encode(array('success' => 'yes'));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

    function edit() {
         
         
         $this->load->model('Member_model');
         $this->load->model('Resources_model');
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        
        
         $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        
        $this->data['file_data'] = $this->Resources_model->get_resource_data($id);
        
        $this->data['categories_select'] = $this->Resources_model->get_categories_select($this->relation_table_name, $id);
        $checkbox_select = array();
        if(!empty($this->data['categories_select'])) {
            foreach ($this->data['categories_select'] as $value) {
                $checkbox_select[] = $value['categories_id'];
            }
        }
        $this->data['selected'] = $checkbox_select;
        
       
        
        
   
       $this->data['announce'] =$this->Resources_model->get_all($id);
   
        $categories = $this->Member_model->get_parent_child('categories', 0);

        foreach ($categories as $key => $value) {
            $child = $this->Member_model->get_parent_child('categories', $value['categories_id']);
            $categories[$key]['child'] = $child;
        }

        $this->data['categories'] = $categories;
        $this->load->model('Event_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }
        $this->data['roles'] = $roles;
        $newString = $this->data['record_info'][0]['resource_member'];
        $test = explode(",",$newString);
        $this->data['test1'] = $test;
        
        /*echo "<pre>";
        print_r($test);
        echo "</pre>";
        exit;*/
        
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function add_categories() {
        $post = $this->input->post();
        $check = $this->Resources_model->create_record('categories', NULL ,$post);
        $option = "<option value='" . $check . "'>" . $post['name'] . "</option>";
        if ($check !== "") {
            echo json_encode(array('success' => 'yes', 'option' => $option));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

    function getcategories(){
    	 $roles = $this->Member_model->get_parent_child('categories', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('categories', $value['categories_id']);
            $roles[$key]['child'] = $child;
        }
        $html = '';
        foreach ($roles as $value) { 
        	$html .= '<option id="role_option_'.$value['categories_id'].'" disabled="disabled" value="'.$value['categories_id'].'"style="font-weight:bold; color:black;"> '.$value['name'].' </option>';

				if(!empty($value['child'])) {
					foreach ($value['child'] as $value2) {
						$html .= '<option id="role_option_'.$value2['categories_id'].'" value="'.$value2['categories_id'].'" > '.$value2['name'].' </option>';
                        }
					} 
                }
                echo $html;
                exit; 
    }
    function getcategories2(){
    	 $roles = $this->Member_model->get_parent_child('categories', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('categories', $value['categories_id']);
            $roles[$key]['child'] = $child;
        }
        $html = '';
        foreach ($roles as $value) { 
        	$html .= '<tr><td><b>'.$value['name'].'</b></td>
        		<td class="td-actions">
        			<a href="javascript:;" class="btn btn-danger btn-small delete_role" id="http://www.wfdsamobileportal.com/Resources/delete_categories/'.$value['categories_id'].'" data-id="'.$value['categories_id'].'"><i class="btn-icon-only icon-remove"> </i></a>
        		</td></tr>';

				if(!empty($value['child'])) {
					foreach ($value['child'] as $value2) {
						$html .= '<tr><td>'.$value2['name'].'</td>
        		<td class="td-actions">
        			<a href="javascript:;" class="btn btn-danger btn-small delete_role" id="http://www.wfdsamobileportal.com/Resources/delete_categories/'.$value2['categories_id'].'" data-id="'.$value2['categories_id'].'"><i class="btn-icon-only icon-remove"> </i></a>
        		</td></tr>';
                        }
					} 
                }
                echo $html;
                exit; 
    }
    function getcategories3(){
    	 $roles = $this->Member_model->get_parent_child('categories', 0);

        foreach ($roles as $key => $value) {
            $child = $this->Member_model->get_parent_child('categories', $value['categories_id']);
            $roles[$key]['child'] = $child;
        }
        $html = '<option value="0">-None-';
        foreach ($roles as $value) { 
        	$html .= '<option style="font-weight:bold;" value="'.$value['categories_id'].'">'.$value['name'].'</option>';

				if(!empty($value['child'])) {
					foreach ($value['child'] as $value2) {
						$html .= '<option disabled value="'.$value2['categories_id'].'">'.$value2['name'].'</option>';
                        }
					} 
                }
                echo $html;
                exit; 
    }
    function delete_categories() {
        $id = $this->uri->segment(3);

        $this->db->where('categories_id', $id);
        $data = $this->db->delete('categories');

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => '<strong> Record Deleted Successfully</strong>'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => '<strong> Some Bad had Happened Contact Developer</strong>'));
        }
    }

    function create_action() {

        $post = $this->input->post();
		$categories = $post['categories'][0];
		$file = "";
		$file = $_FILES["upload_file"]["name"];
		$file = base_url() . "uploads/Resource_File/" . $file;
		$date = date('Y-m-d H:i:s');

	
		
		// echo "<pre>";
		// print_r($post);
		// echo "</pre>";
		// exit;
		
		if ($post['event_user_type'] == 2) {
			$data = array(
				'title_2' => $post['title_2'],
				'upload_file' => $file,
				'resource_member' => "Public",
				'status' => 1,
				'created_on' => $date,
				'updated_on' => $post['title_2'],
				'created_by' => $post['title_2'],
				'updated_by' => $post['title_2']
			);
			$query = $this->db->insert('resources', $data);
			$id = $this->db->insert_id();
		
			// echo $id;
			// exit;
		
			$query1 = "INSERT INTO `resource_categories`(`resources_id`, `categories_id`) VALUES ('" . $id . "','" . $categories . "')";
			$this->db->query($query1);
			if ($query) {
		
				// echo "data inserted successfully";
				// exit;
				// sending response
				// sending fcm notification to all members and non members
		
				$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Resource has been uploaded by WFDSA';
				$click_action = 'com.codiansoftapp.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				foreach($tokens as $key => $value) {
					$device_tokens = $value['device_token'];
					// PUSH NOTIFICATION HAS BEEN BLOCKED
					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				foreach($tokenz as $key => $value) {
					$device_tokens = $value['device_token'];
					// PUSH NOTIFICATION HAS BEEN BLOCKED
					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				// uploading file
		
				$target_dir = "./uploads/Resource_File/";
				$target_file = $target_dir . basename($_FILES["upload_file"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
		
				// Check if image file is a actual image or fake image
				// Check file size
		
				if ($_FILES["upload_file"]["size"] > 500000) {
		
					// echo "Sorry, your file is too large.";
					// $uploadOk = 0;
		
				}
		
				// Allow certain file formats
				// Check if $uploadOk is set to 0 by an error
		
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded.";
		
					// if everything is ok, try to upload file
		
				}
				else {
					if (move_uploaded_file($_FILES["upload_file"]["tmp_name"], $target_file)) {
		
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						
						$this->session->set_flashdata('msg', 'Resources Added Successfully');
						redirect('/Resources');
					}
					else {
		
						// echo "Sorry, there was an error uploading your file.";
						
						$this->session->set_flashdata('msg', 'Resources Added Successfully');
		
					}
				}
			}
		
			// redirect('/Announcement/create/');
		
		}
		else {
			$newString = implode(',', $post['permission']);
		    $notification = $post['permission'];
			// echo "user type must be 1";
		
			$data = array(
				'title_2' => $post['title_2'],
				'upload_file' => $file,
				'resource_member' => "$newString",
				'status' => 1,
				'created_on' => $date,
				'updated_on' => $post['title_2'],
				'created_by' => $post['title_2'],
				'updated_by' => $post['title_2']
			);
			$query = $this->db->insert('resources', $data);
			$id = $this->db->insert_id();
		
			// echo $id;
			// exit;
		
			$query1 = "INSERT INTO `resource_categories`(`resources_id`, `categories_id`) VALUES ('" . $id . "','" . $categories . "')";
			$this->db->query($query1);
			if ($query) {
		
				// echo "data inserted successfully";
				// exit;
				// sending response
				// sending fcm notification to all members and non members
		
				$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Resource has been uploaded by WFDSA';
				$click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				/*foreach ($tokens as $key => $value) {
				$device_tokens = $value['device_token'];
				$send = $this->api_model->send_fcm($device_tokens,$msg,$click_action,$admin_id);
				}*/
				foreach ($notification as $value1)
				{
				    $tokenz = $this->api_model->selectByOtherCol_byrole('status', 1, 'member',$value1);
        				foreach($tokenz as $key => $value) {
        					$device_tokens = $value['device_token'];
        					// PUSH NOTIFICATION BLOCKED 21 AUGUST 2019
        					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				            }
				}            
		
				// uploading file
		
				$target_dir = "./uploads/Resource_File/";
				$target_file = $target_dir . basename($_FILES["upload_file"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
		
				// Check if image file is a actual image or fake image
				// Check file size
		
				if ($_FILES["upload_file"]["size"] > 500000) {
		
					// echo "Sorry, your file is too large.";
					// $uploadOk = 0;
		
				}
		
				// Allow certain file formats
				// Check if $uploadOk is set to 0 by an error
		
				if ($uploadOk == 0) {
		
					// echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
		
				}
				else {
					if (move_uploaded_file($_FILES["upload_file"]["tmp_name"], $target_file)) {
		
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						// echo "data inserted";
						
						$this->session->set_flashdata('msg', 'Resources Added Successfully');
		
						redirect('/Resources');
					}
					else {
		
						// echo "Sorry, there was an error uploading your file.";
						
						$this->session->set_flashdata('msg', 'Resources Added Successfully');
		
					}
				}
			}
		}
    }

    function update_action() {
        $post = $this->input->post();
        $date = date('Y-m-d H:i:s');
      
        
        $categories = $post['categories'][0];
        $file = $_FILES["upload_file"]["name"];
       
        
        if(!empty($file))
        {
            $file = base_url()."uploads/Resource_File/".$file;
        }
        else
        {
            $file= $post['fileSrc'];
        }
        if(empty($post['resources_id']))
        {
            $id = $post['Resources_id'];
        }
        else
        {
            $id= $post['resources_id'];
        }
                
        if($post['event_user_type']==2){
            
            $data = array(
           'title_2'=>  $post['title_2'],
            'upload_file'=> $file,
		  'resource_member'=>"Public",
		  'status'=>1,
		'created_on'=>  $date,
		'updated_on'=>  $post['title_2'], 
        'created_by'=>  $post['title_2'], 
        'updated_by'=>  $post['title_2']);
        
        $this->db->where('resources_id',$id);
        
        $query =  $this->db->update('resources', $data);
        
        
        
        
        if($query)
        {
            //echo "success query 1";
            //uploading file
            $target_dir =  "./uploads/Resource_File/";
            
            $target_file = $target_dir . basename($_FILES["upload_file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            
            // Check file size
            if ($_FILES["upload_file"]["size"] > 500000) {
               // echo "Sorry, your file is too large.";
                //$uploadOk = 0;
            }
            // Allow certain file formats
            
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["upload_file"]["tmp_name"], $target_file)) {
                    //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    //echo "data inserted";
                    
                } else {
                   // echo "Sorry, there was an error uploading your file.";
                }
            }
            $this->db->set('categories_id',$categories);
             $this->db->where('resources_id',$id);
            $query1 = $this->db->update('resource_categories');
            if($query1)
            {

            	$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Resource has been uploaded by WFDSA';
				$click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				/*foreach ($tokens as $key => $value) {
				$device_tokens = $value['device_token'];
				$send = $this->api_model->send_fcm($device_tokens,$msg,$click_action,$admin_id);
				}*/
				foreach ($notification as $value1)
				{
				    $tokenz = $this->api_model->selectByOtherCol_byrole('status', 1, 'member',$value1);
        				foreach($tokenz as $key => $value) {
        					$device_tokens = $value['device_token'];
        					// push notification blocked
        					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				            }
				}            
                redirect('/Resources');
                //echo "success query 2";    
            }
        }
        
        
            
        }//end of if condition of event user type
        
        else
        {
            $newString = implode(',', $post['permission']);
            
            //print_r($newString);
            //exit;
             $data = array(
           'title_2'=>  $post['title_2'],
            'upload_file'=> $file,
		  'resource_member'=>$newString,
		  'status'=>1,
		'created_on'=>  $date,
		'updated_on'=>  $post['title_2'], 
        'created_by'=>  $post['title_2'], 
        'updated_by'=>  $post['title_2']);
        
        $this->db->where('resources_id',$id);
        
        $query =  $this->db->update('resources', $data);
        
        
        
        
        if($query)
        {
            //echo "success query 1";
            //uploading file
            $target_dir =  "./uploads/Resource_File/";
            
            $target_file = $target_dir . basename($_FILES["upload_file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            
            // Check file size
            if ($_FILES["upload_file"]["size"] > 500000) {
               // echo "Sorry, your file is too large.";
                //$uploadOk = 0;
            }
            // Allow certain file formats
            
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["upload_file"]["tmp_name"], $target_file)) {
                    //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    //echo "data inserted";
                    
                } else {
                   // echo "Sorry, there was an error uploading your file.";
                }
            }
            $this->db->set('categories_id',$categories);
             $this->db->where('resources_id',$id);
            $query1 = $this->db->update('resource_categories');
            if($query1)
            {
            	$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Resource has been uploaded by WFDSA';
				$click_action = 'com.codiansoftapp.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				foreach($tokens as $key => $value) {
					$device_tokens = $value['device_token'];
					// push notificTION BLOCKED 27 AUGUST 2019
					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				foreach($tokenz as $key => $value) {
					$device_tokens = $value['device_token'];
					// PUSH NOTIFICaATION BLOCKED
					// $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
                redirect('/Resources');
            }
        }
        
        }
        
    }//end of update_action method pns rahat hospital faheem akhter

    function upload($primary_id) {
        $this->load->library('upload');
        
        $dataInfo = array();
        $files = $_FILES;
        $cpt = count($_FILES['upload_file']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES['upload_file']['name'] = $files['upload_file']['name'][$i];
            $_FILES['upload_file']['type'] = $files['upload_file']['type'][$i];
            $_FILES['upload_file']['tmp_name'] = $files['upload_file']['tmp_name'][$i];
            $_FILES['upload_file']['error'] = $files['upload_file']['error'][$i];
            $_FILES['upload_file']['size'] = $files['upload_file']['size'][$i];
            $new_name = $_FILES["upload_file"]['name'];
            $config['file_name'] = time() . "_" . $new_name;
            $ext = pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION);
            $this->upload->initialize($this->set_upload_options($config['file_name']));
            $this->Resources_model->create_resource_files($config['file_name'], $primary_id);
            $this->upload->do_upload('upload_file');
            $dataInfo[] = $this->upload->data('upload_file');
        } 
       
    }
    
      function do_upload($image) {
        $config['upload_path'] = './uploads/Resource_File/';
        $config['allowed_types'] = 'pdf|txt|docx|doc|pptx|xlsx|ppt';
        $new_name = $_FILES["upload_file"]['name']; 
        $config['file_name'] = time()."_".$new_name; 
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }
    
    public function set_upload_options($file_name) {
        
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/Resource_File/';
        $config['allowed_types'] = 'pdf|txt|docx|doc|xlsx|pptx|ppt';
        $config['max_size'] = '10000';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $file_name;
        return $config;
    }
}
