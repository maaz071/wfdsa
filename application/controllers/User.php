<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];

        
        $tableName = "user";
        $total_records = $this->get_record_total($tableName);
        $offset = ($this->uri->segment(2) == '') ? 0 : $this->uri->segment(2) - 1;
        $limit = LISTING_LIMIT;
        $pagerBaseUrl = base_url() . "user/";

        $this->data['pagination'] = $this->getPagination($pagerBaseUrl,$total_records,$limit);


        $this->data['record_list'] = $this->User_model->get_record($limit,$offset*$limit);

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['roles'] = $this->User_model->get_all_table('role');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view',$this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->data['roles'] = $this->User_model->get_all_table('role');
        
        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);


        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post();
       
        $this->form_validation->set_rules('name', 'Name', 'required');
        // $this->form_validation->set_rules('role_id', 'Role', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[' . $this->table_name . '.email]');
        // $this->form_validation->set_message('valid_email', 'Invalid Email Address');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[' . $this->table_name . '.username]');
        $this->form_validation->set_message('is_unique[' . $this->table_name . '.username]', 'Username Already Exsist');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        
         if($post['password'] != $post['retype_password']) {
                echo json_encode(array('success' => 'no', 'msg' => '<strong> Password Must be Same.</strong>'));
                exit;
          } 

        if ($this->form_validation->run() !== FALSE) {


            $create_id = $this->User_model->create_record($this->table_name, $post);
             
    
            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function update_action() {
        $post = $this->input->post();
        
        if(isset($post['password']) && $post['password'] != $post['retype_password']) {
            echo json_encode(array('success' => 'no', 'msg' => '<strong> Password Must be Same.</strong>'));
            exit;
        }
        
        $file = ($_FILES) ? $_FILES['upload_image']['name'] : NULL;
        $update = $this->User_model->update_record($this->table_name, $this->row_id, $post, $file, $post[$this->row_id]);
        $file_upload = (!empty($_FILES)) ? $this->do_upload('upload_image') : FALSE;

        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->delete($this->table_name);

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Event_Images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
        //  $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');  
    
        $this->db->where($this->row_id, $id);
        $data = $this->db->delete($this->table_name); 
        

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

}
