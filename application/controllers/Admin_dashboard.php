<?php
 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        $this->table = lcfirst(__CLASS__);
        $this->row_id = lcfirst(__CLASS__)."_id";
        $this->data['row_id'] = $this->row_id;
        $this->load->model('Admin_dashboard_model');
    }
    
    
    
    function index() { 
        
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['number_user'] = $this->Admin_dashboard_model->get_user_counts();
        $this->data['events'] = $this->Admin_dashboard_model->get_all_table_events('event','start_date',3);
        //print_r($this->data['events']);
        //exit();
        $this->data['announcement'] = $this->Admin_dashboard_model->get_all_table('announcement','announcement_id',3);
        $this->data['resources'] = $this->Admin_dashboard_model->get_all_table('resources','resources_id',3);
        
        //echo "<pre>";
        //print_r($this->data['events']);
        //exit;
        //list($date,$time) = explode(' ',$this->data['announcement'][0]['date']);
        /*
        echo "<br>";
        echo $time;
        echo "<br>";
        list($month,$day,$year) = explode('-',$date);
        echo $year;
        //$date = date('m-d-Y', strtotime($date));
        echo "<br>";
        echo $date;
        exit;*/
        if($this->session->userdata('admin_data') == TRUE ) {
            $this->load->view('admin/header',$this->data);
            $this->load->view('admin/admin_dashboard_view',$this->data);
            $this->load->view('admin/footer');
        } else {
            $this->load->view('admin/login');
        }  
      
    }
    
    

     

}
