<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Login_model');
    }

    function redirect_wfdsa()
    {
        header("Location: https://wfdsa.org/");
        exit;
    }

    function updatepassword()
    {
        $email = $this->input->get();
       // $admin_data = $this->session->userdata('admin_data'); 
        $data['admin_name'] = $email['email'];
        
        $this->load->view('admin/update_password_view',$data); 
    }

    function update_password_action(){

        $post = $this->input->post();
        $password = $post['password'];
        $email = $post['email'];
        $hashemail = base64_decode($post['hashemail']);

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');        
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        if($hashemail != $email)
        {
            echo json_encode(array('success' => 'no', 'msg' => "Email not matched"));
            exit;
        }

        if ($this->form_validation->run() !== FALSE) {

            $user_record = $this->db->get_where('non_member', array('status' => 1,'email'=>$email))->result_array();
            if(!empty($user_record)) {
                $this->db->where('non_member_id', $user_record[0]['non_member_id']); 
                $upd_check = $this->db->update('non_member', array('password'=>md5($password)));  
                if($upd_check) { 
                        echo json_encode(array('success' => 'yes', 'msg' => "Your password has been changed Successful", 'redirect' => 'login/redirect_wfdsa'));
                        exit; 
                    }
            }
            else
            {

                $user_record = $this->db->get_where('member', array('status' => 1,'email'=>$email))->result_array();
                if(!empty($user_record)) {            
                    $this->db->where('member_id', $user_record[0]['member_id']); 
                    $upd_check = $this->db->update('member', array('password'=>base64_encode($password)));
                    if($upd_check) { 
                        echo json_encode(array('success' => 'yes', 'msg' => "Your password has been changed Successful", 'redirect' => 'login/redirect_wfdsa')); 
                        exit;
                    }
                }
                echo json_encode(array('success' => 'no', 'msg' => "No record found")); 
            }
        } 
        else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function index() {
	    
	    if(!empty($this->session->userdata('admin_data'))) {
			redirect('welcome_admin');  
			exit;
		}
		$this->load->view('admin/login');
    }
     

    function login_check() {
     
        $post = $this->input->post();
		 
 
        $this->form_validation->set_rules('username', 'Usernmae', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() !== FALSE) {
		 
		 $id_admin = $this->Login_model->check_admin_login($post);
		
		 if($id_admin) { 
		 
			echo json_encode(array('success' => 'yes', 'msg' => "Login Successful", 'redirect' => 'admin_dashboard')); 
			
			$this->db->from('user'); 
			$this->db->where('user_id', $id_admin);
			$query = $this->db->get();
			$admin_data = $query->result_array();			
			$this->session->set_userdata('admin_data', $admin_data);
			exit;
		 } else {
			echo json_encode(array('success' => 'no', 'msg' => "Wrong Username or Password."));  exit;
		 } 
         
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    

    function logout_admin() {
        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
        $this->session->sess_destroy();
        redirect('login');
    }
    
      function change_password() {
        $admin_data = $this->session->userdata('admin_data'); 
        $data['admin_name'] = $admin_data[0]['name'];
        
        $this->load->view('admin/header', $data);
        $this->load->view('admin/change_password_view',$data); 
        $this->load->view('admin/footer');
    }
    
    function forget_password() {
        $this->load->view('admin/forget_password_view');
    }
    
    function email_new_password() {
        
        $username = $this->input->post('username');
        $user_record = $this->db->get_where('user', array('status' => 1,'username'=>$username))->result_array();
       
        if(!empty($user_record)) {
            $random_password = time()."_".rand(0,9999); 
            $this->db->where('user_id', $user_record[0]['user_id']); 
            $upd_check = $this->db->update('user', array('password'=>base64_encode(md5($random_password))));  
            
            $this->email->from('masteremail323@gmail.com', 'Password Reset - WFDSA');
            $this->email->to('masteremail323@gmail.com');
            $this->email->subject('Password Reset - WFDSA');
            $this->email->message("Dear ".$user_record[0]['name'].",\r\n \r\n This is your New Password ".$random_password." \r\n \r\n Thank You  \r\n Regards WFDSA");
            $this->email->send();
            
            if($upd_check) {
                 echo json_encode(array('success' => 'yes', 'msg' => 'Password send at '. $user_record[0]['email'],'redirect'=>'admin_dashboard'));
                 exit;
            } 
             
            
        } else {
            echo json_encode(array('success' => 'no', 'msg' => "<strong>Username Doesn't Exist</strong>"));
              exit;
              
        }
        

       
        
    }
    
    function change_password_action() {
        $post = $this->input->post();
        $attempt = $this->Login_model->insert_login_attempt($post); 
         
        
        $this->form_validation->set_rules('old_password', 'Old Password', 'required|trim');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|trim');
        $this->form_validation->set_rules('retype_password', 'Retype Password', 'required|trim'); 
        
        if ($this->form_validation->run() != FALSE) {  
            
            $check_password = $this->Login_model->verify_password($post); 
            
            if(empty($check_password)) {
                if($attempt == 3) { 
                    $user_data = $this->session->all_userdata();
                    $this->session->unset_userdata($key);
                    $this->session->sess_destroy(); 
                    echo json_encode(array('success' => 'yes', 'msg' => '<strong>Your Session Have Expired</strong>','redirect' => 'login'));
                    exit;
                }  
                echo json_encode(array('success' => 'no', 'msg' => '<strong>Wrong Old Password.</strong>','redirect'=>''));
                exit;
            } 
            
            if($post['new_password'] != $post['retype_password']) {
                echo json_encode(array('success' => 'no', 'msg' => '<strong>New and Retype Password and Must be Same.</strong>','redirect'=>''));
                exit;
            }
            
            
            $change_password = $this->Login_model->change_password($post);   
            echo json_encode(array('success' => 'yes', 'msg' => 'Password Changed Successfully','redirect'=>'admin_dashboard'));
            exit;
            
        } else {
           echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
           exit;
        }  
        
    }       

}
