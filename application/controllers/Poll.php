<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Poll extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Poll_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->relation_table_name = 'poll_answer';
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $this->data['event_id'] = $this->input->get('event_id');
        
        if($this->data['event_id']) {
            $event_record = $this->Poll_model->get_all_table('event',$this->data['event_id']);
            $this->data['event_name'] = $event_record[0]['title']; 
        }
        
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['record_list'] = $this->Poll_model->get_relation_records($this->data['event_id']);
        
        if(!empty($this->data['record_list'])) {
            foreach( $this->data['record_list'] as $value ) {
                $this->data['poll_answer'][$value[$this->row_id]] = $this->Poll_model->get_poll_answer($this->relation_table_name,$this->row_id,$value[$this->row_id]);
            }
        } 
       
        $this->data['events'] = $this->Poll_model->get_all_table('event');
        //echo "<pre>" ; print_r($this->data['event_id']); exit;
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['event_id'] = $this->input->get('event_id');
        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_table_record($this->table_name,$this->data['event_id'] );
        $this->data['event'] = $this->Poll_model->get_all_table('event');
        
        $data['admin_name'] = $admin_data[0]['name'];
        $data['event'] = $this->Poll_model->get_all_table('event');
        //$this->data['record_info'] = $this->data['record_info'][0]['event_id'];
        //echo "<pre>";
        //print_r($this->data['record_info'][0]['event_id']); exit;
        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view',$this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        
        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
        $this->data['event'] = $this->Poll_model->get_all_table('event');
        $this->data['poll_answer'] = $this->Poll_model->get_relation_table($this->relation_table_name, $id, $this->table_name);

        $this->data['admin_name'] = $admin_data[0]['name'];
        
        /*echo "<pre>";
        print_r($this->data);
        exit;*/
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }
    
    function poll_result() {
        $poll_id = $this->uri->segment(3);
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name']; 
        
        $this->data['polls_answer_name'] = $this->Poll_model->get_poll_answer_name($this->relation_table_name,$poll_id);
        $this->data['poll_answers'] = $this->Poll_model->get_poll_result($poll_id);
        
         
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_result_view', $this->data);
        $this->load->view('admin/footer');
        
    }

    function create_action() {
        $post = $this->input->post();

        $this->form_validation->set_rules('poll_question', 'Poll Question', 'required|trim');
        $this->form_validation->set_rules('event_id', 'Event', 'required|trim');
        $this->form_validation->set_rules('choice_selection', 'Choice Selection', 'required|trim');  


        if ($this->form_validation->run() !== FALSE) { 
            
             
            $create_id = $this->Poll_model->create_record($this->table_name, $post);
            $ANSWER = $this->Poll_model->create_poll_answer($this->relation_table_name, $create_id, $post);
            $event_id= $post['event_id'];


            if ($create_id !== "" && $ANSWER == TRUE) {
                //redirect('/poll/');
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $event_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function update_action() {
        $post = $this->input->post(); 
        
        $update = $this->Poll_model->update_record($this->table_name, $this->row_id, $post, $post[$this->row_id]);
        $ANSWER = $this->Poll_model->create_poll_answer($this->relation_table_name, $post[$this->row_id], $post, $this->row_id);
        

        if ($update && $ANSWER == TRUE) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Announcement_Images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
        //  $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_relation_record() {
//         $this->output->enable_profiler(true);
        $id = $this->input->post('id');
        $del = $this->Poll_model->delete_record($this->relation_table_name, $id);
        if ($del) {
            echo json_encode(array('success' => 'yes'));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

}
