<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Roster extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Roster_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__); 
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['parent_roles'] = $this->Roster_model->get_parent_role();
        
        if($this->input->get('role_id')){
            $role= $this->input->get('role_id');
            $pagerBaseUrl = base_url() . "roster/?role_id=".$this->input->get('role_id');
            }
        $option = array();
        
        foreach($this->data['parent_roles'] as $value) {
            
            $option[]= '<option value="'.$value['member_role_id'].'" style="font-weight:bold;" disabled="disabled">'.$value['name'].'</option>';
            $child = $this->Roster_model->get_parent_child('member_role', $value['member_role_id']);
            
            if(!empty($child)) {  
               foreach($child as $key => $value2) {
                    $option[]= '<option value="'.$value2['name'].'">&nbsp;&nbsp;'.$value2['name'].'</option>';
               }
            }
        }
        
        $this->data['option'] =  $option;
        
        $this->data['record_list'] = $this->Roster_model->get_all_table($this->table_name);
        /*echo "<pre>";
        print_r($this->data['option'][0]);
        echo "</pre>";
        exit;*/
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }
    
    function get_role_member() {
        $id = $this->input->post('role_id');
        //print_r($id);
        //exit;
        $data =  $this->Roster_model->get_memberByrole($id);
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/

        
        if(!empty($data)) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Members List is generating', 'record' => $data));
            
        } else {

            echo json_encode(array('success' => 'no', 'msg' => 'Members Not Found'));
        }
        
        
    }
    
    function get_role_members() {
        $id = $this->input->get('role_id');
        //print_r($id);
        $data =  $this->Roster_model->get_memberByroles($id);
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/
            


        if(!empty($data)) {
            $response['result']['status']       = 'success';
            $response['result']['response']     = $data;
            // echo json_encode(array('success' => 'yes', 'msg' => 'Members List is generating', 'record' => $data));
        } else {
            // echo json_encode(array('success' => 'no', 'msg' => 'Members Not Found'));
            $response['result']['status']       = 'error';
            $response['result']['response']     = "Members Not Found";
        }

        echo json_encode($response);
                die();
        
    }
    
    function update_row() {
        $sort = $this->input->post('sort');
        
        $count = 1;
        foreach($sort as $key=>$value) {
             $this->db->where('member_select_role_id', $value);
             $this->db->update('member_select_role', array('sort_order'=>$count));
             $count++;
        }
        $data['success'] = 'yes';
        return $data;exit;  
        
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');

        $data['admin_name'] = $admin_data[0]['name'];
        $data['event'] = $this->Roster_model->get_all_table('event');
        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view');
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);

        $this->data['record_info'] = $this->Roster_model->get_all_table($this->table_name, $id);
        $this->data['event'] = $this->Roster_model->get_all_table('event');
        $this->data['poll_answer'] = $this->Roster_model->get_relation_table($this->relation_table_name, $id, $this->table_name);

        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post(); 

        
             
            $create_id = $this->Roster_model->create_record($this->table_name, $post);
           


            if ($create_id !== "" && $ANSWER == TRUE) {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        
    }

    function update_action() {
        $post = $this->input->post(); 
        
        $update = $this->Roster_model->update_record($this->table_name, $this->row_id, $post, $post[$this->row_id]);
        $ANSWER = $this->Roster_model->create_poll_answer($this->relation_table_name, $post[$this->row_id], $post, $this->row_id);
        

        if ($update && $ANSWER == TRUE) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Announcement_Images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
        //  $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_relation_record() {
//         $this->output->enable_profiler(true);
        $id = $this->input->post('id');
        $del = $this->Roster_model->delete_record($this->relation_table_name, $id);
        if ($del) {
            echo json_encode(array('success' => 'yes'));
        } else {
            echo json_encode(array('success' => 'no'));
        }
    }

}
