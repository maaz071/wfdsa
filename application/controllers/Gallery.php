<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Api_model');         
        $this->load->model('Gallery_model');         
    }

    public function index()
    {
    	$admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        // $this->data['record_list'] = $this->Guide_model->get_all_table($this->table_name);
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/gallerymain', $this->data);
        $this->load->view('admin/footer');
    }

    public function videoalbum()
    {
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];

        $this->load->model('Event_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $data['roles'] = $roles;
        
        
        // $this->data['record_list'] = $this->Guide_model->get_all_table($this->table_name);
        $data['videoalbums'] = $this->Gallery_model->getalbums('video');
        $this->load->view('admin/header', $data);
        $this->load->view('admin/videoalbums', $data);
        $this->load->view('admin/footer');   
    }


    public function photoalbum()
    {
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->model('Event_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $data['roles'] = $roles;
        $data['photoalbums'] = $this->Gallery_model->getalbums('image');
        $this->load->view('admin/header', $data);
        $this->load->view('admin/photoalbums', $data);
        $this->load->view('admin/footer');   
    }

    public function selectalbum($argument)
    {
        $arguments = explode('-', $argument);
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];

        $data['items'] = $this->Gallery_model->getgal($arguments[0]);
        $data['type'] = $arguments[1];
        $data['album_id'] = $arguments[0];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/gallery', $data);
        $this->load->view('admin/footer'); 
    }

    public function createalbum()
    {
        $post = $this->input->post();
        $filetouploadname = str_replace(' ', '', $_FILES["fileToUpload"]["name"]);
        $fileupload = time().'_'.$filetouploadname;
        $file = base_url() . "uploads/gallery/" . $fileupload;

        if($_FILES["fileToUpload"]['size'] == 0)
        {
             $this->session->set_flashdata('fail', 'choose file to upload');
                redirect($_SERVER['HTTP_REFERER']);
        }
        
        $newString = implode(',', $post['permission']);
            $notification = $post['permission'];
        
        if(in_array("Public", $notification)){
                $temp = 'Public';
                }
                else{
                $temp = $newString;
                }
                
            $data = array(
                'album_title' => $post['title'],                
                'album_cover' => $file,
                'album_permission' => $temp,
                'album_type' => $post['album_type'],
                'status' => 1,
            );
            $query = $this->db->insert('album', $data);
            if ($query) {
                 $new_name = $filetouploadname;
        $config['upload_path'] = './uploads/gallery/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['file_name'] = $fileupload;
    $config['overwrite'] = TRUE;
    $config['encrypt_name'] = FALSE;
    $config['remove_spaces'] = TRUE;
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('fileToUpload')) {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata('fail', $error);
                redirect($_SERVER['HTTP_REFERER']);
    }
                // $target_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/gallery/";
                // $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                // move_uploaded_file($file, $target_file);
                $this->session->set_flashdata('success', 'New Album created');
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $this->session->set_flashdata('fail', 'Something went wrong');
                redirect($_SERVER['HTTP_REFERER']);
            }
    }

    public function delalbum($id)
    {
        $data = array(
            'status'=>0
        );
        $this->db->where('album_id', $id);
        $query =  $this->db->update('album',$data);
        $this->session->set_flashdata('success', 'Album removed');
                redirect($_SERVER['HTTP_REFERER']);
    }

    public function delitem($id)
    {
        $data = array(
            'status'=>0
        );
        $this->db->where('gallery_id', $id);
        $this->db->delete('gallery'); 
        // $query =  $this->db->update('gallery',$data);
        $this->session->set_flashdata('success', 'item removed');
                redirect($_SERVER['HTTP_REFERER']);
    }

    public function additem()
    {
        $post = $this->input->post();
        if($post['type']=="video")
        {
            $type=1;
        }
        else
        {
            $type=0;
        }
        if($_FILES["fileToUpload"]['size'] != 0)
        {
            $filetouploadname = str_replace(' ', '', $_FILES["fileToUpload"]["name"]);
            $fileupload = time().'_'.$filetouploadname;
            $uploadlink = base_url() . "uploads/gallery/" . $fileupload;
            $config['upload_path'] = './uploads/gallery/';
            if($type==0)
            {
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            }
            else
            {
             $config['allowed_types'] = 'mp4|webm|avi|flv';   
            }
            $config['file_name'] = $fileupload;
            $config['overwrite'] = TRUE;
            $config['encrypt_name'] = FALSE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('fileToUpload')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('fail', $error);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else
        {
            if($type != 1)
            {
                $this->session->set_flashdata('fail', 'choose file to upload');
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
            $uploadlink = $post['upload_link'];
            if(empty($uploadlink))
            {
                $this->session->set_flashdata('fail', 'Enter video url');
                redirect($_SERVER['HTTP_REFERER']);
            }
            if(strpos($uploadlink, 'youtu') !== false && strpos($uploadlink, 'embed') === false){
                $type = 2;
                preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $uploadlink, $code);
                $uploadlink = '<iframe width="100%" height="230px" src="https://www.youtube.com/embed/'.$code[0].'"
                frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
            }
            elseif (strpos($uploadlink, 'youtube') !== false && strpos($uploadlink, 'embed') !== false) {
                $type = 2;
                $newWidth = '100%';
                $newHeight = '230px';

$uploadlink = preg_replace(
   array('/width="\d+"/i', '/height="\d+"/i'),
   array(sprintf('width="%s"', $newWidth), sprintf('height="%s"', $newHeight)),
   $uploadlink);
            }
            if(strpos($uploadlink, 'vimeo') !== false)
            {
             $type = 3;   
             $uploadlink = preg_replace(
   array('/width="\d+"/i', '/height="\d+"/i'),
   array(sprintf('width="%s"', $newWidth), sprintf('height="%s"', $newHeight)),
   $uploadlink);
            }
                }
        }
        
            // print_r($newString);
            // exit;
            $data = array(
                'album_id' => $post['album_id'],                
                'type' => $type,
                'upload_link' => $uploadlink,
                'status' => 1,
            );
            $query = $this->db->insert('gallery', $data);
            if ($query) {
                $this->session->set_flashdata('success', 'Item added successfully');
                redirect($_SERVER['HTTP_REFERER']);
            }
            else
            {
                $this->session->set_flashdata('fail', 'Something went wrong');
                redirect($_SERVER['HTTP_REFERER']);
            }
    }
}
