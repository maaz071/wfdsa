<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Roles_admin_model');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];
        $data['roles_list'] = $this->Roles_admin_model->get_roles();

        $this->load->view('admin/header', $data);
        $this->load->view('admin/roles_admin_view');
        $this->load->view('admin/footer');
    }

    function create_roles() {
        $admin_data = $this->session->userdata('admin_data');
        
        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/roles_admin_form_view');
        $this->load->view('admin/footer');
    }
    
    function edit_roles() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        
        $data['record_info'] = $this->Roles_admin_model->get_role_record($id);
        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/roles_admin_form_view');
        $this->load->view('admin/footer');
    }

    function create_action() { 
      $post = $this->input->post();
      
      $this->form_validation->set_rules('admin_Role_Name', 'Role Name', 'required|is_unique[admin_roles.admin_Role_Name]');
      $this->form_validation->set_rules('admin_Role_Desc', 'Description', 'required');
      

        if ($this->form_validation->run() !== FALSE) {   
            $create_id = $this->Roles_admin_model->create_record($post);  

            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    } 
    
      function update_action() {
        $post = $this->input->post();

        //   $this->form_validation->set_rules('pname', 'Product Name', 'required|is_unique[products.pname]');
        //  $this->form_validation->set_rules('category', 'Category', 'required');
        // $this->form_validation->set_rules('pavailability', 'Product Availability', 'required');
        //  $this->form_validation->set_rules('pprice', 'Price', 'required');
        // $this->form_validation->set_rules('pimage', 'Product Image', 'required');
//         $this->form_validation->error_delimeters('<strong>','</strong>');
        // if ($this->form_validation->run() !== FALSE) {
       
        $update = $this->Roles_admin_model->update_record($post,$post['roles_id']); 
        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
        //  } else {
        //      echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        //  }
    }
    
       function delete() {

        $ids = $this->input->post('ids');


        $this->db->where_in('roles_id', $ids);
        $data = $this->db->update('roles_admin', array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where('roles_id', $id);
        $data = $this->db->update('roles_admin', array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

     

}
