<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_verification extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Country_model');
        $this->load->model('api_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function index() {
		$non_member_id = $this->input->get('key_id');
		$hash = $this->input->get('hash');
		
	    $data = $this->db->get_where('non_member',array('non_member_id'=>$non_member_id,'email_hash'=>$hash))->result_array();
	    
	    if(!empty($data)) {
            $upd_data = array('status'=>1);
            $this->api_model->updateByOtherCol('non_member_id',$non_member_id, $upd_data , 'non_member');


            $edata=array();

        $edata['email']=array([
            'heading' => 'New User Registration',
            'firstname' => $data[0]['first_name'],
            'lastname' => $data[0]['last_name'],
            'text'=> 'Thankyou for registration!! Your email successfully Verified.',
            'link' => ''
            
        ]);
        $emailbody = $this->load->view('template/email',$edata,true);

        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->email->initialize($config);
	       $this->email->set_mailtype("html");
	        $this->email->from('masteremail323@gmail.com', 'Email Verified - WFDSA');
            $this->email->to($data[0]['email']);
            $this->email->subject('Email Verified - WFDSA');
            $this->email->message($emailbody);
            $this->email->send();
	        
	    } 

        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

//do something with this information
if( $iPod || $iPhone || $iPad){
    header("Location: https://itunes.apple.com/us/app/wfdsa/id1422309391?mt=8");
}else if($Android){
               header("Location: https://play.google.com/store/apps/details?id=com.simplicore.app.wfdsa");
}else{
           header("Location: http://wfdsa.org");
}
    }


     

}
