<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guide extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Guide_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['record_list'] = $this->Guide_model->get_all_table($this->table_name);

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');

        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->model('Poll_model');
        $this->data['event'] = $this->Poll_model->get_all_table('event');

        $this->load->view('admin/header', $data);
        //$this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/videotest', $data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);

        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
     
        $this->data['admin_name'] = $admin_data[0]['name'];

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }




    function create_action() {
        $post = $this->input->post();

        




        $this->form_validation->set_rules('title', 'Title', 'required|trim|is_unique[' . $this->table_name . '.title]');
        // $this->form_validation->set_rules('description', 'Description', 'required|trim');
        $upload_video = $this->input->post('upload_video');



        if ($this->form_validation->run() !== FALSE) {

            $flename = (!empty($_FILES)) ? $_FILES['upload_video']['name'] : NULL;
            ($_FILES) ? $this->do_upload('upload_video') : FALSE;
            $create_id = $this->Guide_model->create_record($this->table_name, $post, $flename);

            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/User_Guide/';
        $config['allowed_types'] = array('mp4','webm');
        $new_name = $_FILES["upload_video"]['name'];
        $config['file_name'] = "test"."_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
        //  $config['max_height']           = 768;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function update_action() {
        $post = $this->input->post();

        $file = ($_FILES) ? $_FILES['upload_video']['name'] : NULL;        
        $update = $this->Guide_model->update_record($this->table_name, $this->row_id, $post, $file, $post[$this->row_id]);
        $file_upload = (!empty($_FILES)) ? $this->do_upload('upload_video') : FALSE;

        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

}
