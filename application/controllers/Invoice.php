<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Invoice_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->table_relationship = 'invoice_item';
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
         
        $this->data['record_list'] = $this->Invoice_model->get_joined_record($this->table_name);
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data'); 
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['member'] = $this->Invoice_model->get_all_table('member');
        $this->data['event'] = $this->Invoice_model->get_all_table('event');
        $this->load->view('admin/header',  $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);

        $this->data['record_info'] = $this->Invoice_model->get_all_table($this->table_name, $id);
        $this->data['member'] = $this->Invoice_model->get_all_table('member');
        $this->data['event'] = $this->Invoice_model->get_all_table('event');
        $this->data['invoice_item'] = $this->Invoice_model->get_invoice_item($this->table_relationship, $id);
       
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        
        $post = $this->input->post();  
        //print_r($post);
        //exit;
        
        $this->form_validation->set_rules('member_id', 'Member Name', 'required'); 
        $this->form_validation->set_rules('title', 'Title', 'required|trim'); 
        $this->form_validation->set_rules('description', 'Description', 'required|trim');

        if ($this->form_validation->run() !== FALSE) {
          
            $create_id = $this->Invoice_model->create_record($this->table_name, $post);
            $item_multiple = $this->Invoice_model->create_multi_item($this->table_relationship,$create_id, $post,FALSE);
            
            $post[$this->row_id] = $create_id;
            if(isset($post['payment_status']) && $post['payment_status'] == '1') {
                $this->payment_insert($post);
            }
           
            if ($create_id !== "" && $item_multiple == TRUE) {
                redirect('/invoice/create/');
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
                
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function update_action() {
        
        $post = $this->input->post(); 
        
       
        $update = $this->Invoice_model->update_record($this->table_name, $this->row_id, $post, $post[$this->row_id]);
        $item_multiple = $this->Invoice_model->create_multi_item($this->table_relationship,$post[$this->row_id], $post,$this->row_id);
        
        if($post['payment_status'] == 'Paid') {
            
            $this->payment_insert($post);
        }
        
        if ($update && $item_multiple) {
            echo json_encode(array('success' => 'yes', 'msg' => '<strong>Record Updated SuccessFully.</strong>'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Country_Flags/';
        $config['allowed_types'] = 'gif|jpg|png';
        $new_name = $_FILES["flag_pic"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        $config['max_width']  = 100;
        $config['max_height']   = 100;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    function delete_invoice_item() {
        $id = $this->input->post('id');

        $this->db->where($this->table_relationship.'_id', $id);
        $data = $this->db->delete($this->table_relationship);

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    function update_payment_status() {
        $post = $this->input->post(); 
        
        $this->db->where($this->row_id, $post['pk']);
        $data = $this->db->update($this->table_name, array('payment_status' => $post['value']));
        
        /*if($post['value'] = 1){
            $this->payment_insert($post);
        }*/
        
        if ($data) {
            echo json_encode(array('success' => 'yes'));
        } else {
            echo json_encode(array('success' => 'no'));
        }
        
    }

}
