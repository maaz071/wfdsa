<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

//require APPPATH . '/libraries/BaseController.php';

class Api extends CI_Controller
{
        const MEMBER = 1;   
        const NON_MEMBER = 2;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('api_model');
        header('Content-Type: application/json');
    }
    
    public function register() {
        $first_name = @trim(addSlashes($this->input->post('first_name')));
        $last_name = @trim(addslashes($this->input->post('last_name')));
        $email = @trim(addslashes($this->input->post('email')));
        $password = $this->input->post('password');
        $confirm_password = $this->input->post('confirm_password');
        $contact = @trim(addslashes($this->input->post('contact')));
        $country = @trim(addslashes($this->input->post('country')));
        $firebase_token= $this->input->post('firebase_token');

        if(!empty($email))
        {
            $checkEmail = $this->api_model->selectByOtherCol('email',$email,'non_member');
            if($checkEmail>0)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Email Already Exists.";
                echo json_encode($response);
                die();

            }
            else
            {
                $email = $email;
            }

        }
        if(empty($first_name))
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Empty first name.";
                echo json_encode($response);
                die();
            

        }else
            {
                $first_name = $first_name;
            }

        if(empty($last_name))
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Empty last name.";
                echo json_encode($response);
                die();
            

        }else
            {
                $last_name = $last_name;
            }

        if(empty($password))
        {
                $response['result']['status'] = 'error';
                $response['result']['response'] = 'Password is empty';
                echo json_encode($response);
                die();
        }
            else
            {
                $password = $password;
            }

        if($password !== $confirm_password)
            {
                $response['result']['status'] = 'error';
                $response['result']['response'] = 'Password not matching';
                echo json_encode($response);
                die();
            }
            else
            {
                $password = $password;
            }


                if(empty($country))
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Country is empty.";
                echo json_encode($response);
                die();
            

        }else
            {
                $country = $country;
            }

            if(empty($contact))
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Contact no is empty.";
                echo json_encode($response);
                die();
            

        }else
            {
                $contact = $contact;
            }
        
        $email_hash = md5(rand(0,999)."_".time());
        $data = array(
            'email_hash'=>$email_hash,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'password' => md5($password),
            'email' => $email,
            'contact_no' => $contact,
            'country' => $country,
            'created_at' => date('Y-m-d'),
            'device_token'=>$firebase_token,
            'status'=> 0
            );

        $insert_id = $this->api_model->insert($data,'non_member');

        $data=array();

        $data['email']=array([
            'heading' => 'New User Registration',
            'firstname' => $first_name,
            'lastname' => $last_name,
            'text'=> 'Thankyou for registration!! your account is created successfully.<br>
                <b>Credentials:</b><br>
                usernmae: '.$email.'<br>
                password: '.$password.'<br>
             click below to access your account.',
            'link' => base_url().'email_verification?key_id='.$insert_id.'&hash='.$email_hash
            
        ]);
        $emailbody = $this->load->view('template/email',$data,true);
        // or
        // $emailbody = $this->load->view('template/email','',true);


        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->email->initialize($config);
        
        $this->email->set_mailtype("html");
        $this->email->from('no-reply@wfdsa.org','WFDSA');
        $this->email->to($email);
        $this->email->subject('Email Verification - WFDSA');
        $this->email->message($emailbody);
        $this->email->send();
        
        
        if($insert_id>0)
        {
            $api_id = $this->api_model->generateApiID(array($insert_id));
            $api_secret = $this->api_model->generateApiSecret($api_id);
            $data = array(
                    'non_member_id' => $insert_id,
                    'api_id' => $api_id,
                    'api_secret' => $api_secret
                    );
            $createApiKeys = $this->api_model->insert($data,'api_keys');
            $inserted_user = $this->api_model->selectByOtherCol('non_member_id',$insert_id,'non_member');
            if($createApiKeys>0)
            {
            $response['result']['status']   = 'success';
            $response['result']['api_secret'] = $api_secret;
            $response['result']['user_details'] = $inserted_user[0];
            echo json_encode($response);
            die();
                
            }
        }



    }

    public function login()
    {
       
        $email         = @trim(addslashes($this->input->post('email')));
        $password      = @$this->input->post('password');
        $signin_type   = @$this->input->post('signin_type');
        $device_token = @$this->input->post('device_token');
        

        $email = trim($email);
        //print_r($email);
        //exit();
        if(!empty($signin_type))
        {
            if($signin_type != self::MEMBER &&  $signin_type != self::NON_MEMBER)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Invalid Signin Type.";
                echo json_encode($response);
                die();
            }
        }else
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "signin_type is missing";
                echo json_encode($response);
                die();
        }
        if($signin_type == self::NON_MEMBER)
        {
            if(empty($email))
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Email is missing.";
                // return array($response, 400);
                echo json_encode($response);
                die();
            }
            if(empty($password))
            {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Password is missing.";
                echo json_encode($response);
                die();
                //return array($response, 400);
            }
            if(!empty($email) || !empty($password))
            {
                //echo "tes";
                $checkLogin = $this->api_model->selectByTwoCol('email',$email,'password',md5($password),'non_member',$device_token);
                if($checkLogin>0)
                {
                    if($checkLogin[0]->status == 0)
                    {
                        $response['result']['status']       = 'error';
                        $response['result']['response']     = "Your account is not active check your email for verification";
                        echo json_encode($response);
                        die();        
                    }
                    else
                    {
                        $checkLogin[0]->signin_type = $signin_type;
                        $non_member_id = $checkLogin[0]->non_member_id;
                        $getApiKeys = $this->api_model->selectByOtherCol('non_member_id',$non_member_id,'api_keys');
                        $getApiKeys[0]['api_secret'];

                        //print_r($checkLogin);
                        $response['result']['status']       = 'success';
                        $response['result']['response']     = "User login successful.";
                        $response['result']['api_secret'] = $getApiKeys[0]['api_secret'];
                       // $response['data']['api_id']         = $api_key[0]->api_id;
                        $response['result']['user_data']     = $checkLogin[0]; 
                        echo json_encode($response);
                        die();
                        }
                }
                else
                {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Invalid Credentials";
                echo json_encode($response);
                die();
                }
            }
        }
        
        
        if($signin_type == self::MEMBER)
        {
            if(empty($email))
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Email is missing.";
                // return array($response, 400);
                echo json_encode($response);
                die();
            }
            if(empty($password))
            {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Password is missing.";
                echo json_encode($response);
                die();
                //return array($response, 400);
            }
            if(!empty($email) || !empty($password))
            {
                $checkLogin = $this->api_model->selectByTwoCol('email',$email,'password',base64_encode($password),'member',$device_token);
                //print_r($checkLogin);
                //exit();
                if($checkLogin>0)
                {
                    $checkLogin[0]->signin_type = $signin_type;
                $member_id = $checkLogin[0]->member_id;
                
                
                $getApiKeys = $this->api_model->selectByOtherCol('member_id',$member_id,'api_keys');
                
                
                $upd_data= array('app_user'=>1);
                $this->api_model->updateByOtherCol('member_id',$member_id, $upd_data , 'member');
                $getApiKeys[0]['api_secret'];
                
                if(empty($checkLogin[0]->upload_image)) {
                    $checkLogin[0]->upload_image = base_url().'uploads/member_photos/dummy.jpg';
                }

              
                $response['result']['status']       = 'success';
                $response['result']['response']     = "User login successful.";
                $response['result']['api_secret']     = $getApiKeys[0]['api_secret']; 
                $response['result']['user_data']     = $checkLogin[0]; 
                echo json_encode($response);
                die();
                }
                else
                {
                $response['result']['status']       = 'error';
                $response['result']['response']     = "Invalid Credentials";
                echo json_encode($response);
                die();
                }
            }
        }
        
    }
    
    public function logout()
    {
       
        //$email         = @trim(addslashes($this->input->post('email')));
        //$password      = @$this->input->post('password');
        $signin_type    = @$this->input->post('signin_type');
        $user_id        = @$this->input->post('user_id');
        
        //$device_token = @$this->input->post('device_token');
        
        //print_r($email);
        //exit();
        if(!empty($signin_type))
        {
            if($signin_type != self::MEMBER &&  $signin_type != self::NON_MEMBER)
            {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "Invalid Signin Type.";
                echo json_encode($response);
                die();
            }
        }else
        {
                $response['result']['status']   = 'error';
                $response['result']['response'] = "signin_type is missing";
                echo json_encode($response);
                die();
        }
        if($signin_type == self::NON_MEMBER)
        {
            
            if(!empty($signin_type) || !empty($user_id))
            {
                //echo "tes";
                
                
                $getApiKeys = $this->api_model->logout_non_member('non_member_id',$user_id);
                
                //$getApiKeys = $this->api_model->selectByOtherCol('non_member_id',$non_member_id,'api_keys');
               
                //print_r($checkLogin);
                $response['result']['status']       = 'success';
                $response['result']['response']     = "User logout successful.";
                //$response['result']['api_secret'] = $getApiKeys[0]['api_secret'];
               // $response['data']['api_id']         = $api_key[0]->api_id;
                //$response['result']['user_data']     = $checkLogin[0]; 
                echo json_encode($response);
                die();
                
            }
        }
        
        
        if($signin_type == self::MEMBER)
        {
            if(!empty($signin_type) || !empty($user_id))
            {
                //echo "tes";
                
                
                $getApiKeys = $this->api_model->logout_member('non_member',$user_id);
                
                //$getApiKeys = $this->api_model->selectByOtherCol('non_member_id',$non_member_id,'api_keys');
               
                //print_r($checkLogin);
                $response['result']['status']       = 'success';
                $response['result']['response']     = "User logout successful.";
                //$response['result']['api_secret'] = $getApiKeys[0]['api_secret'];
               // $response['data']['api_id']         = $api_key[0]->api_id;
                //$response['result']['user_data']     = $checkLogin[0]; 
                echo json_encode($response);
                die();
                
            }
            
        }
        
    }

}

?>