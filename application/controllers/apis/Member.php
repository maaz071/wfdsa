<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Member extends REST_Controller
{
    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Calls parent contructor and load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
    
    public function Roles_post()
    {   
        $data = $this->_post_args;
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->Member_Roles($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }

    public function Social_msg_get(){
        try {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_social_msg();
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
     public function Countries_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_Countries();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function DSA_member_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->getDSA_member();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
   
    
    public function Committee_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->getCommittee();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function Committee_byRole_get() {   
         $data = $this->_get_args;
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_Committee_frm_roles($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function Leadership_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->getLeadership();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
    
     public function Leadership_byRole_get() {   
        $data = $this->_get_args;
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_leadership_frm_roles($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
   
    
     public function Edit_member_post() {   
        $data = $this->_post_args;
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->edit_member($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }

    
}
