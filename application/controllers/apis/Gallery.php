<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Gallery extends REST_Controller
{
    
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
     
    
    
    public function Gallery_get() {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_gallery($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
     
    public function VideoAlbums_get() {   
        $data = $this->_get_args;
        $data['type'] = 1;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_album($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }


    public function PhotoAlbums_get() {   
        $data = $this->_get_args;
        $data['type'] = 0;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_album($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }  
     
}
