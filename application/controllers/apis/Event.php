<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Event extends REST_Controller
{
    const api_message_key = "AIzaSyDLwcHk93UVYSGtp-eDNi2RvnGDPoCDnNU";
    
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
     
    public function Events_Notification_post() {   
        $data = $this->_post_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->Event_notification($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
    
    
    public function Events_post() {   
        $data = $this->_post_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->Events($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
    public function Single_event_post() {   
        $data = $this->_post_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->single_event($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }

    public function Event_resources_get(){
        $data = $this->_get_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_event_resources($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }


    public function Event_notifications_get(){
        $data = $this->_get_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_event_notifications($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Gallery_get() {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_event_gallery($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
    
     public function EventDetail_get() { 
        
        $data = $this->_get_args;
        try {
            
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_event_details($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
    
    public function EventMembers_get() { 
        //echo "testing method";
        //exit();
        $data = $this->_get_args;
        
        try {
            
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_members_details($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
      public function add_likes_post() { 
        
        $data = $this->_post_args;
        //echo "test";
        //print_r($data);
        //exit();
        try {
            
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->add_likes($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function Get_Poll_get() {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->get_polls($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Checked_In_get() {   
        $data = $this->_get_args;
        //print_r($data);
        //exit();
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->attendees_checked_in($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Checked_In_post() {   
        $data = $this->_post_args;
        //print_r($data);
        //exit();
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->attendee_checked_in($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
    public function Add_PollAnswer_post() {   
        $data = $this->_post_args;
        try {
            $this->load->library("api/Event_lib");
            $s = new Event_lib;
            $result = $s->add_poll_answer($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Upload_Gallery_post() {   
        $data = $this->_post_args;
        $this->load->model('Event_gallery_model'); 
         
        $json = json_decode(file_get_contents('php://input'),true);
        $user_id = $json["user_id"];       
        $event_id = $json["event_id"];
        //$event_id = $json["event_id"]; //within square bracket should be same as Utils.imageName & Utils.imageList
        $imageList = $json["imageList"];
        
        // print_r($imageList);exit;
        //$time = time();
        
        
        $i = 0;
        $response = array();
     
     
        if (isset($imageList)) {
            if (is_array($imageList)) {
                $j=0;
                foreach($imageList as $image) {
                    $time =time();
                    $j++;
                    $decodedImage = base64_decode($image);
                    
                    $file_name = "./uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG";
                    $uploadfile = base_url()."/uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG";
                    
                    $return = file_put_contents("uploads/Event_Gallery/".$event_id."_".$time."_".$j.".JPG", $decodedImage);
                    
                    if($return !== false) {
                        
                       // $this->CI->Event_gallery_model->insert_userPhoto_data();
                        $insert_id = $this->Event_gallery_model->create_gallery_records($user_id,$event_id,$uploadfile);

                        $edata=array();

                        $edata['email']=array([
                            'heading' => 'New Event Images',
                            'firstname' => 'Admin',
                            'lastname' => '',
                            'text'=> 'New Images have been uploaded by an event member kindly review and verify. <br>  <a target="_blank" href="'.base_url().'/event_gallery?event_id='.$insert_id.'"><b>Click here</b></a> to view images.',
                            'link' => ''
                            
                        ]);
                        $emailbody = $this->load->view('template/email',$edata,true);
                        // or
                        // $emailbody = $this->load->view('template/email','',true);


                        $config=array(
                        'charset'=>'utf-8',
                        'wordwrap'=> TRUE,
                        'mailtype' => 'html'
                        );

                        $this->email->initialize($config);
                        
                        $this->email->from('no-reply@wfdsa.org','WFDSA');
                        $this->email->to('mpaniagua@wfdsa.org');
                        $this->email->cc('nouman.bashir@gmail.com');
                        $this->email->subject('Event Gallery Activity - WFDSA');
                        $this->email->message($emailbody);
                        $this->email->send();
                        
                            $response['success'] = 1;
                            $response['message'] = "Image Uploaded Successfully. It will be visiable after WFDSA(admin) approval ";
                            }
                            else{
                                $response['success'] = 0;
                                $response['message'] = "Image Uploaded Failed";        
                             
                                  
                            
                        } 
                        
                $i++;
                }
            }
        } else{
            $response['success'] = 0;
            $response['message'] = "List is empty.";
        }
            $res['result']['status']  = 'respnse';
            $res['result']['response']  = $response['message'];
        header("Access-Control-Allow-Origin: *");
        $this->response($response);
        // echo json_encode($response);

        // old code
        // $data = $this->_post_args;
        // // $file = $this->_file_args;

        // // print_r($data);
        // // exit;
        
    
        // try {
        //     $this->load->library("api/Event_lib");
        //     $s = new Event_lib;
        //     $result = $s->upload_gallery($data);
        // }
        // catch(Exception $e)
        // {
        //     $response['result']['status']  = 'error';
        //     $response['result']['response']	= $e->getMessage();
        //     $this->response($response, $e->getCode());
        // }
        // header("Access-Control-Allow-Origin: *");
        // $this->response($result[0], $result[1]);
    }
     
}
