<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Resources extends REST_Controller
{
    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Calls parent contructor and load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
    
    public function Get_resource_get() {   
        //$data = $this->_post_args;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_resource();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Get_three_resource_get() {   
        $data = $this->_get_args;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_three_resource($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Get_resource_ctg_get() {   
        //$data = $this->_post_args;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_resource_ctg();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Get_resource_by_ctg_get() {   
        $data = $this->_get_args;
        
        //print_r($data);
        //exit;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_resource_by_ctg($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response'] = $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Get_resource_by_ctg_role_get() {   
        $data = $this->_get_args;
        
        //print_r($data);
        //exit;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_resource_by_role_ctg($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Get_resource_file_get() {   
        $data = $this->_get_args;
        try
        {
            $this->load->library("api/Resource_lib");
            $s = new Resource_lib;
            $result = $s->get_resource_file($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
     public function Countries_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_Countries();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function DSA_member_get() {   
        
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->getDSA_member();
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
    public function member_frmRoles_get() {   
        $data = $this->_get_args;
        try
        {
            $this->load->library("api/Member_lib");
            $s = new Member_lib;
            $result = $s->get_member_frm_roles($data);
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    


    
}
