<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Payment extends REST_Controller
{
    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Calls parent contructor and load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
    
    public function All_Payment_get()
    {   
        
        try {
            $this->load->library("api/Payment_lib");
            $s = new Payment_lib;
            $result = $s->Payments_all();
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    } 
    
    public function All_Payment_by_role_get()
    {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Payment_lib");
            $s = new Payment_lib;
            $result = $s->Payments_byRole($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function All_invoices_by_role_get()
    {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Payment_lib");
            $s = new Payment_lib;
            $result = $s->invoices_byRole($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function All_invoices_get()
    {   
        
        try {
            $this->load->library("api/Payment_lib");
            $s = new Payment_lib;
            $result = $s->invoices();
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
      public function Payment_Verification_post() {  
        
        $data = $this->_post_args;
       //print_r($data);
       //exit();
       
         
        try {
             //$this->load->library("api/Payment_lib"); 
            // $s = new Payment_lib; 
            // $result = $s->Verify_Payment($data);
            
            $this->load->library("stripe/Stripe_Lib");
            $stripe = new Stripe_Lib; 
            $result = $stripe->Charge_Payment($data); 
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
     public function invoice_Verification_post() {  
        
        $data = $this->_post_args;
       //print_r($data);
       //exit();
       
         
        try {
             //$this->load->library("api/Payment_lib"); 
            // $s = new Payment_lib; 
            // $result = $s->Verify_Payment($data);
            
            $this->load->library("stripe/Stripe_Lib");
            $stripe = new Stripe_Lib; 
            $result = $stripe->Charge_invoice_Payment($data); 
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function payment_verification_noAmount_post() {  
        
        $data = $this->_post_args;
       //print_r($data);
       //exit();
       
         
        try {
             //$this->load->library("api/Payment_lib"); 
            // $s = new Payment_lib; 
            // $result = $s->Verify_Payment($data);
            
            $this->load->library("stripe/Stripe_Lib");
            $stripe = new Stripe_Lib; 
            $result = $stripe->payment_zero($data); 
            
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
     
}
