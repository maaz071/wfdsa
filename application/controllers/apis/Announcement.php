<?php

/**  
 * Description of Subject
 *
 * @author      Muhammad Umar Hayat
 * @description Contacts Class For The Basic Functionality of including Syncing Contacts
 */
  
require_once APPPATH.'libraries/api/REST_Controller.php';

class Announcement extends REST_Controller
{
    /*
     * Mehtod:   		__construct
     * Params: 			.....
     * Description:             Calls parent contructor and load CI_Controller instance into $CI
     * Returns: 		.....
     */
    public function __construct() {
         
        parent::__construct();
        
    }
    /*
     * Mehtod:   		add_subject
     * Params: 			.....
     * Description:             .....
     * Returns: 		.....
     */ 
    
    public function Announcement_get()
    {   
        
        try {
            $this->load->library("api/Announcement_lib");
            $s = new Announcement_lib;
            $result = $s->Announcement(array());
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
     public function Announcement_byrole_get()
    {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Announcement_lib");
            $s = new Announcement_lib;
            $result = $s->Announcement_byrole($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Announcement_byrole_loading_get()
    {   
        $data = $this->_get_args;
        //print_r($data);
        //exit;
        try {
            $this->load->library("api/Announcement_lib");
            $s = new Announcement_lib;
            $result = $s->Announcement_byrole_loading($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    
     public function single_announcement_get()
    {   
        $data = $this->_get_args;
        try {
            $this->load->library("api/Announcement_lib");
            $s = new Announcement_lib;
            $result = $s->single_announcement($data);
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
    
    public function Record_get()
    {   
        
        try {
            $this->load->library("api/Announcement_lib");
            $s = new Announcement_lib;
            $result = $s->record(array());
        }
        catch(Exception $e)
        {
            $response['result']['status']  = 'error';
            $response['result']['response']	= $e->getMessage();
            $this->response($response, $e->getCode());
        }
        header("Access-Control-Allow-Origin: *");
        $this->response($result[0], $result[1]);
    }
     
}
