<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Event_gallery extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Event_gallery_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->relation_table_name = 'event_gal_images';
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
     function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name']; 
        $this->data['event_id'] = $this->input->get('event_id');
        
        $this->data['event']  =  $this->Event_gallery_model->get_gallery_record(); 
     /*   echo "<pre>";
        print_r($this->data['event']);
        exit;*/
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');

        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->model('Poll_model');
        $this->data['event'] = $this->Poll_model->get_all_table('event');

        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->data['url_id'] = $this->input->get('event_id');

        $this->data['record_info'] = $this->Event_gallery_model->get_all_table($this->table_name, $id);
        $this->data['record_gal'] = $this->Event_gallery_model->get_relation_table($id, $this->relation_table_name, $this->table_name);
        $this->load->model('Poll_model');
        if($this->data['url_id']){
           $this->data['event'] = $this->Poll_model->get_all_table('event', $this->data['url_id']);
        } else {
            $this->data['event'] = $this->Poll_model->get_all_table('event');
        }
        
        $this->data['admin_name'] = $admin_data[0]['name'];

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post();

        $this->form_validation->set_rules('gallery_title', 'Title', 'required|trim');
        $this->form_validation->set_rules('event_id', 'Event', 'required|trim|is_unique[' . $this->table_name . '.event_id]');
        $this->form_validation->set_rules('description', 'Description', 'required|trim');

        if ($this->form_validation->run() !== FALSE) {
            $create_id = $this->Event_gallery_model->create_record($this->table_name, $post);
            $flename = (!empty($_FILES)) ? $this->upload($create_id) : NULL;

            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function delete_gal_images() {
        $ids = $this->input->post('ids');
        
        
        $this->db->from('event_gal_images');
        $this->db->where_in('event_gal_id', $ids);
        $query = $this->db->get();
        $record = $query->result_array();

        
       /* foreach($record as $value) {
            
            $file = $value['uploaded_image']; 
            $filename = (explode("/",$file)); 
            
            unlink($_SERVER['DOCUMENT_ROOT'].'/wfdsa3/wfdsa/uploads/'.$filename[6]);
        }*/

        $this->db->where_in('event_gal_id', $ids);
        $check = $this->db->delete('event_gal_images');
        
        if ($check) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Gallery Image is Deleted.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }
     
     function update_permission() {
        $ids = $this->input->post('ids');
        
        //print_r($post);
        //exit;
        $data =array(
            'checked_in' => 0
            );
        
        
        //$this->db->set('checked_in',"0");
        $this->db->where_in('event_gal_id', $ids);
        $record = $this->db->update('event_gal_images',$data);
        //$record = $this->db->get();
        //$record = $query->result_array();

        
        
        if ($record) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Gallery Image Permission Updated.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }
     function update_permission_checkin() {
        $ids = $this->input->post('ids');

        //print_r($post);
        //exit;
        $data =array(
            'checked_in' => 1
            );


        //$this->db->set('checked_in',"0");
        $this->db->where_in('event_gal_id', $ids);
        $record = $this->db->update('event_gal_images',$data);
        //$record = $this->db->get();
        //$record = $query->result_array();



        if ($record) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Gallery Image Permission Updated.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }
    function update_action() {
        $post = $this->input->post();
       /* echo "<pre>";
        print_r(($_FILES)); exit;*/
        $event_id = $this->input->post('event_id');
        $checkedin =$this->input->post('allowed');
        
        if(empty($checkedin))
        {
            $checkedin = 0;
        }

        // $this->load->library('upload'); 
      
        $dataInfo = array();
        $files = $_FILES;

        $cpt = count($_FILES['upload_image']['name']);
        $new_name = $_FILES["upload_image"]['name'];
        // custom maaz
        $config['upload_path'] = './uploads/Event_Gallery/';
        $config['allowed_types'] = 'gif|jpg|png';
    // $config['max_size'] = '100';
    // $config['max_width']  = '1024';
    // $config['max_height']  = '768';
    $config['file_name'] = time() . "_" . $new_name;
    $config['overwrite'] = TRUE;
    $config['encrypt_name'] = FALSE;
    $config['remove_spaces'] = TRUE;
    if ( ! is_dir($config['upload_path']) ) die("THE UPLOAD DIRECTORY DOES NOT EXIST");
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('upload_image')) {
        $error = array('error' => $this->upload->display_errors());
        print_r($error);exit;
    }


            // $this->load->library('upload', $config);
            // $this->upload->do_upload('upload_image');
            // $this->upload->initialize($this->set_upload_options($config['file_name']));

            $this->Event_gallery_model->create_gallery_record($config['file_name'], $event_id,$checkedin);
            
            $dataInfo[] = $this->upload->data('upload_image');
        
        
        $filename = true;
        // $flename = (!empty($_FILES['upload_image']['name'][0])) ? $this->upload($event_id,$checkedin) : NULL;
        
        if ($flename) {
            redirect('/event_gallery?event_id='.$event_id);
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            redirect('/event_gallery?event_id='.$event_id);
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }
    
    public function upload($event_id,$checkedin) {
        $this->load->library('upload'); 
      
        $dataInfo = array();
        $files = $_FILES;
        $cpt = count($_FILES['upload_image']['name']);
        
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES['upload_image']['name'] = $files['upload_image']['name'][$i];
            $_FILES['upload_image']['type'] = $files['upload_image']['type'][$i];
            $_FILES['upload_image']['tmp_name'] = $files['upload_image']['tmp_name'][$i];
            $_FILES['upload_image']['error'] = $files['upload_image']['error'][$i];
            $_FILES['upload_image']['size'] = $files['upload_image']['size'][$i];
            $new_name = $_FILES["upload_image"]['name'];
            $config['file_name'] = time() . "_" . $new_name;
            $this->upload->initialize($this->set_upload_options($config['file_name']));

            $this->Event_gallery_model->create_gallery_record($config['file_name'], $event_id,$checkedin);
            $this->upload->do_upload('upload_image');
            $dataInfo[] = $this->upload->data('upload_image');
        }
        
        return TRUE;
    }
    
    public function test(){
        echo "test";
    }

    
    

    private function set_upload_options($file_name) {
        //upload an image options
        $config = array();
        $config['upload_path'] = 'uploads/Event_Gallery';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '10000';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $file_name;
        return $config;
    }

}
