<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_photo extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_photo_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = lcfirst(__CLASS__);
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['event_id'] = $this->input->get('event_id');
        
        if($this->data['event_id']) {
            $event = $this->User_photo_model->get_all_table('event',$this->data['event_id']);
            $this->data['event_name'] = $event[0]['title'];
        }
        
        $this->data['record_list'] = $this->User_photo_model->get_record();
        $this->data['records'] = $this->User_photo_model->get_user_uploads($this->data['event_id'], $this->table_name);
         
        $this->load->model('Poll_model');
        

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function get_user_photos() {
        $event_id = $this->input->post('id');
        $records = $this->User_photo_model->get_user_uploads($event_id, $this->table_name);

        if (!empty($records)) {
            echo json_encode(array('success' => 'yes', 'records' => $records));
        } else if (empty($records)) {
            echo json_encode(array('success' => 'no', 'msg' => 'No Images Uploaded by the User.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function approve_photo() {
        $ids = $this->input->post('ids');
        $event_id = $this->input->post('event_id');
        $event_gallery_id = $this->User_photo_model->get_event_gallery_id($event_id);
       
        $chek = $this->User_photo_model->get_user_gallery_insert($ids, $event_id);

        if ($chek) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Selected Images SuccessFully Approved.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }
    
    function disapprove_photo() {
        $ids = $this->input->post('ids');  
        $chek = $this->User_photo_model->delete_photos($ids);
        
        if ($chek) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Selected Images Deleted Successfully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');

        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->model('Poll_model');
        $this->data['event'] = $this->Poll_model->get_all_table('event');

        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $id = $this->uri->segment(3);

        $this->data['record_info'] = $this->User_photo_model->get_all_table($this->table_name, $id);

        $this->load->model('Poll_model');
        $this->data['event'] = $this->Poll_model->get_all_table('event');

        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post();

        $this->form_validation->set_rules('gallery_title', 'Title', 'required|trim');
        $this->form_validation->set_rules('event_id', 'Event', 'required|trim');
        $this->form_validation->set_rules('description', 'Description', 'required|trim');

        if ($this->form_validation->run() !== FALSE) {
            $create_id = $this->User_photo_model->create_record($this->table_name, $post);
            $flename = (!empty($_FILES)) ? $this->upload($create_id) : NULL;

            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function delete_gal_images() {
        $ids = $this->input->post('ids');

        $this->db->where_in('event_gal_id', $ids);
        $check = $this->db->delete('event_gal_images');

        if ($check) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Gallery Image is Deleted.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function update_action() {
        $post = $this->input->post();

        $update = $this->User_photo_model->update_record($this->table_name, $this->row_id, $post, $post[$this->row_id]);
        $flename = (!empty($_FILES)) ? $this->upload($post[$this->row_id]) : NULL;

        if ($update) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
        }
    }

    function delete() {
//        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    public function upload($id) {
        $this->load->library('upload');


        $dataInfo = array();
        $files = $_FILES;
        $cpt = count($_FILES['upload_image']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES['upload_image']['name'] = $files['upload_image']['name'][$i];
            $_FILES['upload_image']['type'] = $files['upload_image']['type'][$i];
            $_FILES['upload_image']['tmp_name'] = $files['upload_image']['tmp_name'][$i];
            $_FILES['upload_image']['error'] = $files['upload_image']['error'][$i];
            $_FILES['upload_image']['size'] = $files['upload_image']['size'][$i];
            $new_name = $_FILES["upload_image"]['name'];
            $config['file_name'] = time() . "_" . $new_name;
            $this->upload->initialize($this->set_upload_options($config['file_name']));

            $this->User_photo_model->create_gallery_record($config['file_name'], $id);
            $this->upload->do_upload('upload_image');
            $dataInfo[] = $this->upload->data('upload_image');
        }
    }

    private function set_upload_options($file_name) {
        //upload an image options
        $config = array();
        $config['upload_path'] = 'uploads/Event_Gallery';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '10000';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $file_name;
        return $config;
    }

}
