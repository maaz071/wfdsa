<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Announcement extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Announcement_model');
        $this->row_id = lcfirst(__CLASS__) . "_id";
        $this->controller = lcfirst(__CLASS__);
        $this->table_name = 'announcement';
        $this->data['row_id'] = $this->row_id;
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {    	
        $admin_data = $this->session->userdata('admin_data');
        $this->data['admin_name'] = $admin_data[0]['name'];
        $this->data['record_list'] = $this->Announcement_model->get_all_table($this->table_name);
        /*echo"<pre>";
        print_r($this->data['record_list']);
        exit;
        */
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_list_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create() {
        $admin_data = $this->session->userdata('admin_data');
        $this->load->model('Event_model');
        $this->load->model('Announcement_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }

        $this->data['roles'] = $roles;
        $this->data['selected'] = array();
        $test = array(" "," ");
        $this->data['test1'] = $test;
        
        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function edit() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $this->load->model('Event_model');
        $roles = $this->Event_model->get_parent_child('member_role', 0);
        foreach ($roles as $key => $value) {
            $child = $this->Event_model->get_parent_child('member_role', $value['member_role_id']);
            $roles[$key]['child'] = $child;
        }
        
        //$annouce_for = $this->Announcement_model->get_all($id);
        
       $this->data['announce'] = $this->Announcement_model->get_all($id);
      
      // $annouce_for[0]['announce_for'];
        
        $this->data['roles'] = $roles;
        $this->data['selected'] = array();
        
        $this->load->model('User_admin_model');
        $this->data['record_info'] = $this->User_admin_model->get_edit_record($this->table_name, $id);
            
        $this->data['admin_name'] = $admin_data[0]['name'];
         $newString = $this->data['record_info'][0]['announce_for'];
           $test = explode(",",$newString);
        $this->data['test1'] = $test;
        /*echo "<pre>";
        print_r($this->data['record_info']);
        exit;*/
        
        $this->load->view('admin/header', $this->data);
        $this->load->view('admin/' . $this->controller . '_form_view', $this->data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post();
		$date = $post['date'] . ":00";
		$file = $_FILES["fileToUpload"]["name"];
		$file = base_url() . "uploads/Announcement_Image/" . $file;
		
		if ($post['event_user_type'] == 2) {
			$data = array(
				'title' => $post['title'],
				'date' => $date,
				'announcement_message' => $post['announcement_message'],
				'upload_image' => $file,
				'announce_for' => "Public",
				'icon_class' => " ",
				'status' => 1,
				'created_on' => $post['title'],
				'updated_on' => $post['title'],
				'created_by' => $post['title'],
				'updated_by' => $post['title']
			);
			$query = $this->db->insert('announcement', $data);
			if ($query) {
		
				// echo "data inserted successfully";
				// sending response
				// sending fcm notification to all members and non members
		
				$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Announcement has been added by WFDSA';
				$click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				foreach($tokens as $key => $value) {
					$device_tokens = $value['device_token'];
					$send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				foreach($tokenz as $key => $value) {
					$device_tokens = $value['device_token'];
					$send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				}
		
				// uploading image
		
				$target_dir = "./uploads/Announcement_Image/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
		
				// Check if image file is a actual image or fake image
		
				if (isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if ($check !== false) {
						echo "File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
					}
					else {
						echo "File is not an image.";
						$uploadOk = 0;
					}
				}
		
				// Check file size
		
				if ($_FILES["fileToUpload"]["size"] > 500000) {
		
					// echo "Sorry, your file is too large.";
					// $uploadOk = 0;
		
				}
		
				// Allow certain file formats
		
				if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
		
					// echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					// $uploadOk = 0;
		
				}
		
				// Check if $uploadOk is set to 0 by an error
		
				if ($uploadOk == 0) {
		
					// echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
		
				}
				else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						redirect('/announcement');
					}
					else {
						$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						redirect('/announcement');
		
						// echo "Sorry, there was an error uploading your file.";
		
					}
				}
			}
		
			// redirect('/Announcement/create/');
		
		}
		else {
		
			// echo "event user type is 1";
		
			$newString = implode(',', $post['permission']);
			$notification = $post['permission'];
		
			// print_r($newString);
			// exit;
		
			$data = array(
				'title' => $post['title'],
				'date' => $date,
				'announcement_message' => $post['announcement_message'],
				'upload_image' => $file,
				'announce_for' => $newString,
				'icon_class' => " ",
				'status' => 1,
				'created_on' => $post['title'],
				'updated_on' => $post['title'],
				'created_by' => $post['title'],
				'updated_by' => $post['title']
			);
			$query = $this->db->insert('announcement', $data);
			if ($query) {
		
				// sending fcm notification to all members and non members
		
				$this->load->model('api_model');
				$tokens = $this->api_model->selectByOtherCol('status', 1, 'non_member');
				$tokenz = $this->api_model->selectByOtherCol('status', 1, 'member');
				$msg = 'New Announcement has been added by WFDSA';
				$click_action = 'com.example.shariqkhan.wfdsa.SplashActivity';
				$email = $this->session->userdata('email');
				$getAdminInfo = $this->api_model->selectByOtherCol('email', $email, 'user');
				$admin_id = $getAdminInfo[0]['user_id'];
				/*foreach ($tokens as $key => $value) {
				$device_tokens = $value['device_token'];
				$send = $this->api_model->send_fcm($device_tokens,$msg,$click_action,$admin_id);
				}*/
				foreach ($notification as $value1)
				{
				    $tokenz = $this->api_model->selectByOtherCol_byrole('status', 1, 'member',$value1);
				    foreach($tokenz as $key => $value) 
				        {
					    $device_tokens = $value['device_token'];
					    $send = $this->api_model->send_fcm($device_tokens, $msg, $click_action, $admin_id);
				        }
				}
		
				// uploading image
		
				$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/Announcement_Image/";
				$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
				$uploadOk = 1;
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
		
				// Check if image file is a actual image or fake image
		
				if (isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if ($check !== false) {
						echo "File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
					}
					else {
						echo "File is not an image.";
						$uploadOk = 0;
					}
				}
		
				// Check file size
		
				if ($_FILES["fileToUpload"]["size"] > 500000) {
		
					// echo "Sorry, your file is too large.";
					// $uploadOk = 0;
		
				}
		
				// Allow certain file formats
		
				if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
		
					// echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
					// $uploadOk = 0;
		
				}
		
				// Check if $uploadOk is set to 0 by an error
		
				if ($uploadOk == 0) {
		
					// echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
		
				}
				else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						redirect('/announcement');
					}
					else {
		
						//   echo "Sorry, there was an error uploading your file.";
						$this->session->set_flashdata('msg', 'Announcement Added Successfully');
						redirect('/announcement');
					}
				}
			}
		
			// redirect('/Announcement/create/');
		
			/* $this->form_validation->set_rules('title', 'Title', 'required|trim');
			$this->form_validation->set_rules('date', 'Date', 'required|trim');
			$this->form_validation->set_rules('announce_for', 'Announcement For', 'required|trim');
			$this->form_validation->set_rules('announcement_message', 'Announcement Message', 'required|trim');
			if ($this->form_validation->run() !== FALSE) {
			$flename = (!empty($_FILES)) ? $_FILES['upload_image']['name'] : NULL;
			($_FILES) ? $this->do_upload('upload_image') : FALSE;
			$create_id = $this->Announcement_model->create_record($this->table_name, $post, $flename);
			if ($create_id !== "") {
			echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
			} else {
			echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
			}
			} else {
			echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
			}*/
		}
    }

    function update_action() {
        $post = $this->input->post();
        
        /*echo "<pre>";
        print_r($post); exit;*/
        
        if(!empty($post['Announcement_id']))
        {
            $id= $post['Announcement_id'];    
        }
        else
        {
            $id= $post['announcement_id'];
        }
        
        $file = $_FILES["fileToUpload"]["name"];
        
        if(!empty($file))
        {
            $file = base_url()."uploads/Announcement_Image/".$file;
        }
        else
        {
            $file= $post['imageSrc'];
        }
        
        if(empty($post['permission'])){
            
            $newString = $post['previous-permission'];
        }
        else
        {
            $newString = implode(',', $post['permission']);
        }
        //$file = base_url()."uploads/Announcement_Image/".$file;
        
        
        
        
        if($post['event_user_type']==2){
            
            $data = array(
           'title'=>  $post['title'], 
		   'date'=>  $post['date'],
		   'announcement_message'=> $post['announcement_message'],
            'upload_image'=> $file,
		  'announce_for'=>"Public",
		  'icon_class'=>" ",
		  'status'=>1,
		'created_on'=>  $post['title'],
		'updated_on'=>  $post['title'], 
        'created_by'=>  $post['title'], 
        'updated_by'=>  $post['title']
          
	
    );
        //$this->db->set($data);
        $this->db->where('announcement_id', $id);
        $query =  $this->db->update('announcement',$data);
        if($query)
        {
            //echo "record updated successfully";
            //uploading image
            $target_dir =  $_SERVER['DOCUMENT_ROOT']."/uploads/Announcement_Image/";
            
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
            // Check file size
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                //$uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                //$uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            }
            else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    	$this->session->set_flashdata('msg', 'Announcement Updated Successfully');
                    //echo json_encode(array('success' => 'yes'));
                    //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    redirect('/announcement/');
                } else {
                 //   echo "Sorry, there was an error uploading your file.";
                 redirect('/announcement/');
                }
            }
        }
    }
        
        else
        {
            //$newString = implode(',', $post['permission']);
            
            //print_r($newString);
            //exit;
             $data = array(
           'title'=>  $post['title'], 
		   'date'=>  $post['date'],
		   'announcement_message'=> $post['announcement_message'],
            'upload_image'=> $file,
		  'announce_for'=>$newString,
		  'icon_class'=>" ",
		  'status'=>1,
		'created_on'=>  $post['title'],
		'updated_on'=>  $post['title'], 
        'created_by'=>  $post['title'], 
        'updated_by'=>  $post['title']
          
	
    );
        $this->db->where('announcement_id', $id);
        $query =  $this->db->update('announcement',$data);
        if($query)
        {
            //echo "record updated successfully";
            //uploading image
            $target_dir =  $_SERVER['DOCUMENT_ROOT']."/uploads/Announcement_Image/";
            
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
            // Check file size
            if ($_FILES["fileToUpload"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                //$uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                //$uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            }
            else {
                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    	$this->session->set_flashdata('msg', 'Announcement Updated Successfully');
                    //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    redirect('/announcement/');
                } else {
                 //   echo "Sorry, there was an error uploading your file.";
                 redirect('/announcement/');
                }
            }
        }
    }
        
    }
        

    function delete() {
        $this->output->enable_profiler(true);
        $ids = $this->input->post('ids');

        $this->db->where_in($this->row_id, $ids);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/Announcement_Image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $new_name = $_FILES["upload_image"]['name'];
        $config['file_name'] = time() . "_" . $new_name;
        //  $config['max_size']             = 100;
        //  $config['max_width']            = 1024;
          $config['max_height']           = 300;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where($this->row_id, $id);
        $data = $this->db->update($this->table_name, array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

}
