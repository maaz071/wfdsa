<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('About_model');
        $this->row_id = lcfirst(__CLASS__)."_id";
        $this->data['row_id'] = $this->row_id;
         
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function index() {
        $admin_data = $this->session->userdata('admin_data');
        $data['admin_name'] = $admin_data[0]['name'];
        
        $data['record_info'] = $this->About_model->get_about_data(1);

        $this->load->view('admin/header', $data);
        $this->load->view('admin/about_form_view');
        $this->load->view('admin/footer');
    }

    function create_product() {
        $admin_data = $this->session->userdata('admin_data');
        $data['categories'] = $this->About_model->get_categories();
        $data['brand'] = $this->About_model->get_brand();
        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/product_form_view');
        $this->load->view('admin/footer');
    }

    function edit_product() {
        $admin_data = $this->session->userdata('admin_data');
        $id = $this->uri->segment(3);
        $data['categories'] = $this->About_model->get_categories();
         $data['brand'] = $this->About_model->get_brand();
        $data['record_info'] = $this->About_model->get_product_record($id);

        $data['admin_name'] = $admin_data[0]['name'];
        $this->load->view('admin/header', $data);
        $this->load->view('admin/product_form_view', $data);
        $this->load->view('admin/footer');
    }

    function create_action() {
        $post = $this->input->post();

        $this->form_validation->set_rules('pname', 'Product Name', 'required|is_unique[products.pname]');
        //  $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('pavailability', 'Product Availability', 'required');
        $this->form_validation->set_rules('pprice', 'Price', 'required');
        // $this->form_validation->set_rules('pimage', 'Product Image', 'required');
//         $this->form_validation->error_delimeters('<strong>','</strong>');

        if ($this->form_validation->run() !== FALSE) {

            $create_id = $this->About_model->create_record($post, $_FILES['pimage']['name']);

            ($_FILES) ? $this->do_upload('pimage') : FALSE;

            if ($create_id !== "") {
                echo json_encode(array('success' => 'yes', 'msg' => 'Record SuccessFully Inserted.', 'id' => $create_id));
            } else {
                echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
    }

    function update_action() {
        $post = $this->input->post();  
        /*echo "<pre>";
        print_r($post);
        exit;*/
        
        
        
        
        $this->form_validation->set_rules('contact_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('about_text', 'About Us Text', 'required');
        $this->form_validation->set_rules('contact_address', 'Address', 'required');
        
        if ($this->form_validation->run() !== FALSE) {
            
        $file = (!empty($_FILES['upload_pic']['name'])) ? $_FILES['upload_pic']['name'] : '';

        $file_upload = (!empty($file)) ? $this->do_upload('upload_pic') : FALSE;
        $update = $this->About_model->update_record($post,$file,$post['about_id']);
       
 
            if ($update) {
                $this->session->set_flashdata('successmessage', 'About content has been updated successfully');
                redirect('/about/index/');
                // echo json_encode(array('success' => 'yes', 'msg' => 'Record Updated SuccessFully.'));
            } else {
                redirect('/about/index/');
                //echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened , Contact the Developer.'));
            }
        
        } else {
            echo json_encode(array('success' => 'no', 'msg' => validation_errors()));
        }
        
    }

    function do_upload($image) {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
         $new_name = $_FILES["upload_pic"]['name']; 
        $config['file_name'] = time()."_".$new_name;
       // $config['max_size']             = 100;
        $config['max_width']            = 1000;
        $config['max_height']           = 1000;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image)) {
            echo json_encode(array('success' => 'no', 'msg' => $this->upload->display_errors()));
            exit;
        } else {
            $this->upload->data($image);
        }
    }

    function delete() {

        $ids = $this->input->post('ids');


        $this->db->where_in('product_id', $ids);
        $data = $this->db->update('products', array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }

    function delete_individual() {

        $id = $this->input->post('id');

        $this->db->where('product_id', $id);
        $data = $this->db->update('products', array('status' => 0));

        if ($data) {
            echo json_encode(array('success' => 'yes', 'msg' => 'Record Deleted Successfully'));
        } else {
            echo json_encode(array('success' => 'no', 'msg' => 'Some Bad had Happened Contact Developer'));
        }
    }
    
    function get_record() {

        $this->db->select();
        $this->db->from('about');
        $data = $this->db->get();
        $res = $data->result_array();

        if ($res) {
            return $res;
        } else {
            return FALSE;
                }
    }

}
