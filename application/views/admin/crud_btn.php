 


<?php if ($this->uri->segment(1) != 'comment') {
 										
    $pass_EventID_edit = (isset($event_id)) ? "?event_id=".$event_id : ''; 
 ?>  

    <div class="btn-group">

        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i></a>
        <ul class="dropdown-menu" style=" margin-left: -130px !important; z-index:99999;">
            
            <?php if($this->uri->segment(1) == 'event') { ?>
                 <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/view/<?php echo $record[$row_id]; ?>" id="<?php echo $record[$row_id]; ?>" class="view_result"><i class="icon-eye-open"></i> View</a></li> 
            <?php } ?> 
            <?php if($this->uri->segment(1) != 'non_member') {?>
            <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/edit/<?php echo $record[$row_id].$pass_EventID_edit; ?>"><i class="icon-pencil"></i> Edit</a></li>
            <?php } ?>
            <?php if($record['status'] == 1 && $this->uri->segment(1) != 'invoice') {?>
                 <li><a href="#" id="<?php echo $record[$row_id]; ?>" data-val = "active" class="status_change" data-id="<?php echo $this->uri->segment(1);?>"><i class="icon-lock"></i> Inactive</a></li> 
            <?php } ?>
            
            <?php if($record['status'] == 0) {?>
                 <li><a href="#" id="<?php echo $record[$row_id]; ?>" data-val = "inactive" class="status_change" data-id="<?php echo lcfirst($this->uri->segment(1));?>"><i class="icon-unlock"></i> Active</a></li> 
             <?php } ?>
           
           <?php if($this->uri->segment(1) == "member") {?>
           <li>
               <a href="<?php echo base_url() . $this->uri->segment(1); ?>/bulkemail/<?php echo $record[$row_id]?>"><i class="icon-envelope"></i> Resend credentials</a>
           </li>
           <?php }?>
           
            <li><a href="#" id="<?php echo $record[$row_id]; ?>" data-id="<?php echo $this->uri->segment(1);?>" class="delete_individual"><i class="icon-trash"></i> Delete</a></li> 
             <?php if($this->uri->segment(1) == 'poll') { ?>
                 <li><a href="#poll_preview_<?php echo $record[$row_id];?>" role="button" data-toggle="modal" id="<?php echo $record[$row_id]; ?>" class="poll_preview"><i class="icon-eye-open"></i> Poll Preview</a></li> 
                  <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>/poll_result/<?php echo $record[$row_id]; ?>?event_id=<?php echo $this->input->get('event_id');?>" id="<?php echo $record[$row_id]; ?>" class="poll_result"><i class="icon-bar-chart"></i> Poll Result</a></li> 
            <?php } ?>
        </ul>
    </div>
 

<?php } ?>