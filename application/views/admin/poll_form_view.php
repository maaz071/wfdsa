<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-globe"></i>
                            <h3>Add <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() ."poll/?event_id=".$this->input->get('event_id'); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                            <div class="control-row">											
                                                <label class="control-label">Event</label>
                                                <select class="span3" name="event_id">
                                                    <option value="">-Select-</option>
                                                    <?php foreach ($event as $value) { ?>
                                                        <option value="<?php echo $value['event_id']; ?>" <?php echo  $record_info[0]['event_id'] == $value['event_id'] ? 'selected="selected"' : ''; ?>><?php echo $value['title']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>  
                                        </div>     

                                        <div class="span5">    
                                             <div class="control-row"> 
                                                <label class="control-label">Poll Question</label>
                                                <div class="control-group"> 
                                                    <textarea class="form-control" name="poll_question" style="min-width: 63%;" rows="1"><?php echo (!empty($record_info) && !empty($record_info[0]['poll_question'])) ? $record_info[0]['poll_question'] : ''; ?></textarea>
                                                </div>  
                                            </div>  
                                        </div>   
                                        
                                       <div class="span12" style="margin-left:0px;"><legend>Answers</legend></div>
                                        <div class="span12" id="answer_div">
                                            

                                                <div class="control-group">											
                                                    <label class="control-label">Choice Selection</label>
                                                    <select class="span4" name="choice_selection">
                                                        <option value="">-Select-</option>
                                                        <option value="Single Choice" <?php echo (isset($record_info) && !empty($record_info[0]['choice_selection']) == "Single Choice") ? 'selected="selected"' : ''; ?>>Single Choice</option>
                                          <!--              <option value="Multiple Choice" <?php //echo (isset($record_info) && $record_info[0]['choice_selection'] == "Multiple Choice") ? 'selected="selected"' : ''; ?>>Multiple Choice</option>  -->
                                                    </select>
                                                </div>


                                            <?php if ($this->uri->segment(2) == 'create') { ?>
                                                <div class="control-group" id="answer_row_0">	
                                                    <label class="control-label">Answer</label>
                                                    <input type="text" name="answer[]"  id="answer" style="width:312px;">&nbsp;&nbsp;
                                                    <button type="button" href="#" class="btn btn-primary add_btn" id="0"><i class="icon icon-plus"></i></button>
                                                </div>    
                                            <?php } else { ?>
                                                <?php foreach ($poll_answer as $key => $value) { ?>
                                                    <div class="control-group" id="answer_row_<?php echo $key; ?>">	 
                                                    <label class="control-label"></label>
                                                        <input type="text" name="answer[]" id="answer" value="<?php echo $value['poll_answer']; ?>" style="width:312px;">&nbsp;&nbsp;
                                                        <?php if ($key == 0) { ?>
                                                            <button type="button" href="#" class="btn btn-primary add_btn" id="<?php echo count($poll_answer); ?>"><i class="icon icon-plus"></i></button>
                                                        <?php } else { ?>
                                                            <button type="button" href="#" class="btn btn-danger remove" id="<?php echo $key; ?>"  data-id = '<?php echo $value['poll_answer_id']; ?>' ><i class="icon icon-remove"></i></button>
                                                        <?php } ?>
                                                    </div> 
                                                <?php } ?>
                                            <?php } ?>

                                        </div>


                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?> 

                                        </div>

                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->