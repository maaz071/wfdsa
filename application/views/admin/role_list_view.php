<style>
    .toggle_div{ 
        display:none;
        margin:10px;
    }    
</style>

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary toggle_create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            <?php echo form_open(base_url() . ucfirst($this->uri->segment(1)) . "/create_action", 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                            <div class="alert" style="padding:5px; display:none;"  ></div>

                            <div class="toggle_div span12"> 
                                <label class="control-label span1" style="margin-top:5px; margin-left:0px;">Role Name</label> 
                                <div class="control-group">
                                    <input type="text" class="span3" name="role_name" value=""> 
                                    &nbsp;&nbsp;<a class="btn btn-primary submit">  Save </a>
                                </div> 

                            </div> 
                            <?php echo form_close(); ?>
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="99%">Role Name &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <tr id='addr0' data-id="0">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                <td><?php echo ($record['role_name']) ? $record['role_name'] : '-'; ?> </td> 
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->