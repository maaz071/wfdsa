  
<style>
    .move_custom{
        cursor:pointer;
        /*  font-size:20px;*/
    }

    .table-sortable {
        position: relative !important;
    }
    .table-sortable .sortable-placeholder {
        height: 37px !important;
    }
    .table-sortable .sortable-placeholder:after {
        position: absolute !important;
        z-index: 10 !important;
        content: " ";
        height: 37px !important;
        background: #f9f9f9 !important;
        left: 0;
        right: 0;
    }
    
    .control-group {
        margin-bottom: 0px;          
    }
    .dropdown-toggle {
        margin-left:11px !important;
    }
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget "> 
                        
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a href="JavaScript:void(0);" class="btn btn-primary" id="roster_submit"> Submit</a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"  ></div> 
                            
                            
                            
                            <select class="span3" id="roster_select">
                              <option value="" id="test">-Select Role-</option>
                              <?php if(!empty($option)) {?>
                                <?php foreach($option as $value) {?>
                                    
                                    <?php echo $value;?>
                                <?php } ?>
                              <?php } ?>
                            </select>

                            <table class="table table-bordered table-hover table-sortable" id="roster_table">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">Drag</th> 
                                        <th class="text-center" width="15%">Name <i class="icon-sort"></i></th>
                                        <th class="text-center" width="15%">Company <i class="icon-sort"></i></th>
                                        <th class="text-center" width="15%">Designation <i class="icon-sort"></i></th>
                                        <th class="text-center" width="15%">Cell <i class="icon-sort"></i></th>
                                        <th class="text-center" width="15%">Email <i class="icon-sort"></i></th>
                                    </tr>
                                </thead>
                                <tbody id="sort_body">
                                   <tr><td colspan="6">No Record Found</td></tr>  
                                </tbody>
                                <thead>
                                    <th colspan="9"></th> 
                                </thead>
                            </table>



                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->
                
                <br><br>



            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
