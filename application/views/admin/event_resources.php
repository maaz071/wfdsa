<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    .thumbnail:hover {
        background: #f7f7f7;
    }

    .thumbnail{
        margin-bottom: 20px;
    }
    .thumbnail a > img { 
        height: 98px;
        width: 137px;
        border: solid 1px #cccccc;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><span style="color:red;"><?php //echo $event[0]['event_name'] ? $event[0]['event_name'] : 0 ;?></span> Resources</h3>
                           
                          <a class="btn" href="<?php echo base_url() . "/event/view/".$this->input->get('event_id'); ?>" style="float:right; margin:6px;">Back</a> 

                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php if($this->session->flashdata('msg') != ''){?>
                                        <script>
                                            toastr.info("<?php echo $this->session->flashdata('msg');?>")
                                        </script>
                                        <?php }?>
                                   	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/addresource" .'" class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <button type="submit" class="btn btn-primary" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/test'; ?>"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span6" style="margin-left:0px !important;"> 
                                        	<div class="control-row">	  
                                                <label class="control-label">Resource Title</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" required name="title"> 
                                                </div>   
                                            </div>
                                            <div class="control-row">	  
                                                <label class="control-label">Browse file</label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="upload_image" multiple id="upload_image" accept="image/*"> 
                                                </div>   
                                            </div>
                                            <div class="control-row">	  
                                                
                                                <div class="control-group">
                        							<label class="control-label">File Link</label>
                                                    <input type="text" name="resource_link" placeholder="enter reource link (optional)"> <br> 
                                                </div>   
                                            </div>
                                        </div>  
                                     
                                        <input type="hidden" name="event_id" value="<?php echo $event_id ?>">
                                    </fieldset>

                                    <?php echo form_close(); ?>

                                    <style type="text/css">iframe{
                                                    		margin: 10px 30px;
                                                    	}</style>
                                     <div class="span12" style="margin-left:0px;">
                                                <br> 
                                                <legend><h3>Resources</h3> </legend>
                                                <div class="container-fluid">
                                                    
                                                    <div class="row">
                                                    	<?php foreach($resources as $row){?>
                                                    		<div style="position: relative;display: inline-block;">
                                                    		<a href="<?php echo base_url() . "/Event/delete_file?file=".$row['resource']."&id=".$row['id']."&type=".$row['resource_type']."&event_id=".$row['event_id']?>" style="    position: absolute;
    background: red;
    color: #fff;
    left: 13%;
    border: red;
    top: 4%;
    padding: 3px 9px"><i class="icon-trash"></i></a>
                                                    <iframe style="overflow: hidden;" id="ifram" width="180px" height="250px" src="<?php print_r($row['resource']);?>" frameborder="0" scrolling="no" allow="autoplay; encrypted-media" allowfullscreen >
                                                    </iframe> 
                                                </div>
                                                        <?php }?>
                                                    </div>
                                                </div>

                                            </div>
                                </div>  
                            </div>

<script>
	var img = jQuery('#ifram').contents().find('body').html();
	console.log(img)
</script>



                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->