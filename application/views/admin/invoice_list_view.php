<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
    
    .payment_status {
        text-decoration:none;
        border-bottom:none;
    }
    
    .popover-inner {
        background: rgb(245, 245, 245);
    }
</style>

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-flag"></i>
                            <h3><?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></a>
                                <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            
                              <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="15%">Member Name &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Title &nbsp;<i class=""></i></th>
                                         <th class="text-center" width="5%">Payment Status &nbsp;<i class=""></i></th> 
                                         <th class="text-center" width="5%">Payment Amount &nbsp;<i class=""></i></th> 
                                         <th class="text-center" width="5%">Status &nbsp;<i class=""></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { 
                                            $count = 0;
                                        ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>"></td>
                                                <td><?php echo ($record['first_name']) ? $record['first_name']." ".$record['last_name'] : '-'; ?></td>
                                                 <td><center><?php echo ($record['title']) ? $record['title'] : '-'; ?></center></td>
                                                 <td><center>
                                                     <?php if($record['payment_status'] == 0) {?>
                                                    <a href="#" id="payment_status_<?php echo $count?>" data-type="select" data-pk="<?php echo $record[$row_id]; ?>" data-url="<?php echo base_url('invoice/update_payment_status');?>" data-source="{1: 'Paid', 0: 'Un-Paid'}" data-title="Select Status">
                                                        Unpaid
                                                    </a>
                                                    <?php } else { ?>
                                                     <?php echo ($record['payment_status'] == 1) ? "Paid" : "Unpaid"; ?> 
                                                    <?php } ?>
                                                    </center>
                                                </td>
                                                <td style="text-align:right;">$ <?php echo ($record['grand_total']) ? number_format($record['grand_total'],2) : '-'; ?></td>
                                                <td><center><?php echo ($record['status'] == 1) ? 'Active' : 'Inactive'; ?></center></td>
                                                 
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                    <?php  $count++; } ?>
                                <?php } ?>
                                  <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->