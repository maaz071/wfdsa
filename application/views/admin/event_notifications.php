<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">


        	<div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-globe"></i>
                            <h3><span style="color:red;"><?php //echo $event[0]['event_name'] ? $event[0]['event_name'] : 0 ;?></span> WFDSA Event Notifications</h3>
                           
                          <a class="btn" href="<?php echo base_url() . "/event/view/".$this->input->get('event_id'); ?>" style="float:right; margin:6px;">Back</a> 

                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php if($this->session->flashdata('msg') != ''){?>
                                        <script>
                                            toastr.info("<?php echo $this->session->flashdata('msg');?>")
                                        </script>
                                        <?php }?>
                                   	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/addnotification" .'" class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <button type="submit" class="btn btn-primary" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/test'; ?>"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span6" style="margin-left:0px !important;"> 
                                        	<div class="control-row">	  
                                                <label class="control-label">Notification</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" required name="title"> 
                                                </div>   
                                            </div>
                                        </div>  
                                     
                                        <input type="hidden" name="event_id" value="<?php echo $event_id ?>">
                                    </fieldset>

                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--first row ends-->



            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-globe"></i>
                            <h3>WFDSA Event Notifications</h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                                
                            </div>
                            
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            
                            <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="30%">Event Title &nbsp;<i class="icon-sort sort_icon"></i></th>
                                        <th class="text-center" width="70%">Notification  &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>   
                                    <?php if (!empty($notifications)) { ?>
                                        <?php foreach ($notifications as $record) { ?>
                                             
                                            <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            
                                        
                                        
                                        
                                            <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                               <td><?php echo ($record['event_title']) ? $record['event_title'] : '-'; ?> </td>
                                                <td><?php echo ($record['notification']) ? $record['notification'] : '-'; ?> </td> 
                                                  
                                                <td> <a href="<?php echo base_url() . $this->uri->segment(1) . '/delete_single_notification?noti_id='.$record['id'].'&event_id='.$event_id; ?>"><i class="icon-trash"></i> </a> </td> 
                                            </tr>
                                            
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr class="warning no-result">
                                        <td colspan="4"><i class="fa fa-warning"></i> No Result Found</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete_notifications?event_id='.$event_id; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>

    


                          <br><br>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->
              


            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->