<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    textarea.form-control {
        margin-top:2%;
        height: 64px !important;
        width:400px;
        resize: vertical;
    }
    .table-bordered th {
      background: -webkit-linear-gradient(top, #f3f3f3 0%,#ffffff 100%) !important;
    }
    
    .text_right {
        text-align:right !important;
    }
    .mybutton{
        margin-left:94%; 
        margin-bottom:10%; 
         
        background-color:#21a9ec; 
        color:white;
        padding: 4px 8px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 14px;
        transition-duration: 0.4s;
    }
</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-flag"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?> <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></h3>
                           
                             
                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            
                            
                            
                               
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <input type="submit" value="submit" class="mybutton" style=""><br>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    
                                    <fieldset> 

                                        <div class="span6">

                                            <div class="control-row">											
                                                <label class="control-label">Member Name<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <select class="span3" name="member_id" required>
                                                        <option disabled value="">-Select-</option>
                                                        <?php foreach($member as $value) {?>
                                                            <option value="<?php echo $value['member_id'];?>" <?php echo (!empty($record_info) && $record_info[0]['member_id'] == $value['member_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['first_name']." ".$value['last_name'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div> 
                                            </div>  
                                            
                                            
                                            <?php if($this->uri->segment(2) == 'edit') { ?>    
                                                <div class="control-row">											
                                                    <label class="control-label">Payment Status</label>
                                                    <div class="control-group"> 
                                                            <select class="span3" name="payment_status"> 
                                                                <?php if(!empty($record_info) && $record_info[0]['payment_status'] == 0) { ?>
                                                                    <option value="">-Select-</option>
                                                                    <option value="1" <?php echo ($record_info[0]['payment_status'] == 1) ? 'selected="selected"' : ''; ?>>Paid</option>
                                                                    <option value="0" <?php echo ($record_info[0]['payment_status'] == 0) ? 'selected="selected"' : ''; ?>>Un-Paid</option>
                                                                <?php } else if(!empty($record_info) && $record_info[0]['payment_status'] == 1) { ?>
                                                                    <option value="1" <?php echo ($record_info[0]['payment_status'] == 1) ? 'selected="selected"' : ''; ?>>Paid</option>
                                                                <?php } else {?>
                                                                    <option value="1">Paid</option>
                                                                    <option value="0">Un-Paid</option>
                                                                <?php } ?>
                                                            </select> 
                                                          
                                                    </div> 
                                                </div>
                                            <?php } ?>

                                            <div class="control-row">											
                                                <label class="control-label">Title<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="title" value="<?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>" required> 
                                                </div> 
                                            </div> 
                                            
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Description<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="description" value="<?php echo (isset($record_info) && $record_info[0]['description']) ? $record_info[0]['description'] : ''; ?>"required> 
                                                </div> 
                                            </div> 
                                            
                                           

                                        </div> 

                                        <div class="span5">
                                            <div class="control-row">  
                                                <div class="control-group"> 
                                                    <textarea class="form-control" placeholder = "Remarks" name="remarks" ><?php echo (isset($record_info) && $record_info[0]['remarks']) ? $record_info[0]['remarks'] : ''; ?></textarea>
                                                </div>  
                                            </div> 
                                            
                                            <br><br>
                                        </div>  
                                      
                                        <div class="span11 no-margin"> 
                                                 
                                             <a class="btn btn-primary add_invoice_row" style="float:right;">Add Row &nbsp; <i class="icon-plus"></i> </a> <br><br>

                                           
                                                
                                                <table class="table table-bordered table-hover table_item">
                                                <thead>
                                                    <tr class="sortable"> 
                                                        <th class="text-center" width="5%"><center>Name</center></th>
                                                        <th class="text-center" width="5%"><center>Quantity</center></th> 
                                                        <th class="text-center" width="5%"><center>Price </center></th> 
                                                        <th class="text-center" width="10%"><center>Total</center></th> 
                                                        <th class="text-center" width="1%"><center>Action</center></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="invoice_body"> 
                                                <?php if ($this->uri->segment(2) == 'create') { ?>
                                                    <tr id='invoice_row_0'>
                                                        <td><center><input type="text" class="span2" name="name[]"></center></td>
                                                        <td><center><input type="number" class="span2 change_value" id="qty" name="qty[]"></center></td>
                                                        <td><center><input type="number" class="span2 change_value" id="price" name="price[]"></center></td>
                                                        <td class="total_row text_right">0</td><input type="hidden" class="total_row_value" name="total_row[]" value="">
                                                        <td><center><a class="btn btn-danger remove_invoice_row" style="float:right;"><i class="icon-remove"></i> </a></center></td>  
                                                     </tr>  
                                                <?php } else { ?>
                                                    <?php if(!empty($invoice_item)) {?>
                                                        <?php foreach ($invoice_item as $key => $value) { ?>
                                                            <tr id="invoice_row_'<?php echo $key;?>'">
                                                                 <td><center><input type="text" class="span2" name="name[]" value="<?php echo $value['name'];?>"></center></td>
                                                                 <td><center><input type="text" class="span2 change_value" id="qty" name="qty[]" value="<?php echo $value['qty'];?>"></center></td>
                                                                 <td><center><input type="text" class="span2 change_value" id="price" name="price[]" value="<?php echo $value['price'];?>"></center></td> 
                                                                 <td class="total_row text_right">(<span style="font-weight:bold;"> $ </span>) <?php echo number_format($value['total_row'],2);?></td><input type="hidden" class="total_row_value" name="total_row[]" value="<?php echo $value['total_row'];?>">
                                                                 <td><center><a class="btn btn-danger remove_invoice_row" style="float:right;"><i class="icon-remove"></i> </a></center></td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>     
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="3" class="text_right"><b>TOTAL</b></td>
                                                        <td id="total_main" class="text_right"><b>(<span> $ </span>) <?php echo (isset($record_info) && $record_info[0]['grand_total']) ? $record_info[0]['grand_total'] : ''; ?></b></td>
                                                        <input type="hidden" class="grand_total" name="grand_total" value="<?php echo (isset($record_info) && $record_info[0]['grand_total']) ? $record_info[0]['grand_total'] : ''; ?>">
                                                        <td></td>
                                                    </tr>
                
                                                </tfoot> 
                                            </table>  
                                        </div>
 
                                        <?php if($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                            
                                        <?php } ?>
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>
 
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner --> <br><br>

</div> <!-- /main -->