<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?>  <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit_product_form" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 
                                        <div class="span6"> 
                                             
                                             
                                            <div class="control-row">											
                                                <label class="control-label">First Name</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" required name="first_name" value="<?php echo (isset($record_info) && $record_info[0]['first_name']) ? $record_info[0]['first_name'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>  
                                            
                                             <div class="control-row">											
                                                <label class="control-label">Last Name</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="last_name" value="<?php echo (isset($record_info) && $record_info[0]['last_name']) ? $record_info[0]['last_name'] : ''; ?>"> 
                                                </div>   
                                            </div>
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Contact #</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="contact_no" value="<?php echo (isset($record_info) && $record_info[0]['contact_no']) ? $record_info[0]['contact_no'] : ''; ?>"> 
                                                </div>  
                                            </div> 
                                            
                                        </div>   
                                            <div class="span5">   
                                            
                                                <div class="control-row">											
                                                    <label class="control-label">Email</label>
                                                    <div class="control-group">
                                                        <input type="text" class="span3" name="email" value="<?php echo (isset($record_info) && $record_info[0]['email']) ? $record_info[0]['email'] : ''; ?>"> 
                                                    </div>  
                                                </div>
                                                
                                                
                                                <div class="control-row"> 
                                                    <label class="control-label">Password</label>
                                                    <div class="control-group"> 
                                                     <input type="password" class="span3" name="password" value="<?php echo (isset($record_info) && $record_info[0]['password']) ? $record_info[0]['password'] : ''; ?>"> 
                                                    </div>  
                                                </div>  
                                                
                                                 <div class="control-row">											
                                                    <label class="control-label">Country</label>
                                                    <div class="control-group">
                                                        <select class="span3" name="country">
                                                            <option value="">-Select-</option>
                                                            <?php foreach ($country as $value) { ?>
                                                                <option value="<?php echo $value['country_id']; ?>" <?php echo (isset($record_info) && $record_info[0]['country'] == $value['country_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>  
                                                            <?php } ?>
                                                        </select>
                                                    </div>  
                                                </div> 
                                                
                                            </div>
                                            
                                            
                                            
                                        </div> 
                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo lcfirst($this->uri->segment(1)); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?>  
                                        </div>
                                         
                                    </fieldset>
                                    <?php echo form_close(); ?> 
                                   
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->