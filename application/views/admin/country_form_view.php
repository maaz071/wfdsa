<style>
    .buttons{
        width: 12%;
        float: right; 
    }

   

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-flag"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?> <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></h3>
                           
                             
                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit_product_form" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span6">

                                            <div class="control-row">											
                                                <label class="control-label">Country Name </label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="name" value="<?php echo (isset($record_info) && $record_info[0]['name']) ? $record_info[0]['name'] : ''; ?>"> 
                                                </div> 
                                            </div>   

                                            <div class="control-row">	  
                                                <label class="control-label">Flag Image</label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="flag_pic" id="flag_pic"> 
                                                </div>   
                                            </div>   

                                        </div> 

                                        <div class="span5">
                                            <div class="control-row">											
                                                <label class="control-label">Zip Code</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="country_zip_code" value="<?php echo (isset($record_info) && $record_info[0]['country_zip_code']) ? $record_info[0]['country_zip_code'] : ''; ?>"> 
                                                </div> 
                                            </div> 
                                             <?php if (isset($record_info[0]['flag_pic']) && !empty($record_info[0]['flag_pic'])) { ?> 
                                                <label class="control-label">Image Preview</label>
                                                <div class="control-group">
                                                    <img src="<?php echo base_url() . $record_info[0]['flag_pic']; ?>" width="25%" height="25%">
                                                </div> 
                                            <?php } ?> 
                                        </div>
 

                                        <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>
 
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->