<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
	.success {
        background-color: #ddffdd;
        border-left: 6px solid #4CAF50;
        /*width:80%;
        margin-left:3%;*/
		padding: 8px 35px 8px 14px;
		margin-bottom:10px;
    }
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		
                    
                    <?php if($this->session->flashdata('msg')): ?>
                        <div class="success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>  		

                    <div class="widget">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                               <!-- <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/bulkemail">  Send username password to all memeber</a> -->
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>
                                 <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
    
                            <div class="alert" style="padding:5px; display:none;"  ></div>  
                             
                             <select id="role_sort" class="select_go_url" data-id="<?php echo base_url().$this->uri->segment(1)."?role_id="; ?>">
                                 <option value=""><?php //echo (!empty($role_selection)) ? $role_selection : '-Select By Role-'; ?>-Select By Role-</option>
                                 <?php if(!empty($roles_select)) { ?>
                                     <?php foreach($roles_select as $value) {?>
                                        <option value="<?php echo $value['member_role_id']; ?>" disabled="disabled" style="font-weight:bold; color:black;"><?php echo $value['name'];?></option>
                                         <?php foreach($value['child'] as $value1) {?>
                                             <option value="<?php echo $value1['name'];?>"  <?php echo ($this->input->get('role_id') == $value1['name']) ? 'selected="selected"' : ''; ?>>&nbsp;&nbsp;<?php echo $value1['name'];?></option>
                                         <?php } ?>
                                     <?php } ?>
                                 <?php } ?>
                             </select>
                            
                             <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="5%">Title &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="8%">First Name &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Last Name &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Email &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Company &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Designation &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Cell &nbsp;<i class=""></i></th>
                                         <th class="text-center" width="10%">Role &nbsp;<i class=""></i></th> 
                                         <!--<th class="text-center" width="5%">Status &nbsp;<i class="icon-sort sort_icon"></i></th> -->
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>    
                                    <?php if (!empty($record_list1)) { ?>
                                        <?php foreach ($record_list1 as $record) { ?>
                                        
                                             <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            
                                            
                                            <?php //if(!empty($roles[$record['member_id']])) { ?>
                                                <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                    <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                     <td> <?php echo ($record['title']) ? $record['title'] : '-'; ?> </td>
                                                    
                                                    <td> <?php echo ($record['first_name']) ? $record['first_name'] : '-'; ?> </td>
                                                    <td> <?php echo ($record['last_name']) ? $record['last_name'] : '-'; ?> </td>
                                                    <td> <?php echo ($record['email']) ? $record['email'] : '-'; ?> </td>
                                                    <td><center> <?php echo ($record['company']) ? $record['company'] : '-'; ?></center> </td>
                                                    <td><center> <?php echo ($record['designation']) ? $record['designation'] : '-'; ?></center> </td>
                                                    <td><center> <?php echo ($record['cell']) ? $record['cell'] : '-'; ?></center> </td>
                                                     <td> 
                                    <?php echo ($record['role']) ? $record['role'] : '-'; ?>                   
                                                          
                                                     </td>
                                                     <!--<td><center> <?php echo ($record['status'] == 1) ? 'Active' : 'Inactive'; ?></center> </td>-->
                                                    <td> <?php  require(APPPATH.'views/admin/crud_btn.php'); ?>  </td> 
                                                </tr>
                                            <?php //} ?>
                                                
                                            
                                            
                                               
                                            
                                            
                                    <?php } ?>
                                    
                                <?php } else {?>
                                <tr class="warning no-result">
                                    <td colspan="10"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="11"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1).'/delete'; ?>" ><i class="icon-trash"></i></button>
                                <div class="pagination pull-right">
                                        <?php echo $pagination; ?>
                                    </div>
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
<script type="text/javascript">
window.setTimeout(function() {
    $(".success").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>