<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
</style>

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><span style="color:red;"><?php echo (!empty($event_name)) ? $event_name : ''; ?></span>&nbsp; Gallery</h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></a>
                                <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                                <?php if($this->input->get('event_id')) {?>
                                   <a href="<?php echo base_url('event/view/'.$this->input->get('event_id'))?>" class="btn btn-default" style="float:right; margin:8px;">Back</a>
                                <?php } ?>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            
                            <!-- <div class="control-row">	 
                                <div class="control-group">
                                    <select class="select_go_url"  data-id="<?php echo base_url('event_gallery')."?event_id=";?>">
                                        <option value="">-Select Event-</option>
                                         <?php //foreach($events as $value) { ?>
                                             <option value="<?php echo $value['event_id'];?>" <?php echo ($value['event_id'] == $event_id) ? 'selected="selected"' : '' ;?>><?php echo $value['title'];?></option>
                                         <?php// } ?>
                                    </select>
                                </div> 
                            </div> -->
                            
                            <div class="alert" style="padding:5px; display:none;"></div>
                            <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="50%">Event Title &nbsp;<i class="icon-sort sort_icon"></i></th>
                                        <th class="text-center" width="10%">Gallery Name &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>   
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            
                                            <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            
                                            <tr id='addr0' data-id="0"  style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                <td><?php echo ($record['event_name']) ? $record['event_name'] : '-'; ?> </td>
                                                <td><?php echo ($record['gallery_title']) ? $record['gallery_title'] : '-'; ?> </td>
                                                 
                                                 
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                    <?php } ?>
                                <?php } ?>
                                  <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->