<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>
<?php if($this->session->flashdata('success') != ''){?>
                                        <script>
                                            toastr.info("<?php echo $this->session->flashdata('success');?>")
                                        </script>
                                        <?php } elseif($this->session->flashdata('fail') != ''){?>
                                            <script>
                                            toastr.error("<?php echo $this->session->flashdata('fail');?>")
                                        </script>
                                            <?php }?>
<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3>Photo Albums</h3>
                             <a class="btn" href="http://www.wfdsamobileportal.com/gallery" style="float:right; margin:6px;">Back</a>
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="container">
                                <div class="row">
                            <?php echo form_open_multipart(base_url() . 'gallery/createalbum', 'class="form-horizontal"'); ?>
                                    
                                <div class="alert" style="display:none;" role="alert" ></div>
                                    
                            <div class="tabbable">
                                <div class="success" id="success1" style="display:none;">
                                  <p><strong>Success!</strong> Album Added Successfully,Wait Until Page Refresh...</p>
                                </div>
                                    
                                <div class="failed1" id="failed1" style="display:none;">
                                  <p><strong>failed!</strong> Please fill out all Fields...</p>
                                </div>

                                <div class="tab-pane" id="formcontrol-group">
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                            <div class="control-row">                                           
                                                <label class="control-label">Title<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required id="title" type="text" class="span3" name="title" value=""> 
                                                </div> <!-- /control-group -->    
                                            </div>
                                            <input type="hidden" name="album_type" value="0">
                                        <!--permission view copied from events-->
                                                <label class="control-label">Album User Type<span style="color:red">*</span></label>
                                                 
                                                
                                                <select class="span3 select_append" name="permission[]" multiple
                                                        style="width:47%; min-height:200px; " id="permission">
                                              <!--      <option value="<?php //echo $value['name'];?>">-Select Role-</option> -->
                                              <option value="Public" selected="selected">Public</option>
                                                    <?php foreach ($roles as $value) { ?>
                                                        <option disabled="disabled"
                                                                value="<?php echo $value['member_role_id'];
                                                                //echo $value['name'];?>"
                                                                style="font-weight:bold; color:black;"> <?php echo $value['name']; ?> </option>

                                                        <?php if (!empty($value['child'])) { ?>
                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                <option
                                                                    value="<?php echo $value2['name'];?>"> <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            <!-- </div>   -->
                                            <div class="alert alert-error" id="errorname" style="display:none;">
                                            <span>
                                             <p>*Please Select Memebers</p>
                                                 </span>
                                                  </div>  



                                        </div>    
                                           

                                        <div class="span5">   

                                            <div class="control-group">    
                                                <label class="control-label">Album Cover<p style="font-size:7ox;">(max file size is 1Mb)</p></label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="fileToUpload" id="fileToUpload" accept="image/*">
                                                </div>   
                                            </div>
                                             <input type="submit" class="btn btn-primary "
                                style="float:right; margin:3px auto;" value="submit" id="success" />
                                        </div>
                                    </fieldset>
                                    
                                </div>  
                            </div>
                            <?php echo form_close(); ?>
                                </div>
                                <div class="row">   
                                    <div class="span12">
                                    <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if($photoalbums != 'false'){?>
                                    <?php foreach($photoalbums as $row){?> 
                                        <a href="<?php echo base_url()?>/gallery/selectalbum/<?php echo $row['album_id']?>-photo">
                                            <div class="span3" style="border:1px solid grey;height: 250px;    background: #f1f1f1;line-height: 10px">
                                                <img src="<?php echo $row['album_cover']?>" style="width: 95%;
                                        border: 1px solid #cecece;
                                        display: block;
                                        margin: 8px auto;
                                        height: 150px;background-color: #ffff"/>
                                        <span  style="font-size: 9px"><b>For: </b><?php echo $row['album_permission']?></span>
                                    <a href="<?php base_url()?>/gallery/delalbum/<?php echo $row['album_id']?>" style="float: right;color: red"><i class="icon-remove" style="padding: 5px 5px"></i></a>
                                                <h6 style="text-align: center"><?php echo $row['album_title']?></h6>
                                            </div>
                                        </a>
                                    <?php }?>
                                    <?php }
                                    else{?>
                                        <div class="span3">
                                            <h4>No photo albums to show</h4>
                                        </div>
                                        <?php }?>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>