<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    #search_div{ display:none;}
	.success {
        background-color: #ddffdd;
        border-left: 6px solid #4CAF50;
        /*width:80%;
        margin-left:3%;*/
		padding: 8px 35px 8px 14px;
		margin-bottom:10px;
    }
</style>

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <?php if($this->session->flashdata('msg')): ?>
                        <div class="success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>  		

                    <div class="widget">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;"> 
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>
                               <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                                
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            
                            <div class="alert" style="padding:5px; display:none;"  ></div>
                              <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            <div class="form-group pull-left" id="search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>

                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="20%" >Title</th>
                                        <th class="text-center" width="10%" > Parent Category</th>
                                        <th class="text-center" width="10%" >Category</th> 
                                        <th class="text-center" width="5%">Permission</th> 
                                        <th class="text-center" width="5%">File</th> 
                                        <th class="text-center" width="5%">Status</th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                          <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                <td><?php echo ($record['title_2']) ? $record['title_2'] : '-'; ?> </td> 
                                                <td><?php echo ($record['parent_category_name']) ? $record['parent_category_name'] : '-'; ?> </td>
                                                <td><?php echo ($record['category_name']) ? $record['category_name'] : '-'; ?> </td> 
                                                <td><?php echo ($record['resource_member']) ? $record['resource_member'] : '-'; ?> </td>
                                                <td><a href="<?php echo ($record['upload_file']) ? $record['upload_file'] : '-'; ?>">Download File</a> </td>
                                                <td><?php echo ($record['status'] == 1) ? 'Active' : 'Inactive'; ?></td>
                                                 
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td colspan="7"><i class="fa fa-warning"></i> No result</td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                 <div class="pagination pull-right">
                                                                               <?php echo $pagination; ?>
                                                                           </div>
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->
    
</div> <!-- /main -->
<script type="text/javascript">
window.setTimeout(function() {
    $(".success").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>