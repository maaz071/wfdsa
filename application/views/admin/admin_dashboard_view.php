 
<div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span12">  
                    
                      <div class="widget widget-nopad">
                            <div class="widget-header"> <i class="icon-list-alt"></i>
                              <h3> Members</h3>
                            </div>
                            
                            <div class="widget-content">  
                               <div class="row-fluid stats-box" style="margin-bottom: 15px; margin-top: 20px;">
                                   
                                  <?php extract($number_user); ?>
                                  <div class="span4" style="border-right: 1px solid #CCC;">
                                  	<div class="stats-box-title">App Users</div>
                                    <div class="stats-box-all-info"><i class="icon-user" style="color:#3366cc; "  ></i> <?php echo  $member+$non_member;?></div>
                                  </div>
                                  
                                  <div class="span4" style="margin-left:0px;  border-right: 1px solid #CCC;">
                                    <div class="stats-box-title">Members</div>
                                    <div id="mem" class="stats-box-all-info" onclick="location.href='<?php echo base_url('member'); ?>';" style="cursor: pointer;"><i class="icon-user" style="color:#b6cc33;"></i> <?php echo (!empty($member)) ? $member : 0;?></div>
                                  </div>
                                  
                                  <div class="span4">
                                    <div class="stats-box-title">Non-Members</div>
                                    <div id="nonmem" class="stats-box-all-info" onclick="location.href='<?php echo base_url('non_member'); ?>';" style="cursor: pointer;"><i class="icon-user" style="color:#cc7b33;"></i> <?php echo (!empty($non_member)) ? $non_member : 0;?></div>
                                  </div>
                               </div> 
                            </div>
                        </div> 
                    </div> 
                    
                    <div class="span5">
                        <div class="widget widget-nopad">
                            <div class="widget-header"> <i class="icon-list-alt"></i>
                              <h3> Recent Announcements</h3>
                                <a href="<?php echo base_url('announcement');?>" class="btn btn-default" style="float:right; margin-top:6px; margin-right:6px;">View All</a> 
                            </div>
                            <!-- /widget-header -->
                            <div class="widget-content">
                              <ul class="news-items">
                               <?php if(!empty($announcement)) {?>      
                                    <?php foreach($announcement as $value) {?>
                                        <li>
                                            
                                          <?php
                                          
                                            list($date,$time) = explode(' ',$value['date']);
                                          
                                          
                                          
                                          $test1 = str_replace("-","/",$value['date']);
                                          
                                          
                                          
                                            $date = date('M/d/Y',strtotime($test1));
                                           
                                            list($month,$day,$year) = explode('/',$date);
                                          ?>
                                          <div class="news-item-date"> <span class="news-item-day"><?php echo (!empty($date)) ? $day : '29'; ?></span> <span class="news-item-month"><?php echo (!empty($date)) ? $month : 'Aug'; ?></span> <span class="news-item-month"><?php echo (!empty($date)) ? $year : '0000'; ?></span> <span class="news-item-month"><?php echo (!empty($date)) ? $time : '00:00:00'; ?></span></div>
                                          <div class="news-item-detail"> <a href="<?php echo base_url('announcement/edit/'.$value['announcement_id']);?>" class="news-item-title" target="_blank"><?php echo $value['title'];?></a>
                                            <p class="news-item-preview"><?php echo $value['announcement_message'];?></p>
                                          </div>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                        <center>
                                            <li>
                                              <div class="news-item-detail"><a href="#" class="news-item-title">No Announcements Announced Yet</a></div>
                                            </li>
                                        </center>
                                <?php } ?>
                              </ul> 
                            </div>
                            <!-- /widget-content --> 
                           
                          </div><br><br>
                         
                        </div> 
                        
                        <div class="span7">
                             <div class="widget">
                                <div class="widget-header"> <i class="icon-list-alt"></i>
                                  <h3>Recent Events</h3>
                                  <a href="<?php echo base_url('event');?>" class="btn btn-default" style="float:right; margin-top:6px; margin-right:6px;">View All</a> 
                                </div>
                                <!-- /widget-header -->
                                <div class="widget-content">
                                  <ul class="messages_layout">
                                    <?php if(!empty($events)) {?>
                                        <?php foreach($events as $value)  {?>
                                            <li class="from_user"> <a href="#" class="avatar"><img src="img/message_avatar1.png"/></a>
                                              <div class="message_wrap"> <span class="arrow"></span>
                                                <div class="info"> <a href="<?php echo base_url('event/view/'.$value['event_id']);?>" class="name"><?php echo $value['title'];?></a> <span class="time"><?php
                                                $testing = str_replace("-","/",$value['start_date']);
                                                $date = date('d-M-Y',strtotime($testing));
                                                
                                                echo $date;?></span>
                                                   
                                                </div>
                                           <div class="text">A new Event Has Been added and the agenda of event is:<br><?php echo $value['agenda'];?></div>
                                              </div>
                                            </li> 
                                            
                                        <?php } ?>
                                        
                                    <?php } ?>
                                  </ul>
                                </div>
                                <!-- /widget-content --> 
                              </div>
                        </div>
                        
                        
                        <div class="span12">
                             <div class="widget">
                                <div class="widget-header"> <i class="icon-list-alt"></i>
                                  <h3> Recent Resouces</h3>
                                   <a href="<?php echo base_url('resources');?>" class="btn btn-default" style="float:right; margin-top:6px; margin-right:6px;">View All</a> 
                                </div>
                                <!-- /widget-header -->
                                <div class="widget-content">
                                  <table class="table table-striped table-bordered">
                                    <thead>
                                      <tr>
                                        <th width="20%"> Resource Title </th>
                                        <th width="5%"> Permission </th>
                                        <th width="10%"> File</th>
                                        <th width="5%"> Date </th>
                                         
                                      </tr>
                                    </thead>
                                    <tbody>
                                     <?php if(!empty($resources)) {?>
                                          <?php foreach($resources as $value) {?>
                                              <tr>
                                                <td> <?php echo $value['title_2']; ?></td>
                                                <td><center> <?php echo $value['resource_member']; ?></center></td> 
                                                <td><center> <a href="<?php echo $value['upload_file']; ?>">Download File</a></center> </td> 
                                                <td><center> <?php echo date("d-M-Y", strtotime($value['created_on']));?></center></td> 
                                              </tr> 
                                          <?php } ?>
                                      <?php } ?>
                                    </tbody>
                                  </table>
                                </div>
                                <!-- /widget-content --> 
                              </div><br><br><br><br>
                        </div>
                  
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <script type="text/javascript">
      function codeaddress() {
     var a = parseInt(jQuery( "#mem" ).text());
     var b =  parseInt(jQuery( "#nonmem" ).text());
     var c = (a+b);
     jQuery( "#test" ).text(c);
    }
    window.onload = codeaddress;
    </script>