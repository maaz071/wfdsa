<style>
    
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    .thumbnail:hover {
        background: #f7f7f7;
    }

    .thumbnail{
        margin-bottom: 20px;
    }
    .thumbnail a > img { 
        height: 98px;
        width: 137px;
        border: solid 1px #cccccc;
    }
.thumb {
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px;
    width: 200px;
    height: 200px
}

.thumb:hover {
    box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}
</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?> <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></h3>
                           
                            
                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit_product_form" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span6"> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Title </label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="title" value="<?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>"> 
                                                </div> 
                                            </div>  

                                     <div class="control-row"">
                        <label class="control-label">Preview test</label>
                         <div  id="thumb-output" ></div>

                        <video   id="thumb-output" controls width="300" height="350">
                        <source src="<?php echo (isset($record_info) && $record_info[0]['upload_video']) ? $record_info[0]['upload_video'] : ''; ?>" type="video/mp4">
                        </video><br/>
                       <div class="form-group col-md-12">
                       


                        <label class=" control-label" for="icon">Add Icon</label>

                            <input type="file" name="upload_video" id="upload_video" required class="form-control input-md">

                        <span id="filefaild" class="help-block hidden">File Height Max < 555 and Width < 555</span>

                    </div>                        



                                            <!--  <video class="thumb"   src="<?php echo base_url(); ?>/assets/images/test.mp4" type="video/mp4" controls></video><br/>
    -->

                                   <!--        <div class="control-row">	  
                                                <label class="control-label">Upload Video</label>
                                                <div class="control-group">
                                                    <input type="file" class="span2" name="upload_video"  id="upload_video"> 
                                                    <?php if(isset($record_info[0]['upload_video']) && $record_info[0]['upload_video'] != "")  { ?>
                                                     <a href="<?php echo $record_info[0]['upload_video']; ?>" class="btn btn-success"><i class="icon icon-download-alt"></i></a>
                                                    <?php } ?>
                                                </div>   
                                            </div>  -->



                                        </div> 

                                        <div class="span5">
                                            <div class="control-row" id="hideme">  
                                                <div class="control-group"> 
                                                    <textarea value="test123"  class="form-control" name="description" style="display:none; min-width: 95%;" rows="3" >test<?php //echo (isset($record_info) && $record_info[0]['description']) ? $record_info[0]['description'] : ''; ?></textarea>
                                                </div>  
                                            </div>
                                        </div>

                                         

                                        <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->



<script type="text/javascript">

    $(document).ready(function(){

    $('#upload_video').on('change', function(){ //on file input change
        if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
        {
            $('#thumb-output').html(''); //clear html of output element
            var data = $(this)[0].files; //this file data
            
            $.each(data, function(index, file){ //loop though each file
                if(/(\.|\/)(wmv|mp4|png)$/i.test(file.type)){ //check supported file type
                    var fRead = new FileReader(); //new filereader
                    fRead.onload = (function(file){ //trigger function on successful read
                    return function(e) {
                        var video = $('<video/>').addClass('thumb').attr('src', e.target.result); //create image element 
                        $('#thumb-output').append(video); //append image to output element
                    };
                    })(file);
                    fRead.readAsDataURL(file); //URL representing the file's data.
                }
            });
            
            console.log('video uploaded');
        }else{
            alert("Your browser doesn't support File API!"); //if File API is absent
        }
    });
});
</script>