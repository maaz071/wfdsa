
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; <?php echo date('Y');?> <a href="#">World Federation of Direct Selling Association</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(); ?>assets_admin/js/jquery-1.7.2.min.js"></script> 

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->  

<!--<script src="<?php echo base_url(); ?>assets_admin/js/excanvas.min.js"></script> -->
<!--<script src="<?php echo base_url(); ?>assets_admin/js/chart.min.js"></script> -->
<script src="<?php echo base_url(); ?>assets_admin/js/bootstrap.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/base.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/full-calendar/fullcalendar.min.js"></script> 

<script src="<?php echo base_url(); ?>assets_admin/js/ckedit/ckeditor.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/ckedit/config.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/ckedit/sample.js"></script> 

<script src="<?php echo base_url(); ?>assets_admin/js/datepicker/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>assets_admin/js/bootbox.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets_admin/js/jquery.sortElements.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets_admin/js/jquery.sortable.js"></script>-->
<script src="<?php echo base_url(); ?>assets_admin/js/sortable/jquery-ui-1.10.4.custom.min.js"></script>


<script src="<?php echo base_url(); ?>assets_admin/js/cropper/jquery.imgareaselect.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/cropper/jquery.form.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/js/editable/bootstrap-editable.min.js"></script> 
<script src="<?php echo base_url(); ?>assets_admin/cropper-master/dist/cropper.js"></script> 
<!-- <script src="<?php echo base_url(); ?>assets_admin/js/cropper/modal.js"></script>  -->
<!-- <script src="<?php echo base_url(); ?>assets_admin/js/cropper/functions.js"></script>  -->

 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/charts_custom/fusioncharts.js"></script>    
 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/charts_custom/fusioncharts.theme.fint.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/charts_custom/fusioncharts-jquery-plugin.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/charts_custom/fusioncharts.charts.js"></script>
 
 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/password_prev/jquery.prevue.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/password_prev/prettify.js"></script>

<?php require_once( APPPATH.'views/custom_js/main_js.php');   ?>
<?php require_once(APPPATH.'views/custom_js/function.php'); ?> 
 
  
 
</body>
</html>