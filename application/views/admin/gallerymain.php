<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3>Choose Album Type</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="container">
                                <div class="row">   
                                    <div class="span2">
                                    </div> 
                                    <div class="span4">
                                        <div class="card">
                                            <a href="<?php echo base_url()?>/gallery/videoalbum">
                                                <div class="card-image">
                                                    <img src="<?php echo base_url()?>/assets_admin/images.jpeg"/>
                                                </div>
                                                <div class="card-content" style="text-align: center">
                                                <h3 class="card-title">Video albums
                                                </h3>                    
                                                </div><!-- card content -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="span4">
                                        <div class="card">
                                            <a href="<?php echo base_url()?>/gallery/photoalbum">
                                                <div class="card-image">
                                                    <img src="<?php echo base_url()?>/assets_admin/images.jpeg"/>
                                                </div>
                                                <div class="card-content" style="text-align: center">
                                                <h3 class="card-title">Photo albums
                                                </h3>                    
                                                </div><!-- card content -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="span2">
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>