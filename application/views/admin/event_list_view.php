<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
	.success {
        background-color: #ddffdd;
        border-left: 6px solid #4CAF50;
        /*width:80%;
        margin-left:3%;*/
		padding: 8px 35px 8px 14px;
		margin-bottom:10px;
    }
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">    
                
                	<?php if($this->session->flashdata('msg')): ?>
                        <div class="success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    <?php endif; ?>  		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>
                                  <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"></div> 
                            
                            <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="8%">Event Title &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="7%">Start Date &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="7%">End Date &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Place &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="8%">Fee Charges &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Permission &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="5%">No # Seats &nbsp;<i class=""></i></th> 
                                        <th class="text-center" width="5%">Status &nbsp;<i class=""></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                        
                                            <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            
                                            <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                <td><?php echo ($record['title']) ? $record['title'] : '-'; ?> </td>
                                                <td> <center> <?php echo ($record['start_date']); //? date('d-m-Y H:i:s', strtotime($record['start_date'])) : '-'; ?> </center></td>
                                                <td>  <center><?php echo ($record['end_date']); //? date('d-m-Y H:i:s', strtotime($record['end_date'])) : '-'; ?> </center></td>
                                                <td><center> <?php echo ($record['place']) ? $record['place'] : '-'; ?></center> </td>
                                                <td style="text-align:right;"><?php echo ($record['fee']) ? "$ ".number_format($record['fee'],2) : '-'; ?></td>
                                                <td><center> <?php echo ($record['permission']) ? $record['permission'] : '-'; ?></center> </td>
                                                <td><center> <?php echo ($record['no_seats']) ? $record['no_seats'] : '-'; ?></center> </td>
                                                <td><center> <?php echo ($record['status'] == 1) ? 'Active' : 'Inactive'; ?></center> </td>
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                <tr class="warning no-result">
                                    <td colspan="5"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="10"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table> 
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->   <br><br>   <br><br>

    </div> <!-- /main-inner -->

</div> <!-- /main -->
<script type="text/javascript">
window.setTimeout(function() {
    $(".success").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);
</script>