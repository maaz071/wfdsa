<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzUdS8dVvVFJ8K_mxEzMAM_xqe2_YvOlc&libraries=places"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> -->
<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Add <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                               
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div style="float:right; margin:6px;">
                                    <button type="submit" class="btn btn-primary "><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>
                                    <br><br>
                                    <?php if($this->session->flashdata('error')){?>
                                    <div class="errorbox" style="padding: 6px;border: 2px solid red">
                                        <h4 style="color: red">Errors:<?php echo $this->session->flashdata('error');?></h4>
                                    </div>
                                    <?php }?>
                                </div>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                        <div class="control-row">											
                                                <label class="control-label">Company Name</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="company_name"  value="<?php echo (isset($record_info) && $record_info[0]['company_name']) ? $record_info[0]['company_name'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div> 
                                        
                                        <div class="control-row">	
                                         <div class="control-group">	  
                                                <label class="control-label">Company Logo</label>
                                                <div class="control-group">
                                                     <input type="file" class="span3" name="company_logo" id="company_logo"> 
                                                </div>  
                                            </div> 
                                        </div> 
                                        <?php if (isset($record_info[0]['company_logo']) && !empty($record_info[0]['company_logo'])) { ?> 
                                                <label class="control-label">Logo Preview</label>
                                                <div class="control-group">
                                                    <?php if(!empty($record_info[0]['company_logo'])){ ?>
                                                    <img width="150" height="150" src="<?php echo $record_info[0]['company_logo']; ?>">
                                                    <?php }?>
                                                </div> 
                                            <?php } ?>
                                            
                                            <?php if (isset($record_info[0]['company_logo']) && !empty($record_info[0]['company_logo'])) { ?> 
                                            <input type="hidden" name="imageSrc" value ="<?php echo $record_info[0]['company_logo']; ?>" >       
                                            <?php } ?>
                                            <div class="control-row">											
                                                <label class="control-label">Contact Person<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" required class="span3" name="member_name"  value="<?php echo (isset($record_info[0]) && $record_info[0]['member_name']) ? $record_info[0]['member_name'] : ''; ?>">
                                                </div>
                                            </div>  
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Email<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="email" required class="span3" name="email" value="<?php echo (isset($record_info[0]) && $record_info[0]['email']) ? $record_info[0]['email'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div> 
                                            
                                             <div class="control-row">											
                                                <label class="control-label">Website<span style="color:red">*</span> </label>
                                                <div class="control-group">
                                                    <input type="text" required class="span3" name="website"  value="<?php echo (isset($record_info) && $record_info[0]['website']) ? $record_info[0]['website'] : ''; ?>"> 
                                                </div>  
                                            </div>
                                             

                                            <div class="control-row">											
                                                <label class="control-label">Phone<span style="color:red">*</span> </label>
                                                <div class="control-group">
                                                    <input type="text" required class="span3" name="phone"  value="<?php echo (isset($record_info[0]) && $record_info[0]['phone']) ? $record_info[0]['phone'] : ''; ?>">   
                                                </div> 
                                            </div> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Fax<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="fax" value="<?php echo (isset($record_info[0]) && $record_info[0]['fax']) ? $record_info[0]['fax'] : ''; ?>"> 
                                                </div>  
                                            </div> 
                                            
                                            
                                            
                                             
                                            
                                            
                                            
                                            
                                        </div>    
            
                                          
                                        <div class="span5">
                                            
                                            
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Address<span style="color:red">*</span></label> 
                                                 <div class="control-group"> 
                                                    <textarea required class="form-control span3" name="address" style=" resize: vertical" rows="6"><?php echo (isset($record_info) && $record_info[0]['address']) ? $record_info[0]['address'] : ''; ?></textarea>
                                                </div>  
                                            </div> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Country<span style="color:red">*</span> </label>
                                                <div class="control-group">
                                                    <select required class="span3" name="country_id">
                                                        <option value="">-Select-</option>
                                                        <?php foreach($countries as $value) { ?>
                                                             <option value="<?php echo $value['country_id'];?>" <?php echo (isset($record_info[0]) && $record_info[0]['country_id'] == $value['country_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div> 
                                            </div>
                                            
                                             <div class="control-row">											
                                                <label class="control-label">Region<span style="color:red">*</span> </label>
                                                <div class="control-group">
                                                    <select required class="span3" name="region_id">
                                                        <option value="">-Select-</option>
                                                        <?php foreach($region as $value) { ?>
                                                             <option value="<?php echo $value['region_id'];?>" <?php echo (isset($record_info[0]) && $record_info[0]['region_id'] == $value['region_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div> 
                                            </div>  
                                           
                                            
                                        </div> 

                                    


                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?> 

                                        </div>

                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main --> <br><br><br>

     
   