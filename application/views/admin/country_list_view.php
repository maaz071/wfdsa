<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
</style>

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-flag"></i>
                            <h3><?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucwords(str_replace("_", " ", $this->uri->segment(1))); ?></a>
                                <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            
                              <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <th class="text-center" width="50%">Country Name &nbsp;<i class="icon-sort sort_icon"></i></th>
                                        <th class="text-center" width="10%">Country Flag &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="10%">Country Zip &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <tr id='addr0' data-id="0">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>"></td>
                                                <td><?php echo ($record['name']) ? $record['name'] : '-'; ?></td>
                                                <td><center><img src="<?php echo ($record['flag_pic']) ? $record['flag_pic'] : '-'; ?>" style="width:50px; height:50px;"></center></td>
                                                <td><center><?php echo ($record['country_zip_code']) ? $record['country_zip_code'] : '-'; ?> </center></td>
                                                 
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                    <?php } ?>
                                <?php } ?>
                                  <tr class="warning no-result">
                                    <td colspan="4"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->