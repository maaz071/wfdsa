 
 <?php echo link_tag('assets_admin/css/pages/reports.css'); ?> 
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">
                <div class="span12">
                    <div class="info-box">
                        <div class="row-fluid stats-box">
                            <div class="span6">
                                <div class="stats-box-title">Members Registered</div>
                                <div class="stats-box-all-info"><i class="icon-group" style="color:#cc3333;"></i><br><br><?php echo count($record_list); ?>  </div> 
                            </div>

                            <div class="span6">
                                <div class="stats-box-title">Members Check In</div>
                                <div class="stats-box-all-info"><i class="icon-group"  style="color:#3394cc"></i><br><br> <?php echo $checked_in;?></div>                                
                            </div>

                            <!--<div class="span4">
                                <div class="stats-box-title">Members Checked Out</div>
                                <div class="stats-box-all-info"><i class="icon-group" style="color:#3C3"></i><br><br><?php echo $checked_out;?></div> 
                            </div>-->
                        </div>  
                    </div>
                </div>

                <div class="span12"> 

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><span style="color:red;"><?php echo (!empty($event_name)) ? $event_name: ''; ?></span> &nbsp;<?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <?php if($this->input->get('event_id')) {?>
                                   <a href="<?php echo base_url('event/view/'.$this->input->get('event_id'))?>" class="btn btn-default" style="float:right; margin:5px;">Back</a>
                                <?php } ?>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <!-- <div class="control-row">	 
                                <div class="control-group">
                                    <select class="select_go_url"  data-id="<?php echo base_url('attendees')."?event_id=";?>">
                                        <option value="">-Select Event-</option>
                                         <?php foreach($events as $value) { ?>
                                             <option value="<?php echo $value['event_id'];?>" <?php echo ($value['event_id'] == $event_id) ? 'selected="selected"' : '' ;?>><?php echo $value['title'];?></option>
                                         <?php } ?>
                                    </select>
                                </div> 
                            </div> -->

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr>
                                        
                                        <!-- <th class="text-center" width="10%">Event Name</th> -->
                                        <th class="text-center" width="15%">Name</th>
                                        <th class="text-center" width="3%">Profile Pic</th>
                                        <th class="text-center" width="10%">Email</th>
                                        <th class="text-center" width="10%">Contact No.</th>
                                        <th class="text-center" width="3%">Status</th> 
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <tr id='addr0' data-id="0"> 
                                                <?php if($record['member_id'] != 0) { ?>
                                               
                                                    <td><?php echo ($record['member_name']) ? $record['member_name'] : '-'; ?> </td> 
                                                    <td><center><img src="<?php echo $record['upload_image']; ?>" style="width:50px; height:50px;"></center></td> 
                                                    <td><?php echo ($record['member_email']) ? $record['member_email'] : '-'; ?> </td> 
                                                    <td><?php echo ($record['cell']) ? $record['cell'] : '-'; ?> </td> 
                                                
                                                <?php } else if ($record['non_member_id'] != 0) { ?>
                                                
                                                    <td><?php echo ($record['non_member_name']) ? $record['non_member_name'] : '-'; ?> </td> 
                                                    <td><center><img src="<?php echo base_url().'uploads/1515075983_dummy.jpg'; ?>" style="width:50px; height:50px;"> </center></td> 
                                                    <td><?php echo ($record['email']) ? $record['email'] : '-'; ?> </td> 
                                                    <td><?php echo ($record['contact_no']) ? $record['contact_no'] : '-'; ?> </td> 
                                                    
                                                <?php } ?>
                                                
                                                <td> 
                                                    <center>
                                                        <?php if($record['checked_in'] != 1) {?>
                                                            <a href="#" class="btn btn-primary checked_in" id="check_in_<?php echo $record[$row_id]; ?>" data-val="<?php echo $record[$row_id]; ?>">Check In</a>
                                                        <?php } else { ?>
                                                            <span class="label label-success">Check In</span>
                                                        <?php } ?>
                                                    </center>
                                                </td> 
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9">
                                    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main --><br><br><br>