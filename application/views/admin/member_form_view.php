<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }
    .span5{
        float:right;
        margin-top:5%;
        margin-right:-7%;
            }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?>  <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                                    <!--old button -->
                                    
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    
                                    <input type="submit" class="btn btn-primary "
                                    style="float:right; margin:6px;" value="submit"/>                            <div class="alert" style="display:none;" role="alert"></div>
                            
                                    
                                    
                                    <?php //$action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php //echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 
                                        <div class="span6"> 
                                            <div class="control-row">											
                                                <label class="control-label">Title</label>
                                                <div class="control-group">
                                                    <select class="span3" name="title">
                                                        <option disabled value="" required>-Select-</option>
                                                        <option value="Dr."  <?php echo (isset($record_info) && $record_info[0]['title'] == 'Dr.') ? 'selected="selected"' : ''; ?>>Dr.</option>
                                                        <option value="Mr."  <?php echo (isset($record_info) && $record_info[0]['title'] == 'Mr.') ? 'selected="selected"' : ''; ?>>Mr.</option>
                                                        <option value="Mrs." <?php echo (isset($record_info) && $record_info[0]['title'] == 'Mrs.') ? 'selected="selected"' : ''; ?>>Mrs.</option>
                                                        <option value="Miss."<?php echo (isset($record_info) && $record_info[0]['title'] == 'Miss.') ? 'selected="selected"' : ''; ?>>Miss.</option>
                                                    </select>
                                                </div>  	  
                                            </div>  
                                             
                                            <div class="control-row">											
                                                <label class="control-label">First Name<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" required name="first_name" value="<?php echo (isset($record_info) && $record_info[0]['first_name']) ? $record_info[0]['first_name'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>  
                                            
                                             <div class="control-row">											
                                                <label class="control-label">Last Name<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" required name="last_name" value="<?php echo (isset($record_info) && $record_info[0]['last_name']) ? $record_info[0]['last_name'] : ''; ?>"> 
                                                </div>   
                                            </div>
                                            
                                            <div class="control-row">											
                                                <label class="control-label">WFDSA Title</label>
                                                <div class="control-group">
                                                     <input type="text" class="span3" name="wfdsa_title" value="<?php echo (isset($record_info) && $record_info[0]['wfdsa_title']) ? $record_info[0]['wfdsa_title'] : ''; ?>">  
                                                </div>  	  
                                            </div>
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Email<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="email" required value="<?php echo (isset($record_info) && $record_info[0]['email']) ? $record_info[0]['email'] : ''; ?>">  
                                                </div>  
                                            </div> 
                                            
                                            
                                             <div class="control-group">	
                                                <label class="control-label"></label> 
                                                <button type="button" class="btn btn-default btn-small" id="autogen_pass">Auto Generate Password</button>
                                            </div>
                                            
                                              <div class="control-row">	  
                                                <label class="control-label">Password<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <?php if($this->uri->segment(2) == 'edit') {?>
                                                        <input type="password"  class="password preview-password-rgb"  name="password" value="" style="width:191px;">
                                                        <button type="button"  id="password_enable" class="btn btn-default"><i class="icon icon-pencil"></i> Edit</button>
                                                    <?php } else { ?>
                                                    <input required type="password" class="span3 preview-password-rgb span4"  name="password" id="password" value="">
                                                    <?php } ?>
                                                </div>   
                                            </div>
                                            
                                            <div class="control-row">	  
                                                <label class="control-label">Re Enter Password<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    
                                                    <?php if($this->uri->segment(2) == 'edit') {?>
                                                        <input type="password" class="span3 password preview-password-rgb"  name="re_password" value="">
                                                    <?php } else { ?>
                                                        <input required type="password" class="span3 preview-password-rgb span4" name="re_password" id="re_password" value="">
                                                    <?php } ?>
                                                </div>   
                                            </div>
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Website</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="website" value="<?php echo (isset($record_info) && $record_info[0]['website']) ? $record_info[0]['website'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>  
                                             
                                            <div class="control-row"> 
                                                <label class="control-label">Biography</label>
                                                <div class="control-group"> 
                                                    <textarea class="form-control" name="biography" style="min-width: 46%;" rows="6"><?php echo (isset($record_info) && $record_info[0]['biography']) ? $record_info[0]['biography'] : ''; ?></textarea>
                                                </div>  
                                            </div>
                                            
                                            <div class="control-group">	  
                                                <label class="control-label">Image<P style="font-size:10px;">(Max Image Size : 5MB)</P></label>
                                                <div class="control-group">
                                                    
                                                     <!--input type="file" required class="span3" name="fileToUpload" id="upload_image"--> 
                                                <a type="button" class="btn btn-primary" id="change-profile-pic">Upload Image</a>
                                                </div>   
                                            </div>
                                            <?php if (isset($record_info[0]['upload_image']) && !empty($record_info[0]['upload_image'])) { ?> 
                                                <label class="control-label">Image Preview</label>
                                                <div class="control-group">
                                                    <?php
                                                        $checkpos = str_replace("http://www.wfdsamobileportal.com/uploads/Member_Photos/", "", $record_info[0]['upload_image']);

                                                     if(!empty($checkpos)){?>
                                                    <img height="300" width="210" src="<?php echo $record_info[0]['upload_image']; ?>">
                                                    <?php }?>
                                                    <input type="hidden" value="<?php echo $record_info[0]['upload_image']; ?>" name ="imageSrc">
                                                </div> 
                                            <?php } ?>  
                                            
                                            
                                        </div>   
                                        <div class="span5">  
                                        
                                        
                                            <div class="control-row">											
                                                <label class="control-label">Company<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required type="text" class="span3" name="company" value="<?php echo (isset($record_info) && $record_info[0]['company']) ? $record_info[0]['company'] : ''; ?>"> 
                                                </div>  
                                            </div>
                                            
                                            
                                             <div class="control-row"> 
                                                <label class="control-label">Designation<span style="color:red">*</span></label>
                                                <div class="control-group"> 
                                                 <input required type="text" class="span3" name="designation" value="<?php echo (isset($record_info) && $record_info[0]['designation']) ? $record_info[0]['designation'] : ''; ?>"> 
                                                    
                                                </div>  
                                            </div> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Address</label>
                                                <div class="control-group">
                                                    <input  type="text" class="span3" name="address" value="<?php echo (isset($record_info) && $record_info[0]['address']) ? $record_info[0]['address'] : ''; ?>"> 
                                                </div>  
                                            </div> 
                                            
                                             <div class="control-row">											
                                                <label class="control-label">Cell #</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="cell" value="<?php echo (isset($record_info) && $record_info[0]['cell']) ? $record_info[0]['cell'] : ''; ?>"> 
                                                </div>  
                                            </div>
                                            <div class="control-row">											
                                                <label class="control-label">Telephone</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="telephone" value="<?php echo (isset($record_info) && $record_info[0]['telephone']) ? $record_info[0]['telephone'] : ''; ?>"> 
                                                </div>  
                                            </div>
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Fax</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="fax" value="<?php echo (isset($record_info) && $record_info[0]['fax']) ? $record_info[0]['fax'] : ''; ?>"> 
                                                </div>  
                                            </div> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Country<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <select required class="span3" name="country">
                                                        <option value="">-Select-</option>
                                                        <?php foreach ($country as $value) { ?>
                                                            <option value="<?php echo $value['country_id']; ?>" <?php echo (isset($record_info) && $record_info[0]['country'] == $value['country_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>  
                                                        <?php } ?>
                                                    </select>
                                                </div>  
                                            </div>
                                                <?php if(!empty($record_info[0]['role'])) {?>
                                                
                                                
                                                 <div class="control-row">			<p>Current Role: &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $record_info[0]['role'];?> </p>
                                            <p><?php //echo $announce;?></p>
                                        <input type = "hidden" value ="<?php echo $record_info[0]['role'];?>" name="roleSrc">            
                                                      
                                                
                                                </div> 
                                            <?php } ?>    
                                    
                                             <div class="control-group">											
                                                <label class="control-label">Role<span style="color:red">*</span><br> <small>(hold ctrl key to select multiple roles)</small></label>
                                                <select required class="span3 select_append roles" name="member_role[]" multiple style="width:57%; min-height:200px; ">
                                                <!--    <option value="" required>-Select Role-</option> -->
                                                    <?php foreach ($roles as $value) { ?>
                                                        <option id="role_option_<?php echo $value['member_role_id']; ?>" disabled="disabled" value="<?php echo $value['member_role_id']; ?>" <?php echo (in_array($value['member_role_id'],$selected)) ? 'selected="selected"' : '';?> style="font-weight:bold;"> <?php echo $value['name']; ?> </option>
                                                       
                                                    <?php if(!empty($value['child'])) {?>
                                                        <?php foreach ($value['child'] as $value2) { ?> 
                                                                <option id="role_option_<?php echo $value2['member_role_id']; ?>" value="<?php echo $value2['name']; ?>" <?php echo (in_array($value2['name'],$test1)) ? 'selected="selected"' : '';?>> <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>        
                                                    <?php } ?>        
                                                </select>  
                                        </div> 
                                                <div id="Add_Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">Add Roles</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="control-group">
                                                            <label class="control-label">Role Parent</label>
                                                            <select class="span3 role_select"> 
                                                                <option value="">-Select-</option>
                                                                <?php foreach ($roles as $value) { ?>
                                                                    <option value="<?php echo $value['member_role_id']; ?>" style="font-weight:bold;"> <?php echo $value['name']; ?> </option>
                                                                    <?php foreach ($value['child'] as $value2) { ?> 
                                                                        <option value="<?php echo $value2['member_role_id']; ?>"> <?php echo $value2['name']; ?> </option>
                                                                    <?php } ?>
                                                                <?php } ?>   
                                                            </select>  
                                                        
                                                        <div class="control-row">											
                                                            <label class="control-label">Role Name</label>
                                                            <div class="control-group">
                                                                <input type="text" class="span3 role_name"> 
                                                            </div>  
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                        <button class="btn btn-primary add_value_select" data-val="<?php echo base_url('member/add_role'); ?>">Submit</button>
                                                    </div>
                                                </div>
                                                </div>


                                                <div id="Delete_Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h3 id="myModalLabel">Delete Roles</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="alert alert_modal" style="display:none;" role="alert"> </div>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th width="100%"> Role Name </th> 
                                                                    <th class="td-actions">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> 
                                                                <?php if (!empty($roles)) { ?>
                                                                    <?php foreach ($roles as $value) { ?>
                                                                        <tr>  
                                                                            <td><b><?php echo $value['name']; ?></b></td>
                                                                            <td class="td-actions"></td>
                                                                            <!--<a href="javascript:;" class="btn btn-danger btn-small delete_role" id="<?php echo base_url('member/delete_role/' . $value['member_role_id']); ?>" data-id="<?php echo $value['member_role_id']; ?>"><i class="btn-icon-only icon-remove"> </i></a>-->
                                                                        </tr> 
                                                                        <?php if (!empty($value['child'])) { ?>
                                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                                <tr>
                                                                                    <td>&nbsp;&nbsp;&nbsp;<?php echo $value2['name']; ?></td>
                                                                                    <td class="td-actions"><a href="javascript:;" class="btn btn-danger btn-small delete_role" id="<?php echo base_url('member/delete_role/' . $value2['member_role_id']); ?>" data-id="<?php echo $value2['member_role_id']; ?>"><i class="btn-icon-only icon-remove"> </i></a></td>
                                                                                </tr> 
                                                                            <?php } ?>   
                                                                        <?php } ?>               
                                                                    <?php } ?>               
                                                                <?php } ?>               
                                                            </tbody>
                                                        </table>        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                                        <button class="btn btn-primary add_value_select" data-val="<?php echo base_url('member/add_role'); ?>">Submit</button>
                                                    </div>
                                                </div> 
                                            </div>
                                            
                                            <div class="control-group">
                                                <label class="control-label"></label>
                                                <a href="#Add_Modal" role="button" data-toggle="modal" class="btn btn-small btn-primary"><i class="icon-plus"></i></a>
                                               <!--  <a href="#Delete_Modal" role="button" data-toggle="modal" class="btn btn-small btn-danger"><i class="icon-trash"></i></a>
 -->
                                            </div>
                                            
                                        </div> 
                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo lcfirst($this->uri->segment(1)); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?>  
                                        </div>
                                        
                                        
                                        <input type="hidden" name="hdn-x1-axis" id="hdn-x1-axis" value="" />
                                        <input type="hidden" name="hdn-y1-axis" id="hdn-y1-axis" value="" />
                                        <input type="hidden" name="hdn-x2-axis" value="" id="hdn-x2-axis" />
                                        <input type="hidden" name="hdn-y2-axis" value="" id="hdn-y2-axis" />
                                        <input type="hidden" name="hdn-thumb-width" id="hdn-thumb-width" value="" />
                                        <input type="hidden" name="hdn-thumb-height" id="hdn-thumb-height" value="" />
                                        <input type="hidden" name="image_name" value="" id="image_name" />                   
                                        
                                       
                                         
                                     
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                    
                                    <div id="profile_pic_modal" class="modal fade">
                                        		<div class="modal-dialog">
                                        			<div class="modal-content">
                                        				<div class="modal-header">
                                        					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        				   <h3>Upload Image</h3>
                                        				</div>
                                        				<div class="modal-body">
                                        				    <div class="alert alert_modal" style="display:none;"></div>
                                        				    <div class="progress progress-striped active" id="upload_progress" style="display:none;">
                                                              <div class="bar"></div>
                                                            </div>
                                        				    <?php echo form_open_multipart(base_url(ucfirst($this->uri->segment(1))."/".'do_upload_resize'), 'class="" id="cropimage"'); ?>
                                        			
                                        						<strong>Upload Image:</strong> <br><br> 
                                        					    <input type="file" name="profile-pic" id="profile-pic" />
                                        						<div id='preview-profile-pic'></div>
                                        					<div id="thumbs" style="padding:5px; width:600p"></div>
                                        			        <input type="hidden" name="primary_key" value="<?php echo time(); ?>">
                                        			         
                                        			             
                                                            
                                        					<?php echo form_close(); ?>
                                        				</div>
                                        				<div class="modal-footer">
                                        					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        					<button type="button" id="save_crop" class="btn btn-primary">Crop & Save</button>
                                        				</div>
                                        			</div>
                                        		</div>
                                        	</div>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->