<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-globe"></i>
                            <h3><span style="color:red;"> <?php echo (!empty($event_name)) ? $event_name : '' ;?></span>&nbsp;<?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <a class="btn btn-primary" href="<?php echo base_url('poll/create?event_id='.$this->input->get('event_id'));?>" > Add <?php echo ucfirst($this->uri->segment(1)); ?></a>
                                <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                                
                                <?php if($this->input->get('event_id')) {?>
                                   <a href="<?php echo base_url('event/view/'.$this->input->get('event_id'))?>" class="btn btn-default" style="float:right; margin:8px;">Back</a>
                                <?php } ?>
                                
                            </div>
                            
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            
                            <!--<select id="poll_event_select" class="select_go_url" data-id="<?php echo base_url('poll')."?event_id=";?>">
                                <option value="">-Select Event-</option>  
                                <?php foreach($events as $value) { ?>
                                    <option value="<?php echo $value['event_id']; ?>" <?php echo ($value['event_id'] == $event_id) ? "Selected='Selected'" : "" ;?>><?php echo $value['title'];?></option>  
                                <?php } ?>
                            </select> <br> -->
                            
                            <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                            
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                        <th class="text-center" width="1%">
                                            <input type="checkbox" id="check_all" value="0">   
                                        </th>
                                        <!-- <th class="text-center" width="60%">Event &nbsp;<i class="icon-sort sort_icon"></i></th> -->
                                        <th class="text-center" width="47%">Poll Question  &nbsp;<i class="icon-sort sort_icon"></i></th> 
                                        <th class="text-center" width="1%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>   
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                             
                                            <?php if($record['status'] == 1) { ?>
                                                <?php $color = '#f7fffd';?>
                                            <?php } ?>
                                            
                                            <?php if($record['status'] == 0) { ?>
                                                <?php $color = '#ffff';?>
                                            <?php } ?>
                                            
                                        
                                        
                                        
                                            <tr id='addr0' data-id="0" style="background-color:<?php echo $color;?>;">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                               <!-- <td><?php echo ($record['title']) ? $record['title'] : '-'; ?> </td> -->
                                                <td><?php echo ($record['poll_question']) ? $record['poll_question'] : '-'; ?> </td> 
                                                  
                                                <td> <?php require(APPPATH . 'views/admin/crud_btn.php'); ?>  </td> 
                                            </tr>
                                            
                                            <div id="poll_preview_<?php echo $record[$row_id];?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                               <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                  <h3 id="myModalLabel">Poll Preview </h3>
                                               </div>
                                               <div class="modal-body">
                                                   <h4><?php echo ($record['poll_question']) ? $record['poll_question'] : '-'; ?></h4><br>
                                                   
                                                   <?php for($x=0; $x<count($poll_answer[$record[$row_id]]); $x++) { ?>
                                                       <div class="control-group" style="margin-left:15px;">		
                                                        <?php if($record['choice_selection'] == 'Single Choice') {?>
                                                            <input type="radio" name="test"> &nbsp; 
                                                        <?php } else { ?>
                                                          <input type="checkbox" name="test"> &nbsp; 
                                                        <?php } ?> 
                                                        <?php echo $poll_answer[$record[$row_id]][$x]['poll_answer']; ?> 
                                                       </div> 
                                                    <?php } ?>   
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 
                                                </div>
                                            </div>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr class="warning no-result">
                                        <td colspan="4"><i class="fa fa-warning"></i> No Result Found</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>

    


                          <br><br>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->
              


            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->