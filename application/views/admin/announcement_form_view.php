<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzUdS8dVvVFJ8K_mxEzMAM_xqe2_YvOlc&libraries=places"></script>

<script>
$( document ).ready(function() {
  var eventVal = $('#event_user_type').val();
            if(eventVal == 2){
                $(".control-group.perm_member").hide();
                $(".control-group.perm_member").find("option").each(function () {
                   $(this).prop("selected",false);
                });
            }else{
                $(".control-group.perm_member").show();
            }
});
</script>
<style>
    .success {
        background-color: #ddffdd;
        border-left: 6px solid #4CAF50;
        width:80%;
        margin-left:3%;
		padding: 8px 35px 8px 14px;
    }
    .failed1 {
        background-color: #ff9d8e;
        border-left: 6px solid #ff6149;
        width:80%;
        margin-left:3%;
    }
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>


<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Add <?php echo ucfirst($this->uri->segment(1)); ?></h3>

                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                           
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>
                            <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    
                                <input type="submit" class="btn btn-primary "
                                style="float:right; margin:6px;" value="submit" id="success" />
                                <div class="alert" style="display:none;" role="alert" ></div>
                                    
                            <div class="tabbable">
                                <div class="success" id="success1" style="display:none;">
                                  <p><strong>Success!</strong> Announcement Added Successfully,Wait Until Page Refresh...</p>
                                </div>
                                    
                                <div class="failed1" id="failed1" style="display:none;">
                                  <p><strong>failed!</strong> Please fill out all Fields...</p>
                                </div>

                                <div class="tab-pane" id="formcontrol-group">
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                            <div class="control-row">											
                                                <label class="control-label">Title<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required id="title" type="text" class="span3" name="title" value="<?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>
                                        
                                        <!--Showing permissions-->
                                        <div class="control-group">
                                        <?php if (isset($announce[0]['announce_for']) && !empty($announce[0]['announce_for'])) { ?> 
                                                
                                            
                                                <div class="control-group">
                                        
                                        <p>Current Permission : <?php echo $announce[0]['announce_for']; ?></p>
                                                    
                                                </div>
                                        <input type="hidden" name="previous-permission" value="<?php echo $announce[0]['announce_for']; ?>">        
                                            <?php } ?>    
                                            
                                            
                                            
                                        </div>
    
                                            

                                            
                                        <!--permission view copied from events-->
                                        <div class="control-row">
                                        <label class="control-label">Event User Type<span style="color:red">*</span></label>
                                        <div class="control-group" onload="myFunction()">
                                        <select class="span3" name="event_user_type" id="event_user_type">
                                        <option value="2" <?php echo (isset($record_info) && $record_info[0]['announce_for'] == "Public") ? 'selected="selected"': ''; ?>   >Public</option>
                                        <option value="1" <?php echo (isset($record_info) && $record_info[0]['announce_for'] == " ") ? 'selected="selected"': ''; ?> >Member</option>
                                        
                                        
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="control-group perm_member" style="<?php //echo (isset($record_info) && $record_info[0]['event_user_type'] && $record_info[0]['event_user_type'] == 1) ? 'display:block' : 'display:none'; ?>">
                                                <label class="control-label">Permission<span style="color:red">*</span></label>
                                                <?php
                                                $selected_permission = array();
                                                if (isset($record_info)) {
                                                    if (!empty($record_info[0]['permission']))
                                                        $selected_permission = @unserialize($record_info[0]['permission']);
                                                }
                                                ?>
												
												
												
                                                <select class="span3 select_append" name="permission[]" multiple
                                                        style="width:47%; min-height:200px; " id="permission">
                                              <!--      <option value="<?php //echo $value['name'];?>">-Select Role-</option> -->
                                                    <?php foreach ($roles as $value) { ?>
                                                        <option disabled="disabled"
                                                                value="<?php echo $value['member_role_id'];
                                                                //echo $value['name'];?>"
                                                                style="font-weight:bold; color:black;"> <?php echo $value['name']; ?> </option>

                                                        <?php if (!empty($value['child'])) { ?>
                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                <option
                                                                    value="<?php echo $value2['name'];?>" <?php echo (in_array($value2['name'] , $test1)) ? 'selected="selected"' : ''; ?>> <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>  
                                            <div class="alert alert-error" id="errorname" style="display:none;">
                                            <span>
                                             <p>*Please Select Memebers</p>
                                                 </span>
                                                  </div>  



                                        </div>    
                                           

                                        <div class="span5">   

                                            <div class="control-group">											
                                                <label class="control-label">Date<span style="color:red">*</span></label>
                                                <div class="control-row input-append date form_datetime" data-date-format="dd mm yyyy - HH:ii:ss" data-link-field="dtp_input1">
                                                    <input required size="22" name="date" type="text" value="<?php echo (isset($record_info) && $record_info[0]['date']) ? ($record_info[0]['date']) : ''; ?>" >
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" value="" /> 
                                            </div>

                                            <div class="control-group">	   
                                                <label class="control-label">Image<p style="font-size:7ox;">(max file size is 1Mb)</p></label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="fileToUpload" id="fileToUpload" accept="image/*">
                                                </div>   
                                            </div>
                                            <?php if (isset($record_info[0]['upload_image']) && !empty($record_info[0]['upload_image'])) { ?> 
                                                <label class="control-label">Image Preview</label>
                                                <div class="control-group">
                                        <input type="hidden" name="imageSrc" value="<?php echo $record_info[0]['upload_image']; ?>">            
                                                    <img src="<?php echo $record_info[0]['upload_image']; ?>" width="200" height="200">
                                                </div> 
                                            <?php } ?> 
                                        </div>
                                        
                                        
                                        <div class="span12">
                                            <div class="control-row"> 
                                                <label class="control-label">Announcement</label>
                                                <div class="control-group"> 
                                                    <textarea class="form-control" name="announcement_message" style="min-width: 74%;" rows="6" onclick="checkmember()"><?php echo (isset($record_info) && $record_info[0]['announcement_message']) ? $record_info[0]['announcement_message'] : ''; ?></textarea>
                                                </div>  
                                            </div>
                                        </div>  


                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?>

                                    </fieldset>
                                    
                                </div>  
                            </div>
                            <?php echo form_close(); ?>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
<script>

 
    $('#event_user_type').on('change', function () {
        var eventVal = $(this).val();
        if(eventVal == 2){
            $(".control-group.perm_member").hide();
            $(".control-group.perm_member").find("option").each(function () {
               $(this).prop("selected",false);
            });
        }else{
            $(".control-group.perm_member").show();
        }
    });
    $(document).ready(function(){
        var eventVal = $('#event_user_type option:selected').val();
        if(eventVal == 2){
            $(".control-group.perm_member").hide();
            $(".control-group.perm_member").find("option").each(function () {
               $(this).prop("selected",false);
            });
        }else{
            $(".control-group.perm_member").show();
        }
    })
</script>

<script type="text/javascript">

 function checkmember()
 {

  var user = $('#event_user_type :selected').val();
 var member = $('#permission :selected').val();
  
 if (user == 1 && member== null)
 {
   
   document.getElementById("success").disabled = true;
   $('#errorname').show();

   
 } 

}
</script>

<script type="text/javascript">
    $('#permission').on('change', function(){
   document.getElementById("success").disabled = false;
   $('#errorname').hide();

    }); 
</script>


<script type="text/javascript">
     
        $('#event_user_type').on('change', function(){
            var user = $('#event_user_type :selected').val();
     // alert(user);
    if (user == 2)
     {   
    document.getElementById("success").disabled = false;
    $('#errorname').hide();
 }
  else {
     document.getElementById("success").disabled = true;
    $('#errorname').show();
 }

  }); 
</script>