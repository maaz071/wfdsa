<style>
    .buttons{
        width: 12%;
        float: right; 
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3><?php //echo ucfirst($this->uri->segment(1));?>Change Password</h3>
                             <button type="submit" class="btn btn-primary login_submit" style="float: right; margin: 6px;">Submit</button>   

                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group"> 
                                     
                                    <?php echo form_open_multipart(base_url() . 'login/change_password_action', 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span11">
                                            
                                            <div class="control-row">	  
                                                <label class="control-label">Old Password</label>
                                                <div class="control-group">
                                                     <input type="password" class="span3" name="old_password" value=""> 
                                                </div>   
                                            </div>
                                            
                                            <div class="control-row">	  
                                                <label class="control-label">New Password</label>
                                                <div class="control-group">
                                                    <input type="password" class="span3" name="new_password" value=""> 
                                                </div>   
                                            </div>   
                                            
                                            <div class="control-row">	  
                                                <label class="control-label">Retype Password</label>
                                                <div class="control-group">
                                                    <input type="password" class="span3" name="retype_password" value=""> 
                                                </div>   
                                            </div>  
                                        </div>  
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

<script>

// ckeditor calling function
     initSample();
// ckeditor calling function
</script>