<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzUdS8dVvVFJ8K_mxEzMAM_xqe2_YvOlc&libraries=places"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> -->
<script>
     
</script>
<style>
    .buttons {
        width: 12%;
        float: right;
    }

    html, body, .container {
        height: 100%;
    }

    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    .pac-container {
        z-index: 99999;
        
    }
    .span5{
        float:right;
        margin-top:4%;
        margin-right:-7%;
            }
</style>

<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="widget ">
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Add <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>"
                               style="float:right; margin:6px;">Back</a>
                            <!--<button type="submit" class="btn btn-primary submit_product_form"
                                    style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?>
                                Submit
                            </button>
                            -->
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>
                            <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                            
                            <input type="submit" class="btn btn-primary "
                                    style="float:right; margin:6px;" value="submit" id="button" />
                            <div class="alert" style="display:none;" role="alert"></div>

                            
                            
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    
                                    <fieldset>
                                        <div class="span6">
                                            <div class="control-row">
                                                <label class="control-label">Title<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required type="text" class="span3" name="title"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>">
                                                </div> <!-- /control-group -->
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">Start Date<span style="color:red">*</span></label>
                                                <div class="control-row input-append date form_datetime"
                                                     
                                                     data-date-format="dd mm yyyy - HH:ii:ss"
                                                     data-link-field="dtp_input1">
                                                    <input id="startDate" required size="20" name="start_date" type="text"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['start_date']) ? ($record_info[0]['start_date']) : ''; ?>"
                                                            style="width:206px">
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" value=""/><br/>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label">End Date<span style="color:red">*</span></label>
                                                <div class="control-row input-append date form_datetime"
                                                     
                                                     data-date-format="dd mm yyyy - HH:ii:ss"
                                                     data-link-field="dtp_input1">
                                                    <input id="endDate" required size="20" name="end_date" type="text"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['end_date']) ? ($record_info[0]['end_date']): ''; ?>"
                                                            style="width:206px">
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" value=""/><br/>
                                            </div>

                                            <div class="control-row">
                                                <label class="control-label">Venue Name<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required type="text" class="span3" name="venue"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['venue']) ? $record_info[0]['venue'] : ''; ?>">

                                                </div>

                                            </div>
                                            <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyCnwRqya5Uy_0Gu93jEl9tvNUV2PTfLfP0"></script> -->
                                            
        <script>
            console.log('in event');
            var autocomplete;
            function initialize() {
              autocomplete = new google.maps.places.Autocomplete(
                  /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
                  { types: ['geocode'] });
              google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                
                document.getElementById('info').value = place.geometry.location.lat()+","+place.geometry.location.lng();
              });
            }
        </script>
                                            <div class="control-row">
                                                <label class="control-label">Venue Address<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="text" id="autocomplete" required class="span3" name="venue_address"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['venue_address']) ? $record_info[0]['venue_address'] : ''; ?>">
                                                    <!-- <button type="button" class="btn btn-danger btn-xs" id="loc_btn"
                                                            data-toggle="modal" data-target="#myModal_branch"><i
                                                            class="fa fa-map-marker" aria-hidden="true"></i></button> -->
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="control-row">
                                                <label class="control-label">Country<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <select required class="span3" name="country_id" id="country_id">
                                                        <option value="">-Select-</option>
                                                        <?php foreach ($country as $value) { ?>
                                                            <option
                                                                value="<?php echo $value['country_id']; ?>" <?php echo (isset($record_info) && $record_info[0]['country_id'] == $value['country_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['name']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        <?php if(!empty($announce[0]['permission'])) {?>
                                                
                                                
                                                 <div class="control-row">			<p>Current Permission: <?php echo $announce[0]['permission'];?> </p>
                                            <p><?php //echo $announce;?></p>
                                                    
                                                      
                                                
                                                </div> 
                                        <input type="hidden" value ="<?php echo $announce[0]['permission'];?>" name="previous-permission">        
                                                
                                            <?php } ?>    
                                            
                                        
                                        
                                        <!--  CITY DIV HAS BEEN HIDDEN
                                            <div class="control-row">
                                                <label class="control-label">City</label>
                                                <div class="control-group">
                                                    <select class="span3" name="city_id" id="city_id">
                                                        <option value="">-Select-</option>
                                                    </select>
                                                </div>
                                            </div>
                                            -->
                                            <div class="control-row">
                                                <label class="control-label">Event User Type<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <select class="span3" name="event_user_type" id="event_user_type">
                                                        <option value="2" <?php echo (isset($record_info) && $record_info[0]['event_user_type'] && $record_info[0]['event_user_type'] == 2) ? 'selected="selected"': ''; ?>   >Public</option>
                                                        <option value="1" <?php echo (isset($record_info) && $record_info[0]['event_user_type'] && $record_info[0]['event_user_type'] == 1) ? 'selected="selected"': ''; ?> >Member</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="control-group perm_member" style="<?php echo (isset($record_info) && $record_info[0]['event_user_type'] && $record_info[0]['event_user_type'] == 1) ? 'display:block' : 'display:none'; ?>">
                                                <label class="control-label">Permission<span style="color:red">*</span>(hold ctrl key for multiple selections)</label>
                                                <?php
                                                $selected_permission = array();
                                                if (isset($record_info)) {
                                                    if (!empty($record_info[0]['permission']))
                                                        $selected_permission = @unserialize($record_info[0]['permission']);
                                                }
                                                ?>
												
												
												
                                                <select id="aaa" class="span3 select_append" name="permission[]" multiple
                                                        style="width:47%; min-height:200px; " >
                                            <!--        <option value="<?php //echo $value['name'];?>">-Select Role-</option> -->
                                                    <?php foreach ($roles as $value) { ?>
                                                        <option disabled="disabled"
                                                                value="<?php echo $value['member_role_id'];
                                                                //echo $value['name'];?>"
                                                                style="font-weight:bold; color:black;"> <?php echo $value['name']; ?> </option>

                                                        <?php if (!empty($value['child'])) { ?>
                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                <option
                                                                    value="<?php echo $value2['name'];?>" <?php echo (in_array($value2['name'] , $test1)) ? 'selected="selected"' : ''; ?>> <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>


                                        </div>

                                        <div class="span5">
                                        <div class="alert alert-error" id="errorname" style="display:none;">
                                            <span>
                                             <p>*Please Select Memebers</p>
                                                 </span>
                                                  </div>  
                                            <div class="control-row">
                                                <label class="control-label">Speaker<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <textarea required class="form-control span2_3" name="speaker"
                                                              style="min-width: 72%;  resize: vertical"
                                                              rows="6" ><?php echo (isset($record_info) && $record_info[0]['speaker']) ? $record_info[0]['speaker'] : ''; ?></textarea>
                                                </div>
                                            </div>  

                                            <div class="control-row">
                                                <label class="control-label">Agenda<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <textarea required class="form-control span2_3" name="agenda"
                                                              style="min-width: 72%; resize: vertical"
                                                              rows="6"><?php echo (isset($record_info) && $record_info[0]['agenda']) ? $record_info[0]['agenda'] : ''; ?></textarea>
                                                </div>
                                            </div>


                                            <div class="control-row">
                                                <label class="control-label">Fee <i class="icon-dollar"></i></label>
                                                <div class="control-group">
                                                    <input  type="number" class="span3" name="fee" min ="0"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['fee']) ? $record_info[0]['fee'] : ''; ?>">
                                                </div>
                                            </div>

                                            <div class="control-row">
                                                <label class="control-label">No. Seats<span style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required type="number" class="span3" min="1" name="no_seats"
                                                           value="<?php echo (isset($record_info) && $record_info[0]['no_seats']) ? $record_info[0]['no_seats'] : ''; ?>">
                                                </div> <!-- /control-group -->
                                            </div>

                                        
                                        </div>

                                        <input type="hidden" name="location" id="location">
                                        <input type="hidden" name="latLng" id="info" value="<?php echo (isset($record_info) && $record_info[0]['latLng']) ? $record_info[0]['latLng'] : ''; ?>">

                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id"
                                                   value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?>

                                	</fieldset>
                                	
                                </div>
                            </div>
                            <?php echo form_close(); ?>

                        <div class="modal fade" id="myModal_branch" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form id="branch_form">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Select Location</h4>
                                        </div>
                                        <div class="modal-body" id="smallModalBody">

                                            <!--         <input type="text" name="branch" placeholder="" required>
                                                    <br/>
                                                    <br/> -->

                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                            <div id="googleMap" style="width:100%;height:400px;"></div>
                                            <b>Marker status:</b>
                                            <div id="markerStatus"><i>Click and drag the marker.</i></div>
                                            <!-- <b>Current position:</b> -->
                                            <div></div>
                                            <b>Closest matching address:</b>
                                            <div id="address"></div>


                                        </div>
                                        <div class="modal-footer" id="smallModalFooter">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="button" class="btn btn-primary btn-sm" id="done"
                                                    data-dismiss="modal" name="done">Done
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div> <!-- /widget-content -->

                </div> <!-- /widget -->

            </div> <!-- /span8 -->


        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main --> <br><br><br>

<!-- modal for map start -->


<!-- modal for map end -->

<script type="text/javascript">

    $(document).keypress(
    function(event){
     if (event.which == '13') {
        event.preventDefault();
      }


});

    $('document').ready(function () {

        function myMap() {
            var latlng = new google.maps.LatLng(30.3753, 69.3451);

            var mapProp = {
                center: latlng,
                zoom: 5,
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            google.maps.event.trigger(map, 'resize');

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: 'Set lat/lon values for this property',
                draggable: true
            });

            //search start
// Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    /*markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));*/
                    marker.setPosition(place.geometry.location);
                    updateMarkerPosition(place.geometry.location);
                    geocodePosition(place.geometry.location);
                    /*var marker = new google.maps.Marker({
                        position: place.geometry.location,
                        map: map,
                        title: 'Set lat/lon values for this property',
                        draggable: true
                    });
                    */

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
            //search end

            // var lat = homeMarker.getPosition().lat();
            // var lng = homeMarker.getPosition().lng();
            // console.log(lat);
            // console.log(lng);
            updateMarkerPosition(latlng);
            geocodePosition(latlng);

            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'dragstart', function () {
                updateMarkerAddress('Dragging...');
            });

            google.maps.event.addListener(marker, 'drag', function (evt) {
                updateMarkerStatus('Dragging...');
                updateMarkerPosition(marker.getPosition());
            });

            google.maps.event.addListener(marker, 'dragend', function (evt) {
                updateMarkerStatus('Drag ended');
                geocodePosition(evt.latLng);
            });

            // google.maps.event.addListener(marker, 'dragend', function(a) {
            //     console.log(a);
            //     var div = document.createElement('div');
            //     div.innerHTML = a.latlng.lat().toFixed(4) + ', ' + a.latlng.lng().toFixed(4);
            //     document.getElementsByTagName('body')[0].appendChild(div);
            // });

        }

        var geocoder = new google.maps.Geocoder();

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function (responses) {
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        }

        function updateMarkerStatus(str) {
            document.getElementById('markerStatus').innerHTML = str;
        }

        // function updateMarkerPosition(latlng) {
        //     document.getElementById('info').value = [
        //         latlng.lat(),
        //         latlng.lng()
        //     ].join(',');
        // }

        function updateMarkerAddress(str) {
            document.getElementById('address').innerHTML = str;
            document.getElementById('location').value = str;
        }

        var isINI = false;
        $('#myModal_branch').on('shown.bs.modal', function () {
            // e.preventDefault();
            if(!isINI){
                isINI = true;
                myMap();
            }

        });


        $('#done').on('click', function () {
            var address = $("#address").text();
            $('[name="venue_address"]').val(address);
        });


        $('#event_user_type').on('change', function () {
            var eventVal = $(this).val();
            if(eventVal == 2){
                $(".control-group.perm_member").hide();
                $(".control-group.perm_member").find("option").each(function () {
                   $(this).prop("selected",false);
                });
            }else{
                $(".control-group.perm_member").show();
            }
        });
        
        
       

        // $("#myModal_branch").on("hidden.bs.modal", function(){
        // $("#smallModalBody").html("");
        // });

    });
</script>
<script type="text/javascript">
function checkmember()
 {

  var user = $('#event_user_type :selected').val();
 var member = $('select_append :selected').val();

 alert(member);
  
 if (user == 1 && member== null)
 {
   
  // swal("Here's a message!")
  // document.getElementById("button").disabled = true;
   $('#errorname').show();

   
 } 

}
</script>
<!-- <script type="text/javascript">
 function checkmember()
 {

  var user = $('#event_user_type :selected').val();
 var member = $('#aaa :selected').val();

// alert(user);

  
 if (user == 1 && member== null)
 {
   
   document.getElementById("button").disabled = true;
   $('#errorname').show();

   
 } 

  else
  {
     document.getElementById("button").disabled = false;
   $('#errorname').hide();
  }
}
</script> -->

<script type="text/javascript">
$(document).ready(function(){
    $("#aaa").click(function(){
        //alert("The paragraph was clicked.");
   document.getElementById("button").disabled = false;
   $('#errorname').hide();
    });
});
  
</script>
<script>
  $(document).ready(function(){
// $("#endDate").change(function(){
//   var startDate = $('#startDate').val();
//    var endDate = $('#endDate').val();
   
//       var strd = new Date(startDate);
//       var endd = new Date(endDate);
      
//       var strtyear = strd.getYear();
//       var endyear = endd.getYear();
      
//       var strtdate = strd.getMonth()+1;
//       var endda = endd.getMonth()+1;
      
//       var datestr = strd.getDate();
//       var dateend = endd.getDate();
//         if(strtyear == endyear){
//         if(strtdate <= endda ){ // monthly condition
        
//               if(strtdate == endda){ // monthly condition
                    
//                     if(datestr <= dateend){
//                         document.getElementById("button").disabled = false;
//                     }
                    
//                     else
//                     {
//                         document.getElementById("button").disabled = true;
//                     }
                    
//                   //alert('success');
                  
//                 }
//              else if(strtdate < endda ){
//                   document.getElementById("button").disabled = false;  
//               }
//               else{
//                     document.getElementById("button").disabled = true;   
//               }
          
//         }
          
//           else{
//               alert('end month must be grater then start month');
//               document.getElementById("button").disabled = true;
//           }
          
          
//         }
//         else if(strtyear < endyear){
//             document.getElementById("button").disabled = false;
//         }
//         else
//         {
//             document.getElementById("button").disabled = true;
//         }
     
// });    
  });
  
</script>
<script type="text/javascript">
/*$(document).ready(function(){
   

    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    
   var strdate=startDate.split(' ')[0];
   var enddate=endDate.split(' ')[0];
   
   var strdateagain = Date.parse(strdate);
   var enddateagain = Date.parse(enddate);
   
   alert(strdateagain);
   alert(enddateagain);
        
        if (strdateagain < enddateagain){
    alert('hello');
    }
});*/
  
</script>

<script type="text/javascript">
     $(document).ready(function(){
        $('#event_user_type').on('change', function(){
            var user = $('#event_user_type :selected').val();
      //alert(user);
    if (user == 2)
     {   
  document.getElementById("button").disabled = false;
   $('#errorname').hide();
 } 

 else{
    document.getElementById("button").disabled = true;
   $('#errorname').show();
 }
        });
        });
</script>