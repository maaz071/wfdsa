<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>
<?php if($this->session->flashdata('success') != ''){?>
                                        <script>
                                            toastr.info("<?php echo $this->session->flashdata('success');?>")
                                        </script>
                                        <?php } elseif($this->session->flashdata('fail') != ''){?>
                                            <script>
                                            toastr.error("<?php echo $this->session->flashdata('fail');?>")
                                        </script>
                                            <?php }?>
<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3>Gallery</h3>
                             <a class="btn" href="http://www.wfdsamobileportal.com//gallery/<?php echo trim($type)?>album" style="float:right; margin:6px;">Back</a>
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="container">
                                <div class="row">
                            <?php echo form_open_multipart(base_url() . 'gallery/additem', 'class="form-horizontal"'); ?>
                                    
                                <div class="alert" style="display:none;" role="alert" ></div>
                                    
                            <div class="tabbable">
                                    
                                <div class="failed1" id="failed1" style="display:none;">
                                  <p><strong>failed!</strong> Please fill out all Fields...</p>
                                </div>

                                <div class="tab-pane" id="formcontrol-group">
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <input type="hidden" name="type" value="<?php echo $type?>">     
                                        <input type="hidden" name="album_id" value="<?php echo $album_id?>">                           
                                        <?php if($type != "photo"){?>
                                        <div class="span10"> 
                                            
                                        <!--permission view copied from events-->
                                                <label class="control-label">Uplaod URL
                                                    <p style="font-size:10px;">(Embeded code for youtube OR vemeo videos)</p></label>
                                                <div class="control-group">
                                                    <input id="upload_link" type="text" class="span3" name="upload_link" value=""> 
                                                </div> <!-- /control-group -->    


                                        </div>    
                                            <?php }
                                            elseif($type != "video") {?>
                                                <div class="span10">   
                                            <div class="control-group">    
                                                <label class="control-label">Upload <?php echo $type?><p style="font-size:10px;">(max file size is 5Mb)</p></label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="fileToUpload" id="fileToUpload" accept="image/*">
                                                </div>   
                                            </div>
                                        </div>
                                        <?php }?>
                                        <div class="span2">
                                             <input type="submit" class="btn btn-primary "
                                style="margin:3px auto;" value="submit" id="success" />
                                        </div>
                                    </fieldset>
                                    
                                </div>  
                            </div>
                            <?php echo form_close(); ?>
                                </div>
                                <div class="row">   
                                    <div class="span12">
                                    <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php if($items != 'false'){?>
                                    <?php foreach($items as $row){?> 
                                                <?php if($row['type'] == 0){?>
                                                    <div class="span3" style="border:1px solid grey;height: 180px; background: #f1f1f1;margin-bottom: 15px">
                                                <img src="<?php echo $row['upload_link']?>" style="width: 95%;
    border: 1px solid #cecece;
    display: block;
    margin: 6px auto;
    height: 165px;background-color: #ffff"/>
    <?php }
    elseif($row['type'] == 1){
    ?>
    <div class="span5" style="border:1px solid grey;height: 260px; background: #f1f1f1;margin-bottom: 15px">
<video src="<?php echo $row['upload_link']?>" style="width: 95%;
    border: 1px solid #cecece;
    display: block;
    margin: 6px auto;
    max-height: 230px;background-color: #ffff" controls>
</video>
<?php }
else{?>
    <div class="span5" style="border:1px solid grey;height: 260px; background: #f1f1f1;margin-bottom: 15px">
    <div style="width: 95% !important;
    border: 1px solid #cecece;
    display: block;
    margin: 6px auto;
    max-height: 230px !important;background-color: #ffff">
        <?php echo $row['upload_link'];?>
    </div>
    <?php }?>
    <a href="<?php base_url()?>/gallery/delitem/<?php echo $row['gallery_id']?>" style="float: right;color: red"><i class="icon-remove" style="padding: 5px 5px"></i></a>
    <!-- <a style="float: right" class="icon-info-sign"></a> -->
                                            </div>
                                    <?php }?>
                                    <?php }
                                    else{?>
                                        <div class="span3">
                                            <h4>Empty gallery</h4>
                                        </div>
                                        <?php }?>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>