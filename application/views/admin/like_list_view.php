<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-thumbs-up-alt"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                                <!--<a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>-->
                            </div>
                        </div> <!-- /widget-header --> 


                        <div class="widget-content">

                            <div class="control-row">	 
                                <div class="control-group">
                                    <select class="" name="">
                                        <option>-Select Event-</option>
                                        <option>Event 1</option>
                                        <option>Event 2</option>
                                    </select>
                                </div> 
                            </div>

                            <div class="alert" style="padding:5px; display:none;"  ></div>
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr> 
                                        <th class="text-center" width="70%">Event Name</th> 
                                        <th class="text-center" width="15%">Likes</th> 
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <tr id='addr0' data-id="0">
                                                <td> <input type="checkbox" name="check[]"  class="checkbox" value="<?php echo $record[$row_id]; ?>">   </td>
                                                <td>H.R Training </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    <tr id='addr0' data-id="0"> 
                                        <td>H.R Training </td>
                                        <td><center><b>898 &nbsp;&nbsp;</b> <a href="#like_Modal" role="button" data-toggle="modal">View Members</a></center></td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9"><button class="btn btn-danger" id="delete_selected"  data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete'; ?>" ><i class="icon-trash"></i></button>    
                                </th> 
                                </thead>

                                </tfoot> 
                            </table>

                           
                            <div id="like_Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Thank you for visiting EGrappler.com</h3>
                                </div>
                                <div class="modal-body">
                                    <p>One fine body…</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                    <button class="btn btn-primary">Save changes</button>
                                </div>
                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->