<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        var eventVal = $('#event_user_type').val();
        if (eventVal == 2) {
            $(".control-group.perm_member").hide();
            $(".control-group.perm_member").find("option").each(function () {
                $(this).prop("selected", false);
            });
        } else {
            $(".control-group.perm_member").show();
        }
    });
</script>

<style>
    .buttons {
        width: 12%;
        float: right;
    }

    .span5 {
        float: right;
        margin-top: 4%;
        margin-right: -7%;
    }
</style>

<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="widget ">
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>

                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>"
                               style="float:right; margin:6px;">Back</a>
                            <!-- adding submit button   -->
                            <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>
                            <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>

                            <input type="submit" class="btn btn-primary "
                                   style="float:right; margin:6px; " value="submit" id="button"/>
                            <!--  <button onclick="myFunction()">Click me</button> -->
                            <div class="alert" style="display:none;" role="alert"></div>

                            <!-- end submit button -->


                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"></div>
                                    <!--adding submit button-->


                                    <!-- end buttton-->


                                    <fieldset>
                                        <div class="span6">

                                            <div class="control-group">
                                                <label class="control-label">Categories<span style="color:red">*</span></label>
                                                <select required class="span3 select_append refreshcategory" name="categories[]"
                                                        size="8" style="width:48%;">
                                                    <!--  <option value="">-Select Categories-</option> -->
                                                    <?php foreach ($categories as $value) { ?>
                                                        <option id="role_option_<?php echo $value['categories_id']; ?>"
                                                                disabled="disabled"
                                                                value="<?php echo $value['categories_id']; ?>" <?php echo (!empty($selected) && in_array($value['categories_id'], $selected)) ? 'selected="selected"' : ''; ?>
                                                                style="font-weight:bold; color:black;"> <?php echo $value['name']; ?> </option>

                                                        <?php if (!empty($value['child'])) { ?>
                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                <option
                                                                    id="role_option_<?php echo $value2['categories_id']; ?>"
                                                                    value="<?php echo $value2['categories_id']; ?>" <?php echo (!empty($selected) && in_array($value2['categories_id'], $selected)) ? 'selected="selected"' : ''; ?>>
                                                                    &nbsp;&nbsp;&nbsp; <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>


                                                <div id="Add_Modal" class="modal hide fade" tabindex="-1" role="dialog"
                                                     aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">×
                                                        </button>
                                                        <h3 id="myModalLabel">Add Category</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="control-group">
                                                            <label class="control-label">Category Parent</label>
                                                            <select class="span3 role_select">
                                                                <option value="0">-None-</option>

                                                                <?php foreach ($categories as $value) { ?>
                                                                    <option
                                                                        value="<?php echo $value['categories_id']; ?>"
                                                                        style="font-weight:bold;"> <?php echo $value['name']; ?> </option>
                                                                    <?php foreach ($value['child'] as $value2) { ?>
                                                                        <option disabled
                                                                                value="<?php echo $value2['categories_id']; ?>">
                                                                            &nbsp;
                                                                            &nbsp;<?php echo $value2['name']; ?> </option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="control-row">
                                                            <label class="control-label">Category Name</label>
                                                            <div class="control-group">
                                                                <input type="text" class="span3 role_name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                            Cancel
                                                        </button>
                                                        <button class="btn btn-primary add_value_select"
                                                                data-val="<?php echo base_url('Resources/add_categories'); ?>">
                                                            Submit
                                                        </button>

                                                    </div>
                                                </div>


                                                <div id="Delete_Modal" class="modal hide fade" tabindex="-1"
                                                     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">×
                                                        </button>
                                                        <h3 id="myModalLabel">Delete Categories</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="alert alert_modal" style="display:none;"
                                                             role="alert"></div>
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th width="100%"> Category Name</th>
                                                                <th class="td-actions">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="deletebody">
                                                            <?php if (!empty($categories)) { ?>
                                                                <?php foreach ($categories as $value) { ?>
                                                                    <tr>
                                                                        <td><b><?php echo $value['name']; ?></b></td>
                                                                        <td class="td-actions"><a href="javascript:;" class="btn btn-danger btn-small delete_role"
                                                                                        id="<?php echo base_url('Resources/delete_categories/' . $value['categories_id']); ?>"
                                                                                        data-id="<?php echo $value['categories_id']; ?>">
                                                                                        <i
                                                                                            class="btn-icon-only icon-remove"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php if (!empty($value['child'])) { ?>
                                                                        <?php foreach ($value['child'] as $value2) { ?>
                                                                            <tr>
                                                                                <td>
                                                                                    &nbsp;&nbsp;&nbsp;<?php echo $value2['name']; ?></td>
                                                                                <td class="td-actions"><a
                                                                                        href="javascript:;"
                                                                                        class="btn btn-danger btn-small delete_role"
                                                                                        id="<?php echo base_url('Resources/delete_categories/' . $value2['categories_id']); ?>"
                                                                                        data-id="<?php echo $value2['categories_id']; ?>"><i
                                                                                            class="btn-icon-only icon-remove"> </i></a>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>

                                            </div>
                                            <div class="control-group">
                                                <label class="control-label"></label>

                                                <a href="#Add_Modal" role="button" data-toggle="modal"
                                                   class="btn btn-small btn-primary"><i class="icon-plus"></i></a>
                                                <!-- <a href="#Delete_Modal" role="button" data-toggle="modal"
                                                   class="btn btn-small btn-danger"><i class="icon-trash"></i></a> -->
                                            </div>


                                        </div>
                                        <div class="span5">

                                            <div class="control-row">
                                                <label class="control-label">Resource Title<span
                                                        style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input required type="text" class="span3" name="title_2"
                                                           value="<?php echo (!empty($record_info) && $record_info[0]['title_2']) ? $record_info[0]['title_2'] : ''; ?>">
                                                </div> <!-- /control-group -->
                                            </div>

                                            <?php if (!empty($announce[0]['resource_member'])) { ?>


                                                <div class="control-row"><p>Current
                                                        Permission: <?php echo $announce[0]['resource_member']; ?> </p>
                                                    <p><?php //echo $announce;?></p>


                                                </div>
                                            <?php } ?>


                                            <div class="control-row">
                                                <label class="control-label">Permission<span
                                                        style="color:red">*</span></label>
                                                <div class="control-group" onload="myFunction()">
                                                    <select class="span3" name="event_user_type" id="event_user_type">
                                                        <!--  <option disabled="disabled">-Select Role-</option>  -->
                                                        <option
                                                            value="1" <?php echo (isset($record_info) && $record_info[0]['resource_member'] == " ") ? 'selected="selected"' : ''; ?> >
                                                            Member
                                                        </option>
                                                        <option
                                                            value="2" <?php echo (isset($record_info) && $record_info[0]['resource_member'] == "Public") ? 'selected="selected"' : ''; ?> >
                                                            Public
                                                        </option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-row">
                                                <label class="control-label">Select Event (for event resource only)</label>
                                                <div class="control-group" onload="myFunction()">
                                                    <select class="span3" name="event_id" id="event_id">
                                                        <!--  <option disabled="disabled">-Select Role-</option>  -->
                                                        <?php foreach($events as $row){?>
                                                        <option
                                                            value="<?= $row['event_id']?>">
                                                            <?= $row['title']?>
                                                        </option>
                                                        <?php }?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="control-group perm_member"
                                                 style="<?php //echo (isset($record_info) && $record_info[0]['event_user_type'] && $record_info[0]['event_user_type'] == 1) ? 'display:block' : 'display:none'; ?>">
                                                <label class="control-label">Member Role<span style="color:red">*</span></label>
                                                <?php
                                                $selected_permission = array();
                                                if (isset($record_info)) {
                                                    if (!empty($record_info[0]['permission']))
                                                        $selected_permission = @unserialize($record_info[0]['permission']);
                                                }
                                                ?>


                                                <select class="span3 select_append" name="permission[]" multiple
                                                        style="width:47%; min-height:200px;  " id="permission">
                                                    <option disabled="disabled">-Select Role-</option>
                                                    <?php foreach ($roles as $value) { ?>
                                                        <option disabled="disabled"
                                                                value="<?php echo $value['member_role_id'];
                                                                //echo $value['name'];?>"
                                                                style="font-weight:bold; color:black;"> <?php echo $value['name']; ?> </option>

                                                        <?php if (!empty($value['child'])) { ?>
                                                            <?php foreach ($value['child'] as $value2) { ?>
                                                                <option
                                                                    value="<?php echo $value2['name']; ?>" <?php echo (in_array($value2['name'], $test1)) ? 'selected="selected"' : ''; ?>> <?php echo $value2['name']; ?> </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>


                                            <div class="alert alert-error" id="errorname" style="display:none;">
                                            <span>
                                             <p>*Please Select Memebers</p>
                                                 </span>
                                            </div>

                                            <!--  <span id="errorname"  style="color: red; "> </span>  -->


                                            <div class="control-row">
                                                <label class="control-label">File<span
                                                        style="color:red">*</span></label>
                                                <div class="control-group">
                                                    <input type="file" name="upload_file" id="file"
                                                           onclick="checkmember()" accept=
                                                           "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                                        text/plain, application/pdf"/>
                                                </div>
                                            </div>


                                            <?php if (!empty($record_info[0]['upload_file'])) { ?>
                                                <div class="control-row">
                                                    <!-- <label class="control-label">Image Preview</label> -->
                                                    <div class="control-group">
                                                        <!-- <img src="<?php echo $record_info[0]['upload_file']; ?>" style="max-width: 100%" />  -->
                                                    </div>
                                                </div>
                                                <?php
                                                $file = explode('/', $record_info[0]['upload_file']);
                                                # $file_name = preg_split("/[0-9]+/", $file[6]);
                                                ?>
                                                <div class="control-row"><a target="_blank"
                                                        href="<?php echo $record_info[0]['upload_file']; ?>"><?php //echo  ltrim($file_name[1].$file_name[3],'_');?></a>
                                                    <a target="_blank" download href="<?php echo $record_info[0]['upload_file']; ?>">Download
                                                        File<?php //echo  ltrim($file_name[1].$file_name[3],'_');?></a>
                                                    <input type="hidden"
                                                           value="<?php echo $record_info[0]['upload_file']; ?>"
                                                           name="fileSrc">


                                                </div>

                                            <?php } ?>
                                        </div>


                                        <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id"
                                               value="<?php echo $this->uri->segment(3); ?>">


                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>


                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->


            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
<script>

    $(document).ready(function () {
        $('#event_user_type').on('change', function () {
            var eventVal = $(this).val();
            if (eventVal == 2) {
                $(".control-group.perm_member").hide();
                $(".control-group.perm_member").find("option").each(function () {
                    $(this).prop("selected", false);
                });
            } else {
                $(".control-group.perm_member").show();
            }
        });
    });

    $('#event_user_type').on('change', function () {
        var eventVal = $(this).val();
        if (eventVal == 2) {
            $(".control-group.perm_member").hide();
            $(".control-group.perm_member").find("option").each(function () {
                $(this).prop("selected", false);
            });
        } else {
            $(".control-group.perm_member").show();
        }
    });

</script>
<script type="text/javascript">

    function checkmember() {

        var user = $('#event_user_type :selected').val();
        var member = $('#permission :selected').val();

        if (user == 1 && member == null) {
            // alert("Select members");
            // swal("Here's a message!")
            document.getElementById("button").disabled = true;
            $('#errorname').show();


        }

    }
</script>

<script type="text/javascript">
    $('#permission').on('change', function () {
        document.getElementById("button").disabled = false;
        $('#errorname').hide();

    });
</script>

<script type="text/javascript">

    $('#event_user_type').on('change', function () {
        var user = $('#event_user_type :selected').val();
        // alert(user);
        if (user == 2) {
            document.getElementById("button").disabled = false;
            $('#errorname').hide();
        }
        else {
            document.getElementById("button").disabled = true;
            $('#errorname').show();
        }

    });
</script>
