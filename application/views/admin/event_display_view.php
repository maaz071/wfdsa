<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>View <?php echo ucfirst($this->uri->segment(1)); ?></h3>  
                              
                              
                            
                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <a href="<?php echo base_url('poll?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">View Polls</a>
                            <a href="<?php echo base_url('attendees?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">View Attendees</a>
                            <a href="#Likes_Modal" role="button" data-toggle="modal" class="btn  btn-primary" style="float:right; margin:6px;"><?php echo count($likes) ;?> &nbsp; Likes    </a>
                            <!-- <a href="<?php echo base_url('comment?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">View Comments</a> -->
                            <a href="<?php echo base_url('event_gallery?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">Gallery</a>
                            <a href="<?php echo base_url('user_photo?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">Member photos</a>
                            <a href="<?php echo base_url('event/event_resources?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">Event Resources</a>
                            <a href="<?php echo base_url('event/event_notifications?event_id='.$this->uri->segment(3));?>" class="btn btn-primary" style="float:right; margin:6px;">Event Notifications</a>
                            
                            
                            <div id="Likes_Modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <i class="icon-thumbs-up"></i> <h3 id="myModalLabel">Likes </h3>
                               </div>
                               <div class="modal-body">
                                    <?php foreach($likes as $value) {?>
                                        <ul class="messages_layout">
                                            <li class="from_user left"> <a href="<?php echo $value['upload_image'];?>" class="avatar"><img src="<?php echo $value['upload_image'];?>" style="width:50px; height:50px;"/></a>
                                              <div class="message_wrap span4" style="margin-left:0px;">  
                                                <div class="info"> <a class="name"><?php echo $value['member_name'];?></a> <span class="time"></span>
                                                </div>
                                              </div>
                                            </li> 
                                        </ul>
                                    <?php } ?>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 
                                </div>
                            </div>
                            
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                            <div class="control-row">											
                                                <label class="control-label"><b>Title</b></label>
                                                <div class="control-group">
                                                   <span>
                                                       <?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>
                                                   </span>
                                                </div> <!-- /control-group -->	  
                                            </div>   

                                            <div class="control-group">
                                                <label class="control-label"><b>Start Date</b></label>
                                                <div class="control-row">
                                                   <?php echo (isset($record_info) && $record_info[0]['start_date']) ? ($record_info[0]['start_date'])  : ''; ?>
                                                </div>
                                                
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label"><b>End Date</b></label>
                                                <div class="control-row">
                                                   <?php echo (isset($record_info) && $record_info[0]['end_date']) ? ($record_info[0]['end_date']) : ''; ?>
                                                </div>
                                               
                                            </div>

                                            <div class="control-row">											
                                                <label class="control-label"><b>Place</b></label>
                                                <div class="control-group">
                                                    <?php echo (isset($record_info) && $record_info[0]['place']) ? $record_info[0]['place'] : ''; ?>
                                                </div>  
                                            </div> 

                                            <div class="control-row">											
                                                <label class="control-label"><b>Permission</b> </label>
                                                <div class="control-group">
                                                   <?php echo (isset($record_info)) ? ucfirst($record_info[0]['permission']) : ''; ?>
                                                </div> 
                                            </div>
                                            
                                            <div class="control-row"> 
                                                <label class="control-label"><b>Agenda</b></label>
                                                <div class="control-group"> 
                                                   <?php echo (isset($record_info) && $record_info[0]['agenda']) ? $record_info[0]['agenda'] : ''; ?>
                                                </div>  
                                            </div>

                                             
                                        </div>    

                                          
                                        <div class="span5">
                                            
                                            <div class="control-row">											
                                                <label class="control-label"><b>Speaker</b></label> 
                                                 <div class="control-group"> 
                                                   <?php echo (isset($record_info) && $record_info[0]['speaker']) ? $record_info[0]['speaker'] : ''; ?>
                                                </div>  
                                            </div>   

                                            <div class="control-row">											
                                                <label class="control-label"><b>Fee</b> <i class="icon-dollar"></i></label>
                                                <div class="control-group">
                                                   <?php echo (isset($record_info) && $record_info[0]['fee']) ? $record_info[0]['fee'] : ''; ?>
                                                </div>  
                                            </div>

                                            <div class="control-row">											
                                                <label class="control-label"><b>No. Seats</b> </label>
                                                <div class="control-group">
                                                   <?php echo (isset($record_info) && $record_info[0]['no_seats']) ? $record_info[0]['no_seats'] : ''; ?>
                                                </div> <!-- /control-group -->	  
                                            </div>
<style>
.archive img{
    width: 70px
}
</style>
                                            <div class="archive">
                                                <a href="<?php echo base_url() . $this->uri->segment(1) . '/event_archive?event_id='.$this->uri->segment(3); ?>"><img src="http://www.iconeasy.com/icon/png/Object/Package%20Icons/PackageIcon%20%20%20Zip.png" /> </a>
                                            </div> 
                                            
                                        </div> 


                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?> 

                                        </div>

                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main --> <br><br><br>
 