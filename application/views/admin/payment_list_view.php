<style>
    #tab_logic tr[visible='false'],

    .no-result{
        display:none;
    }

    #tab_logic tr[visible='true']{
        display:table-row;
    }
    
    .icon{ margin-left:0px !important;}
    .search_div{ display:none;}
</style>
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-money"></i>
                            <h3><?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;">
                               <!-- <a class="btn btn-primary" href="<?php echo base_url() . $this->uri->segment(1); ?>/create">  Add <?php echo ucfirst($this->uri->segment(1)); ?></a>-->
                                  <a class="btn btn-default search_btn"><i class="icon icon-search"></i></a>
                            </div>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="alert" style="padding:5px; display:none;"></div>
                            
                            <select class="select_go_url" data-id="<?php echo base_url('payment?event_id=');?>">
                                <option value="">-Event-</option>
                                <?php foreach($event as $value) { ?>
                                    <option value="<?php echo $value['event_id'];?>" <?php echo ($value['event_id'] == $event_id) ? 'selected="selected"' : ''; ?>><?php echo $value['title'];?></option>
                                <?php } ?>
                            </select>
                            
                             
                            
                            <div class="form-group pull-left search_div"> 
                                <input type="text" class="search form-control" placeholder="Search Here"> 
                            </div>
                           
                            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                                <thead>
                                    <tr class="sortable">
                                         
                                        <th class="text-center" width="10%">Event Title &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Member Name &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Payment Date &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Payment Status &nbsp;<i class=""></i></th>
                                        <th class="text-center" width="10%">Payment Amount &nbsp;<i class=""></i></th> 
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php if (!empty($record_list)) { ?>
                                        <?php foreach ($record_list as $record) { ?>
                                            <tr id='addr0' data-id="0">
                                               
                                                <td><?php echo ($record['event_name']) ? $record['event_name'] : '-'; ?> </td>
                                                <td> <center> <?php echo ($record['member_name']) ? ($record['member_name']): '-'; ?> </center></td>
                                                <td>  <center><?php echo ($record['payment_date']) ? date('d-m-Y H:i:s', strtotime($record['payment_date'])) : '-'; ?> </center></td>
                                                <td><center> <?php echo ($record['payment_status'] == 1) ? "Paid" : 'Unpaid'; ?></center> </td>
                                                <td style="text-align:right;"><?php echo ($record['payment_amount']) ? number_format($record['payment_amount'],2) : '-'; ?></td>
                                               
                                                 
                                            </tr>
                                    <?php } ?>
                                <?php } else { ?> 
                                <tr>
                                    <td colspan="6"><i class="fa fa-warning"></i> No Result Found</td>
                                </tr>
                                <?php } ?>
                                
                                </tbody>
                                <tfoot>
                                <thead>
                                <th colspan="9">     
                                </th> 
                                </thead>

                                </tfoot> 
                            </table> 
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->   <br><br>   <br><br>

    </div> <!-- /main-inner -->

</div> <!-- /main -->