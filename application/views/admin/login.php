<!DOCTYPE html>
<html lang="en">
 
    <head>
        <meta charset="utf-8">
        <title>WFDSA</title>
        <link rel="icon" href="<?php echo base_url();?>/uploads/login_image/wfdsa_icon.png" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes"> 

        <?php echo link_tag('assets_admin/css/bootstrap.min.css'); ?>
        <?php echo link_tag('assets_admin/css/bootstrap-responsive.min.css'); ?>
        <?php echo link_tag('assets_admin/css/bootstrap_fonts.css'); ?>
        <?php echo link_tag('assets_admin/css/font-awesome.css'); ?>
        <?php echo link_tag('assets_admin/css/style.css'); ?>
        <?php echo link_tag('assets_admin/css/pages/signin.css'); ?>
         <?php echo link_tag('assets_admin/css/datepicker/bootstrap-datetimepicker.min.css'); ?> 

    </head>
    <style>
    
        body {
            
           
        	background: url('<?php echo base_url();?>./uploads/login_image/background.jpg') no-repeat center center fixed;
            
        }
        
        
       
    </style>

    <body> 
        
          <div class="navbar navbar-fixed-top"> 
            <div class="navbar-inner" style="background:none !important; -webkit-box-shadow:none !important;"> 
              
               <div class="container">
                    
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                     <center>
                        <a class="brand" href="#" style="margin-left:43%;">
                            <img src=<?php echo base_url('/uploads/login_image/splash_logo.png'); ?>>			
                        </a>	 
                      </center>
                </div> 
               
            </div> 
        </div>  

        <div class="account-container" style="margin: 20px auto 0 auto !important; ">

            <div class="content clearfix">


                <?php echo form_open(base_url() . 'login/login_check', 'class="login" id="' . $this->uri->segment(1) . '_form"'); ?>

                <h1>Please Login</h1>		

                <div class="login-fields">
                    <p>Provide your Credentials</p>
                    <div class="alert" style="display:none;"></div>
                    

                    <div class="field">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" value="" placeholder="Username" class="login username-field" />
                        <input type="hidden" id="uri_segment" name="uri_segment" value="<?php echo $this->uri->segment(1);?>" />
                    </div> <!-- /field -->

                    <div class="field">
                        <label for="password">Password:</label>
                        <input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field"/>
                    </div> <!-- /password -->

                </div> <!-- /login-fields -->

                <div class="login-actions">
                    <a href="<?php echo base_url('forget_password');?>">Forget Password</a>
                    <input type="submit" class="button btn btn-success btn-large login_submit" value="Sign In"> 
                </div> <!-- .actions -->



                <?php echo form_close(); ?>

            </div> <!-- /content -->

        </div> <!-- /account-container -->



        <script src="<?php echo base_url(); ?>assets_admin/js/jquery-1.7.2.min.js"></script>  
        <script src="<?php echo base_url(); ?>assets_admin/js/bootstrap.js"></script> 
        <script src="<?php echo base_url(); ?>assets_admin/js/signin.js"></script> 
        <?php require_once( APPPATH . 'views/custom_js/main_js.php'); ?>
        <script src="<?php echo base_url(); ?>assets_admin/js/datepicker/bootstrap-datetimepicker.js"></script>

    </body>

</html>
