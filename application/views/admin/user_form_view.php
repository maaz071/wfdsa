<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }


</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><?php echo ($this->uri->segment(2) == 'create') ? 'Add' : 'Edit'; ?> <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit_product_form" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>

                                        <div class="span5">
                                            <div class="control-group">											
                                                <label class="control-label">Name</label>
                                                <div class="control-row">
                                                    <input type="text" class="span3" name="name" value="<?php echo (!empty($record_info[0])) ? $record_info[0]['name'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>   
                                             
                                            <div class="control-group">											
                                                <label class="control-label">Email</label>
                                                <div class="control-row">
                                                    <input type="text" class="span3" name="email" value="<?php echo (!empty($record_info[0])) ? $record_info[0]['email'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>   
                                              

                                        </div>   
                                        <div class="span6">  

                                           
                                            <div class="control-group">											
                                                <label class="control-label">Username</label>
                                                <div class="control-row">
                                                    <input type="text" class="span4" name="username" value="<?php echo (!empty($record_info[0])) ? $record_info[0]['username'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>  
                                            <div class="control-group">											
                                                <label class="control-label">Password</label>
                                                <div class="control-row">
                                                    <?php if($this->uri->segment(2) == 'edit') {?>
                                                        <input type="password" disabled class="password preview-password-rgb"  name="password" value="<?php echo (isset($record_info[0]['password'])) ? '123456789' : ''; ?>" style="width:291px;">
                                                        <button type="button"  id="password_enable" class="btn btn-default"><i class="icon icon-pencil"></i> Edit</button>
                                                    <?php } else { ?>
                                                        <input type="password" class="preview-password-rgb span4" name="password" value="">
                                                    <?php } ?>
                                                </div> <!-- /control-group -->	  
                                            </div>
                                            
                                            <div class="control-group">											
                                                <label class="control-label">Retype Password</label>
                                                <div class="control-row">
                                                    <?php if($this->uri->segment(2) == 'edit') {?>
                                                        <input type="password" class="span4 password preview-password-rgb" disabled name="retype_password" value="<?php echo (isset($record_info[0]['password'])) ? '123456789' : ''; ?>">
                                                    <?php } else { ?>
                                                        <input type="password" class="span4 preview-password-rgb"  name="retype_password" value="">
                                                    <?php } ?>
                                                </div> <!-- /control-group -->	  
                                            </div>
                                            


                                        </div>







                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?> 

                                        </div>

                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->