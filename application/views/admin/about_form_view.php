<style>
    .buttons{
        width: 12%;
        float: right; 
    }
    
    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3><?php //echo ucfirst($this->uri->segment(1));?>About WFDSA</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group"> 
                                    <?php echo form_open_multipart(base_url() . 'about/update_action', 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <?php if($this->session->flashdata('successmessage') != ''){?>
                                        <script>
                                            toastr.info("<?php echo $this->session->flashdata('successmessage');?>")
                                        </script>
                                        <?php }?>
                                    <button type="submit" data-id="" class="btn btn-primary " style="float: right; margin: 6px;">Submit</button>
                                    <fieldset> 
                
                                        <div class="span11">

                                            <div class="control-group">
                                                <textarea class="form-control" name="about_text" style="min-width: 100%;" rows="6"><?php echo (isset($record_info) && $record_info[0]['about_text']) ? $record_info[0]['about_text'] : ''; ?></textarea>
                                            </div>
                                            
                                            <div class="control-group">	  
                                                <?php $image = (isset($record_info[0]['upload_pic'])) ? $record_info[0]['upload_pic'] : ''; ?> 

                                                <label class="control-label">About Image
                                                <p style="font-size:10px;">Max Dimensions(500*500)</p></label>
                                                
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="upload_pic" id="upload_pic">
                                                     <input type="hidden" name="about_id" value="<?php echo $record_info[0]['about_id'] ?>"> 
                                                </div>   
                                            </div> 
                                            

                                            <?php if (isset($record_info[0])) { ?>
                                                <label class="control-label">Image Preview</label>
                                                <div class="control-group">
                                                    <img width="350" height="300" src="<?php echo  $image; ?>">
                                                </div> 
                                            <?php } ?>
                                             <br>
                                        </div> 
                                       
                                       <div class="span11">
                                            <legend><h3>Contact Us</h3></legend> 
                                       </div>
                                        
                                        <div class="span6">
                                               <div class="control-group"> 
                                                    <label class="control-label">Email</label>
                                                    <div class="control-group">
                                                        <input type="text" class="span3" name="contact_email" id="contact_email" value="<?php echo (isset($record_info[0]['contact_email'])) ? $record_info[0]['contact_email']:'';?>">
                                                    </div>
                                                </div> 
                                                
                                                <div class="control-group"> 
                                                    <label class="control-label">Phone</label>
                                                    <div class="control-group">
                                                        <input type="text" class="span3" name="phone" id="phone" value="<?php echo (isset($record_info[0]['phone'])) ? $record_info[0]['phone'] : ''; ?>">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="span5">
                                             <div class="control-group">	   
                                                <label class="control-label">Address</label>
                                                <div class="control-group">
                                                   <textarea name="contact_address" style="width:338px;"><?php echo (isset($record_info[0]['contact_address'])) ? $record_info[0]['contact_address'] : ''; ?></textarea>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="span11">
                                            <legend><h3>Social sharing message</h3></legend> 
                                       </div>
                                        <div class="span11">

                                            <div class="control-group">
                                                <textarea class="form-control" name="social_msg" style="min-width: 100%;" rows="6"><?php echo (isset($record_info) && $record_info[0]['social_msg']) ? $record_info[0]['social_msg'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        

                                        </div> 
                                        <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

 