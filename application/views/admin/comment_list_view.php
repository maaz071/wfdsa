
<style>
    .message_wrap {
        min-width:455px !important;
    }
</style>
<script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.0/firebase-messaging.js"></script>

<!-- Leave out Storage -->
<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-comments-alt"></i>
                            <h3><span style="color:red;"><?php echo ($event_name) ? $event_name : '';?></span>&nbsp;<?php echo ucfirst($this->uri->segment(1)); ?></h3>
                            <div class="button" style="float:right; margin-right: 10px;"> 
                                <?php if($this->input->get('event_id')) {?>
                                   <a href="<?php echo base_url('event/view/'.$this->input->get('event_id'))?>" class="btn btn-default" style="float:right; margin:5px;">Back</a>
                                <?php } ?>
                            </div>
                        </div> <!-- /widget-header --> 
                        <!-- /firebase-header -->
                        
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAEltTjrdvia0li-g6iFIXfjPkuhoqbSQg",
    authDomain: "pharmacypasha.firebaseapp.com",
    databaseURL: "https://pharmacypasha.firebaseio.com",
    projectId: "pharmacypasha",
    storageBucket: "pharmacypasha.appspot.com",
    messagingSenderId: "126488850229"
  };
  firebase.initializeApp(config);
</script>
                        <!-- /firebase-end --> 

                        <div class="widget-content">
                            
                            <div class="alert" style="padding:5px; display:none;"></div>
                            
                             <?php if (!empty($record_list)) { ?>
                                <?php foreach ($record_list as $key=> $record) { ?> 
                                    <?php if($key % 2 == 0) {?>
                                        <div class="span6" style="float:right;">
                                             <ul class="messages_layout">
                                                 <li class="from_user left"> <a href="#" class="avatar"><img src="<?php echo base_url().$record['upload_image'];?>" style="width:50px; height:50px;"/></a>
                                                    <div class="message_wrap"> <span class="arrow"></span>
                                                            <div class="info"> 
                                                                <a class="name"><?php echo (!empty($record['member_name'])) ? $record['member_name'] : '-' ;?></a>
                                                                <span class="time"><?php echo (!empty($record['created_on'])) ? $record['created_on'] : '-' ;?></span>
                                                                <div class="options_arrow">
                                                                    <div class="pull-right"> 
                                                                        <a href="" class="btn btn-small btn-danger delete_comment" id="<?php echo $record['comment_id'];?>" data-val="<?php echo base_url('comment/delete_comment');?>"  style="margin-top:-10px;"> <i class=" icon-remove"></i></a> 
                                                                    </div>
                                                                </div>
                                                              
                                                             </div>
                                                             
                                                         <div class="text"> <?php echo $record['comment_text'];?></div>
                                                    </div>
                                                  </li>
                                             </ul>
                                        </div>
                                    <?php } else { ?>
                                        <div class="span6" style="float:left;">
                                             <ul class="messages_layout">
                                                 <li class="from_user left"> <a href="#" class="avatar"><img src="<?php echo base_url().$record['upload_image'];?>" style="width:50px; height:50px;"/></a>
                                                      <div class="message_wrap"> <span class="arrow"></span>
                                                            <div class="info">
                                                                 <a class="name"><?php echo (!empty($record['member_name'])) ? $record['member_name'] : '-' ;?></a> 
                                                                 <span class="time"><?php echo (!empty($record['created_on'])) ? $record['created_on'] : '-' ;?></span>
                                                                 <div class="options_arrow">
                                                                    <div class="pull-right"> 
                                                                        <a href="" class="btn btn-small btn-danger delete_comment" id="<?php echo $record['comment_id'];?>" data-val="<?php echo base_url('comment/delete_comment');?>"  style="margin-top:-10px;"> <i class=" icon-remove"></i></a> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         <div class="text"> <?php echo $record['comment_text'];?></div>
                                                          </div>
                                                  </li>
                                             </ul>
                                        </div> 
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?> 

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main --> <br><br>