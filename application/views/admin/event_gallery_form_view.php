<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    .thumbnail:hover {
        background: #f7f7f7;
    }

    .thumbnail{
        margin-bottom: 20px;
    }
    .thumbnail a > img { 
        height: 98px;
        width: 137px;
        border: solid 1px #cccccc;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3><span style="color:red;"><?php //echo $event[0]['event_name'] ? $event[0]['event_name'] : 0 ;?></span> Gallery</h3>
                           
                          <a class="btn" href="<?php echo base_url() . "/event/view/".$this->input->get('event_id'); ?>" style="float:right; margin:6px;">Back</a> 
                          
                          <button type="button" class="btn btn-danger delete_img" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/delete_gal_images'; ?>">Delete Image</button>
                          <button type="button" class="btn btn-danger update_image" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/update_permission'; ?>">For Registered Users</button>
                          <button type="button" class="btn btn-danger update_image_checkin" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/update_permission_checkin'; ?>"> For Checked In Users </button>

                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <button type="submit" class="btn btn-primary" style="float:right; margin:6px;" data-id="<?php echo base_url() . $this->uri->segment(1) . '/test'; ?>"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>
                                    
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset> 

                                        <div class="span6" style="margin-left:0px !important;"> 
                                            <div class="control-row">	  
                                                <label class="control-label">Select Image</label>
                                                <div class="control-group">
                                                    <input type="file" class="span3" name="upload_image" multiple id="upload_image" accept="image/*"> 
                                                </div>   
                                            </div>
                                            <div class="control-row">	  
                                                
                                                <div class="control-group">
                        
                                                    <input type="checkbox" name="allowed" value="1"> Only For Checked In Users <br> 
                                                </div>   
                                            </div>
                                        </div>  
                                       
                                            <div class="span12" style="margin-left:0px;">
                                                <br> 
                                                <legend><h3>Gallery Images</h3> </legend>
                                               
                                                <div class="container-fluid">
                                                    
                                                    <div class="row"> 
                                                        <?php foreach ($event as $key => $value) { ?>

                                                            <div class="span3">  <center><?php echo ($value['checked_in'] == 0) ? '<span class="label label-success">For Registerd Users</span>' : '<span class="label label-primary">For Checked-in Users</span>'; ?></center>
                                                                <div class="thumbnail">
                                                                    <center>
                                                                        <input type="checkbox" class="delete_chk_img" value="<?php echo $value['event_gal_id'];?>" style="float:left;">
                                                                            <img src="<?php echo  $value['uploaded_image']; ?>" alt="team" style="max-width: 150px;max-height: 150px">                         
                                                                    </center>
                                                                </div>
                                                            </div>   
                                                        <?php } ?> 
                                                    </div>
                                                </div>

                                            </div>
                                     
                                        <input type="hidden" name="event_id" value="<?php echo $this->input->get('event_id'); ?>">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->