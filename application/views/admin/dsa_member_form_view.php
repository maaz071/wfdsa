<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzUdS8dVvVFJ8K_mxEzMAM_xqe2_YvOlc&libraries=places"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> -->
<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    html, body, .container {
        height: 100%;
    }
    textarea.form-control {
        height: 100%;
    }

    input.span2_3, textarea.span2_3, .uneditable-input.span2_3 {
        width: 340px;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Add <?php echo ucfirst($this->uri->segment(1)); ?></h3>


                            <a class="btn" href="<?php echo base_url() . $this->uri->segment(1); ?>" style="float:right; margin:6px;">Back</a>
                            <button type="submit" class="btn btn-primary submit_product_form" style="float:right; margin:6px;"><?php //echo ($this->uri->segment(2) == 'create') ? 'Save' : 'Update'; ?> Submit</button>   
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group">
                                    <?php $action = ($this->uri->segment(2) == 'create') ? 'create_action' : 'update_action'; ?>	
                                    <?php echo form_open_multipart(base_url() . ucfirst($this->uri->segment(1)) . "/" . $action, 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>
                                        <div class="span6"> 
                                            <div class="control-row">											
                                                <label class="control-label">Company Name</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="title" value="<?php echo (isset($record_info) && $record_info[0]['title']) ? $record_info[0]['title'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div>   

                                            <div class="control-group">
                                                <label class="control-label">Start Date</label>
                                                <div class="control-row input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd mm yyyy - HH:ii:ss" data-link-field="dtp_input1">
                                                    <input size="16" name="start_date" type="text" value="<?php echo (isset($record_info) && $record_info[0]['start_date']) ? date('d-m-Y H:i:s', strtotime($record_info[0]['start_date']))  : ''; ?>" readonly style="width:206px"> 
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" value="" /><br/>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label">End Date</label>
                                                <div class="control-row input-append date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd mm yyyy - HH:ii:ss" data-link-field="dtp_input1">
                                                    <input size="16" name="end_date" type="text" value="<?php echo (isset($record_info) && $record_info[0]['end_date']) ? date('d-m-Y H:i:s', strtotime($record_info[0]['end_date'])) : ''; ?>" readonly style="width:206px">
                                                    <span class="add-on"><i class="icon-remove"></i></span>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <input type="hidden" id="dtp_input1" value="" /><br/>
                                            </div>

                                            <div class="control-row">											
                                                <label class="control-label">Member Name</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="venue" style="width:224px;" value="<?php echo (isset($record_info) && $record_info[0]['venue']) ? $record_info[0]['venue'] : ''; ?>">
                                                
                                                <button type="button" class="btn btn-danger btn-xs" id="loc_btn" data-toggle="modal" data-target="#myModal_branch"><i class="fa fa-map-marker" aria-hidden="true"></i></button>  
                                                    
                                                </span> 
                                                </div>

                                            </div> 

                                            <div class="control-row">											
                                                <label class="control-label">Phone </label>
                                                <div class="control-group">
                                                    <select class="span3" name="permission">
                                                        <option value="">-Select-</option>
                                                        <option value="member" <?php echo (isset($record_info) && $record_info[0]['permission'] == 'member') ? 'selected="selected"' : ''; ?>>Member</option>
                                                        <option value="public" <?php echo (isset($record_info) && $record_info[0]['permission'] == 'public') ? 'selected="selected"' : ''; ?>>Public</option>
                                                    </select>
                                                </div> 
                                            </div> 
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Fax <i class="icon-dollar"></i></label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="fee" value="<?php echo (isset($record_info) && $record_info[0]['fee']) ? $record_info[0]['fee'] : ''; ?>"> 
                                                </div>  
                                            </div>

                                            <div class="control-row">											
                                                <label class="control-label">Email</label>
                                                <div class="control-group">
                                                    <input type="text" class="span3" name="no_seats" value="<?php echo (isset($record_info) && $record_info[0]['no_seats']) ? $record_info[0]['no_seats'] : ''; ?>"> 
                                                </div> <!-- /control-group -->	  
                                            </div> 
                                             
                                             
                                        </div>    

                                          
                                        <div class="span5">
                                            
                                            <div class="control-row">											
                                                <label class="control-label">Address</label> 
                                                 <div class="control-group"> 
                                                    <textarea class="form-control span2_3" name="speaker" style="min-width: 72%;  resize: vertical" rows="8"><?php echo (isset($record_info) && $record_info[0]['speaker']) ? $record_info[0]['speaker'] : ''; ?></textarea>
                                                </div>  
                                            </div>  
                                            
                                            <div class="control-row"> 
                                                <label class="control-label">Website</label>
                                                <div class="control-group"> 
                                                    <textarea class="form-control" name="agenda" style="min-width: 72%; resize: vertical" rows="9"><?php echo (isset($record_info) && $record_info[0]['agenda']) ? $record_info[0]['agenda'] : ''; ?></textarea>
                                                </div>  
                                            </div>

                                            
                                            
                                        </div> 

                          


                                        <?php if ($this->uri->segment(2) == 'edit') { ?>
                                            <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                        <?php } ?> 

                                        </div>

                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main --> <br><br><br>

     
