<?php echo link_tag('assets_admin/css/ckedit/samples.css'); ?>  
<style>
    .buttons{
        width: 12%;
        float: right; 
    }

    .thumbnail:hover {
        background: #f7f7f7;
    }

    .thumbnail{
        margin-bottom: 20px;
    }
    .thumbnail a > img { 
        height: 98px;
        width: 137px;
        border: solid 1px #cccccc;
    }
    .label-danger {
        background-color: #a72e2a;
    }

</style>

<div class="main"> 
    <div class="main-inner"> 
        <div class="container"> 
            <div class="row"> 
                <div class="span12">   
                    <div class="widget "> 
                        <div class="widget-header">
                            <i class="icon-picture"></i>
                            <h3>Member Photos <span style="color:red;"><?php echo (!empty($event_name)) ? $event_name : ''; ?>&nbsp;</span></h3>
                            
                            
                            <?php if($this->input->get('event_id')) {?>
                               <a href="<?php echo base_url('event/view/'.$this->input->get('event_id'))?>" class="btn btn-default" style="float:right; margin:5px;">Back</a>
                            <?php } ?> 
                            <button type="button" class="btn btn-success approve_photo" data-id = "<?php echo base_url().$this->uri->segment(1);?>/approve_photo" style="float:right; margin:5px;">Approve Photos</button>   
                            <button type="button" class="btn btn-danger delete_photo" data-id = "<?php echo base_url().$this->uri->segment(1);?>/disapprove_photo"style="float:right; margin:5px;">Delete Photos</button>    
                            
                        </div> <!-- /widget-header -->

                        <div class="widget-content"> 
                            <div class="tabbable">
                                <div class="tab-pane" id="formcontrol-group"> 

                                    <?php echo form_open_multipart(base_url() . 'about/update_action', 'class="form-horizontal" id="' . $this->uri->segment(1) . '_form"'); ?>
                                    <div class="alert" style="display:none;" role="alert"> </div>
                                    <fieldset>  
                                      <!--  <div class="span11"> 
                                            <div class="control-row">											
                                                <label class="control-label">Event </label>
                                                <div class="control-group">
                                                    <select class="span3 user_upload_event_id" name="event_id" id="event_id">
                                                        <option value="">-Select-</option>
                                                        <?php foreach ($event as $value) { ?>
                                                            <option value="<?php echo $value['event_id']; ?>" <?php echo (isset($record_info) && $record_info[0]['event_id'] == $value['event_id']) ? 'selected="selected"' : ''; ?>><?php echo $value['title']; ?></option>  
                                                        <?php } ?>
                                                    </select>
                                                </div> 
                                            </div>  
                                        </div> -->

                                        <div class="span12" style="margin-left:0px;"> 

                                            <div class="container-fluid">
                                                <div class="row upload_img_div">  
                                                  <?php if(!empty($records)) { ?>
                                                    <?php foreach($records as $value) {?>
                                                        <div class="span3">   
                                                        
                                                        <center><?php echo ($value['approved'] == 0) ? '<span class="label label-danger">Not Approved</span>' : '<span class="label label-success">Approved</span>'; ?></center>
                                                        <div class="thumbnail">  
                                                            <center>  
                                                                <a href="<?php echo $value['uploaded_image'];?>">
                                                                    <?php if($value['approved'] == 0) { ?><input type="checkbox" class="chk_img" value="<?php echo $value['user_photo_id'];?>" style="float:left;"><?php } ?>
                                                                 <img src="<?php echo $value['uploaded_image'];?>">  
                                                                </a>  
                                                            </center>   
                                                        </div> 
                                                        
                                                             <ul class="messages_layout"> 
                                                                <li class="from_user left">  
                                                                    <a href="#" class="avatar"><img src="<?php echo $value['uploaded_image'];?>" style="width:50px;height:50px"/></a>
                                                                    <div class="message_wrap span2" style="margin-left:0px"> 
                                                                        <div class="info">  
                                                                           <span class="time"><?php echo $value['name']; ?> &nbsp;&nbsp; <?php echo $value['created_on']; ?></span>
                                                                        </div> 
                                                                    </div> 
                                                                </li> 
                                                            </ul>
                                                       </div>
                                                     <?php } ?>
                                                    <?php } else { ?>
                                                      <div class="alert alert-danger"><strong>No Photos Uploaded by Members</strong></div>
                                                    <?php } ?>
                                                </div>
                                            </div>


                                        </div>
                                        <input type="hidden" name="<?php echo $this->uri->segment(1); ?>_id" value="<?php echo $this->uri->segment(3); ?>">
                                    </fieldset>
                                    <?php echo form_close(); ?>
                                </div>  
                            </div>





                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
