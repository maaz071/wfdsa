<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 

<body>
    <h1> administrator Page</h1>

    <div class="banner">
        <div class="container">
            <div class="banner-bottom">
                <div class="banner-bottom-left">

                </div>
                <div class="banner-bottom-right">
                    <div  class="callbacks_container">
                        <ul class="rslides" id="slider4">
                            <li>
                                <div class="banner-info">
                                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;online shop</h3>


                                </div>
                            </li>
                            <li>
                                <div class="banner-info">
                                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shop Online</h3>

                                </div>
                            </li>
                            <li>
                                <div class="banner-info">
                                    <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pack your Bag</h3>

                                </div>								
                            </li>
                        </ul>
                    </div>
                    <!--banner-->
                    <script src="<?php echo base_url('assets/js/responsiveslides.min.js'); ?>"></script>
                    <script>
                        // You can also use "$(window).load(function() {"
                        $(function () {
                            // Slideshow 4
                            $("#slider4").responsiveSlides({
                                auto: true,
                                pager: true,
                                nav: false,
                                speed: 500,
                                namespace: "callbacks",
                                before: function () {
                                    $('.events').append("<li>before event fired.</li>");
                                },
                                after: function () {
                                    $('.events').append("<li>after event fired.</li>");
                                }
                            });

                        });
                    </script>
                </div>
                <div class="clearfix"> </div>
            </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <div class="shop">
                <a href="single.html"><b>SHOP COLLECTION NOW</b></a>
            </div>
        </div>
    </div>
    <!-- content-section-starts-here -->
    <div class="container">
        <div class="main-content">
            <div class="online-strip">
                <div class="col-md-4 follow-us">
                    <h3>follow us : <a class="twitter" href="#"></a><a class="facebook" href="#"></a></h3>
                </div>
                <div class="col-md-4 shipping-grid">
                    <div class="shipping">
                        <img src="<?php echo base_url('assets/images/shipping.png'); ?>" alt="" />
                    </div>
                    <div class="shipping-text">
                        <h3>Free Shipping</h3>
                        <p>on orders over Rs 199</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 online-order">
                    <p>Order online</p>
                    <h3>Tel:03312083282</h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="products-grid">
                <header>
                    <h3 class="head text-center">Latest Products</h3>
                </header>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="single.html"><img src="<?php echo base_url('assets/images/p1.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="single.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Pepsi</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">$50</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="single1.html"><img src="<?php echo base_url('assets/images/p2.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="single1.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">VIMTO</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">$40</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="single3.html"><img src="<?php echo base_url('assets/images/p3.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="single3.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Coca Cola Can</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 43</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="fc1s.html"><img src="<?php echo base_url('assets/images/ff1.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="fc1s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Mon Salwa Chicken Nuggets</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 499.99</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="fc2s.html"><img src="<?php echo base_url('assets/images/ff2.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="fc2s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Mon Salwa Chicken Shami Kabab</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 200</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="hhc2s.html"><img src="<?php echo base_url('assets/images/c2.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="hhc2s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">HARPIC POWER TOILET CLEANER LIME</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 135</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="mw2s.html"><img src="<?php echo base_url('assets/images/mw2.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="mw2s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Nestle Pure Life</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 25</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="t4s.html"><img src="<?php echo base_url('assets/images/tc7.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="t4s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Lipton Green Tea Bags - Lemon</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 85</span></a></p>
                </div>
                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="t3s.html"><img src="<?php echo base_url('assets/images/tc3.jpg'); ?>" alt="" /></a>
                    <div class="mask">
                        <a href="t3s.html">Quick View</a>
                    </div>
                    <a class="product_name" href="single.html">Nescafe Gold Blend Coffee</a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">Rs 592</span></a></p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <div class="other-products">
        <div class="container">
            <h3 class="like text-center">Featured Collection</h3>        			
            <ul id="flexiselDemo3">
                <li><a href="hhc1s.html"><img src="<?php echo base_url('assets/images/c1.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="single.html">Dettol Surface Cleaner</a>
                        <p><a class="item_add" href="#"><i></i> <span class=" item_price">Rs 195</span></a></p>
                    </div>
                </li>
                <li><a href="hh2s.html"><img src="<?php echo base_url('assets/images/t2.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="single.html">Rose Petal Tulip Regular</a>
                        <p><a class="item_add" href="#"><i></i> <span class=" item_price">Rs 57</span></a></p>
                    </div>
                </li>
                <li><a href="go3s.html"><img src="<?php echo base_url('assets/images/o3.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="single.html">Dalda Canola Oil</a>
                        <p><a class="item_add" href="#"><i></i> <span class=" item_price">Rs 540</span></a></p>
                    </div>
                </li>
                <li><a href="ks3s.html"><img src="<?php echo base_url('assets/images/k3.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="single.html">National Tomato Ketchup</a>
                        <p><a class="item_add" href="#"><i></i> <span class=" item_price">Rs 185</span></a></p>
                    </div>
                </li>
                <li><a href="t3s.html"><img src="<?php echo base_url('assets/images/tc3.jpg'); ?>" class="img-responsive" alt="" /></a>
                    <div class="product liked-product simpleCart_shelfItem">
                        <a class="like_name" href="single.html">Nescafe GoldBlend 100 gm</a>
                        <p><a class="item_add" href="#"><i></i> <span class=" item_price">Rs 592</span></a></p>
                    </div>
                </li>
            </ul>
            <script type="text/javascript">
                $(window).load(function () {

                    $("#flexiselDemo3").flexisel({
                        visibleItems: 4,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint: 480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint: 640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint: 768,
                                visibleItems: 3
                            }
                        }
                    });

                });
            </script>
            <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.flexisel.js'); ?>"></script>
        </div>
    </div>
    <!-- content-section-ends-here -->
    <div class="news-letter">
        <div class="container">
            <div class="col-md-4 follow-us">
                <h3>follow us : <a class="twitter" href="#"></a><a class="facebook" href="#"></a></h3>
            </div>
        </div>
    </div>
</body>

