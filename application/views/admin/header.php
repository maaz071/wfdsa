<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>WFDSA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="icon" href="<?php echo base_url();?>/uploads/login_image/wfdsa_icon.png" />
        
        <?php echo link_tag('assets_admin/css/bootstrap.min.css'); ?>
        <?php echo link_tag('assets_admin/css/bootstrap-responsive.min.css'); ?>
        <?php // echo link_tag('assets_admin/css/bootstrap_fonts.css'); ?>
        <?php echo link_tag('assets_admin/css/font-awesome.css'); ?>
        <?php echo link_tag('assets_admin/css/style.css'); ?>
        <?php echo link_tag('assets_admin/css/pages/dashboard.css'); ?> 
        <?php echo link_tag('assets_admin/css/datepicker/bootstrap-datetimepicker.min.css'); ?> 
        <?php echo link_tag('assets_admin/css/pages/reports.css'); ?> 
        <?php echo link_tag('assets_admin/css/editable/bootstrap-editable.css'); ?>
        <?php echo link_tag('assets_admin/css/password_prev/font-awesome-eyes.css'); ?>
        
        
       <?php echo link_tag('assets_admin/css/cropper/imgareaselect.css'); ?>

       <?php echo link_tag('assets_admin/cropper-master/dist/cropper.css'); ?>
        <!-- <link  href="/path/to/cropper.css" rel="stylesheet"> -->

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

<!--         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
<!--         <script src="dist_files/jquery.imgareaselect.js" type="text/javascript"></script>
        <script src="dist_files/jquery.form.js"></script>
        <link rel="stylesheet" href="dist_files/imgareaselect.css">
        <script src="functions.js"></script> -->
        
          
        <!--<script type="text/javascript" src="<?php echo base_url(); ?>assets_admin/js/datepicker/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>-->
        <?php // echo link_tag('assets_admin/css/ckedit/samples.css'); ?> 

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>  

    </head>
                                   <style>
        .toast-top-right{ top:10% !important; right:39% !important}
        </style>
    <style>
        .footer
        {
        position:fixed;
        left:0px;
        bottom:0px; 
        width:100%;
        background:#999;
        }
        
        .main {
            padding-bottom: 2em;
             border-bottom: 0px; 
        }
        
    </style>
    <body onload="initialize()">
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                            class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html"><img src="<?php echo base_url('uploads/login_image/logo_horizontal.png');?>" </a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown"> 
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:;">Settings</a></li>
                                    <li><a href="javascript:;">Help</a></li>
                                </ul>
                            </li>

                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                        class="icon-user"></i>&nbsp;&nbsp;<?php echo ($admin_name) ? $admin_name : 'None'; ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu"> 
                                    <li><a href="<?php echo base_url(); ?>login/change_password">Change Password</a></li>
                                    <li><a href="<?php echo base_url(); ?>login/logout_admin">Logout</a></li> 
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!--/.nav-collapse --> 
                </div>
                <!-- /container --> 
            </div>
            <!-- /navbar-inner --> 
        </div>
        <!-- /navbar -->
        <div class="subnavbar">
            <div class="subnavbar-inner">
                <div class="container">
                    <ul class="mainnav">

                        <li  class="<?php echo ($this->uri->segment(1) == 'admin_dashboard') ? 'active' : ''; ?>"><a href="<?php echo base_url('admin_dashboard'); ?>"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li> 
                        <li class="dropdown <?php echo ($this->uri->segment(1) == 'user' || $this->uri->segment(1) == 'role') ? 'active' : ''; ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><span>Administration</span> </a> 
                            <ul class="dropdown-menu">
                               <!-- <li><a href="<?php echo base_url('role'); ?>">Manage Roles</a></li>-->
                                <li><a href="<?php echo base_url('user'); ?>">Manage Users</a></li>            

                            </ul>
                        </li>  
                        <li class="dropdown <?php echo ($this->uri->segment(1) == 'non_member' || $this->uri->segment(1) == 'dsa' || $this->uri->segment(1) == 'member' || $this->uri->segment(1) == 'roster') ? 'active' : ''; ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-group"></i><span>Membership</span> </a> 
                            <ul class="dropdown-menu"> 
                                <li><a href="<?php echo base_url('member'); ?>">Members</a></li>
                                <li><a href="<?php echo base_url('non_member'); ?>">Non Member</a></li>
                                <li><a href="<?php echo base_url('dsa'); ?>">DSAs</a></li>
                                <li><a href="<?php echo base_url('roster'); ?>">Member Roster</a></li> 
                            </ul>
                        </li>

                        <li class="<?php echo ($this->uri->segment(1) == 'about') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>about/index/1"><i class="icon-info-sign"></i><span>About WFDSA</span> </a></li>
                        <!--<li class="<?php echo ($this->uri->segment(1) == 'contact_us') ? 'active' : ''; ?>"><a href="<?php echo base_url('contact_us'); ?>"><i class="icon-phone"></i><span>Contact Us</span> </a></li>-->
                        <li class="<?php echo ($this->uri->segment(1) == 'resources') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>resources/index/1"><i class="icon-group"></i><span>Resources</span> </a></li>
                        <li class="<?php echo ($this->uri->segment(1) == 'announcement') ? 'active' : ''; ?>"><a href="<?php echo base_url('announcement'); ?>"><i class="icon-bullhorn"></i><span>Announcement</span> </a></li>

                        <li class="<?php echo ($this->uri->segment(1) == 'event') ? 'active' : ''; ?>"><a href="<?php echo base_url('event'); ?>"><i class="icon-calendar"></i><span>Event</span> </a></li>
                        
                     <!--   <li class="dropdown <?php echo ($this->uri->segment(1) == 'event' || $this->uri->segment(1) == 'event_gallery' || $this->uri->segment(1) == 'user_photo' || $this->uri->segment(1) == 'poll' || $this->uri->segment(1) == 'attendees') ? 'active' : ''; ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar"></i><span>Events</span> </a> 
                            <ul class="dropdown-menu">
                                <li class="<?php echo ($this->uri->segment(1) == 'event') ? 'active' : ''; ?>"><a href="<?php echo base_url('event'); ?>">Event</a></li> 
                                <li class="<?php echo ($this->uri->segment(1) == 'event_gallery') ? 'active' : ''; ?>"><a href="<?php echo base_url('event_gallery'); ?>">Gallery</a></li>
                                <li class="<?php echo ($this->uri->segment(1) == 'user_photo') ? 'active' : ''; ?>"><a href="<?php echo base_url('user_photo'); ?>"> Member Photos  </a> </li>
                                <!-- <li class="<?php echo ($this->uri->segment(1) == 'attendees') ? 'active' : ''; ?>"><a href="<?php echo base_url('attendees'); ?>">Attendees</a></li> 
                                <li class="<?php echo ($this->uri->segment(1) == 'comment') ? 'active' : ''; ?>"><a href="<?php echo base_url('comment'); ?>">Comments</a></li>  
                                <li class="<?php echo ($this->uri->segment(1) == 'like') ? 'active' : ''; ?>"><a href="<?php echo base_url('like'); ?>">Likes</a></li>  
                                <li class="<?php echo ($this->uri->segment(1) == 'poll') ? 'active' : ''; ?>"><a href="<?php echo base_url('poll'); ?>"> <span>Poll</span> </a></li> 
                            </ul>
                        </li> -->
                        
                        
                        <li class="dropdown <?php echo ($this->uri->segment(1) == 'payment' || $this->uri->segment(1) == 'invoice') ? 'active' : ''; ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-money"></i><span>Payment</span> </a> 
                            <ul class="dropdown-menu">
                                <li class="<?php echo ($this->uri->segment(1) == 'payment') ? 'active' : ''; ?>"><a href="<?php echo base_url('payment'); ?>">Event Registration
                                </a></li> 
                                <li class="<?php echo ($this->uri->segment(1) == 'invoice') ? 'active' : ''; ?>"><a href="<?php echo base_url('invoice'); ?>">Invoice</a></li>
                            </ul>
                        </li>
                        
                        
                        
                        <li class="<?php echo ($this->uri->segment(1) == 'guide') ? 'active' : ''; ?>"><a href="<?php echo base_url('guide'); ?>"><i class="icon-question-sign"></i><span>User Guide</span> </a></li>
                        
                        <li class="<?php echo ($this->uri->segment(1) == 'gallery') ? 'active' : ''; ?>"><a href="<?php echo base_url('gallery'); ?>"><i class="icon-film"></i><span>Gallery</span> </a></li>
                        
                        
                         

  <!--<li><a href="shortcodes.html"><i class="icon-code"></i><span>Shortcodes</span> </a> </li>
  <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="icons.html">Icons</a></li>
      <li><a href="faq.html">FAQ</a></li>
      <li><a href="pricing.html">Pricing Plans</a></li>
      <li><a href="login.html">Login</a></li>
      <li><a href="signup.html">Signup</a></li>
      <li><a href="error.html">404</a></li>
    </ul>
  </li> -->
                    </ul>
                </div>
                <!-- /container --> 
            </div>
            <!-- /subnavbar-inner --> 
        </div>