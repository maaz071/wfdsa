<!--<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>--> 
<!--<script src="<?php echo base_url('assets/js/jquery.form.js'); ?>"></script>-->
<script src="<?php echo base_url('assets/js/jquery.form.min.js'); ?>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<!--https://www.jqueryscript.net/form/jQuery-Plugin-For-Previewing-Password-Field-Values-Prevue.html  password eye-->
<script type="text/javascript">


    $(document).ready(function () { 
        
        <?php if($this->uri->segment(1) =='user' || $this->uri->segment(1) =='member') {?>
        
        window.prettyPrint && prettyPrint();

        $('.preview-password-rgb').prevue({
            color: 'rgb(33,33,33)'
        });
        
        <?php } ?>
        
        
        $('#password_enable').on('click', function() {
            $('.password').removeAttr('disabled');
            $('.password').val('');
        });
        
       
        <?php if ($this->uri->segment(1) == 'invoice') { 
            if(isset($record_list)){
            for($i=0;$i<count($record_list);$i++){
            ?>
             $('#payment_status_<?php echo $i?>').editable();
        <?php } } }?>
        
        <?php if($this->uri->segment(1) == 'announcement' || $this->uri->segment(1) == 'event') {?>
            
            
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "mm-dd-yyyy hh:ii",
                todayBtn: true,
                pickerPosition: "bottom-left",
                minuteStep: 5,
    
            });
        <?php } ?>
        //startDate: "<?php //echo date('m-d-Y H:i:s'); ?>",
         /* Table Pagnation for Non member */ 
        //   var totalRows = $('#tab_logic').find('tbody tr:has(td)').length;
        //   var recordPerPage = 5;
        //   var totalPages = Math.ceil(totalRows / recordPerPage);
        //   var $pages = $('<div id="pages"></div>');
        //   for (i = 0; i < totalPages; i++) {
        //     $('<span class="pageNumber">&nbsp;' + (i + 1) + '</span>').appendTo('.pagination');
        //   }
        //   $pages.appendTo('#tab_logic');
        
        //   $('.pageNumber').hover(
        //     function() {
        //       $(this).addClass('focus');
        //     },
        //     function() {
        //       $(this).removeClass('focus');
        //     }
        //   );
        
        //   $('table').find('tbody tr:has(td)').hide();
        //   var tr = $('table tbody tr:has(td)');
        //   for (var i = 0; i <= recordPerPage - 1; i++) {
        //     $(tr[i]).show();
        //   }
        //   $('span').click(function(event) {
        //     $('#tab_logic').find('tbody tr:has(td)').hide();
        //     var nBegin = ($(this).text() - 1) * recordPerPage;
        //     var nEnd = $(this).text() * recordPerPage - 1;
        //     for (var i = nBegin; i <= nEnd; i++) {
        //       $(tr[i]).show();
        //     }
        //   });
        
        /* Table Pagnation for Non member */
      
    
        $('.announce_hover').hover(function () {
           
              var id = $(this).attr('data-id');
            $('#popoverData_'+id).popover();
            $('#popoverOption').popover({trigger: "hover"});
            
        });
            
        $('.delete_file').click(function (e) {   
            var file_path = $(this).attr('data-id');
            
             $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . $this->uri->segment(1) . "/delete_file/"; ?>',
                dataType: 'json',
                data: {file: file_path},
            }).success(function (data) {

                if (data.success === 'yes') {
                    
                     $('.alert').show('fast');
                     $('.alert').html('<strong>' + data.msg + '</strong>');
                     $('.alert').removeClass('alert-danger');
                     $('.alert').addClass('alert-success'); 
                     setTimeout(function () {
                           $('.alert').slideUp('slow');
                      }, 3000);
                     
                } else if (data.success === 'no') {
                    alert('Somebad Had Happened , Contact Developer');
                }
            });
            
        });
        
        
        //$('.alert').hide();
        $('.submit').click(function (e) {

            e.preventDefault();
            $form = $('#<?php echo $this->uri->segment(1); ?>_form');

            $alert = $form.find('.alert');
            $action = $form.prop('action');
            $form.find('input,textarea,button,select');
            $serializeData = $form.find('input,textarea,button,select').serialize();
            //response.addHeader("Access-Control-Allow-Origin", "http://localhost/saad_shop/admin");

            $.ajax({
                type: 'POST',
                url: $action,
                data: $serializeData,
                datatype: 'json',
                target: false,
                cache: false,
                success: function (data) {

                    var data = JSON.parse(data);
                    //console.log(data);
                    if (data.success === 'yes') {
                        $('html, body').animate({scrollTop: 0}, 'fast');
                        $alert.show();
                        $alert.html('<strong>' + data.msg + '</strong>');
                        $alert.removeClass('alert-danger');
                        $alert.addClass('alert-success'); 
                           
                         <?php if ($this->uri->segment(2) == 'create') { ?>
                                setTimeout(function () {
                                    window.location = '<?php echo base_url() . $this->uri->segment(1) . '/?event_id='; ?>' + data.id;
                                }, 1000);
                         <?php } else { ?>
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                         <?php } ?>
 

                    } else if (data.success === 'no') { 
                        $alert.show('fast');
                        $alert.removeClass('alert-success');
                        $alert.addClass('alert-danger');
                        $alert.html(data.msg);
                        $alert.find("p").css('color', '#ff0500');

                        setTimeout(function () {
                            $('.alert').slideUp('slow');
                        }, 3000);

                    }

                }

            });

        });



         $('.login_submit').click(function (e) {
           
            e.preventDefault();
            $form = $('#<?php echo $this->uri->segment(1); ?>_form');

            $alert = $form.find('.alert');
            $action = $form.prop('action');
            $form.find('input,textarea,button,select');
            $serializeData = $form.find('input,textarea,button,select').serialize(); 

            $.ajax({
                type: 'POST',
                url: $action,
                data: $serializeData,
                datatype: 'json',
                target: false,
                cache: false,
                success: function (data) {

                    var data = JSON.parse(data); 
                    if (data.success === 'yes') {
                        $('html, body').animate({scrollTop: 0}, 'fast');
                        $alert.show();
                        $alert.html('<strong>' + data.msg + '</strong>');
                        $alert.removeClass('alert-danger');
                        $alert.addClass('alert-success');   
                           setTimeout(function () {
                               window.location.replace('<?php echo base_url();?>'+data.redirect);
                           }, 1000);  
        
                    } else if (data.success === 'no') {
        
                        $alert.show('fast');
                        $alert.removeClass('alert-success');
                        $alert.addClass('alert-danger');
                        $alert.html(data.msg);
                        $alert.find("p").css('color', '#ff0500');
                
                        setTimeout(function () { 
                            $('.alert').slideUp('slow');
                        }, 3000);
                        
                         
        
                 }
        
                }
    
            });
        
        });

 
         

        


        $('.submit_product_form').click(function (e) {

            e.preventDefault();
            e.returnValue = false;
            
            $form = $('#<?php echo $this->uri->segment(1); ?>_form');
            $alert = $form.find('.alert');
            $action = $form.prop('action');
            $form.find('input,textarea,select');
            
            $form.ajaxForm({
                type: 'post',
                url: $action,
                dataype: 'json', 
                success: function (data) {

                    var data = JSON.parse(data);
                  //var data = JSON.stringify(JSON.parse(data));
                  //var dat = JSON.parse(JSON.stringify(data));   
                   //console.log(dat);
                    //console.log("loo");

                    // alert(data.m);
                    if (data.success === 'yes') {
                         $('.alert').show('fast');
                        $alert.html('<strong>' + data.msg + '</strong>');
                        $alert.removeClass('alert-danger');
                        $alert.addClass('alert-success');

                        <?php if ($this->uri->segment(2) == 'create') { ?>
                                setTimeout(function () {
                                    window.location = '<?php echo base_url() . $this->uri->segment(1) . '/edit/'; ?>' + data.id;
                                }, 1000);
                         <?php } else { ?>
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                         <?php } ?>

                    } else if (data.success === 'no') {

                        $('.alert').show('fast');
                        $alert.removeClass('alert-success');
                        $alert.addClass('alert-danger');
                        $alert.html(data.msg);
                        $alert.find("p").css('color', '#ff0500');

                        setTimeout(function () {
                            $('.alert').slideUp('slow');
                        }, 3000);

                    }

                }

            }).submit();

        });



        $('#delete_selected').click(function (e) {
            e.preventDefault();
            var ids = $('.checkbox').map(function () {
                if (this.checked) {
                    return  $(this).val();
                }
            }).get();
            
            if(ids.length == 0) {
                 bootbox.alert("Record Must be Checked!");
                return false;
            } 
            
            var table_name = '<?php echo $this->uri->segment(1);?>';
            
            
            bootbox.confirm({
                message: "Are You Sure ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url() . "User_admin/delete_selected/"; ?>',
                            data: {ids: ids,table:table_name},
                            datatype: 'json',
                            success: function (data) {
                                var data = JSON.parse(data);
            
                                if (data.success === 'yes') {
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                    $('.alert').show();
                                    $('.alert').html('<strong>' + data.msg + '</strong>');
                                    $('.alert').removeClass('alert-danger');
                                    $('.alert').addClass('alert-success');
            
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 2000);
            
                                } else if (data.success === 'no') {
            
                                    $('.alert').show('fast');
                                    $('.alert').removeClass('alert-success');
                                    $('.alert').addClass('alert-danger');
                                    $('.alert').html(data.msg);
                                    $('.alert').find("p").css('color', '#ff0500');
            
                                    setTimeout(function () {
                                        $('.alert').slideUp('slow');
                                    }, 3000);
                                }
                            }
            
                        });
                      
                    }

                }
            }); 

           

        });

        $('.delete_individual').click(function () {
            
            var id =$(this).attr('id');
             var table_name = $(this).attr('data-id');
           
            bootbox.confirm({
                message: "Are You Sure ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo base_url() . "User_admin/delete_individual/"; ?>',
                            dataType: 'json',
                            data: {id: id,table:table_name},
                        }).success(function (data) {
            
                            if (data.success === 'yes') {
                                $('html, body').animate({scrollTop: 0}, 'fast');
                                $('.alert').show();
                                $('.alert').html('<strong>' + data.msg + '</strong>');
                                $('.alert').removeClass('alert-danger');
                                $('.alert').addClass('alert-success');
            
                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
            
                            } else if (data.success === 'no') {
            
                                $('.alert').show();
                                $('.alert').removeClass('alert-success');
                                $('.alert').addClass('alert-danger');
                                $('.alert').html(data.msg);
                                $('.alert').find("p").css('color', '#ff0500');
            
                                setTimeout(function () {
                                    $('.alert').slideUp('slow');
                                }, 3000);
                            }
                        });
                    }

                }
            }); 
           
        }); 
        
        
         $('.status_change').click(function () {
            
            var id = $(this).attr('id'); 
            var table_name = $(this).attr('data-id');
            var current_status = $(this).attr('data-val');
             
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . "User_admin/change_status/"; ?>',
                dataType: 'json',
                data: {id: id,table:table_name,current:current_status},
                
            }).success(function (data) {

                if (data.success === 'yes') {
                    $('html, body').animate({scrollTop: 0}, 'fast');
                    $('.alert').show();
                    $('.alert').html('<strong>' + data.msg + '</strong>');
                    $('.alert').removeClass('alert-danger');
                    $('.alert').addClass('alert-success');

                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);

                } else if (data.success === 'no') {

                    $('.alert').show();
                    $('.alert').removeClass('alert-success');
                    $('.alert').addClass('alert-danger');
                    $('.alert').html(data.msg);
                    $('.alert').find("p").css('color', '#ff0500');

                    setTimeout(function () {
                        $('.alert').slideUp('slow');
                    }, 3000);
                }
            });
        });
        
        
        
        $('.add_btn').click(function () {
            var count = parseInt($(this).attr('id'));
            count = count + 1;
            $(this).attr('id', count);

            $("#answer_div").append('<div class="control-group" id="answer_row_' + count + '">\n\
                                            <label class="control-label"></label>\n\
                                            <input type="text" name="answer[]"  style="width:312px;">&nbsp;&nbsp;\n\
                                             <button type="button" class="btn btn-danger remove" id="' + count + '"><i class="icon icon-remove"></i></button>\n\
                                        </div>');
        });

        $("#answer_div").on("click", ".remove", function () {
            var id = $(this).attr('id');
            var table_id = $(this).attr('data-id');

            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . $this->uri->segment(1) . "/delete_relation_record/"; ?>',
                dataType: 'json',
                data: {id: table_id},
            }).success(function (data) {

                if (data.success === 'yes') {
                    $('#answer_row_' + id).fadeOut(1000, function () {
                        $('#answer_row_' + id).remove();
                    });
                } else if (data.success === 'no') {
                    alert('Somebad Had Happened , Contact Developer');
                }
            });

        });



        $('#check_all').click(function () {
            // $('.checkbox').prop(true);
            $(this).prop('checked', this.checked);
            $('input:checkbox').prop('checked', this.checked);

        });
        
        
        
         $('.toggle_create').click(function () {
            // $('.checkbox').prop(true);
            $('.toggle_div').toggle("fast");

        });
        
         
        approve_delete_checkbox('.delete_photo', '.chk_img');

        $('.delete_img').click(function (e) {

            e.preventDefault();
            var action = $(this).attr('data-id');
            var ids = $('.delete_chk_img').map(function () {
                if (this.checked) {
                    return  $(this).val();
                }
            }).get();

            if (ids.length == 0) {
                bootbox.alert("Image must be Checked !");
                return false;
            }

            bootbox.confirm({
                message: "Are You Sure ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: action,
                            data: {ids: ids},
                            datatype: 'json',
                            success: function (data) {
                                var data = JSON.parse(data);
                                if (data.success === 'yes') {

                                    $('.alert').show("fast");
                                    $('.alert').html('<strong>' + data.msg + '</strong>');
                                    $('.alert').removeClass('alert-danger');
                                    $('.alert').addClass('alert-success');
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 1000);
                                } else if (data.success === 'no') {

                                    $('.alert').show();
                                    $('.alert').removeClass('alert-success');
                                    $('.alert').addClass('alert-danger');
                                    $('.alert').html(data.msg);
                                    $('.alert').find("p").css('color', '#ff0500');
                                    setTimeout(function () {
                                        $('.alert').slideUp('slow');
                                    }, 3000);
                                }
                            }

                        });
                    }

                }
            });

        });
        
         approve_delete_checkbox('.approve_photo', '.chk_img');
        approve_delete_checkbox('.delete_photo', '.chk_img');

        $('.update_image').click(function (e) {

            e.preventDefault();
            var action = $(this).attr('data-id');
            var ids = $('.delete_chk_img').map(function () {
                if (this.checked) {
                    return  $(this).val();
                }
            }).get();

            if (ids.length == 0) {
                bootbox.alert("Image must be Checked !");
                return false;
            }

            bootbox.confirm({
                message: "Are You Sure ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo base_url(); ?>event_gallery/update_permission",
                            data: {ids: ids},
                            datatype: 'json',
                            success: function (data) {
                                console.log(ids);
                                var data = JSON.parse(data);
                                if (data.success === 'yes') {

                                    $('.alert').show("fast");
                                    $('.alert').html('<strong>' + data.msg + '</strong>');
                                    $('.alert').removeClass('alert-danger');
                                    $('.alert').addClass('alert-success');
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 1000);
                                } else if (data.success === 'no') {

                                    $('.alert').show();
                                    $('.alert').removeClass('alert-success');
                                    $('.alert').addClass('alert-danger');
                                    $('.alert').html(data.msg);
                                    $('.alert').find("p").css('color', '#ff0500');
                                    setTimeout(function () {
                                        $('.alert').slideUp('slow');
                                    }, 3000);
                                }
                            }

                        });
                    }

                }
            });

        });
        $('.update_image_checkin').click(function (e) {

            e.preventDefault();
            var action = $(this).attr('data-id');
            var ids = $('.delete_chk_img').map(function () {
                if (this.checked) {
                    return  $(this).val();
                }
            }).get();

            if (ids.length == 0) {
                bootbox.alert("Image must be Checked !");
                return false;
            }

            bootbox.confirm({
                message: "Are You Sure ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo base_url(); ?>event_gallery/update_permission_checkin",
                            data: {ids: ids},
                            datatype: 'json',
                            success: function (data) {
                                console.log(ids);
                                var data = JSON.parse(data);
                                if (data.success === 'yes') {

                                    $('.alert').show("fast");
                                    $('.alert').html('<strong>' + data.msg + '</strong>');
                                    $('.alert').removeClass('alert-danger');
                                    $('.alert').addClass('alert-success');
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 1000);
                                } else if (data.success === 'no') {

                                    $('.alert').show();
                                    $('.alert').removeClass('alert-success');
                                    $('.alert').addClass('alert-danger');
                                    $('.alert').html(data.msg);
                                    $('.alert').find("p").css('color', '#ff0500');
                                    setTimeout(function () {
                                        $('.alert').slideUp('slow');
                                    }, 3000);
                                }
                            }

                        });
                    }

                }
            });

        });
        $('.delete_comment').click(function (e) {
            var id = $(this).attr('id');
            var url = $(this).attr('data-val'); 
            var $li = $(this).closest('li');
             
            bootbox.confirm({
                message: "Are You Really want to Delete ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        
                        $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            data: {id: id},
                        }).success(function (data) {
            
                            if (data.success === 'yes') { 
                                
                                 
                                
                                  $('.alert').show("fast");
                                  $('.alert').html('<strong>' + data.msg + '</strong>');
                                  $('.alert').removeClass('alert-danger');
                                  $('.alert').addClass('alert-success');    
                                
                                  setTimeout(function () {
                                            window.location.reload();
                                    }, 1000);
                                    
                               
                                
                                
                                
                                
                            } else if (data.success === 'no') {
                                
                                 $('.alert').show();
                                 $('.alert').removeClass('alert-success');
                                 $('.alert').addClass('alert-danger');
                                 $('.alert').html(data.msg);
                                 $('.alert').find("p").css('color', '#ff0500');
                                 setTimeout(function () {
                                     $('.alert').slideUp('slow');
                                 }, 3000); 
                            }
                        });
                    }
                }
            });
        });
        
        
         $('.user_upload_event_id').change(function (e) {
            e.preventDefault();

            var event_id = $(".user_upload_event_id option:selected").val();

            $('.alert').show("fast");
            $('.alert').html('<strong>Loading. Kindly Wait......</strong>');
            $('.alert').removeClass('alert-danger');
            $('.alert').addClass('alert-success');

            $('.upload_img_div').empty();
            
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() . $this->uri->segment(1); ?>/get_user_photos',
                data: {id: event_id},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);
                    
                    
                    if (data.success === 'yes') {
                        $('.alert').slideUp('slow');
                        for (var x = 0; x < data.records.length; x++) {

                            var span = (data.records[x]['approved'] == 1) ? '<span class="label label-success">Approved</span>' : '<span class="label label-danger">Not Approved</span>';
                            var checkbox = (data.records[x]['approved'] == 0) ? '<input type="checkbox" class="chk_img" value="' + data.records[x]['user_photo_id'] + '" style="float:left;">' : '';
                            var user_detail = '<b>Name :</b> ' + data.records[x]['name'] + '<br>';
                            var user_detail = user_detail + '<b>Date :</b> ' + data.records[x]['created_on'] + '<br>';
                            var user_detail = '<ul class="messages_layout">\n\
                                                    <li class="from_user left"> \n\
                                                        <a href="#" class="avatar"><img src="<?php echo base_url();?>'+	data.records[x]['uploaded_image']+'" style="width:50px;height:50px"/></a>\n\
                                                        <div class="message_wrap span2" style="margin-left:0px">\n\
                                                            <div class="info"> \n\
                                                                <a class="name">'+data.records[x]['name']+'</a> <span class="time">'+data.records[x]['created_on']+'</span>\n\
                                                            </div>\n\
                                                        </div>\n\
                                                    </li>\n\
                                                </ul>';
                            $('.upload_img_div').append('<div class="span3"> \n\
                                                        <center>' + span + '</center>\n\
                                                        <div class="thumbnail"> \n\
                                                            <center> \n\
                                                                <a href="">' + checkbox + '\n\
                                                                 <img src="' + data.records[x]['uploaded_image'] + '"> \n\
                                                                </a> \n\
                                                            </center> \n\
                                                        </div> \n\
                                                         \n' + user_detail + '\n\
                                                       </div>');
                        }

                    } else if (data.success === 'no') {

                        $('.alert').show();
                        $('.alert').removeClass('alert-success');
                        $('.alert').addClass('alert-danger');
                        $('.alert').html(data.msg);
                        $('.alert').find("p").css('color', '#ff0500');
                        setTimeout(function () {
                            $('.alert').slideUp('slow');
                        }, 3000);
                    }
                }

            });

        });
        
          $(".search_btn").click(function () {
            $('.search_div').toggle('fast');
         });
        
        
        $(".search").keyup(function () {
        
            var searchTerm = $(".search").val();
            var listItem = $('#tab_logic tbody').children('tr');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

                $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
                      return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
                  }
                });

                $("#tab_logic tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
                  $(this).attr('visible','false');
                });

                $("#tab_logic tbody tr:containsi('" + searchSplit + "')").each(function(e){
                  $(this).attr('visible','true');
                });

                var jobCount = $('#tab_logic tbody tr[visible="true"]').length;
                  $('.counter').text(jobCount + ' item');

                if(jobCount == '0') {$('.no-result').show();}
                  else {$('.no-result').hide();}
            });
            
            
            
        /* Sort Funtion Generalize*/
        
        var table = $('.table');
    
        $('.sortable th').wrapInner('<span title="sort this column"/>').each(function(){
    
            var th = $(this),
                thIndex = th.index(),
                inverse = false;
    
            th.click(function(){
    
                table.find('td').filter(function(){
    
                    return $(this).index() === thIndex;
    
                }).sortElements(function(a, b){
    
                    if( $.text([a]) == $.text([b]) )
                        return 0;
    
                    return $.text([a]) > $.text([b]) ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;
    
                }, function(){
    
                    // parentNode is the element we want to move
                    return this.parentNode; 
    
                });
    
                inverse = !inverse;
    
            });
    
        });
      
        /* Sort Funtion Generalize*/   
        
      <?php if($this->uri->segment(1) == 'roster') { ?>  
     
      $('#roster_table tbody').sortable({   
            axis: 'y',
            opacity: 0.7,
            handle: 'span',
            update: function(event, ui) {
                var role_id = $("#roster_select option:selected").val();
                var sort_arr = [];
                 console.log(role_id);
                $(".tr").each(function() { 
                   sort_arr.push($(this).attr('data-id'));
                }); 
                console.log(sort_arr);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url($this->uri->segment(1).'/update_row');?>',
                    data: {sort: sort_arr,role_id:role_id},
                    datatype: 'json',
                    success: function (data) {
                        // var data = JSON.parse(data);
                        
                        // if (data.success === 'yes') {  
                            toastr.success('position updated successfully')
                        // }
                         // else if (data.success === 'no') { 
                            
                        // }
                    },
        
                });
            }
      });
      
     
      
      $('.list-group-sortable').sortable({
          
             
      });
      
       <?php } ?>
            
            
       $('.add_value_select').click(function (e) {
            e.preventDefault();
            var role_name = $('.role_name').val();
            var role_select = $('.role_select').val(); 
            var action = $(this).attr('data-val');
            
            $.ajax({
                type: 'POST',
                url: action,
                data: {name: role_name,parent_id:role_select},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.success === 'yes') { 
                        $('.refreshcategory').empty();
                        url = '<?php echo base_url() . $this->uri->segment(1) . '/getcategories'; ?>'
                        $.get(url,function(data){
                            console.log(data);
                            $('.refreshcategory').html(data);
                        });

                        // delete category
                        $('.deletebody').empty();
                        url2 = '<?php echo base_url() . $this->uri->segment(1) . '/getcategories2'; ?>'
                        $.get(url2,function(data){
                            console.log(data);
                            $('.deletebody').html(data);
                        });

                        // new sub category
                        $('.role_select').empty();
                        url2 = '<?php echo base_url() . $this->uri->segment(1) . '/getcategories3'; ?>'
                        $.get(url2,function(data){
                            console.log(data);
                            $('.role_select').html(data);
                        });

                        $('#Add_Modal').modal('hide');
                        // window.location.reload();
                    } else if (data.success === 'no') { 
                        
                    }
                }
    
            });
        });
        
         $('body').on('click','.delete_role',function (e) {
            e.preventDefault();
            var action = $(this).attr('id');
            var $tr = $(this).closest('tr');
            var id = $(this).attr('data-id');
            
            $.ajax({
                type: 'POST',
                url: action,
                data: {},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.success === 'yes') {
                        
                        $tr.fadeOut(1000,function(){
                            $tr.remove();  
                        });
                        
                        $('.alert_modal').show();
                        $('.alert_modal').removeClass('alert-danger');
                        $('.alert_modal').addClass('alert-success');
                        $('.alert_modal').html(data.msg);
                        $('.alert_modal').find("p").css('color', '#ff0500'); 
                        $('#role_option_'+id).remove();
                        
                        setTimeout(function () {
                            $('.alert_modal').slideUp('slow');
                        }, 3000);
                    } else if (data.success === 'no') { 
                        
                    }
                }
    
            });
        }); 
        
        $('#autogen_pass').click(function (e) {
            var random = randomPassword(10);
           $('#re_password').val(random);
           $('#password').val(random);
        }); 
        
        
         $('#roster_select').change(function (e) {
            var id= $("#roster_select option:selected").val();
            
            $('.alert').show("fast");
            $('.alert').html('<strong>Member List is Generating test.</strong>');
            $('.alert').removeClass('alert-danger');
            $('.alert').addClass('alert-success');
                        
           
            
             $.ajax({
                type: 'POST',
                url: '<?php echo base_url('roster/get_role_member');?>',
                data: {role_id: id},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.success === 'yes') {
                        console.log(data);
                        $('.alert').slideUp();   
                        $('#sort_body').empty();
                        // $i = 0;
                        // $j = 1;
                        // do{
                        //         if(data.record[$i]['sort_order'] == $j)
                        //         {
                                    
                        //         }
                        //         $i++;
                        // }while(data.record.length != $j);
                        
                        for(var i=0; i < data.record.length; i++) {
                            
                            $('#sort_body').append('<tr class="tr" data-id="'+data.record[i]['member_select_role_id']+'"> <td><center><span class="icon-move move_custom"></span></center></td> <td>'+data.record[i]['member_name']+'</td><td>'+data.record[i]['company']+'</td><td>'+data.record[i]['designation']+'</td><td>'+data.record[i]['cell']+'</td><td>'+data.record[i]['email']+'</td> </tr>');     
                          
                        }
                         
                         
                    } else if (data.success === 'no') { 
                        
                        $('#sort_body').empty();
                        
                        $('.alert').show();
                        $('.alert').removeClass('alert-success');
                        $('.alert').addClass('alert-danger');
                        $('.alert').html(data.msg);
                        $('.alert').find("p").css('color', '#ff0500');

                        setTimeout(function () {
                            $('.alert').slideUp('slow');
                        }, 3000);
                        
                        $('#sort_body').append('<tr><td colspan="3">No Records Found</td></tr>');
                        
                    }
                }
    
            });
            
             $('#roster_submit').click(function (e) {  
                $('html, body').animate({scrollTop: 0}, 'fast');
                                    $('.alert').show();
                                    $('.alert').html('<strong> positions updated successfully</strong>');
                                    $('.alert').removeClass('alert-danger');
                                    $('.alert').addClass('alert-success');
            
                                    setTimeout(function () {
                                        $('.alert').hide();
                                    }, 3000);
             }); 
            
            
        }); 
        
        
         $('.checked_in').click(function (e) {
            e.preventDefault();
            var attendees_id = $(this).attr('data-val');
            
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('attendees/checked_in');?>',
                data: {attendees_id: attendees_id},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data.success === 'yes') { 
                        
                        $('.alert').show("fast");
                        $('.alert').html('<strong>' + data.msg + '</strong>');
                        $('.alert').removeClass('alert-danger');
                        $('.alert').addClass('alert-success');
                        
                         setTimeout(function () {
                            window.location.reload();
                         }, 1000);
                         
                    } else if (data.success === 'no') { 
                        
                    }
                }
    
            });
        });
        
         
         $('.select_go_url').change(function (e) {
             var url = $(this).attr('data-id');
             var id= $(".select_go_url option:selected").val();
             
             window.location = url+id;
         });    
         
         $('.add_invoice_row').click(function () {            
            var count = parseInt($(this).attr('id'));
            count = count + 1;
            $(this).attr('id', count);

            $("#invoice_body").append('<tr id="invoice_row_'+count+'">\n\
                                         <td><center><input type="text" class="span2" name="name[]"></center></td>\n\
                                         <td><center><input type="number" class="span2 change_value" id="qty" name="qty[]"></center></td>\n\
                                         <td><center><input type="number" class="span2 change_value" id="price" name="price[]"></center></td> \n\
                                         <td class="total_row text_right">0</td><input type="hidden" class="total_row_value" name="total_row[]" value="0">\n\
                                         <td><center><a class="btn btn-danger remove_invoice_row" style="float:right;"><i class="icon-remove"></i> </a></center></td>\n\
                                      </tr>');
        }); 
         
        
         $("#invoice_body").on("click", ".remove_invoice_row", function () { 
             $tr = $(this).closest('tr');
             $tr.fadeOut(500,function(){
                $tr.remove();  
             });
        });        
         
         
       $("#invoice_body").on("blur", ".change_value", function () { 
            var price = ($(this).closest('tr').find('#price').val()) ? $(this).closest('tr').find('#price').val() : null;
            var tot = parseInt($(this).closest('tr').find('#qty').val()) * parseFloat(price);
            console.log(tot);
            if(tot > 0)
            {
                $(this).closest('tr').find('.total_row').html(tot);
                $(this).closest('tr').find('.total_row_value').val(tot);
            }
            else
            {
                console.log('in nan');
                $(this).closest('tr').find('.total_row').html(0);
                $(this).closest('tr').find('.total_row_value').val(0);    
            }
            
            
            var sum = 0;
            $('.total_row').each(function(){
                   sum += parseFloat($(this).text());
            });
            
            $("#total_main").text(sum);
            $(".grand_total").val(sum);
             
        }); 
        
        
        $('.add_resource_file').click(function () {
            var count = parseInt($(this).attr('id'));
            count = count + 1;
            $(this).attr('id', count);
            $('#empty_row').remove();
            $("#resource_body").append('<tr id="invoice_row_"><td><input type="file" name="upload_file[]"></td><td> <center><a class="btn btn-danger remove_file" style="float:right;"><i class="icon-remove"></i></a></center></td></tr>');
             
             
        });
        $('#country_id').change(function () {
             
            var country_id = $("#country_id option:selected").val();
            
            $.ajax({
                   type: 'POST',
                   url: '<?php echo base_url('event/get_cities');?>',
                   dataType: 'json',
                   data: {country_id:country_id},
            }).success(function (data) {
                if(data.success == yes) {
                      alert(data.cities.length);
                    $('#city_id').append('');
                  
                    for(var x=0 ; x <data.cities.length; x++) {
                        $('#city_id').append('<option value="'+data[x]['id']+'">'+data[x]['name']+'</option>');    
                    }
                }
               
            });
             
        });
        
         $("#resource_body").on("click", ".remove_file", function () { 
             $tr = $(this).closest('tr');
             var id = $(this).attr('id');
             var file = $(this).attr('data-id');
             
             var uri = '<?php echo $this->uri->segment(2);?>';
             
             if(uri == 'edit' && file != "") {
              
                 $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('resources/delete_file');?>',
                    dataType: 'json',
                    data: {id: id , file:file},
                }).success(function (data) {
                
                    if (data.success === 'yes') { 
                     $tr.fadeOut(500,function(){
                         $tr.remove();  
                        });
                    }  
                });
                 
             }
             
              $tr.fadeOut(500,function(){
                 $tr.remove();  
              });
        });      
        
         

    });   /*Document ready */  
     
       
       
         
    
     function approve_delete_checkbox(id, check_ids) {
        $(id).click(function (e) {
            e.preventDefault();
            var ids = $(check_ids).map(function () {
                if (this.checked) {
                    return  $(this).val();
                }
            }).get();

            if (ids.length == 0) {
                $('.alert').show();
                $('.alert').removeClass('alert-success');
                $('.alert').addClass('alert-danger');
                $('.alert').html('Images Must be Checked');
                $('.alert').find("p").css('color', '#ff0500');

                setTimeout(function () {
                    $('.alert').slideUp('slow');
                }, 3000);
                return false;
            }

            var action = $(this).attr('data-id');
            var event_id = '<?php echo (!empty($event_id)) ? $event_id: ''; ?>';
            
            
            $.ajax({
                type: 'POST',
                url: action,
                data: {ids: ids,event_id:event_id},
                datatype: 'json',
                success: function (data) {
                    var data = JSON.parse(data);

                    if (data.success === 'yes') {
                        $('html, body').animate({scrollTop: 0}, 'fast');
                        $('.alert').show();
                        $('.alert').html('<strong>' + data.msg + '</strong>');
                        $('.alert').removeClass('alert-danger');
                        $('.alert').addClass('alert-success');

                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);

                    } else if (data.success === 'no') {

                        $('.alert').show();
                        $('.alert').removeClass('alert-success');
                        $('.alert').addClass('alert-danger');
                        $('.alert').html(data.msg);
                        $('.alert').find("p").css('color', '#ff0500');

                        setTimeout(function () {
                            $('.alert').slideUp('slow');
                        }, 3000);
                    }
                }

            });

        });

    }
    
    function randomPassword(length) {
        var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
        var pass = "";
        for (var x = 0; x < length; x++) {
            var i = Math.floor(Math.random() * chars.length);
            pass += chars.charAt(i);
        }
        return pass;
    }
    
    
   
 
 
 </script>
 <!--http://www.bestjquery.com/?5O0Kd0oX-->
 <!--http://www.bestjquery.com/example/jquery-chart-graph/page/3/-->
 <script type="text/javascript">
        jQuery('document').ready(function () {
             <?php if($this->uri->segment(2) == 'poll_result'){ ?>
            $("#chart-container").insertFusionCharts({
                type: "column2d",
                width: "1100",
                height: "500",
                dataFormat: "json",
                dataSource: {
                    "chart": {
                        "caption": "",
                        "xAxisName": "Poll Answer",
                        "yAxisName": "No. of Comments",
                        // "numberPrefix": "$",
                        "theme": "fint"
                    },
                    
                    
                        "data": [
                        <?php if(!empty($poll_answers)) {?>    
                            <?php foreach($poll_answers as $value) {?>    
                                {
                                    "label": "<?php echo $value['poll_answer'];?>",
                                    "value": "<?php echo $value['y_value'];?>"
                                },  
        					<?php } ?>
    					<?php } ?>
    					]
					
                }
            });
            <?php } ?>
        });
        
    </script>
 
 
 

