<script>
$(document).ready(function(){
	/* When click on change profile pic */	
	$('#change-profile-pic').on('click', function(e){
        $('#profile_pic_modal').modal({show:true});        
    });	
	$('#profile-pic').on('change', function()	{ 
		$("#preview-profile-pic").html('');
		$("#preview-profile-pic").html('Uploading....');
		$action = $("#cropimage").attr('action'); 
		$("#cropimage").ajaxForm({
		xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.onprogress = function(e) {
                 var complete = Math.floor(e.loaded / e.total *100) + '%';
                 $('#upload_progress').show('fast');
                 $('.bar').css('width',complete);
            }; 
           return xhr;
        },    
		url: $action,    
		type: 'post',
		target: '#preview-profile-pic',
		dataype: 'json', 
		success:  function(data) { 
		        var obj = JSON.parse;
		        $('#upload_progress').slideUp('slow'); 
		        var $image = $('img#photo');

				$image.cropper({
				  aspectRatio: 12 / 14,
				  autoCropArea: 0.85,
				  strict: false,
				  guides: false,
				  highlight: false,
				  dragCrop: false,
				  movable: true,
				  resizable: true,
				  mouseWheelZoom: true
				});
		        // old
				// $('img#photo').imgAreaSelect({
				// aspectRatio: '1:1',
				// onSelectEnd: getSizes,
			// });
			$('#image_name').val($('#photo').attr('file-name'));
			}
		}).submit();

	});
	/* handle functionality when click crop button  */
	$('#save_crop').on('click', function(e){
    e.preventDefault();
    var $image = $('img#photo');
    $size = $image.cropper('getData');

    console.log($size.x);
    console.log($size.y);
    console.log($size.width);
    console.log($size.height);
    $('#hdn-thumb-width').val($size.width);
    $('#hdn-thumb-height').val($size.height);
    $('#hdn-x1-axis').val($size.x);
    $('#hdn-y1-axis').val($size.y);
    $('#hdn-y2-axis').val($('#photo').attr('src'));

     	// $('#profile_pic_modal').modal('hide');
		$(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');
		var params = {
			targetUrl: ""
		};
        // saveCropImage(params);
        $('#profile_pic_modal').modal('hide');
    });	


    function getSizes(img, obj){
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = obj.width;
        var thumb_height = obj.height;
        if(thumb_width > 0) {
			$('#hdn-x1-axis').val(x_axis);
			$('#hdn-y1-axis').val(y_axis);
			$('#hdn-x2-axis').val(x2_axis);
			$('#hdn-y2-axis').val(y2_axis);
			$('#hdn-thumb-width').val(thumb_width);
			$('#hdn-thumb-height').val(thumb_height);
        } else {
            alert("Please select portion..!");
		}
    }
    /* Function to save crop images */
    function saveCropImage(params) {
		$.ajax({
			url: params['targetUrl'],
			cache: false,
			dataType: "html",
			data: {
				action: params['action'],
				id: $('#hdn-profile-id').val(),
				 t: 'ajax',
									w1:params['thumb_width'],
									x1:params['x_axis'],
									h1:params['thumb_height'],
									y1:params['y_axis'],
									x2:params['x2_axis'],
									y2:params['y2_axis'],
									image_name :$('#image_name').val()
			},
			type: 'Post',
		   	success: function (response) {
					$('#profile_pic_modal').modal('hide');
					$(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');
					
					$("#profile_picture").attr('src', response);
					$("#preview-profile-pic").html('');
					$("#profile-pic").val();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert('status Code:' + xhr.status + 'Error Message :' + thrownError);
			}
		});
    }
});
</script>