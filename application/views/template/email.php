<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Anil Labs - Codeigniter mail templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>

<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic&subset=latin,cyrillic);

@media only screen and (min-width: 0) {
    .wrapper {
        text-rendering: optimizeLegibility;
    }
}
@media only screen and (max-width: 620px) {
    [class=wrapper] {
        min-width: 302px !important;
        width: 100% !important;
    }
    [class=wrapper] .block {
        display: block !important;
    }
    [class=wrapper] .hide {
        display: none !important;
    }

    [class=wrapper] .top-panel,
    [class=wrapper] .header,
    [class=wrapper] .main,
    [class=wrapper] .footer {
        width: 302px !important;
    }

    [class=wrapper] .title,
    [class=wrapper] .subject,
    [class=wrapper] .signature,
    [class=wrapper] .subscription {
        display: block;
        float: left;
        width: 300px !important;
        text-align: center !important;
    }
    [class=wrapper] .signature {
        padding-bottom: 0 !important;
    }
    [class=wrapper] .subscription {
        padding-top: 0 !important;
    }
}
body {
    margin: 0;
    padding: 0;
    mso-line-height-rule: exactly;
    min-width: 100%;
}

.wrapper {
    display: table;
    table-layout: fixed;
    width: 100%;
    min-width: 620px;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
}

body, .wrapper {
    background-color: #ffffff;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
table.center {
    margin: 0 auto;
    width: 602px;
}
td {
    padding: 0;
    vertical-align: top;
}

.spacer,
.border {
    
}
.spacer {
    width: 100%;
    line-height: 16px
}
.border {
    background-color: #e0e0e0;
    width: 1px;
}

img {
    border: 0;
    -ms-interpolation-mode: bicubic;
}
.image {
    font-size: 12px;
}
.image img {
    display: block;
}
strong, .strong {
    font-weight: 700;
}
h1,
h2,
h3,
p,
ol,
ul,
li {
    margin-top: 0;
}
ol,
ul,
li {
    padding-left: 0;
}

a {
    text-decoration: none;
    color: #5d556d;
}
.btn {
    background-color:#2196F3;
    border:1px solid #2196F3;
    border-radius:2px;
    color:#ffffff;
    display:inline-block;
    font-family:Roboto, Helvetica, sans-serif;
    font-size:14px;
    font-weight:400;
    line-height:36px;
    text-align:center;
    text-decoration:none;
    text-transform:uppercase;
    width:200px;
    height: 36px;
    padding: 0 8px;
    margin: 0;
    outline: 0;
    outline-offset: 0;
    -webkit-text-size-adjust:none;
    mso-hide:all;
}

.title {
    text-align: left;
}

.subject {
    text-align: right;
}

.title, .subject {
    width: 300px;
    padding: 8px 0;
    color: #616161;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 14px;
}

.logo {
    padding: 16px 0;
}

.logo-image {

}

.main {
    -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
}

.columns {
    margin: 0 auto;
    width: 600px;
    background-color: #ffffff;
    font-size: 14px;
}

.column {
    
}

.column-top {
    font-size: 24px;
    line-height: 24px;
}

.content {
    width: 100%;
}

.column-bottom {
    font-size: 8px;
    line-height: 8px;
}

.content h1 {
    margin-top: 0;
    margin-bottom: 16px;
    color: #212121;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 20px;
    line-height: 28px;
    background: #eaeaea;
    text-align: center;
    padding: 2%;
}

.content p {
    margin-top: 0;
    margin-bottom: 16px;
    color: #212121;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
}
.content .caption {
    color: #616161;
    font-size: 12px;
    line-height: 20px;
}

.signature, .subscription {
    vertical-align: bottom;
    width: 300px;
    padding-top: 8px;
    margin-bottom: 16px;
}

.signature {
    text-align: left;
}
.subscription {
    text-align: right;
}

.signature p, .subscription p {
    margin-top: 0;
    margin-bottom: 8px;
    color: #616161;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
}
</style>

<center class="wrapper">
    <table class="top-panel center" width="602" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td>
                <img style="margin: 1%; display:block" src="<?php echo base_url().'uploads/logowfdsa.png'?>"  alt="logo-alt" >
            </td>
            <td>
                <a href="https://wfdsa.org/" style="float: left; "></a>
            </td>
        </tr>
        <tr>
            <td class="border" style="font-size: 1px;line-height: 1px;" colspan="2">&nbsp;</td>
        </tr>
        </tbody>
    </table>

    <div class="spacer">&nbsp;</div>

    <table class="main center" style="-webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);" width="602" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="column" style="text-align: left;
    background-color: #ffffff;
    font-size: 14px;">
                <div class="column-top">&nbsp;</div>
                <table class="content" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td style="padding: 0 24px;">
                          <h1><?php echo $email[0]['heading'];?></h1>
                          <p>Dear <strong><?php echo $email[0]['firstname'];?> <?php echo $email[0]['lastname'];?>,</strong></p>
                          <p><?php echo $email[0]['text']?></p>
                          <p style="text-align:center;">
                            <?php if(!empty($email[0]['link'])){?>
                            <a style="background-color:#2196F3;
    border:1px solid #2196F3;
    border-radius:2px;
    color:#ffffff;
    display:inline-block;
    font-family:Roboto, Helvetica, sans-serif;
    font-size:14px;
    font-weight:400;
    line-height:36px;
    text-align:center;
    text-decoration:none;
    text-transform:uppercase;
    width:200px;
    height: 36px;
    padding: 0 8px;
    margin: 0;
    outline: 0;
    outline-offset: 0;
    -webkit-text-size-adjust:none;
    mso-hide:all;" href="<?php echo $email[0]['link'];?>" class="btn">Verify Email</a>
<?php } ?>
</p>

    <!-- <p class="caption">

    </p> -->

                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0 24px;">
                            <a href="https://play.google.com/store/apps/details?id=com.simplicore.app.wfdsa">
                                <img width="130px" src="<?php echo base_url().'assets_admin/androidplayicon.jpg'?>"/></a>
                            <a href="https://itunes.apple.com/us/app/wfdsa/id1422309391?mt=8">
                                <img width="130px" src="<?php echo base_url().'assets_admin/iosappicon.jpg'?>"/></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="column-bottom">&nbsp;</div>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="spacer">&nbsp;</div>

    <table class="footer center" width="602" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="border" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="signature" style="vertical-align: bottom;
    width: 300px;
    padding-top: 8px;
    margin-bottom: 16px;
    text-align: left" width="300">
                <p style=" margin-top: 0;
    margin-bottom: 8px;
    color: #616161;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;
    padding: 0 24px;">
                    With best regards,<br>
                    WORLD FEDERATION OF DIRECT SELLING ASSOCIATION<br>
                    Phone: +202.416.6442<br>
                    </p>
                <p style=" margin-top: 0;
    margin-bottom: 8px;
    color: #616161;
    font-family: Roboto, Helvetica, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 18px;padding: 0 24px;">
                    Support: <a class="strong" href="mailto:#" target="_blank">info@wfdsa.org</a>
                </p>
            </td>
            <td class="subscription" width="300" style="vertical-align: bottom;
    width: 300px;
    padding-top: 8px;
    margin-bottom: 16px;
    text-align: right;padding: 0 24px;">
                <!-- <div class="logo-image">
                    <a href="https://wfdsa.org/" target="_blank"><img src="<?php echo base_url().'uploads/log_mini.jpg'?>" alt="logo-alt" style="margin-top: 6%;" width="80" height="80"></a>
                </div>
                <p>
                    <a class="strong block" href="https://wfdsa.org/about-us/" target="_blank">
                        About 
                    </a>
                    <span class="hide">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                    <a class="strong block" href="https://wfdsa.org/category/blog/" target="_blank">
                        Blog
                    </a>
                </p> -->
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0 24px;">
                <p class="caption">This is an auto-generated e-mail, please do not reply. If you have any query please e-mail us at info@wfdsa.org</p>
                <!-- <small>
                    <a href="https://play.google.com/store/apps/details?id=com.simplicore.app.wfdsa"><img src="<?php echo base_url().'uploads/new111.png'?>" width="30px" /> android app</a>
                </small> -->
            </td>
            <!-- <td>
                <small>
                    <a href="https://itunes.apple.com/us/app/wfdsa/id1422309391?mt=8"><img src="<?php echo base_url().'uploads/appleicon.png'?>" width="28px" /> iOS app</a>
                </small>
            </td> -->
        </tr>
        </tbody>
    </table>
</center>
</body>
</html>