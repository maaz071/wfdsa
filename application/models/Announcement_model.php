<?php

class Announcement_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        $this->db->order_by('announcement_id','DESC');
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_all_table_byrole($table_name,$data) {
     
        $role = $data['role'];
        $role = explode(',',$role);
        
        if($data['signin_type'] == 1)
        {
            foreach($role as $value){
                $this->db->from($table_name);
                $this->db->where('status', 1);
                
                $this->db->like('announce_for',$value,'both');
                $this->db->or_like('announce_for','Public','before');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('6');
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                }
            }    
        }
        else
        {
                $this->db->from($table_name);
                $this->db->where('status', 1);
                $this->db->where('announce_for','public');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('6');
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                } 
        }
        
    }
    
    function get_all_table_byrole_loading($table_name,$data) {
        
        $id= $data['announcement_id'];
        $role = $data['role'];
        $role = explode(',',$role);
        //print_r($id);
        //exit;
        
        if($data['signin_type'] == 1)
        {
            foreach($role as $value){
                $this->db->from($table_name);
                $this->db->where('status', 1);
                
                $this->db->like('announce_for',$value,'both');
                $this->db->or_like('announce_for','Public','before');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('6');
                $this->db->where('announcement_id <',$id);
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                }
            }    
        }
        else
        {
                $this->db->from($table_name);
                $this->db->where('status', 1);
                
                //$this->db->like('announce_for',$value,'both');
                $this->db->where('announce_for','Public');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('6');
                $this->db->where('announcement_id <',$id);
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                } 
        }
        
    }
    
    function get_all_table_rows($table_name,$data) {
       
        $role = $data['role'];
        $role = explode(',',$role);
        
        if($data['signin_type'] == 1)
        {
            foreach($role as $value){
                $this->db->from('announcement');
                $this->db->where('status', 1);
                
                $this->db->like('announce_for',$value,'both');
                $this->db->or_like('announce_for','Public','before');
                $this->db->order_by('announcement_id','DESC');
                
                return $this->db->count_all_results();
             
            }    
            
            
        }
        else
        {
                $this->db->from($table_name);
                $this->db->where('status', 1);
                $this->db->where('announce_for','public');
                $this->db->order_by('announcement_id','DESC');
                return $this->db->count_all_results();
                //$query = $this->db->get();
                
        }
        
    }
    
    function get_single_announcement($table_name,$data) {
       
        $role = $data['role'];
        $role = explode(',',$role);
        
        if($data['signin_type'] == 1)
        {
           foreach($role as $value){
                $this->db->from('announcement');
                $this->db->where('status', 1);
                
                $this->db->like('announce_for',$value,'both');
                $this->db->or_like('announce_for','Public','before');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('1');
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                }
            }    
            
        }
        else
        {
                $this->db->from($table_name);
                $this->db->where('status', 1);
                $this->db->where('announce_for','public');
                $this->db->order_by('announcement_id','DESC');
                $this->db->limit('1');
                $query = $this->db->get();
                
                $data = $query->result_array();
        
                if (!empty($data)) {
                    return $data;
                } else {
                    return FALSE;
                } 
                
        }
        
    }
    
    function get_all_table_record($table_name) {
        $this->db->from($table_name);
       // $this->db->where('status', 1);
        
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
 

    function create_record($table_name,$post, $file_name = NULL) {

        if ($file_name) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = base_url().'uploads/Announcement_Image/' . time() . "_" . $file_name;
        }
        $data = $this->db->insert($table_name, $post);
        $post['date'] = date('Y-m-d H:i:s', strtotime($post['date'])); 
        
        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($table_name,$primary_key_name , $post, $file_name, $primary_value) {
        //$this->output->enable_profiler(TRUE);
        
        if ($file_name !== NULL) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = base_url().'uploads/Announcement_Image/' . time() . "_" . $file_name;
        } else {
            unset($post['upload_image']);
        }
        
        $post['date'] = date('Y-m-d H:i:s', strtotime($post['date'])); 

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function get_all($id)
    {
        $query = "SELECT * FROM announcement where announcement_id = $id";
        $res = $this->db->query($query);
        return $res->result_array();
        
    }

}

?>