<?php

class Gallery_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getalbums($type) {
    	if($type=="video")
    	{
    		$type=1;
    	}
    	else
    	{
    		$type=0;
    	}
    	$this->db->from('album');
        $this->db->where('album_type', $type);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return 'false';
        }
    }
    function get_albums_api($role) {
        
        $this->db->from('album');
        $this->db->where('(FIND_IN_SET("'.$role.'", album_permission) OR album_permission = "Public")',null,false);
        // $this->db->where('(`album_permission` LIKE %'.$role.'% OR `album_permission` LIKE %Public%)',null,false);
        $this->db->where('status', 1);
        $this->db->where('album_type',1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return 'false';
        }
    }
    function get_photoalbums_api($role) {
    	
    	$this->db->from('album');
        $this->db->where('(FIND_IN_SET("'.$role.'", album_permission) OR album_permission = "Public")',null,false);
        // $this->db->where('(`album_permission` LIKE %'.$role.'% OR `album_permission` LIKE %Public%)',null,false);
        $this->db->where('status', 1);
        $this->db->where('album_type',0);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return 'false';
        }
    }
    function getgal($id)
    {
    	$this->db->from('gallery');
        $this->db->where('album_id', $id);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return 'false';
        }		
    }
}