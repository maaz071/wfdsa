<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_record($limit,$offset) {
        $this->db->Select('us.*,r.role_name');
        $this->db->from('user us');
        $this->db->join('role r', 'r.role_id = us.role_id', 'Left');
        
        $this->db->limit($limit, $offset);
       // $this->db->where('us.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post, $file_name = NULL) {
         unset($post['retype_password']);
        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];
        $post['password'] = base64_encode(md5($post['password']));
        $post['ip'] = $_SERVER['REMOTE_ADDR'];

        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($table_name, $primary_key_name, $post, $file_name, $primary_value) {
        //$this->output->enable_profiler(TRUE);
       
        unset($post['retype_password']);
        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];
        if(isset($post['password'])) {
            $post['password'] = base64_encode(md5($post['password']));
        } 

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>