<?php

class Comment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
       
       
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     function get_comment_table($table_name, $id = NULL,$event_id = NULL) {
       
        $this->db->select('com.*, ev.title as event_name,CONCAT(m.first_name, m.last_name) as member_name,m.upload_image');
        $this->db->from($table_name." com");
        $this->db->join("event ev","ev.event_id = com.event_id" , "Left");
        $this->db->join("member m","m.member_id = com.user_id" , "Left");
        $this->db->where('com.status', 1);
        if (isset($id)) {
            $this->db->where("com.".$table_name . "_id", $id);
        }
        if($event_id) {
            $this->db->where('com.event_id', $event_id);
        }
        
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
 

    function create_record($table_name,$post, $file_name = NULL) {

        if ($file_name) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = 'uploads/Event_Images/' . time() . "_" . $file_name;
        }
        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($table_name,$primary_key_name , $post, $file_name, $primary_value) {
        //$this->output->enable_profiler(TRUE);
        
        if ($file_name !== NULL) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = 'uploads/Event_Images/' . time() . "_" . $file_name;
        } else {
            unset($post['upload_image']);
        }
        
        $post['start_date'] = date('Y-m-d H:i:s', strtotime($post['start_date']));
        $post['end_date'] = date('Y-m-d H:i:s', strtotime($post['end_date']));

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>