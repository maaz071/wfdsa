<?php

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create_record($post) {
        unset($post['retype_password']);

        $this->db->insert('registration', $post);
        return $this->db->insert_id();
    }
    
    
    
    function check_admin_login($post) {
		 
        $this->db->from('user');
        $this->db->where('status',1);
        $this->db->where('username',$post['username']);
        $this->db->where('password',  base64_encode(md5($post['password'])));
        $query = $this->db->get();
        $data = $query->result_array(); 
		 
        if(!empty($data)) {
            return $data[0]['user_id'];
        } else {
            return 0;
        }
    }
    
     function insert_login_attempt($post) {
      //   $this->output->enable_profiler(TRUE); 
	  
        $post['ip_address'] = $_SERVER["REMOTE_ADDR"];
        $post['user_id'] = $this->session->admin_data[0]['user_id']; 
        $post['created_on'] = date('Y-m-d H:i:s');
        
        unset($post['old_password']);
        unset($post['new_password']);
        unset($post['retype_password']);
        
        $data = $this->db->insert('login_attempt', $post);

        if ($data) {
            
            $this->db->from('login_attempt');
            $this->db->where('ip_address',$_SERVER["REMOTE_ADDR"]);
            $query = $this->db->get();
            $count_attempt = $query->num_rows(); 
            
            if($count_attempt == 3) {
                 $this->db->where('ip_address', $_SERVER["REMOTE_ADDR"]);
                 $this->db->delete('login_attempt');
            }
            
            return ($count_attempt) ? $count_attempt : 0 ;
             
        } else {
            return FALSE;
        }
       
    }
    
    function verify_password($post) {
        
        $user_id = $this->session->admin_data[0]['user_id'];    
    
        $this->db->from('user');
        $this->db->where('user_id',$user_id);
        $this->db->where('password',base64_encode(md5($post['old_password'])));
        $query = $this->db->get();
        $data = $query->result_array(); 
            
        return ($data) ? $data :array() ;
    }
    
    function change_password($post) {
        $user_id = $this->session->admin_data[0]['user_id'];    
        $this->db->where('user_id', $user_id);
        $data = $this->db->update('user', array('password' => base64_encode(md5($post['new_password']))));
        
        return $data; 
    }

}

?>