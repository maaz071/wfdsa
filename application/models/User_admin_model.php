<?php

class User_admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    } 
    
    function get_users() { 
       
        $this->db->select('u.*, r.admin_Role_Name');
        $this->db->from('users_admin u');
        $this->db->join('roles_admin r', 'r.roles_id = u.role_Id', 'Left');
        $this->db->where('u.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        } 
        
       $data = $this->db->get_where('users_admin', array('status' => 1))->result_array();
       if(!empty($data)){
           return $data;
       } else {
           return FALSE;
       } 
    }
    
     function get_edit_record($table_name, $id = NULL) {
        $this->db->from($table_name); 
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_table_record($table_name, $id = NULL) {
        $this->db->select('event_id');
        $this->db->from($table_name); 
        if (isset($id)) {
            $this->db->where("event_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    
    function get_roles() {
       $data = $this->db->get_where('roles_admin', array('status' => 1))->result_array();
       if(!empty($data)){
           return $data;
       } else {
           return FALSE;
       } 
    }
    
    function create_record($post) {
        
        $post['password'] = base64_encode(md5($post['password']));
        $data = $this->db->insert('users_admin', $post);  
         
        if($data) {
           return $this->db->insert_id();
        } else {
            return FALSE;
        } 
      
    }
    
    
    function get_user_record($id) {

        $this->db->from('users_admin');
        $this->db->where('status', 1);
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function update_record($post , $id) {
        // $this->output->enable_profiler(TRUE);
 
         $post['password'] = base64_encode(md5($post['password']));
        $this->db->where('user_id', $id);
        $data = $this->db->update('users_admin', $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>