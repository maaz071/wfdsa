<?php

class Dsa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
       // $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    
    
    function get_dsa_member_record($table_name,$limit="",$offset="") {
        $this->db->select('dsa.*, c.name as country,c.flag_pic,r.name as region');
        $this->db->from($table_name.' dsa'); 
        $this->db->join('country c','c.country_id = dsa.country_id','Left');
        $this->db->join('region r','r.region_id = dsa.region_id','Left');
        $this->db->order_by("dsa.country_id", "asc");
       
       if ($limit != "" && $offset >= 0) {
            $this->db->limit($limit, $offset);
        }
       
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_roles($id){
        
        $this->db->select('msr.*, r.name as role_name');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Left');
        $this->db->where('r.status', 1);
        $this->db->where('msr.member_id', $id);
      
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    
    }

    function create_record($table_name, $post, $clogo = NULL,$member_photo = NULL) {

        if ($clogo) {
            $clogo = str_replace(' ', '_', $clogo);
            $post['company_logo'] = base_url().'uploads/DSA_Members/companyLogo/' . time() . "_" . $clogo;
        }
        
        if ($member_photo) {
            $member_photo = str_replace(' ', '_', $member_photo);
            $post['company_logo'] = base_url().'uploads/DSA_Members/' . time() . "_" . $member_photo;
        }
        $post['created_on'] = date('Y-m-d H:i:s');
        
        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
     function get_likes_records($event_id) {
        
        $this->db->select('lik.*, CONCAT(me.first_name," " ,me.last_name) as member_name , me.upload_image');
        $this->db->from('likes lik'); 
        $this->db->join('member me','me.member_id = lik.user_id','Left');
         $this->db->where('lik.status', 1);
         
         if($event_id) {
            $this->db->where('lik.event_id', $event_id);
         }
      
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
    } 

    function update_record($tableName, $row_id, $post, $image) {
        /*echo "<pre>";
        print_r($post);
        exit;
        */
        
        //$this->output->enable_profiler(TRUE);
        if(empty($post['dsa_id'])){
            $id = $post['Dsa_id'];
        }
        else
        {
            $id =$post['dsa_id'];
        }
        
        /*$id= $post['dsa_id'];
        $id = strtolower($id);
        */
        
        /*if ($clogo !== NULL) {
            $clogo = str_replace(' ', '_', $clogo);
            $post['company_logo'] = base_url().'uploads/DSA_Members/companyLogo/' . time() . "_" . $clogo;
        } else {
            unset($post['company_logo']);
        } 
        
        if ($member_photo !== NULL) {
             $member_photo = str_replace(' ', '_', $member_photo);
            $post['member_photo'] = base_url().'uploads/DSA_Members/' . time() . "_" . $member_photo;
        } else {
             unset($post['member_photo']);
        }*/

        //$post['updated_on'] = date('Y-m-d H:i:s');
        
        $data =array(
            'company_name' => $post['company_name'],
            'member_name' => $post['member_name'],
            'phone' => $post['phone'],
            'fax' => $post['fax'],
            'email' => $post['email'],
            'address' => $post['address'],
            'website' => $post['website'],
            'country_id' => $post['country_id'],
            'region_id' => $post['region_id'],
            'company_logo' => $image,
            'member_photo' => $image,
            'status' => 1
            );

        $this->db->where($row_id, $id);
        //unset($post[$primary_key_name]);
        $data = $this->db->update('dsa', $data);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>