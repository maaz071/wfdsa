<?php

require_once APPPATH."models/__Base.php";
class Non_member_model extends __Base_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        //$this->db->where('status', 1);
        if (!empty($id)) {
            $this->db->where($table_name . "_id", $id);
        }
      
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get($id) {
        $data = $this->db->get_where('categories', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     
    
    function get_parent_role() {
        
        $this->db->from('member_role');
        $this->db->where("status", 1); 
        $this->db->where("parent_id", 0); 
        $query = $this->db->get();
        return $data = $query->result_array(); 
    }

    function create_record($table_name,$post) {
        
       
        $post['password'] = base64_encode(md5($post['password']));
         
        $data = $this->db->insert($table_name, $post);
        $recent_id = $this->db->insert_id();
         
        $param  = time().rand(999, 99999999999);
        $api_secret = md5($param).substr(str_shuffle($param), 0, 10);
        $this->db->insert('api_keys', array('member_id'=>$recent_id,'api_secret'=>$api_secret)); 
        
        if ($data) {
            return $recent_id;
        } else {
            return 0;
        }
    }
    
     function get_join_record($table_name,$limit="",$offset="") {
        
         
        $this->db->select('nm.*');
        $this->db->from($table_name.' nm'); 
        $this->db->join('country c','c.country_id = nm.country','Left'); 
        $query = $this->db->get();
        
        if ($limit != "" && $offset >= 0) {
            $this->db->limit($limit, $offset);
        }
        
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
        
    }
    
    

    function update_record($table_name,$primary_key_name,$post,$primary_value) {
        //$this->output->enable_profiler(TRUE); 
         
        $post['password'] = base64_encode(md5($post['password']));
        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);
        $this->session->unset_userdata('image_key');
        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    
    
     function update_api_member($post) {
       // $this->output->enable_profiler(TRUE);
       
        $member_record = $this->db->get_where('member', array('status' => 1,'member_id'=>$post['member_id']))->result_array();
        $member_record = (!empty($member_record)) ? $member_record : array();
      
        if(!empty($post['image'])) {
            
             // $old_file =   explode("/",$member_record[0]['upload_image']);
            //  unlink('./uploads/member_photos/'.$old_file[7]);
           /* $data = base64_decode($post['image']);
            $file = base_url().'/uploads/member_photos/'.time().'_'.'api_member_image.jpeg';
            file_put_contents('./uploads/member_photos/'.time().'_'.'api_member_image.jpeg', $data);*/
            
        } 
        
        if(empty($post['image'])) { 
            $file = base_url().'/uploads/member_photos/dummy.jpg'; 
            $member_record[0]['upload_image'] = $file;
        }
        
        $this->db->where('member_id', $post['member_id']); 
        $data = $this->db->update('member', array('first_name'=>$post['first_name'],'last_name' => $post['last_name'], 'cell'=> $post['cell'],'upload_image'=>$post['image'])); 
        
        $member_record[0]['first_name'] = $post['first_name'];
        $member_record[0]['last_name'] = $post['last_name'];
        $member_record[0]['cell'] = $post['cell'];
       
        
        if ($data) {
            return $member_record;
        } else {
            return array();
        }
    }
    
     
    
     function get_member_select($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("member_id", $id); 
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     
    
    function get_committee() {
         
        $data = $this->db->get_where('member_role',array('name'=>'Committees'))->result();
        $role_data = $this->db->get_where('member_role',array('parent_id'=>$data[0]->member_role_id))->result_array();
        
        if (!empty($role_data)) {
            return $role_data;
        } else {
            return array();
        }
    }
    
    
    function get_committee_member($role_id) { 
        
        $data = $this->db->get_where('member_role',array('name'=>'Committees'))->result();
         
        $this->db->select('msr.*, m.*, CONCAT(m.first_name," ",m.last_name) as member_name, r.name as role_name,r.member_role_id,r.parent_id,c.name,c.flag_pic');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','right outer');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('country c','c.country_id = m.country','Inner');
        $this->db->where('m.status', 1);
        $this->db->where('r.member_role_id', $role_id);
        $this->db->where('r.parent_id', $data[0]->member_role_id);
        
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
    function getApi_Leadership() {
         
        $data = $this->db->get_where('member_role',array('name'=>'WFDSA Leadership'))->result();
        $role_data = $this->db->get_where('member_role',array('parent_id'=>$data[0]->member_role_id))->result_array();
        
        if (!empty($role_data)) {
            return $role_data;
        } else {
            return array();
        }
    }
    
    function get_Leaderby_Role($role_id) {  
        //  $this->output->enable_profiler(TRUE);
        $data = $this->db->get_where('member_role',array('name'=>'WFDSA Leadership'))->result();
         
        $this->db->select('msr.*,m.*, r.name as role_name,r.member_role_id,r.parent_id,c.name,c.flag_pic');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','Left');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('country c','c.country_id = m.country','Inner');
        $this->db->where('m.status', 1);
        $this->db->where('r.member_role_id', $role_id);
        $this->db->where('r.parent_id', $data[0]->member_role_id);
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
  

    function get_parent_child($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("parent_id", $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function create_multi_record($table_name, $id, $post, $primary_key = NULL) {

        if ($primary_key) {
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        }

        $insert_arr = array();
         
            foreach ($post['member_role'] as $value) {
                $insert_arr[] = array(
                    'member_id' => $id,
                    'member_role_id' => $value
                );
            }
        

        $data = $this->db->insert_batch($table_name, $insert_arr);
        if ($data) {
            return TRUE;
        }
    }
    
  
    function get_roles($id){ 
          
        $this->db->select('msr.*, r.name as role_name');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Left');
        $this->db->where('r.status', 1);
        $this->db->where('msr.member_id', $id); 
       
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    
    }
    
    
}

?>