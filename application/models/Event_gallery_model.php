<?php

class Event_gallery_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name."_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }

    }

    function get_gallery_record() {
        
        $event_id = $this->input->get('event_id');
         
        $this->db->Select('g.*,ev.title as event_name');
        $this->db->from('event_gal_images g');
        $this->db->join('event ev', 'ev.event_id = g.event_id', 'Left');
        //$this->db->join('user_photo up', 'up.event_id = ev.event_id','Left');
        $this->db->where('g.event_id', $event_id);
        //$this->db->where('up.approved',1);
        $query = $this->db->get();
       /* echo "<pre>";
        print_r($query->result_array());
        exit;*/
        return $query->result_array();
        
        
    }


    function get_record($event_id = NULL) {
        $this->db->Select('eg.*,ev.title as event_name');
        $this->db->from('event_gallery eg');
        $this->db->join('event ev', 'ev.event_id = eg.event_id', 'Left');
        //$this->db->where('eg.status', 1);
        
        if($event_id) {
            $this->db->where('eg.event_id', $event_id);
        }
        
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_gallery_img($table_name , $event_id, $userid) {
        //, up.uploaded_image as member_image
        $userquery = "SELECT * FROM `attendees` WHERE event_id = '".$event_id."' AND checked_in=1 AND (member_id = '".$userid."' OR non_member_id='".$userid."')";     

        $this->db->Select('e.event_id ,g.uploaded_image as gallery_images,g.checked_in as permission');
        $this->db->from('event e');
        $this->db->join('event_gal_images g', 'g.event_id = e.event_id', 'Left');
        //$this->db->join('user_photo up','up.status = g.status','Right');
        //$this->db->where('up.approved',1);
        $this->db->where('e.status', 1);
        $this->db->where('e.event_id', $event_id); 

        $userquery = $this->db->query($userquery);
        if($userquery->num_rows() < 1)
        {
            $this->db->where('g.checked_in !=', 1);                   
        }
        
        //$this->db->group_by('g.uploaded_image');
        //$this->db->group_by('up.uploaded_image');
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post, $file_name = NULL) {

        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];


        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function delete_gallery_record($id) {
        if ($id) {
            $this->db->where('event_gallery_id', $id);
            $this->db->delete('event_gal_images');
            return TRUE;
        }
    }
    
    function create_gallery_record($file_name,$event_id,$checkedin) { 
        $post['checked_in'] = $checkedin;        
        $post['event_id'] = $event_id;
        $post['uploaded_image'] = base_url().'uploads/Event_Gallery/' . $file_name;
        return $this->db->insert('event_gal_images', $post); 
    }
    
    function create_gallery_records($user_id,$event_id,$file_name) { 
        $date = date('Y-m-d H:i:s');
        $post = array(
            'event_id' => $event_id,
            'user_id' => $user_id,
            'uploaded_image' => $file_name,
            'image_title' =>"abc",
            'approved' => 0,
            'status' =>1,
            'created_on'=>$date
            );
            
        //$post['uploaded_image'] = base_url().'uploads/Event_Gallery/' . $file_name;
        $this->db->insert('user_photo', $post); 

        return $insert_id = $this->db->insert_id(); 
    }
    
    function update_record($table_name, $primary_key_name, $post, $primary_value) {
        //$this->output->enable_profiler(TRUE);

        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_relation_table($id, $relation_table, $table_name) {
        $this->db->from($relation_table);
        $this->db->where($table_name . '_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
    function add_api_gallery_image($post) {
        
        $data = base64_decode($post['image_1']);
        $file = base_url().'/uploads/Event_Gallery/'.time().'_'.'gallery_image.jpeg';
        file_put_contents($file, $data);
    }

}

?>