<?php

class About_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_product($keyword, $clear, $cat_id, $brand_id) {
        // $this->output->enable_profiler(TRUE);
        $this->db->from('products');
        $this->db->where('status', 1);
        if ($keyword == TRUE && $clear == NULL) {
            $this->db->like('pname', $keyword);
        }
        if ($cat_id) {
            $this->db->where('category', $cat_id);
        }
        if($brand_id){
             $this->db->where('brand_id', $brand_id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }

    function get_recent_product() {

        $this->db->from('products');
        $this->db->where('status', 1);
        $this->db->order_by('product_id desc');
        $this->db->limit(6);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }

    function item_cart() {

        $this->db->from('shopping_cart');
        $this->db->where('session_Id', session_id());
        $query = $this->db->get();
        $items = $query->num_rows();

        $this->session->set_userdata('item_cart', $items);
    }

    function get_products() {

        $this->db->select('p.*, cat.category_name as category,b.brand_Name');
        $this->db->from('products p');
        $this->db->join('categories cat', 'cat.category_id = p.category', 'Left');
        $this->db->join('brand b', 'b.brand_id = p.brand_id', 'Left');
        $this->db->where('p.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_categories() {
        $data = $this->db->get_where('categories', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_brand() {
        $data = $this->db->get_where('brand', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get($id) {
        $data = $this->db->get_where('categories', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    public function get_social_msg(){
        $this->db->select('social_msg');
        $this->db->from('about');
        $this->db->where('status', 1);
        $this->db->where('about_id', 1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }        
    }

    function get_about_data($id) {

        $this->db->from('about');
        $this->db->where('status', 1);
        $this->db->where('about_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($post, $file_name) {

        $file_name = str_replace(' ', '_', $file_name);
        $post['pimage'] = base_url().'uploads/' . time() . "_" . $file_name;
        $data = $this->db->insert('products', $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($post, $file_name, $id) {
         //$this->output->enable_profiler(TRUE);
        if (!empty($file_name)) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_pic'] = base_url().'uploads/' . time() . "_" . $file_name;
        } else {
            unset($post['upload_pic']);
        }

        $this->db->where('about_id', 1);
        unset($post['about_id']);
        $data = $this->db->update('about', $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>