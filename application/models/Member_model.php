<?php

require_once APPPATH."models/__Base.php";
class Member_model extends __Base_Model {

    function __construct() {
        parent::__construct();
    }

     function get_all_table($table_name, $id = NULL,$limit="",$offset="") {
        $this->db->from($table_name);
        //$this->db->where('status', 1);
        if (!empty($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        // $this->db->where("role", 'developer');
      if($limit != "" && $offset >= 0){
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();

        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_all_table_rev($table_name, $id = NULL,$limit="",$offset="") {
        $this->db->from($table_name);
        //$this->db->where('status', 1);
        if (!empty($id)) {
            $this->db->where($table_name . "_id", $id);
        }
      if($limit != "" && $offset >= 0){
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('member_id','DESC');
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_join_record($table_name,$role_id=NULL) {
        //$this->output->enable_profiler(TRUE);
        $this->db->select('m.*,msr.member_id,msr.member_role_id,r.name,c.name as country');
        $this->db->from('member m'); 
        $this->db->join('member_select_role msr','msr.member_id = m.member_id','Left');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Left');
        $this->db->join('country c','c.country_id = m.country','Left');
        
       // $this->db->where('m.status', 1);
        if($role_id != NULL) {
        $this->db->where('r.member_role_id', $role_id); 
        }
        $query = $this->db->get();
        return $query->result_array();
         
    }

    function get($id) {
        $data = $this->db->get_where('categories', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     
    
    function get_parent_role() {
        
        $this->db->from('member_role');
        $this->db->where("status", 1); 
        $this->db->where("parent_id", 0); 
        $this->db->where("visibility", 1); 
        $query = $this->db->get();
        return $data = $query->result_array(); 
    }

    function create_record($table_name,$post, $file_name = NULL) {
        
        unset($post['hdn-profile-id']);
        unset($post['hdn-x1-axis']);
        unset($post['hdn-y1-axis']);
        unset($post['hdn-x2-axis']);
        unset($post['hdn-y2-axis']);
        unset($post['hdn-thumb-width']);
        unset($post['hdn-thumb-height']);
        unset($post['image_name']);
          
        unset($post['re_password']);
        
        if ($file_name !== NULL) { 
            $post['upload_image'] = base_url().'uploads/Member_Photos/'.$file_name;
        }  
         $post['password'] = base64_encode(md5($post['password']));
         unset($post['member_role']);
        $data = $this->db->insert($table_name, $post);
        $recent_id = $this->db->insert_id();
         
        $param  = time().rand(999, 99999999999);
        $api_secret = md5($param).substr(str_shuffle($param), 0, 10);
        $this->db->insert('api_keys', array('member_id'=>$recent_id,'api_secret'=>$api_secret));
        
        
        $this->session->unset_userdata('image_key');
        if ($data) {
            return $recent_id;
        } else {
            return FALSE;
        }
    }
    
    
    function create_role($table_name , $post) {
        
         $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($table_name,$primary_key_name,$post,$file_name=NULL, $primary_value) {
        //$this->output->enable_profiler(TRUE);
         
        unset($post['hdn-x1-axis']);
        unset($post['hdn-y1-axis']);
        unset($post['hdn-x2-axis']);
        unset($post['hdn-y2-axis']);
        unset($post['hdn-thumb-width']);
        unset($post['hdn-thumb-height']);
        unset($post['image_name']);
   
        unset($post['re_password']);
        if ($file_name !== NULL) { 
            $post['upload_image'] = base_url().'uploads/Member_Photos/'.$file_name;
        }  
        unset($post['member_role']);
        if(isset($post['password'])) {
            $post['password'] = base64_encode(md5($post['password']));    
        }
        
        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);
        $this->session->unset_userdata('image_key');
        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    
    
     function update_api_member($post) {
         /*print_r($_SERVER['DOCUMENT_ROOT']);
         
         exit;
         */
         /*
        echo "<pre>";
        print_r($post);
        exit;*/
       // $this->output->enable_profiler(TRUE);
       $path = "./wfdsa3/wfdsa";

       $time  = time();
        // $member_record = $this->db->get_where('member', array('status' => 1,'member_id'=>$post['member_id']))->result_array();
        $nonmemberrequest = 0;
        // $this->db->where('member_id', $post['member_id']);
        // $this->db->where('status', 1);
        // $q = $this->db->get('member');
        // $count = $q->db->num_rows();

        //  $query = $this->db->get_where('member', array('member_id' => $post['member_id']
        // ));

        // $count = $query->num_rows();
 
if ( $post['type'] != 1 ) {
            $member_record = $this->db->get_where('non_member', array('status' => 1,'non_member_id'=>$post['member_id']))->result_array();            
            $nonmemberrequest = 1;
        }
        else{
            $member_record = $this->db->get_where('member', array('status' => 1,'member_id'=>$post['member_id']))->result_array();
        }
        $member_record = (!empty($member_record)) ? $member_record : array();
      
      if($nonmemberrequest == 1){
        if(!empty($post['image'])) {
            
             //$old_file =   explode("/",$member_record[0]['upload_image']);
             //unlink('./uploads/member_photos/'.$old_file[7]);
            $data = base64_decode($post['image']);
            $file = './uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG';
            file_put_contents('./uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG', $data);
            $uploadimage = base_url().'/uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG';
            
        } 
        
        if(empty($post['image'])) { 
            //$file = base_url().'/uploads/member_photos/dummy.jpg'; 
            //$member_record[0]['upload_image'] = $file;
            $file = $member_record[0]['upload_image'];
            $uploadimage = $member_record[0]['upload_image'];
        }

        $this->db->where('non_member_id', $post['member_id']); 
        $data = $this->db->update('non_member', array('first_name'=>$post['first_name'],'last_name' => $post['last_name'], 'contact_no'=> $post['cell'],'upload_image'=>$uploadimage)); 
        
        $member_record[0]['first_name'] = $post['first_name'];
        $member_record[0]['last_name'] = $post['last_name'];
        $member_record[0]['cell'] = $post['cell'];
        $member_record[0]['upload_image'] = $uploadimage;
        // $member_record[0]['upload_image'] = $uploadimage;
      }
      else{
        if(!empty($post['image'])) {
            
             //$old_file =   explode("/",$member_record[0]['upload_image']);
             //unlink('./uploads/member_photos/'.$old_file[7]);
            $data = base64_decode($post['image']);
            $file = './uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG';
            file_put_contents('./uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG', $data);
            $uploadimage = base_url().'/uploads/Member_Photos/'.$time.'_'.'api_member_image.JPG';
            
        } 
        
        if(empty($post['image'])) { 
            //$file = base_url().'/uploads/member_photos/dummy.jpg'; 
            //$member_record[0]['upload_image'] = $file;
            $file = $member_record[0]['upload_image'];
            $uploadimage = $member_record[0]['upload_image'];
        }
        
        $this->db->where('member_id', $post['member_id']); 
        $data = $this->db->update('member', array('first_name'=>$post['first_name'],'last_name' => $post['last_name'], 'cell'=> $post['cell'],'upload_image'=>$uploadimage)); 
        
        $member_record[0]['first_name'] = $post['first_name'];
        $member_record[0]['last_name'] = $post['last_name'];
        $member_record[0]['cell'] = $post['cell'];
        $member_record[0]['upload_image'] = $uploadimage;
       }
        
        if ($data) {
            return $member_record;
        } else {
            return array();
        }
    }
    
     
    
     function get_member_select($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("member_id", $id); 
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     
    
    function get_committee() {
         
        $data = $this->db->get_where('member_role',array('name'=>'Committee'))->result();
        
        
        $role_data = $this->db->order_by("priority", "asc")->get_where('member_role',array('parent_id'=>$data[0]->member_role_id,'visibility'=>1))->result_array();
        
        if (!empty($role_data)) {
            return $role_data;
        } else {
            return array();
        }
    }
    
    
    function get_committee_member($role_id) { 
        
        $data = $this->db->get_where('member_role',array('name'=>'Committee'))->result();
         
        $this->db->select('msr.*, m.*, CONCAT(m.first_name," ",m.last_name) as member_name, r.name as role_name,r.member_role_id,r.parent_id,c.name,c.flag_pic');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','right outer');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        //$this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('country c','c.country_id = m.country','Inner');
        $this->db->where('m.status', 1);
        $this->db->where('r.member_role_id', $role_id);
        $this->db->where('r.parent_id', $data[0]->member_role_id);
        
        $query = $this->db->get();
        $data = $query->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
    function getApi_Leadership() {
         
        $data = $this->db->get_where('member_role',array('name'=>'WFDSA Leadership'))->result();
        $role_data = $this->db->order_by('priority','asc')->get_where('member_role',array('parent_id'=>$data[0]->member_role_id,'visibility'=>1))->result_array();
        
        if (!empty($role_data)) {
            return $role_data;
        } else {
            return array();
        }
    }
    
    function get_Leaderby_Role($role_id) {  
        //  $this->output->enable_profiler(TRUE);
        //$query= "SELECT * FROM member where role like '".$role_id."'";
        //$result = $this->db->query($query);
        //print_r($result);
        //exit;
        
        
        $data = $this->db->get_where('member_role',array('name'=>'WFDSA Leadership'))->result();
         
        $this->db->select('msr.*,m.*, r.name as role_name,r.member_role_id,r.parent_id,c.name,c.flag_pic');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','Left');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('country c','c.country_id = m.country','Inner');
        $this->db->where('m.status', 1);
        $this->db->where('r.member_role_id', $role_id);
        $this->db->where('r.parent_id', $data[0]->member_role_id);
        $query = $this->db->get();
        $data = $result->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
    function get_Leaderby_Roles($role_id) {  
        //  $this->output->enable_profiler(TRUE);
        $query= "SELECT * FROM member INNER JOIN country ON member.country=country.country_id WHERE role like '".$role_id."' ";
        $result = $this->db->query($query);
        //print_r($result->result_array());
        //exit;
        
        /*
        $data = $this->db->get_where('member_role',array('name'=>'WFDSA Leadership'))->result();
         
        $this->db->select('msr.*,m.*, r.name as role_name,r.member_role_id,r.parent_id,c.name,c.flag_pic');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','Left');
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Inner');
        $this->db->join('country c','c.country_id = m.country','Inner');
        $this->db->where('m.status', 1);
        $this->db->where('r.member_role_id', $role_id);
        $this->db->where('r.parent_id', $data[0]->member_role_id);
        $query = $this->db->get();*/
        $data = $result->result_array();
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
  

    function get_parent_child($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("parent_id", $id);
        $query = $this->db->get();
        $data = $query->result_array();
        
        //print_r($data);
        //exit;
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }
    
     
    
    function create_multi_record($table_name, $id, $post, $primary_key = NULL) {

        if ($primary_key) {
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        }

        $insert_arr = array();
         
            foreach ($post['member_role'] as $value) {
                $insert_arr[] = array(
                    'member_id' => $id,
                    'member_role_id' => $value
                );
            }
        

        $data = $this->db->insert_batch($table_name, $insert_arr);
        if ($data) {
            return TRUE;
        }
    }
    
  
    function get_roles($id,$role_id){ 
          
        $this->db->select('msr.*, r.name as role_name');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Left');
        $this->db->where('r.status', 1);
        $this->db->where('msr.member_id', $id); 
        $this->db->where('msr.member_role_id', $role_id); 
       
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    
    }
    
    
}

?>