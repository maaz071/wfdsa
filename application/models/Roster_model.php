<?php

class Roster_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     function get_parent_role() {
        $select_role = array();
        
        $this->db->from('member_role');
        $this->db->where('name', 'WFDSA Leadership');
        $this->db->or_where('name', 'Committee');
        $query_r = $this->db->get();
        return $query_r->result_array(); 
    }
    
    
    function get_memberByrole($id) {
        //  $this->output->enable_profiler(TRUE);
        $this->db->select('msr.*,CONCAT(m.first_name," ",m.last_name) as member_name, m.email , m.company , m.designation , m.cell , m.status,m.role');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','Left');
        $this->db->where('m.status', 1);
        $this->db->where('msr.member_role_id', $id);
        
        //$this->db->where('msr.member_role_id', $id); 
        $this->db->order_by('msr.sort_order','asc');
       
        $query = $this->db->get();
        $data = $query->result_array();
        //print_r($data);
        //exit;
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
        
    }
    
    function get_memberByroles($id) {
        //  $this->output->enable_profiler(TRUE);
        $this->db->select('msr.*,CONCAT(m.first_name," ",m.last_name) as member_name, m.email , m.company , m.designation , m.cell , m.telephone, m.upload_image, m.fax, m.wfdsa_title, m.address, m.title, c.name, c.flag_pic' );
        $this->db->from('member_select_role msr'); 
        $this->db->join('member m','m.member_id = msr.member_id','Left');
        $this->db->join('country c','m.country = c.country_id','Left');
        $this->db->where('m.status', 1);
        $this->db->where('msr.member_role_id', $id); 
        $this->db->order_by('msr.sort_order','ASC');
       
        $query = $this->db->get();
        $data = $query->result_array();
        //print_r($data);
        //exit;
        
        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
        
    }

    function get_relation_table($table_name, $id = NULL, $relation_table) {
//        $this->output->enable_profiler(TRUE);

        $this->db->from($table_name);
        if (isset($id)) {
            $this->db->where($relation_table . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post) {
         
        
        
        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];
        $data = $this->db->insert_batch($table_name, $insert_arr); 

        if ($data) {
            return $this->db->insert_id();
        } else {
            return NULL;
        }
    }
    
    function get_parent_child($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("parent_id", $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_poll_answer($table_name, $id, $post,$primary_key = NULL) {
        
        if($primary_key) { 
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        } 
        
        $insert_arr = array();
        foreach ($post['answer'] as $value) {
            $insert_arr[] = array(
                'poll_id' => $id,
                'poll_answer' => $value
            );
        }

        $data = $this->db->insert_batch($table_name, $insert_arr);
        if ($data) {
            return TRUE;
        }
    }

    function update_record($table_name, $primary_key_name, $post, $primary_value) {
        //$this->output->enable_profiler(TRUE); 
        unset($post['answer']);
        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]); 
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function delete_record($table_name,$id) {
        $this->db->where($table_name.'_id', $id);
        $dat = $this->db->delete($table_name);
        if($dat) {
            return TRUE;
        }
    }

}

?>