<?php

class Roles_admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_roles() {
        $data = $this->db->get_where('roles_admin', array('status' => 1))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_role_record($id) {
        $data = $this->db->get_where('roles_admin', array('status' => 1, 'roles_id' => $id))->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($post) {

        $data = $this->db->insert('roles_admin', $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($post, $id) {
        // $this->output->enable_profiler(TRUE); 
       
        $this->db->where('roles_id', $id);
        $data = $this->db->update('roles_admin', $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>