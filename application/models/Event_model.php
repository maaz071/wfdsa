<?php

class Event_model extends CI_Model
{

    const MEMBER = 1;
    const NON_MEMBER = 2;

    function __construct()
    {
        parent::__construct();
    }
    function get_event_title($table_name,$id){
        $this->db->select('title');
        $this->db->from($table_name);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_event_gallery($event_id){
        $this->db->select('uploaded_image');
        $this->db->from('event_gal_images');
        if (isset($event_id)) {
            $this->db->where("event_id", $event_id);
        }
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    function get_all_table($table_name, $id = NULL)
    {
        $date = date('m-d-Y H:i');
        $this->db->from($table_name);
        //$this->db->where('start_date >=',$date);
        //  $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        //$this->db->order_by('start_date','ASC');
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    function get_events_by_usertype($user_type=0){
        $this->db->select('event_id');
        $this->db->from('event');
            $this->db->where("event_user_type", $user_type);
        $this->db->order_by('event_id','DESC');
        $query = $this->db->get();
        $event_ids = $query->result_array();
        $key = 0;
        foreach($event_ids as $row)
        {
            $event_id = $row['event_id'];
            $this->db->from('event_notifications');
            $this->db->where("event_id", $event_id);
        $query1 = $this->db->get();
        $result = $query1->result_array();
        if(count($result)){
            $data[$key] = $result;
            $key++;
        }
        }        
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }   
    }
    function get_all_table_events($table, $id = NULL)
    {
        $date = date('m-d-Y H:i');
        $this->db->from($table);
        // $this->db->where('start_date >=',$date);
        //  $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table . "_id", $id);
        }
        // $this->db->where('start_date >=',date('m-d-Y'));
        // $this->db->where('end_date >=',date('m-d-Y'));
        $this->db->order_by('start_date','ASC');
        $query = $this->db->get();
        $data = $query->result_array();
        
        
        /*echo "<pre>";
        print_r($data);
        exit;*/
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_resources($event_id){
        $this->db->from('event_resources');
        $this->db->where('event_id',$event_id);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_notifications($event_id)
    {
        $this->db->from('event_notifications');
        $this->db->where('event_id',$event_id);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_single_events($table, $id = NULL)
    {
        $date = date('m-d-Y H:i');
        $this->db->from($table);
        // $this->db->where('start_date >=',$date);
        //  $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table . "_id", $id);
        }
        $this->db->order_by('start_date','ASC');
        $this->db->limit(1);
        $query = $this->db->get();
        //$data1  =$query->first_row();
        $data = $query->result_array();
        /*echo "<pre>";
        print_r($data);
        exit;*/
        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    

    function get_upcomming_event($user_token)
    {

        $token_check = $this->CI->db->get_where('notification_token', array('user_token' => $user_token))->result_array();

        if (empty($token_check[0])) {
            $this->CI->db->insert('notification_token', array('user_token' => $user_token));
        }

        $this->db->from('event');
        $this->db->where('status', 1);
        $this->db->where('DATEDIFF(`start_date`,CURDATE())', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_parent_child($table_name, $id)
    {
        $this->db->from($table_name);
        $this->db->where("parent_id", $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }


    public function send_fcm($token, $body, $click_action, $admin_id = NULL, $API_ACCESS_KEY)
    {

        $registrationIds = $token;

        $msg = array
        (
            'body' => $body,
            'title' => 'PMLN',
            'icon' => 'myicon',
            'sound' => 'mySound',
            'click_action' => $click_action,
            'admin_id' => strval($admin_id)
        );

        $fields = array
        (
            'to' => $registrationIds,
            'notification' => $msg,

        );


        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        curl_close($ch);
        //echo json_encode($result);

    }

    function get_event_poll($event_id)
    {


        $this->db->from('poll');
        $this->db->where('status', 1);
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        
        
        $data = $query->result_array();
        
        //print_r($poll_answer);
        //exit();
        if (!empty($data)) {

            $poll_answer = $this->db->get_where('poll_answer', array('poll_id =' => $data[0]['poll_id']))->result_array();
            
            //print_r($poll_answer);
            //exit();

            foreach ($poll_answer as $key => $value) {
                $data['poll_answer'][] = $value['poll_answer'];
                $data['id'][] = $value['poll_answer_id'];
            }
        }

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }

    }

    function get_roles($id)
    {

        $this->db->select('msr.*, r.name as role_name');
        $this->db->from('member_select_role msr');
        $this->db->join('member_role r', 'r.member_role_id = msr.member_role_id', 'Left');
        $this->db->where('r.status', 1);
        $this->db->where('msr.member_id', $id);

        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }

    }

    function create_record($table_name, $post, $file_name = NULL)
    {

        if ($file_name) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = 'uploads/Event_Images/' . time() . "_" . $file_name;
        }

        $post['start_date'] = date('Y-m-d H:i:s', strtotime($post['start_date']));
        //print_r($post['start_date']);
        //exit();
        $post['end_date'] = date('Y-m-d H:i:s', strtotime($post['end_date']));

        $data = $this->db->insert($table_name, $post);
        //print_r($data);
        //exit();
        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function get_likes_records($event_id)
    {
        //print_r($event_id);
        $this->db->select('lik.*, CONCAT(me.first_name," " ,me.last_name) as member_name , me.upload_image');
        $this->db->from('likes lik');
        $this->db->join('member me', 'me.member_id = lik.member_id', 'Left');
        $this->db->where('lik.status', 1);

        if ($event_id) {
            $this->db->where('lik.event_id', $event_id);
        }

        $query = $this->db->get();
        $data = $query->result_array();
        
        //echo "<pre>"; print_r($data); exit;
        
        if (!empty($data)) {
            return $data;
        } else {
            return $data;
        }

    }

    function update_record($table_name, $primary_key_name, $post, $file_name, $primary_value)
    {
        //$this->output->enable_profiler(TRUE);

        if ($file_name !== NULL) {
            $file_name = str_replace(' ', '_', $file_name);
            $post['upload_image'] = 'uploads/Event_Images/' . time() . "_" . $file_name;
        } else {
            unset($post['upload_image']);
        }

        $post['start_date'] = date('Y-m-d H:i:s', strtotime($post['start_date']));
        $post['end_date'] = date('Y-m-d H:i:s', strtotime($post['end_date']));

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);

        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_total_likes($request)
    {
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;

        $this->db->select('count(likes_id) as total_like');
        $this->db->from('likes');
        $this->db->where('is_like', 1);
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();
    }
    
    function is_like($request){
        $member_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $query = "SELECT is_like FROM likes WHERE member_id=$member_id AND event_id=$event_id";
        $res = $this->db->query($query);
        //echo "<pre>";
        //print_r ($res->result_array());
        //echo "</pre>";
        //exit();
        if(!empty($res->result_array())){
            return 1;
            
        }
        else
        {
            
            return 0;
            
        }
        
    }
    
    function is_registered($request)
    {
        $member_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        if($signin_type == 1)
        {
            $query = "SELECT member_id,event_id FROM payment WHERE member_id='".$member_id."' AND event_id='".$event_id."' ";
            $res= $this->db->query($query);
            $check = $res->result_array();
            if(!empty($check[0]))
            {
                return 1;
                
            }
            else{
                return 0;
            }
            //exit();
        }
        else
        {
            $query = "SELECT non_member_id,event_id FROM payment WHERE non_member_id='".$member_id."' AND event_id='".$event_id."' ";
            $res= $this->db->query($query);
            $check = $res->result_array();
             if(!empty($check[0]))
            {
                return 1;
                
            }
            else{
                return 0;
            }
        }
        
    }
    
    function total_registered($request)
    {
        $member_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        //if($signin_type = 1)
        //{
            $this->db->select('count(event_id) as total_registered');
            $this->db->from('payment');
            //$this->db->where('is_like', 1);
            $this->db->where('event_id', $event_id);
            $query = $this->db->get();
            //print_r($query);
            
            return $query->result_array();
            
            
            /*$query = "SELECT * FROM payment WHERE event_id=$event_id ";
            $res= $this->db->query($query);
            //return $res->result_array();
            if(!empty($res->result_array()))
            {
                return $res->result_array();
                
            }
            else{
                return 0;
            }*/
            //exit();
        /*}
        else
        {
            $query = "SELECT * FROM payment WHERE event_id=$event_id ";
            $res= $this->db->query($query);
            //return $res->result_array();
             if(!empty($res->result_array()))
            {
                return $res->result_array();
                
            }
            else{
                return 0;
            }
        }*/
        
    }
    
    function add_likes($request){
        //print_r($request);
        $user_id = $request['user_id'];
        $event_id = $request['event_id'];
        if($request['signin_type'] ==1)
        {
          $query ="INSERT INTO `likes`(`event_id`, `member_id`, `non_member_id`, `is_like`, `status`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES ($event_id,$user_id,0,1,1,0,0,0,0)"; 
          $res = $this->db->query($query);
          return 0;
        }
        else
        {
            $query ="INSERT INTO `likes`(`event_id`, `member_id`, `non_member_id`, `is_like`, `status`, `created_on`, `updated_on`, `created_by`, `updated_by`) VALUES ($event_id,0,$user_id,1,1,0,0,0,0)"; 
          $res = $this->db->query($query);
          if($res){
          return 0;
          }
          else{
              return 1;
          }
        }
        //exit();
    }

    function get_events($request)
    {

        $this->output->enable_profiler(TRUE);

        $user_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;

        //$data = $this->db->insert('likes',array('event_id' => $event_id , 'user_id'=>$user_id,'is_like' => 1));
        $this->db->select('e.*, l.is_like, at.checked_in,at.attendees_id');
        $this->db->from('event e');
        $this->db->join('likes l', 'l.event_id = e.event_id', 'Right');
        $this->db->join('attendees at', 'at.event_id = e.event_id', 'Right');
        $this->db->where('e.status', 1);
        $this->db->where('e.event_id', $event_id);

        if (self::MEMBER == $signin_type) {

            $this->db->where('l.member_id', $user_id);
            $this->db->where('at.member_id', $user_id);

        } else if (self::NON_MEMBER == $signin_type) {

            $this->db->where('l.non_member_id', $user_id);
            $this->db->where('at.non_member_id', $user_id);
        }

        $query = $this->db->get();
        return $query->result_array();

    }

    function get_events_detail($request)
    {
        $this->output->enable_profiler(TRUE);

        $user_id = ($request['user_id']) ? $request['user_id'] : 0;
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;

        //print_r($event_id);
        //echo "<br>";
        //print_r($user_id);
        //echo "<br>";

        //print_r($signin_type);
        //exit();

        //$data = $this->db->insert('likes',array('event_id' => $event_id , 'user_id'=>$user_id,'is_like' => 1));
        $this->db->select();
        //$this->db->select('e.*, l.is_like, at.checked_in,at.attendees_id');
        $this->db->from('event e');
        //$this->db->join('likes l','l.event_id = e.event_id','Right');
        //$this->db->join('attendees at','at.event_id = e.event_id','Right');
        //$test = 1;
        //$this->db->where('e.status', $test);
        $this->db->where('e.event_id', $event_id);

        //if(self::MEMBER == $signin_type) {

        //  $this->db->where('l.member_id', $user_id);
        //$this->db->where('at.member_id', $user_id);

        //}
        //else
        //if (self::NON_MEMBER == $signin_type) {

        //$this->db->where('l.non_member_id', $user_id);
        //$this->db->where('at.non_member_id', $user_id);
        //}

        $query = $this->db->get();
        //echo "<pre>";
        //print_r($query->result_array());
        //echo "</pre>";
        //exit();
        return $query->result_array();

    }
    
    function get_members_detail($request){
        $this->output->enable_profiler(TRUE);
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        //print_r($request['event_id']);
        //exit();
        
        //$query = "SELECT * FROM payment WHERE event_id = $event_id ";
        $query = "SELECT member.member_id, member.first_name, member.last_name, member.upload_image FROM member member
        WHERE EXISTS (SELECT 1
  FROM payment
  WHERE payment.event_id=$event_id AND payment.member_id=member.member_id)";
                    // INNER JOIN payment
                    // ON payment.member_id=member.member_id WHERE payment.event_id=$event_id";
        
        $result = $this->db->query($query);
        
        return $result->result_array();
    }

    function api_checked_in($id)
    {
        $this->db->select();
        $this->db->from('attendees');
        $this->db->where('attendees_id', $id);
        return $this->db->update('attendees', array('checked_in' => '1'));

    }
    
    function is_checkedin($request){
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $user_id = ($request['user_id']) ? $request['user_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        if($signin_type == 1)
        {   
            $query = "SELECT checked_in FROM attendees WHERE event_id= ".$event_id." AND member_id=".$user_id;
            $res= $this->db->query($query);
            //return $res->result_array();
            $check = $res->result_array();
            
            //$answer = $check['checked_in'];
            //print_r($check); exit;
            if(empty($check))
            {
                return 0;
                
            }
            else{
                return $check[0]['checked_in'];
            }
        
            
        }
        else
        {
              $query = "SELECT checked_in FROM attendees WHERE event_id=".$event_id." AND non_member_id=".$user_id ;
            $res= $this->db->query($query);
            //return $res->result_array();
            $check = $res->result_array();
            if(!empty($check))
            {
                return $check[0]['checked_in'];
                
            }
            else{
                return 0;
            }
        }
    }
    
    function is_answered($request){
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $user_id = ($request['user_id']) ? $request['user_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        if($signin_type == 1)
        {   
            $this->db->select('pas.user_id,pas.poll_id,pas.poll_answer_selected_id');
            $this->db->from('poll_answer_selected pas');
            $this->db->join('poll_answer pa','pa.poll_answer_id = pas.poll_answer_id','Left');
            $this->db->join('poll p', 'p.poll_id = pa.poll_id','Left');
            $this->db->where('p.event_id',$event_id);
            $this->db->where('pas.user_id',$user_id);
            $data = $this->db->get();
            $res = $this->db->count_all_results();
            //print_r();
            //exit;
            $test =  $data->result_array();
            //exit;
            
            
            if(empty($test))
            {
                return "no";
                
            }
            else{
                return "yes";
            }
        
            
        }
    
    }
    
    function photo_uploaded($request){
        $event_id = ($request['event_id']) ? $request['event_id'] : 0;
        $user_id = ($request['user_id']) ? $request['user_id'] : 0;
        $signin_type = ($request['signin_type']) ? $request['signin_type'] : 0;
        
        if($signin_type == 1)
        {   
            $this->db->select('up.*');
            $this->db->from('user_photo up');
            //$this->db->join('poll_answer pa','pa.poll_answer_id = pas.poll_answer_id','Left');
            //$this->db->join('poll p', 'p.poll_id = pa.poll_id','Left');
            $this->db->where('up.event_id',$event_id);
            $this->db->where('up.user_id',$user_id);
            $data = $this->db->get();
            $res = $this->db->count_all_results();
            //print_r();
            //exit;
            $test =  $data->result_array();
            
            $test = count($test);
            //exit;
            
            
            if(empty($test))
            {
                return "0";
                
            }
            else{
                return $test;
            }
        
            
        }
    
    }
    
    function api_checked_in_check($attendees_id,$event_id,$signin_type)
    {
        //$this->db->Insert();
        //$this->db->into('attendees');
        //$this->db->where('attendees_id', $id);
        
        if($signin_type == 1){
            
            $this->db->set('checked_in', 1);
            $this->db->where('member_id', $attendees_id);
            $this->db->where('event_id',$event_id);
            $query = $this->db->update('attendees');
        
            if($query){
              return TRUE;  
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            $this->db->set('checked_in', 1);
            $this->db->where('non_member_id', $attendees_id);
            $this->db->where('event_id',$event_id);
            $query = $this->db->update('attendees');
            
            if($query){
              return TRUE;  
            }
            else
            {
                return FALSE;
            }
        }
        //return $this->db->update('attendees', array('checked_in' => '1'));

    }
    
    function get_all($id)
    {
        $query = "SELECT * FROM event where event_id = $id";
        $res = $this->db->query($query);
        return $res->result_array();
        
    }

}

?>