<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Api_model extends CI_Model{

    public function validate_api($api_secret)
    {
        $this->db->select('non_member_id');
        $this->db->where('api_secret',$api_secret);
        $query = $this->db->get('api_keys');
        if ($query->num_rows() > 0) 
        {
            //return $query->result_array();
            foreach ($query->result_array() as $row)
{
        //return $row['api_secret'];
        //echo $row['api_id'];
        return $row['non_member_id'];
}
        } 
        else 
        {
            return 0;
        }

    }

    

    public function generateApiID($param = array())
    {
        $user_data  = implode(',', $param);
        $user_data  .= time();
        return md5($user_data);
    }
    /*
     * Generate API Secret From Api ID
     */
    public function generateApiSecret($param = '')
    {
        $param  .= time().rand(999, 99999999999);
        return md5($param).substr(str_shuffle($param), 0, 10);
    }

    
    
    public function insert($data, $table) {

        if ($this->db->insert($table, $data)) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {

            return false;
        }
    }
    
    public function logout_non_member($table, $id) {
        
        $this->db->set('device_token','');
        $this->db->where('non_member_id',$id);
        $query = $this->db->update('non_member');
        if($query)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
        /*if ($this->db->insert($table, $data)) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {

            return false;
        }*/
    }
    
    public function logout_member($table, $id) {
        
        $this->db->set('device_token','');
        $this->db->where('member_id',$id);
        $query = $this->db->update('member');
        if($query)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
        /*if ($this->db->insert($table, $data)) {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {

            return false;
        }*/
    }
    public function selectAll($table) {
//        $this->db->select("*")
//                ->from($table);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function selectByOtherCol($col, $col_val, $table) {

       
        
        $this->db->where($col, $col_val);

        //$this->db->where('is_active', 1); @NOTE changes

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return 0;

        }

    }
    
    public function selectByOtherCol_byrole($col, $col_val, $table,$value1) {

       
        
        $this->db->where($col, $col_val);
        //$this->db->where('role', $value1);
        $this->db->like('role', $value1 , 'both'); 
        //$this->db->where('is_active', 1); @NOTE changes

        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return 0;

        }

    }
    

    public function selectByTwoCol($col_one, $col_val_one, $col_two, $col_val_two, $table,$device_token) 
    {
        
        
         
        
        $this->db->where($col_one, $col_val_one);
        $this->db->where($col_two, $col_val_two);
        // $this->db->where('status',1);
        $query = $this->db->get($table);
        if($table=="member"){
        $query1="UPDATE member SET device_token='".$device_token."' WHERE email='".$col_val_one."' AND password='".$col_val_two."'";
        $this->db->query($query1);}
        else{
            $query1="UPDATE non_member SET device_token='".$device_token."' WHERE email='".$col_val_one."' AND password='".$col_val_two."'";
        $this->db->query($query1);}
       
        if ($query->num_rows() > 0) 
        {
            return $query->result();
        } 
        else 
        {
            return 0;
            //exit();
        }
        
        
    }
    ## update by other column
    public function updateByOtherCol($col,$col_val, $data, $table) {
        
        //$this->db->trans_start();
        $this->db->where($col, $col_val);
        $update=$this->db->update($table, $data);
        //$this->db->trans_complete();
        //if ($this->db->trans_status() === FALSE) 
        if($update>0)
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
    //sending fcm notification
    public function send_fcm($token,$body,$click_action,$admin_id=NULL)
	{
	  
    $registrationIds = $token;


     $msg = array
          (
		'body' 	=> $body,
		'title'	=> 'WFDSA',
             	'icon'	=> 'myicon',
              	'sound' => 'mySound',
                // 'click_action' => $click_action,                
              	'admin_id' => strval($admin_id)
          );

          $ios = array(
            'aps' => ['category'=> 'com.aliabduljabbar.WFDSA']
          );

	$fields = array
			(
				'to'		=> $registrationIds,
				'notification'	=> $msg,
			);
	
	$headers = array
			(
				'Authorization: key='.'AAAAHr99yLY:APA91bEkzNE5wvQkCr8seK2sUqPcMIr_R9Y0RUSojS8HrCEN5SKZG2gxSHAqQ1YP2nCyXH7zTv95wLzcNlnurJcd37VO4DyRjhtv1RDt_PJCwMzijJ22hCduOvhAKrYVDl8Qyil9EbRw4FP6N6ttaPxQrMoUgPcwNA',
				'Content-Type: application/json'
			);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		// curl_setopt( $ch,CURLOPT_POSTFIELDS, $newmsg );
		$result = curl_exec($ch );

		curl_close( $ch );
        //echo json_encode($result);
    
	}
    
    
}
