<?php

class Admin_dashboard_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $orderby = NULL,$limit = NULL) {
        $this->db->from($table_name);
        //print_r($table_name);
        //exit();
        $this->db->where('status', 1);
       
       
        if ($limit != NULL) {
            $this->db->limit($limit);
        }
        
        if ($orderby != NULL) {
             $this->db->order_by($orderby,'DESC');
        }
        
        $query = $this->db->get();
        $data = $query->result_array();
        //print_r($data);
        //exit();
        

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_all_table_events($table_name, $orderby = NULL,$limit = NULL) {
        $date = date('m-d-Y H:i');;
        //echo $date;
        //exit;
        
        $this->db->from($table_name);
        //print_r($table_name);
        //exit();
        $this->db->where('status', 1);
        $this->db->where('start_date >=',$date);
       
       
        if ($limit != NULL) {
            $this->db->limit($limit);
        }
        
        if ($orderby != NULL) {
             $this->db->order_by($orderby,'ASC');
        }
        
        $query = $this->db->get();
        $data = $query->result_array();
        //print_r($data);
        //exit();
        

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_user_counts() {
        
        $member_count = array();
        
        $query1 = $this->db->get('member');
        $all_members = $query1->num_rows();
        $query2 = $this->db->get('non_member');
        $all_non_members = $query2->num_rows();
        $query3 = $this->db->get_where('member',array('app_user'=>1));
        $app_user = $query3->num_rows();
        
        $member_count['member'] = $all_members;
        $member_count['non_member'] = $all_non_members;
        $member_count['app_user'] = (int)$app_user+$all_non_members;
        
        return $member_count;
    }
    
    
    
     
}

?>