<?php

class Payment_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'payment';
    }

    function get_all_table($table_name,$id = NULL) {
        //print_r($table_name);
        //exit;
        $this->db->from($table_name);
        $this->db->where('status', 1);
        //$res = "SELECT * FROM invoice";
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_all_table_payment_byRole($data)
    {
        $user_id=$data['user_id'];
        $signin_type = $data['signin_type'];
        if($signin_type == 1){
        //$query = "SELECT * FROM payment WHERE member_id=$user_id ";
        
        $this->db->select('p.*,e.title');
        $this->db->from('payment p');
        $this->db->join('event e','e.event_id = p.event_id','left');
        $this->db->where('p.member_id',$user_id);
        $this->db->where('e.fee !=',0);
        $this->db->where('e.fee !=',"");
        $this->db->where('p.payment_amount !=',"NaN");
        $this->db->where('p.payment_amount !=',"");
        $this->db->where('p.payment_amount !=',0);
        $res = $this->db->get();
            //$res= $this->db->query($query);
            $result = $res->result_array();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return FALSE;
            }
        }
        else{
            //$query = "SELECT * FROM payment WHERE non_member_id=$user_id ";
            //$res= $this->db->query($query);
            
             $this->db->select('p.*,e.title');
        $this->db->from('payment p');
        $this->db->join('event e','e.event_id = p.event_id','left');
        $this->db->where('p.non_member_id',$user_id);
        $res = $this->db->get();
            $result = $res->result_array();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return FALSE;
            }
        }
        }
    
    function get_all_table_invoices_byRole($data)
    {
        $user_id=$data['user_id'];
        //$signin_type = $data['signin_type'];
        
        $query = "SELECT * FROM invoice WHERE member_id=$user_id ORDER BY payment_status ASC";
            $res= $this->db->query($query);
            $result = $res->result_array();
            if(!empty($result))
            {
                return $result;
            }
            else
            {
                return FALSE;
            }
        
        
        }
    
    function get_all_table_payment() {
        //$this->db->distinct('');
        $this->db->select('p.*');
        $this->db->from('payment p');
        //$this->db->join('invoice i','i.member_id = p.member_id','LEFT');
        $query = $this->db->get();
        $data = $query->result_array();
        
        //print_r($table_name);
        //exit;
        //$this->db->from($table_name);
        //$this->db->where('status', 1);
        //$res = "SELECT * FROM invoice,payment";
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        //$query = $this->db->query($res);
        //$data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_invoice_item($invoice_id) {
        
        $this->db->from('invoice_item');
        $this->db->where('invoice_id', $invoice_id); 
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_roles($id){
        
        $this->db->select('msr.*, r.name as role_name');
        $this->db->from('member_select_role msr'); 
        $this->db->join('member_role r','r.member_role_id = msr.member_role_id','Left');
        $this->db->where('r.status', 1);
        $this->db->where('msr.member_id', $id);
      
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    
    }
    
    function get_payment_data($table_name,$event_id) {
        
        
              
              if(!empty($event_id))
              {
                    $q = "SELECT payment.*,event.title as event_name,CONCAT(member.first_name,' ',member.last_name) as member_name FROM payment INNER JOIN event ON
              event.event_id = payment.event_id INNER JOIN member ON member.member_id = payment.member_id WHERE payment.event_id=$event_id";          
                    $query = $this->db->query($q);
                    $data = $query->result_array();
              }
              else
              {
                  $data = 0;
              }
              
        
        
        /*$this->db->select('p.*, e.title as event_name');
        $this->db->from($table_name.' p'); 
        $this->db->join('event e','e.event_id = p.event_id','Left');
        $this->db->where('p.payment_status' , 1);  
        $this->db->where('e.event_id', $event_id);  
        $query = $this->db->get();*/
        
        

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
    }
    
    function get_payment_invoice() {
        
         
        $this->db->select('p.*, inv.title,it.name,it.total_row');
        $this->db->from($this->table_name.' p'); 
        $this->db->join('invoice inv','inv.invoice_id = p.invoice_id','Left');
        $this->db->join('invoice_item it','it.invoice_id = inv.invoice_id','Left');
        $this->db->where('p.status', 1);   
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
    }

    function create_record($table_name, $post) {
        
        $data = $this->db->insert($table_name, $post);
        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
    function create_record_attendees($table_name, $post) 
    {
        $date = date('Y-m-d H:i:s');

        if(!empty($post['member_id'])) {
                
                $data1 =array(
                    'event_id' => $post['event_id'],
                    'member_id' => $post['member_id'],
                    'non_member_id' =>"0",
                    'checked_in' => "0",
                    'status' => "1",
                    'created_on' => $date
                    );
            
                    $this->db->insert($table_name, $data1);
                        
            } 
            else 
            {
                $data1 =array(
                    'event_id' => $post['event_id'],
                    'member_id' =>"0",
                    'non_member_id' => $post['non_member_id'],
                    'checked_in' => "0",
                    'status' => "1",
                    'created_on' => $date
                    );
            
                     $this->db->insert($table_name, $data1);
                    
        }
        
        
    }
    
    function update_record($table_name, $post) {
        $id=$post['invoice_id'];
        $query = "UPDATE invoice SET payment_status = 1 WHERE invoice_id=$id";
        
        $data = $this->db->query($query);
        
        
        //$this->db->set('payment_status',1);
        //$this->db->where('invoice_id',$id);
        //$data =  $this->db->update($table_name);
        //$data = $this->db->insert($table_name, $post);
        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    function all_invoices($table_name, $id = NULL) {
        $query = "SELECT * FROM invoice";
        $res = $this->db->query($query);
        //$data = $this->db->insert($table_name, $post);
        if ($res) {
            return $res->result_array();
        } else {
            return FALSE;
        }
    }
 

}

?>