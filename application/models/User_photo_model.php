<?php

class User_photo_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_record() {
        $this->db->Select('eg.*,ev.title as event_name');
        $this->db->from('event_gallery eg');
        $this->db->join('event ev', 'ev.event_id = eg.event_id', 'Left');
        $this->db->where('eg.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_event_gallery_id($event_id) {
        $this->db->from('event_gallery');
        $this->db->where('status', 1);
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $data = $query->result();

        if (!empty($data)) {
            return $data[0]->event_gallery_id;
        } else {
            return 0;
        }
    }

    function get_user_gallery_insert($ids, $event_id) {

        $this->db->from('user_photo');
        $this->db->where('status', 1);
        $this->db->where_in('user_photo_id', $ids);
        $query = $this->db->get();
        $user_photo_record = $query->result_array();
       

        if (!empty($user_photo_record)) {
            $insert_arr = array();
            foreach ($user_photo_record as $value) {
                $insert_arr[] = array(
                    'event_id' => $event_id,
                    'uploaded_image' => $value['uploaded_image'],
                    'image_title' => $value['image_title']
                );
            }
            $data = $this->db->insert_batch('event_gal_images', $insert_arr);
            if ($data) {

                $this->db->where_in('user_photo_id', $ids);
                $this->db->update('user_photo', array('approved' => 1));

                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function delete_photos($ids) {
        
        $this->db->where_in('user_photo_id', $ids);
        $data = $this->db->delete('user_photo');
        
        if($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_user_uploads($event_id, $table_name) {
//        $this->output->enable_profiler(TRUE);
        $this->db->Select('up.*,us.name,us.email,us.telephone_no as number');
        $this->db->from($table_name . ' up');
        $this->db->join('user us', 'us.user_id = up.user_id', 'Left');
        $this->db->where('up.status', 1);
        $this->db->where('up.event_id', $event_id);
        $this->db->order_by("up.user_photo_id", "asc");
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }

    function create_record($table_name, $post, $file_name = NULL) {

        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];


        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function delete_gallery_record($id) {
        if ($id) {
            $this->db->where('event_gallery_id', $id);
            $this->db->delete('event_gal_images');
            return TRUE;
        }
    }

    function create_gallery_record($file_name, $id) {

        $post['event_gallery_id'] = $id;
        $post['uploaded_image'] = 'uploads/Event_Gallery/' . $file_name;
        $data = $this->db->insert('event_gal_images', $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_record($table_name, $primary_key_name, $post, $primary_value) {
        //$this->output->enable_profiler(TRUE);

        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_relation_table($id, $relation_table, $table_name) {
        $this->db->from($relation_table);
        $this->db->where($table_name . '_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }

}

?>