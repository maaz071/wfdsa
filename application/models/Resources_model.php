<?php

class Resources_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
    
    function create_record($table_name, $file_name, $post) {

       
        $post['created_on'] = date('Y-m-d');
      
        
        if(!empty($file_name)) {
            $post['upload_file'] =  base_url('uploads/Resource_File/').$file_name;
        }
        
        unset($post['categories']);
        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
     function create_multi_record($table_name, $id, $post, $primary_key = NULL) {

        if ($primary_key) {
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        }
     
        $insert_arr = array();
        if(!empty($post['categories'])){
            foreach ($post['categories'] as $value) {
                $insert_arr[] = array(
                    'resources_id' => $id,
                    'categories_id' => $value
                );
            }
            $data = $this->db->insert_batch($table_name, $insert_arr);
            
            if ($data) {
                return TRUE;
            }  
        } 
    }
    
    function get_api_resources() {
        
        $this->db->select('msr.*,r.* ,ctg.*, pctg.name as parent_category_name');
        $this->db->from('resources msr'); 
        $this->db->join(' resource_categories r','r.resources_id = msr.resources_id','Left');
        $this->db->join('categories ctg','ctg.categories_id = r.categories_id','Left');
        $this->db->join('categories pctg','pctg.categories_id = ctg.parent_id','Left');
        $this->db->where('msr.status', 1);
        
        
       
        $query = $this->db->get();
        return $data = $query->result_array();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/
        /*$this->db->from('resources');
        $this->db->where("status", 1); 
        $query = $this->db->get();
        */
        //return $resources =  $query->result_array();
    
    }
    
    function get_three_resources($role = "Public") {
        
        if($role == "" || $role['role_id'] == "")
        {
                $role = "Public";
        $query = "SELECT * FROM `resources` WHERE resource_member ='Public' ORDER BY resources_id DESC LIMIT 3";    
            
        }
        else{
        $query = "SELECT * FROM resources WHERE FIND_IN_SET('".$role['role_id']."',resource_member) OR resource_member = 'Public' ORDER BY resources_id DESC LIMIT 3";

        }
        
        
        /*$this->db->select('msr.*,r.* ,ctg.*, pctg.name as parent_category_name');
        $this->db->from('resources msr'); 
        $this->db->join(' resource_categories r','r.resources_id = msr.resources_id','Left');
        $this->db->join('categories ctg','ctg.categories_id = r.categories_id','Left');
        $this->db->join('categories pctg','pctg.categories_id = ctg.parent_id','Left');
        $this->db->where('msr.status', 1);
        */
        $res= $this->db->query($query);
       
        //$query = $this->db->get();
        return $data = $res->result_array();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/
        /*$this->db->from('resources');
        $this->db->where("status", 1); 
        $query = $this->db->get();
        */
        //return $resources =  $query->result_array();
    
    }
    
    function get_api_resources_by_ctg($data) {
        /*echo "in model";
        print_r($data);
        exit;*/
        $id=$data['category_id'];        
       $this->db->select('ctg.*,ctg.name');
        $this->db->from('categories ctg'); 
       // $this->db->join(' resource_categories rctg','rctg.categories_id = ctg.categories_id','Left');
       // $this->db->join('resources r','r.resources_id = rctg.resources_id','Left');
        //$this->db->join('categories pctg','pctg.categories_id = ctg.parent_id','Left');
        
        
        $this->db->where('ctg.parent_id',$id);
        //$this->db->group_by('ctg.name','DESC');
       
       
        $query = $this->db->get();
        //print_r($query->result_array());
        //exit;
       
       $data = $query->result_array();
      
        if($data){
                
            $resources = array();
            foreach($data as $k => $d){
                
                $this->db->select('r.*,rctg.*');
                $this->db->from('resources r'); 
                 $this->db->join('resource_categories rctg','r.resources_id = rctg.resources_id','Left');
                $this->db->where('r.status', 1);
                $this->db->where('rctg.categories_id',  $d['categories_id']);
                $this->db->order_by("title_2",'asc');
                
                $query = $this->db->get();
                $data_inner = $query->result_array();
                $data[$k]['resources'] = $data_inner;
                
            }
            
        }
        
       return $data;
    }
    function get_ios_api_resources_by_ctg($data) {
       // $id=$data['category_id'];
       //  $role=$data['role'];
       //  if(empty($role))
       //      { $role = 'Public';}
       // $this->db->select('ctg.*,ctg.name');
       //  $this->db->from('categories ctg');     
       //  $this->db->where('ctg.parent_id',$id);
       //  $query = $this->db->get();
       
       // $data = $query->result_array();
      
       //  if($data){
                
       //      $resources = array();
       //      foreach($data as $k => $d){
                
       //          $this->db->select('r.*,rctg.*');
       //          $this->db->from('resources r'); 
       //           $this->db->join('resource_categories rctg','r.resources_id = rctg.resources_id','Left');
       //          $this->db->where('r.status', 1);
       //          $this->db->where('(FIND_IN_SET("'.$role.'",r.resource_member) OR r.resource_member = "Public")',null,false);
       //          $this->db->where('rctg.categories_id',  $d['categories_id']);
                
       //          $query = $this->db->get();
       //          $data_inner = $query->result_array();
       //          $data[$k]['resources'] = $data_inner;
                
       //      }
            
       //  }
                $id=$data['category_id'];        
       $this->db->select('ctg.*,ctg.name');
        $this->db->from('categories ctg'); 
       
        $this->db->where('ctg.parent_id',$id);
       
       
       
        $query = $this->db->get();
       
       $data = $query->result_array();
      
        if($data){
                
            $resources = array();
            foreach($data as $k => $d){
                
                $this->db->select('r.*,rctg.*');
                $this->db->from('resources r'); 
                 $this->db->join('resource_categories rctg','r.resources_id = rctg.resources_id','Left');
                $this->db->where('r.status', 1);
                $this->db->where('rctg.categories_id',  $d['categories_id']);
                $this->db->order_by("title_2",'asc');
                
                $query = $this->db->get();
                $data_inner = $query->result_array();
                $data[$k]['resources'] = $data_inner;
                
            }
            
        }
        
       return $data;
    }
    
    function get_api_resources_ctg() {
        
       $query = "SELECT * FROM categories WHERE parent_id=0";
       $res = $this->db->query($query);
        return $data = $res->result_array();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;*/
        /*$this->db->from('resources');
        $this->db->where("status", 1); 
        $query = $this->db->get();
        */
        //return $resources =  $query->result_array();
    
    }
    
    function get_api_resource_file($request) {
        
        $id = $request['resources_id'];
        $result = array();
        $this->db->from('resources');
        $this->db->where("status", 1);
        $this->db->where("resources_id", $id);
        $query = $this->db->get();
        $data =  $query->result_array();
        
        $this->db->select('rc.*, c.name as category_name');
        $this->db->from('resource_categories rc'); 
        $this->db->join('categories c','c.categories_id = rc.categories_id','Left');
        $this->db->where('c.status', 1);
        $this->db->where('rc.resources_id', $id); 
        $query = $this->db->get();
        $cat_join = $query->result_array();
        
        $file_name = explode('/',$data[0]['upload_file']);
        $file_stripe = explode('_',$file_name[6]);
      
        if(!empty($cat_join) && !empty($data)) {
            
            $result[0]['Schemes'] = $cat_join[0]['category_name'];
            
            $result[0]['subscheme'][0]['file_name'] = $file_name[6];
            $result[0]['subscheme'][0]['file'] = $data[0]['upload_file'];
        }
        
        // if(!empty($data)) {
        //     foreach($data as $key=>$value) {
                 
        //         $api_arr[$key]['id'] = $value['resources_id'];
        //         $api_arr[$key]['title'] = $value['title_2']; 
                
        //         $this->db->from('resource_file'); 
        //         $this->db->where("resources_id", $value['resources_id']); 
        //         $query = $this->db->get();
        //         $re_file =  $query->result_array();
                
        //         $api_arr[$key]['sub_scheme'] = $re_file;
        //     }
        // }
        
       
        
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        } 
        
    }
    
   
    function create_resource_files($file_name, $id) { 

        $post['resources_id'] = $id;
        $post['file_path'] = base_url().'uploads/Resource_File/' . $file_name;
        return $data = $this->db->insert('resource_file', $post);
    }
    
    function get_resource_data($id) {
        
        $this->db->from('resource_file');
        $this->db->where("resources_id", $id); 
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_categories_select($table_name, $id) {
        $this->db->from($table_name);
        $this->db->where("resources_id", $id); 
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function update_record($table_name, $primary_key_name,$file_name, $post, $primary_value) {
        // $this->output->enable_profiler(TRUE); 
        
        if(!empty($file_name)) {
            $post['upload_file'] =  base_url('uploads/Resource_File/').$file_name;
        }
       
        unset($post['categories']);
        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
      //  $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_joint_records($table_name,$limit="",$offset="") {
        
        $this->db->select('res.*,rc.*,rc.categories_id, c.name as category_name,pctg.name as parent_category_name,c.status as categories_status,c.parent_id,c.name');
        $this->db->from('resources res');  
        $this->db->join('resource_categories rc','rc.resources_id = res.resources_id','Left');
        $this->db->join('categories c','c.categories_id = rc.categories_id','Left');
        //$this->db->where('res.status', 1);
        
        $this->db->join('categories pctg','pctg.categories_id = c.parent_id','Left');
        $this->db->order_by('res.resources_id','DESC');
        
        
        if ($limit != "" && $offset >= 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        //echo "<pre>";
        //print_r($query->result_array());
        //echo "</pre>";
        //exit;
        
        return $query->result_array();
        
    }
    function get_all($id)
    {
        $query = "SELECT * FROM resources where resources_id = $id";
        $res = $this->db->query($query);
        return $res->result_array();
        
    }

}

?>