<?php

class Guide_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
       // $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
            print_r($table_name);
            exit();
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_contactdata() {
        $this->db->select('contact_email as email , contact_long as lng , contact_lat as lat , phone as contact_no , contact_address as address');
        $this->db->from('about');
       // $this->db->where('status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function get_record() {
        $this->db->Select('eg.*,ev.title as event_name');
        $this->db->from('event_gallery eg');
        $this->db->join('event ev', 'ev.event_id = eg.event_id', 'Left');
        $this->db->where('eg.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post, $file_name) {

        $file_name = str_replace('','_',$file_name);
        $post['upload_video'] = base_url().'uploads/User_Guide/'."test"."_" . $file_name;
        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];
        //print_r($post);
        //exit();
        $data = $this->db->insert($table_name, $post);
        //print_r($this->db->insert($table_name, $post));
        //exit();

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

  

    function delete_gallery_record($id) {
        if ($id) {
            $this->db->where('event_gallery_id', $id);
            $this->db->delete('event_gal_images');
            return TRUE;
        }
    }

    function create_gallery_record($file_name, $id) {

        $post['event_gallery_id'] = $id;
        $post['uploaded_image'] = base_url().'uploads/Event_Gallery/' . $file_name;
        $data = $this->db->insert('event_gal_images', $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_record($table_name,$primary_key_name , $post, $file_name, $primary_value)  {
        //$this->output->enable_profiler(TRUE);

        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];

        $file_name = str_replace(' ', '_', $file_name);
        $post['upload_video'] = base_url().'uploads/User_Guide/test'."_" . $file_name;
       

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_relation_table($id, $relation_table, $table_name) {
        $this->db->from($relation_table);
        $this->db->where($table_name . '_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return array();
        }
    }

}

?>