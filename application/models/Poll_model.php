<?php

class Poll_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_poll_answer_name($table_name,$poll_id){
        
        $this->db->from($table_name);
        $this->db->where("poll_id", $poll_id);
        $this->db->order_by("poll_answer_id", "asc");
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    function get_relation_records($event_id=NULL) {
       
        $this->db->select('p.*, e.title');
        $this->db->from('poll p'); 
        $this->db->join('event e','e.event_id = p.event_id','Left');
        // $this->db->where('p.status', 1);
         
         if($event_id) {
            $this->db->where('p.event_id', $event_id);
         }
      
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    } 
    
    function get_poll_answer($table_name ,$key_name, $poll_id) {
        
        $this->db->from($table_name);
        $this->db->where($key_name, $poll_id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
        
    }
    
    function add_api_poll_answer($post) { 
        
        $poll_id= $post['poll_id'];
        $user_id = $post['user_id'];
        $poll_answer_id = $post['poll_answer_id'];
        $member_type = $post['member_type'];
        
        //print_r($post);
        //exit();
        
        $query ="INSERT INTO `poll_answer_selected`( `poll_id`, `member_type`, `user_id`, `poll_answer_id`) VALUES ('".$poll_id."','".$member_type."','".$user_id."','".$poll_answer_id."')";
        
        $res = $this->db->query($query);
        
        return $res;
       
       /* if (strpos($post['poll_answer_id'], ',') == TRUE) {
             $multi_review = explode(",",$post['poll_answer_id']);
             
             foreach($multi_review as $key=> $value) {
                $data = $this->db->insert('poll_answer_selected', array('poll_id' => $post['poll_id'],'member_type'=> $post['member_type'] ,'user_id' => $post['user_id'],'poll_answer_id' => $value));
             }
             
             
             return $data;
            
        } else {
            return  $this->db->insert('poll_answer_selected', $post);
        }*/
        
        
        
    }
    

    function get_relation_table($table_name, $id = NULL, $relation_table) {
//        $this->output->enable_profiler(TRUE);

        $this->db->from($table_name);
        if (isset($id)) {
            $this->db->where($relation_table . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post, $file_name = NULL) {

        unset($post['answer']);
        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];
        $data = $this->db->insert($table_name, $post);


        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function create_poll_answer($table_name, $id, $post,$primary_key = NULL) {
        
        if($primary_key) { 
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        } 
        
        $insert_arr = array();
        foreach ($post['answer'] as $value) {
            $insert_arr[] = array(
                'poll_id' => $id,
                'poll_answer' => $value
            );
        }

        $data = $this->db->insert_batch($table_name, $insert_arr);
        if ($data) {
            return TRUE;
        }
    }

    function update_record($table_name, $primary_key_name, $post, $primary_value) {
        //$this->output->enable_profiler(TRUE); 
        unset($post['answer']);
        $admin_data = $this->session->userdata('admin_data');
        $post['updated_on'] = date('Y-m-d H:i:s');
        $post['updated_by'] = $admin_data[0]['user_id'];

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]); 
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function delete_record($table_name,$id) {
        $this->db->where($table_name.'_id', $id);
        $dat = $this->db->delete($table_name);
        if($dat) {
            return TRUE;
        }
    }
    
    function get_poll_result($poll_id) {
        
       // $this->output->enable_profiler(TRUE); 
        $this->db->select('count(pn.poll_answer_id) as y_value, p.poll_answer,pl.status');
        $this->db->from('poll_answer_selected pn'); 
        $this->db->join('poll_answer p','p.poll_answer_id = pn.poll_answer_id','Left');
        $this->db->join('poll pl','pl.poll_id = pn.poll_id','Left');
        $this->db->where('p.poll_id',$poll_id);
        $this->db->group_by('p.poll_answer');
        $this->db->group_by('pl.status'); 
        $this->db->group_by('pn.poll_answer_id'); 
        $this->db->order_by("pn.poll_answer_id", "asc");
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    } 

}

?>