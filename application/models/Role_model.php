<?php

class Role_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }

    function create_record($table_name, $post, $file_name = NULL) {
        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];

        $data = $this->db->insert($table_name, $post);

        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function update_record($table_name, $primary_key_name, $post, $file_name, $primary_value) {
        //$this->output->enable_profiler(TRUE);


        $admin_data = $this->session->userdata('admin_data');
        $post['created_on'] = date('Y-m-d H:i:s');
        $post['created_by'] = $admin_data[0]['user_id'];

        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post);

        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>