<?php

class Invoice_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_table($table_name, $id = NULL) {
        $this->db->from($table_name);
        $this->db->where('status', 1);
        if (isset($id)) {
            $this->db->where($table_name . "_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
     function get_invoice_item($table_name, $id = NULL) {
        $this->db->from($table_name);
        if (isset($id)) {
            $this->db->where("invoice_id", $id);
        }
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
    }
    
    
    function get_joined_record() {
        
        $this->db->select('inv.*,m.first_name,m.last_name');
        $this->db->from('invoice inv');
        $this->db->join('member m','m.member_id	= inv.member_id','Left');
        //$this->db->where('inv.status', 1);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
        
        
    }
    
    function payment_details($request) {
        
        $payment_id = $request['payment_id'];
        //exit;
        $this->db->select('inv.*,CONCAT (m.first_name, ,m.last_name) as member_name,e.title ');
        $this->db->from('payment inv');
        $this->db->join('member m','m.member_id	= inv.member_id','Left');
        $this->db->join('event e','e.event_id = inv.event_id','Left');
        $this->db->where('inv.status', 1);
        $this->db->where('payment_id',$payment_id);
        $query = $this->db->get();
        $data = $query->result_array();

        if (!empty($data)) {
            return $data;
        } else {
            return FALSE;
        }
        
        
        
    }

    function create_record($table_name,$post) { 
        
        unset($post['name']);
        unset($post['qty']);
        unset($post['price']);
        unset($post['total_row']);
        $post['payment_status'] = 0;
        $data = $this->db->insert($table_name, $post);
         
        
        if ($data) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
    
    function create_multi_item($table_name, $id, $post,$primary_key) {
        
        if($primary_key) { 
            $this->db->where($primary_key, $id);
            $this->db->delete($table_name);
        } 
        
        $insert_arr = array();
        foreach ($post['name'] as $key => $value) {
            $insert_arr[] = array(
                'invoice_id' => $id,
                'name' => $value,
                'qty' => $post['qty'][$key], 
                'price' => $post['price'][$key],
                'total_row' => $post['total_row'][$key]
            );
        }

        $data = $this->db->insert_batch($table_name, $insert_arr);
        if ($data) {
            return TRUE;
        }
    }
    
    function update_record($table_name,$primary_key_name , $post,  $primary_value) {
        //$this->output->enable_profiler(TRUE); 
        unset($post['name']);
        unset($post['qty']);
        unset($post['price']);
        unset($post['total_row']);
        $this->db->where($primary_key_name, $primary_value);
        unset($post[$primary_key_name]);
        $data = $this->db->update($table_name, $post); 
        
        if ($data) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
     function invoice_item($request) { 
 
        $api_arr = array();
        
        $this->db->select('inv.*, CONCAT(m.first_name," ",m.last_name) as name');
        $this->db->from('invoice inv');
        $this->db->join('member m','m.member_id	= inv.member_id','Left');
        $this->db->where('inv.status', 1);
        $this->db->where('inv.invoice_id', $request['invoice_id']);
        $query = $this->db->get();
        $data = $query->result();
        
        $api_arr['title'] = $data[0]->title;
        $api_arr['name'] = $data[0]->name;
        $api_arr['amount'] = $data[0]->grand_total;
        $api_arr['desc'] = $data[0]->description;
        $api_arr['payment_status'] = $data[0]->payment_status;
        
        $this->db->from('invoice_item');
        $this->db->where('invoice_id', $request['invoice_id']);
        
        $query_item = $this->db->get();
        $items = $query_item->result_array();
        
        $item = "";
        foreach($items as $key=>$value) {
           
           $api_arr['item'][$key]['name'] =  $value['name'];
           $api_arr['item'][$key]['amount'] =  $value['price']; 
           $api_arr['item'][$key]['qty'] =  $value['qty']; 
            
           /*$item .= "Item : ".$value['name']."  ";
           $item .= "Amount : ".$value['total_row']; 
           $api_arr['item'][] = $item;
            $item = "";*/
        }
        
       //array_push($api_arr,$item);
          
        if ($api_arr) {
            return $api_arr;
        } else {
            return array();
        }
        
        
    }

}

?>